# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request, Response
import os


class ServiceUpdate(http.Controller):

    @http.route('/reject/<path:job_id>/<path:token>', type='http', auth='public', website=True)
    def render_decline_page(self, job_id, token):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        job_obj = http.request.env['claimed.service'].sudo().search(
            [('id', '=', job_id), ('state', '=', 'dispatched'), ('token', '=', token)])
        if job_obj:
            job_obj.write({'state': 'rejected'})
            with open(dir_path + "/confirmation.html", "r") as f:
                data = f.read()
        else:
            with open(dir_path + "/not-found.html", "r") as f:
                data = f.read()
        return "%s" % data

    @http.route('/accept/<path:job_id>/<path:token>', type='http', auth='public', website=True)
    def render_accept_page(self, job_id, token):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        job_obj = http.request.env['claimed.service'].sudo().search(
            [('id', '=', job_id), ('state', '=', 'dispatched'), ('token', '=', token)])
        if job_obj:
            job_obj.write({'state': 'accepted'})
            with open(dir_path + "/confirmation.html", "r") as f:
                data = f.read()
        else:
            with open(dir_path + "/not-found.html", "r") as f:
                data = f.read()
        return "%s" % data

from odoo import api, fields, models
from pyfcm import FCMNotification
import json
import logging

logger = logging.getLogger(__name__)

push_service = FCMNotification(
    api_key="AAAAerQCSIc:APA91bEBUnZdEu-lOzXsLEn4OXR0Vy-jUNyGugziAd9mP7mk"
            "-q2FbmfJbuPmQaLKVLQ9NalcOCfCiRfvqI17OshHI90hPSDcwVypkX5HwnPJr9f5M8Hjhz2385vzAX7CPW3b7TeSCajN")


class drivers_menu(models.Model):
    _inherit = 'drivers.menu'

    @api.model
    def DriverDashboard(self, args):
        res = {}
        d_id = args.get('driver_id', None)
        if d_id:
            try:
                driver_id = self.search([('related_user', 'in', d_id)]).id
                if driver_id:
                    accepted = self.env['claimed.service'].search_count(
                        [('state', '=', 'accepted'), ('assigned_driver', '=', driver_id)])
                    ongoing = self.env['claimed.service'].search_count(
                        [('state', '=', 'ongoing'), ('assigned_driver', '=', driver_id)])
                    reaching = self.env['claimed.service'].search_count(
                        [('state', '=', 'reaching'), ('assigned_driver', '=', driver_id)])
                    doc_received = self.env['claimed.service'].search_count(
                        [('state', '=', 'documents-received'), ('assigned_driver', '=', driver_id)])
                    leaving = self.env['claimed.service'].search_count(
                        [('state', '=', 'leaving'), ('assigned_driver', '=', driver_id)])
                    reached_rta = self.env['claimed.service'].search_count(
                        [('state', '=', 'reached-rta'), ('assigned_driver', '=', driver_id)])
                    car_loaded = self.env['claimed.service'].search_count(
                        [('state', '=', 'car-loaded'), ('assigned_driver', '=', driver_id)])
                    reached_dropoff = self.env['claimed.service'].search_count(
                        [('state', '=', 'reached-dropoff'), ('assigned_driver', '=', driver_id)])
                    car_off_loaded = self.env['claimed.service'].search_count(
                        [('state', '=', 'car-off-loaded'), ('assigned_driver', '=', driver_id)])
                    left_rta = self.env['claimed.service'].search_count(
                        [('state', '=', 'left-rta'), ('assigned_driver', '=', driver_id)])
                    dropped_car = self.env['claimed.service'].search_count(
                        [('state', '=', 'dropped-car'), ('assigned_driver', '=', driver_id)])

                    # Completed
                    completed_jobs = self.env['claimed.service'].search_count(
                        [('state', '=', 'feedback'), ('assigned_driver', '=', driver_id)])
                    future_jobs = self.env['claimed.service'].search_count(
                        [('state', '=', 'dispatched'), ('assigned_driver', '=', driver_id)])
                    garages = self.env['garages.menu'].search_count([])
                    res = {
                        'active_jobs': accepted + ongoing + reaching + doc_received + leaving + reached_rta + car_loaded + reached_dropoff + car_off_loaded + left_rta + dropped_car,
                        'completed_jobs': completed_jobs,
                        'future_jobs': future_jobs,
                        'garages': garages,
                        'id': driver_id,
                    }
            except Exception as e:
                logger.exception("Error: %s;" % e)
                return {"Error": 'Something went wrong'}
        return res

    @api.model
    def DriverProfile(self, args):
        res = {}
        d_id = args.get('driver_id', None)
        driver_data = {}
        try:
            if d_id:
                driver_id = self.search([('related_user', '=', d_id[0])])
                if driver_id:
                    user_data = self.env['res.users'].browse(
                        driver_id.related_user.id)
                    driver_data.update({
                        'name': user_data.name,
                        'email': user_data.login,
                        'contact_no': driver_id.contact_no,
                        'image': user_data.image,
                        'id': driver_id.id,
                    })
        except Exception as e:
            logger.exception("Error: %s;" % e)
            return {"Error": 'Something went wrong'}
        return driver_data


class UserAPI(models.Model):
    _inherit = 'res.users'

    gcm_dev_id = fields.Char('GCM Dev ID')

    @api.model
    def getUserType(self, args):
        res = {}
        try:
            user_id = args.get('driver_id', None)
            is_driver = self.env.user.has_group('imc.group_driver')
            is_sub_contractor = self.env.user.has_group(
                'imc.group_sub_contractor')
            is_sub_contractor_driver = self.env.user.has_group(
                'imc.group_sub_contractor_driver')
            sub_cont_id = self.env['service.providers'].search(
                [('user_id', '=', user_id)]).id
            sub_cont_drv_id = self.env['drivers.menu'].search(
                [('related_user', '=', user_id)]).id
            res = {
                'is_driver': is_driver,
                'is_sub_contractor': is_sub_contractor,
                'is_sub_contractor_driver': is_sub_contractor_driver,
                'sub_cont_id': sub_cont_id,
                'sub_cont_drv_id': sub_cont_drv_id,
            }
        except Exception as e:
            logger.exception("Error: %s;" % e)
            return {"Error": 'Something went wrong'}
        return res

    @api.model
    def SubContractorDashboard(self, args):
        res = {}
        d_id = args.get('driver_id', None)
        try:
            if d_id:
                sub_cont_obj = self.env['service.providers'].search(
                    [('user_id', 'in', d_id)])
                sub_cont_id = sub_cont_obj.id
                if sub_cont_id:
                    assigned_jobs = self.env['claimed.service'].search_count(
                        [('state', 'in', ['dispatched', 'accepted', 'ongoing', 'reaching', 'documents-received',
                                          'leaving', 'reached-rta', 'car-loaded', 'reached-dropoff', 'car-off-loaded',
                                          'left-rta', 'dropped-car']), ('assigned_party', '=', sub_cont_id)])
                    busy_drivers = self.env['claimed.service'].search_count([('state', 'not in', [
                        'requsted', 'feedback', 'service-provided', 'completed', 'cancelled', 'rejected']), (
                                                                             'assigned_party_driver', 'in',
                                                                             sub_cont_obj.driver_ids.ids)])
                    free_drivers = len(
                        sub_cont_obj.driver_ids.ids) - busy_drivers
                    if free_drivers < 0:
                        free_drivers = 0

                    if busy_drivers < 0:
                        busy_drivers = 0

                    res = {
                        'total_assigned_jobs': assigned_jobs,
                        'free_drivers': free_drivers,
                        'engaged_drivers': busy_drivers,
                    }
        except Exception as e:
            logger.exception("Error: %s;" % e)
            return {"Error": 'Something went wrong'}
        return res

    @api.model
    def SubContractorEngagedDrivers(self, args):
        busy_drivers = {}
        d_id = args.get('driver_id', None)
        try:
            if d_id:
                sub_cont_obj = self.env['service.providers'].search(
                    [('user_id', 'in', d_id)])
                sub_cont_id = sub_cont_obj.id
                if sub_cont_id:
                    busy_driver_ids = self.env['claimed.service'].search([('state', 'not in', [
                        'requsted', 'feedback', 'service-provided', 'completed', 'cancelled', 'rejected']), (
                                                                          'assigned_party_driver', 'in',
                                                                          sub_cont_obj.driver_ids.ids)]).assigned_party_driver.ids
                    busy_drivers = self.env['drivers.menu'].search_read(
                        [('id', 'in', busy_driver_ids)])
        except Exception as e:
            logger.exception("Error: %s;" % e)
            return {"Error": 'Something went wrong'}
        return busy_drivers

    @api.model
    def SubContractorFreeDrivers(self, args):
        free_drivers = {}
        d_id = args.get('driver_id', None)
        try:
            if d_id:
                sub_cont_obj = self.env['service.providers'].search(
                    [('user_id', 'in', d_id)])
                sub_cont_id = sub_cont_obj.id
                if sub_cont_id:
                    busy_driver_ids = self.env['claimed.service'].search([('state', 'not in', [
                        'requsted', 'feedback', 'service-provided', 'completed', 'cancelled', 'rejected']), (
                                                                          'assigned_party_driver', 'in',
                                                                          sub_cont_obj.driver_ids.ids)]).assigned_party_driver.ids
                    all_drivers = sub_cont_obj.driver_ids.ids
                    free_driver_ids = [
                        i for i in busy_driver_ids + all_drivers if (busy_driver_ids + all_drivers).count(i) == 1]
                    free_drivers = self.env['drivers.menu'].search_read(
                        [('id', 'in', free_driver_ids)])
        except Exception as e:
            logger.exception("Error: %s;" % e)
            return {"Error": 'Something went wrong'}
        return free_drivers

    @api.model
    def SubContractorProfile(self, args):
        d_id = args.get('driver_id', None)
        driver_data = {}
        try:
            if d_id:
                sub_cont_obj = self.env['service.providers'].search(
                    [('user_id', '=', d_id)])
                driver_data.update({
                    'name': sub_cont_obj.user_id.name,
                    'email': sub_cont_obj.user_id.login,
                    'contact_no': sub_cont_obj.contact_no,
                    'image': sub_cont_obj.user_id.image,
                    'id': sub_cont_obj.id,
                })
        except Exception as e:
            logger.exception("Error: %s;" % e)
            return {"Error": 'Something went wrong'}
        return driver_data

    @api.model
    def SubContractorDrivers(self, args):
        drivers = []
        d_id = args.get('driver_id', None)
        try:
            if d_id:
                sub_cont_obj = self.env['service.providers'].search(
                    [('user_id', '=', d_id)])
                drivers = self.env['drivers.menu'].search_read(
                    [('id', 'in', sub_cont_obj.driver_ids.ids)])
        except Exception as e:
            logger.exception("Error: %s;" % e)
            return {"Error": 'Something went wrong'}
        return drivers

    @api.model
    def DelegateTask(self, args):
        d_id = args.get('assigning_to', None)
        job_id = args.get('job_id', None)
        try:
            if d_id:
                job_obj = self.env['claimed.service'].browse(job_id)
                job_obj.write({'assigned_party_driver': d_id})
                sub_driver_obj = self.env['drivers.menu'].browse(d_id)
                gcm_dev_id = str(sub_driver_obj.gcm_dev_id)
                data_message = {
                    'data': json.dumps({
                        'Job_id': job_obj.id,
                        'Title': "New Job Received"
                    })
                }
                result = push_service.notify_single_device(
                    registration_id=gcm_dev_id, data_message=data_message)
        except Exception as e:
            logger.exception("Error: %s;" % e)
            return {"Error": 'Something went wrong'}
        return {"Success": 'Job has been delegated successfully'}

    @api.model
    def SubContractorDriverDashboard(self, args):
        res = {}
        d_id = args.get('driver_id', None)
        try:
            if d_id:
                sub_cont_drv_obj = self.env['drivers.menu'].search(
                    [('related_user', 'in', d_id)])
                sub_cont_drv_id = sub_cont_drv_obj.id
                if sub_cont_drv_id:
                    active_jobs = self.env['claimed.service'].search_count(
                        [('state', 'in', ['accepted', 'ongoing', 'reaching', 'documents-received', 'leaving',
                                          'reached-rta', 'car-loaded', 'reached-dropoff', 'car-off-loaded', 'left-rta',
                                          'dropped-car']), ('assigned_party_driver', '=', sub_cont_drv_id)])
                    completed_jobs = self.env['claimed.service'].search_count(
                        [('state', 'in', ['feedback', 'service-provided', 'completed']),
                         ('assigned_party_driver', '=', sub_cont_drv_id)])
                    future_jobs = self.env['claimed.service'].search_count(
                        [('state', '=', 'dispatched'), ('assigned_party_driver', '=', sub_cont_drv_id)])
                    res = {
                        'active_jobs': active_jobs,
                        'completed_jobs': completed_jobs,
                        'future_jobs': future_jobs,
                        'garages': 0,
                    }
        except Exception as e:
            logger.exception("Error: %s;" % e)
            return {"Error": 'Something went wrong'}
        return res

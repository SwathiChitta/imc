from odoo import api, fields, models, registry, SUPERUSER_ID, _
from datetime import datetime
from odoo.exceptions import UserError, AccessError
from random import randint
import re
import requests
import xmlrpc.client
import logging
import socket

logger = logging.getLogger(__name__)


def get_ip_address():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8", 80))
	return s.getsockname()[0]


def luhn_checksum(card_number):
	def digits_of(n):
		return [int(d) for d in str(n)]

	digits = digits_of(card_number)
	odd_digits = digits[-1::-2]
	even_digits = digits[-2::-2]
	checksum = 0
	checksum += sum(odd_digits)
	for d in even_digits:
		checksum += sum(digits_of(d * 2))
	return checksum % 10


def is_luhn_valid(card_number):
	return luhn_checksum(card_number) == 0


def phone_format(mobile):
	clean_phone_number = re.sub('[^0-9]+', '', str(mobile))
	formatted_phone_number = re.sub(
		"(\d)(?=(\d{3})+(?!\d))", r"\1-", "%d" % int(clean_phone_number[:-1])) + clean_phone_number[-1]
	return formatted_phone_number


def random_with_N_digits(n):
	range_start = 10 ** (n - 1)
	range_end = (10 ** n) - 1
	return randint(range_start, range_end)


class CustomerAPi(models.Model):
	_inherit = 'imc.clients'

	@api.model
	def CustomerSignup(self, args):
		''' Your logic goes here '''
		customer_vals = args.get('customer_vals', None)
		vehicle_details = args.get('vehicle_details', None)

		make_name = vehicle_details.get('vehicle_make', None)
		model = vehicle_details.get('model', None)

		if make_name and model:
			make = self.env['vehicles.master'].search([('name','=',make_name)],limit=1).id
			if not make:
				make = self.env['vehicles.master'].create({'name': make_name}).id
			model_id = self.env['vehicle.model'].search([('model','ilike',model),('make_id','=',make)],limit=1).id
			if not model_id:
				model_id = self.env['vehicle.model'].create({'model': model, 'make_id': make}).id
			vehicle_details['vehicle_make'] = make
			vehicle_details['model'] = model_id
		type = args.get('type', None)
		try:
			if type:
				cus_group = self.env['res.groups'].search([('name', '=', 'Customer')]).id
				if type == 'insurance':
					insurance_number = args.get('policy_no', None)
					expiry_date = datetime.strptime(args['expiry'], '%Y-%m-%d').date()
					membership = self.env['membership.menu'].search(
						[('insurance_number', 'ilike', insurance_number),
						 ('chassis_no', 'ilike', vehicle_details['chassis_no']), ('status', '!=', 'cancel'),('expiry_date','=', expiry_date)])
					if not membership:
						if self.env['res.users'].search([('login', 'ilike', customer_vals['username'])]):
							return {'custom_error': 'Already registered! Please contact our helpline for assistance!'}

						temp_membership = self.env['temporary.users'].search(
							[('policy_no', 'ilike', insurance_number),
							 '|',('chassis_no', 'ilike', vehicle_details['chassis_no']),('mobile_no','ilike', args['mobile_no'])])
						if temp_membership:
							return {'custom_error': 'Already registered ! Please contact our helpline for assistance!'}

						user_id = self.env['res.users'].create({'name': customer_vals['name'], 'login': customer_vals[
							'username'], 'password': str(customer_vals['password']), 'groups_id': [(4, cus_group)]})
						# customer_id = self.env['res.partner'].create({'customer': True, 'email': customer_vals[
						# 											 'username'], 'name': customer_vals['name'], 'mobile': args['mobile_no']})
						temp_id = self.env['temporary.users'].create(
							{"customer_id": user_id.partner_id.id, 'name': customer_vals['name'],
							 'email_id': args['email_id'], 'mobile_no': args['mobile_no'],
							 'policy_no': insurance_number,
							 'vehicle_make': vehicle_details['vehicle_make'], 'model': vehicle_details['model'],
							 'chassis_no': vehicle_details['chassis_no'], 'car_plate': vehicle_details['car_plate_no'],
							 'client': vehicle_details['client'], 'credit_no': '', 'card_issue': False,
							 'card_expiry': False, 'expiry': args['expiry'], 'veh_type': '',
							 'back_rc': vehicle_details['back_rc'], 'front_rc': vehicle_details['front_rc']})
						temp_data = self.env['temporary.users'].search_read(
							[('id', '=', temp_id.id)])
						user_id.partner_id.write(
							{'customer': True, 'email': customer_vals['username'], 'mobile': args['mobile_no']})
						return {"temporary_data": temp_data}

				elif type == 'bank':
					card_number = args.get('card_no', None)
					result = is_luhn_valid(card_number)
					if not result:
						return {'custom_error': "Card is not valid! Kindly recheck your card number!!"}
					veh_obj = self.env['membership.vehicle.info'].search(
						[('chassis_no', 'ilike', vehicle_details['chassis_no'])])

					membership = self.env['membership.menu'].search(
						[('credit_card_number', '=', card_number), ('id', '=', veh_obj.membership_id.id),
						 ('status', '!=', 'cancel')])
					if not membership:
						if self.env['res.users'].search([('login', 'ilike', customer_vals['username'])]):
							return {'custom_error': 'Already registered! Please contact our helpline for assistance!'}

						temp_membership = self.env['temporary.users'].search(
							[('card_expiry', 'ilike', card_number),
							 '|',('chassis_no', 'ilike', vehicle_details['chassis_no']),('mobile_no','ilike', args['mobile_no'])])
						if temp_membership:
							return {'custom_error': 'Already registered! Please contact our helpline for assistance!'}

						user_id = self.env['res.users'].create({'name': customer_vals['name'], 'login': customer_vals[
							'username'], 'password': str(customer_vals['password']), 'groups_id': [(4, cus_group)]})
						# customer_id = self.env['res.partner'].create({'customer': True, 'email': customer_vals[
						# 											 'username'], 'name': customer_vals['name'], 'mobile': args['mobile_no']})
						temp_id = self.env['temporary.users'].create(
							{"customer_id": user_id.partner_id.id, 'name': customer_vals['name'],
							 'email_id': args['email_id'], 'mobile_no': args['mobile_no'], 'policy_no': '',
							 'vehicle_make': vehicle_details['vehicle_make'], 'model': vehicle_details['model'],
							 'chassis_no': vehicle_details['chassis_no'], 'car_plate': vehicle_details['car_plate_no'],
							 'client': vehicle_details['client'], 'credit_no': card_number,
							 'card_issue': args['card_issue'], 'card_expiry': args['card_expiry'], 'veh_type': '',
							 'back_rc': vehicle_details['back_rc'], 'front_rc': vehicle_details['front_rc']})
						temp_data = self.env['temporary.users'].search_read(
							[('id', '=', temp_id.id)])
						user_id.partner_id.write(
							{'customer': True, 'email': customer_vals['username'], 'mobile': args['mobile_no']})
						return {"temporary_data": temp_data}
				elif type == 'cash':
					temp_membership = self.env['temporary.users'].search(
							[('chassis_no', 'ilike', vehicle_details['chassis_no'])])
					if temp_membership:
						return {'custom_error': 'Already registered ! Please contact our helpline for assistance!'}

					try:
						user_id = self.env['res.users'].create({'name': customer_vals['name'], 'login': customer_vals[
							'username'], 'password': str(customer_vals['password']), 'groups_id': [(4, cus_group)]})
					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {'custom_error': "Email already taken !"}
					temp_id = self.env['temporary.users'].create(
						{"customer_id": user_id.partner_id.id, 'name': customer_vals['name'],
						 'email_id': args['email_id'], 'mobile_no': args['mobile_no'],
						 'policy_no': '',
						 'vehicle_make': vehicle_details['vehicle_make'], 'model': vehicle_details['model'],
						 'chassis_no': vehicle_details['chassis_no'], 'car_plate': vehicle_details['car_plate_no'],
						 'client': False, 'credit_no': '', 'card_issue': False,
						 'card_expiry': False, 'expiry': False, 'veh_type': '',
						 'back_rc': vehicle_details['back_rc'], 'front_rc': vehicle_details['front_rc']})
					temp_data = self.env['temporary.users'].search_read(
						[('id', '=', temp_id.id)])
					user_id.partner_id.write(
						{'customer': True, 'email': customer_vals['username'], 'mobile': args['mobile_no']})
					return {"temporary_data": temp_data}

				else:
					return {'custom_error': "Oops!! Something went wrong"}
		
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {'custom_error': "Oops!! Something went wrong"}

		try:
			if customer_vals and membership:
				if self.env['res.users'].search([('login', 'ilike', customer_vals['username'])]):
					return {'custom_error': 'Already registered ! Try with another !'}
				if membership.is_registered:
					return {'custom_error': 'Already registered ! Please contact our helpline for assistance!'}
				user_id = self.env['res.users'].create({'name': customer_vals['name'], 'login': customer_vals[
					'username'], 'password': str(customer_vals['password']), 'groups_id': [(4, cus_group)]})
				if type == 'insurance':
					membership.write({'end_user': user_id.partner_id.id, 'email': customer_vals['username'],
									  'contact_number': args['mobile_no'], 'back_rc': vehicle_details['back_rc'],
									  'front_rc': vehicle_details['front_rc'],
									  'year': vehicle_details['year'],
									  'car_plate': vehicle_details['car_plate_no'],
									  'is_registered': True})
				else:
					veh_obj.write({'back_rc': vehicle_details[
						'back_rc'], 'front_rc': vehicle_details['front_rc'], 'status': 'active',
								   'mfg_year': vehicle_details['year'], 'car_plate': vehicle_details['car_plate_no'], })
					membership.write({'end_user': user_id.partner_id.id, 'email': customer_vals[
						'username'], 'contact_number': args['mobile_no'], 'is_registered': True})

				user_id.partner_id.write(
					{'customer': True, 'email': customer_vals['username'], 'mobile': args['mobile_no']})

				return {'customer_id': user_id.partner_id.id, 'membership_id': membership.id, 'user_id': user_id.id}
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"customer_error": "Oops!! Something went wrong"}
		return {}

	@api.model
	def CustomerLogin(self, args):
		username = args.get('username')
		password = args.get('password')
		dbname = 'IMC11'
		# dbname = 'IMC'
		if username and password:
			try:
				ip = get_ip_address()
				URL = "http://" + ip + ":8069/xmlrpc/common"
				sock_common = xmlrpc.client.ServerProxy(URL, transport=None,
														encoding=None, verbose=False, allow_none=False,
														use_datetime=False, context=None)
				uid = sock_common.login(dbname, username, password)
				if uid:
					user = self.env['res.users'].search([('id', '=', uid)])
					membership = self.env['membership.menu'].search([('end_user', '=', user.partner_id.id),('expiry_date', '>', datetime.now().date())])
					vehicle_data = self.env['membership.vehicle.info'].search_read(
						[('membership_id', '=', membership.id), ('status', '=', 'active')],
						['model_id', 'make_id', 'chassis_no', 'car_plate', 'mfg_year',
						 'status'])
					mem_data = self.env['membership.menu'].search_read(
						[('end_user', '=', user.partner_id.id), ('status', 'in', ['temp', 'activate']),('expiry_date', '>', datetime.now().date())])
					temp_data = self.env['temporary.users'].search_read([('customer_id', '=', user.partner_id.id)])
					if mem_data:
						if vehicle_data:
							for temp in mem_data:
								temp['vehicle_make'] = vehicle_data[0]['make_id']
								temp['model'] = vehicle_data[0]['model_id']
								temp['car_plate'] = vehicle_data[0]['car_plate']
								temp['chassis_no'] = vehicle_data[0]['chassis_no']
								temp['year'] = vehicle_data[0]['mfg_year']
						for name in mem_data:
							name['customer_name'] = str(name['end_user'][1]).title()
							customer_image = self.env['res.partner'].search_read([('id', '=', membership.end_user.id)],
																				 ['image'])
							name['customer_image'] = customer_image
						return {'success': 'Login Success!', 'membership_data': mem_data}
					elif temp_data:
						for temp in temp_data:
							customer_name = str(temp['customer_id'][1]).title()
							temp['customer_name'] = customer_name
							customer_image = self.env['res.partner'].search_read([('id', '=', user.partner_id.id)],
																				 ['image'])
							temp['customer_image'] = customer_image
						return {'success': 'Membership not found! Still you can request for a service!',
								"temporary_data": temp_data}

					else:
						return {'custom_error': 'Details not found!'}
					# mem_obj = mem_obj[0] contact = phone_format(mem_obj.contact_number) code =
					# random_with_N_digits(4) if contact: r = requests.get(
					# "https://api.smsglobal.com/http-api.php?action=sendsms&user=ml2ihgqq&password=mvYSrtg8&&from
					# =IMC&to=%s&text=%s is your OTP to login.Do not share it with anyone, IMC Team." % (contact,
					# str(code))) if r.status_code == 200: mem_obj.write({'otp': code})
				else:
					return {'custom_error': 'Invalid Credentials!'}
			except Exception as e:
				logger.exception("Error: %s;" % e)
				return {"custom_error": "Oops!! Something went wrong"}

		return {'custom_error': 'Membership not found!'}

	@api.model
	def SendOTP(self, args):
		contact = args.get('contact', None)
		code = random_with_N_digits(4)
		try:
			if contact:
				membership = self.env['membership.menu'].search(['|',('end_user.mobile', 'ilike', contact),('contact_number', 'like', contact)])
				temp_membership = self.env['temporary.users'].search([('mobile_no', 'ilike', contact)])

				if not membership and not temp_membership:
					return {'custom_error': 'Details not found!'}

				r = requests.get("https://api.smsglobal.com/http-api.php?action=sendsms&user=ml2ihgqq&password=mvYSrtg8&&from=IMC&to=%s&text=%s is your OTP to login.Do not share it with anyone, IMC Team." % (contact,str(code)))
				if r.status_code == 200:
					if membership:
						membership[0].write({'otp': code})
					if temp_membership:
						temp_membership[0].write({'otp': code})
					
					return {'success': 'OTP sent successfully!'}
				else:
				 return {'custom_error': 'Unable to send OTP !'}
			else:
				 return {'custom_error': 'Unable to send OTP !'}
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"custom_error": "Oops!! Something went wrong"}

	@api.model
	def ResetPassword(self, args):
		contact = args.get('contact', None)
		password = args.get('confirm_password', None)
		try:
			if contact:
				membership = self.env['membership.menu'].search(['|',('end_user.mobile', 'ilike', contact),('contact_number', 'ilike', contact)])
				temp_membership = self.env['temporary.users'].search([('mobile_no', 'ilike', contact)])

				if not membership and not temp_membership:
					return {'custom_error': 'Details not found!'}
				# r = requests.get(
				# "https://api.smsglobal.com/http-api.php?action=sendsms&user=ml2ihgqq&password=mvYSrtg8&&from=IMC&to=%s&text=%s is your OTP to login.Do not share it with anyone, IMC Team." % (contact,str(code)))
				# if r.status_code == 200:
				if membership:
					user = self.env['res.users'].search([('partner_id', '=', membership.end_user.id)])
					user.write({'password': password})
				if temp_membership:
					user = self.env['res.users'].search([('login', 'ilike', temp_membership.email_id)])
					user.write({'password': password})
				return {'success': 'Password reset has been done!'}
			else:
				 return {'custom_error': 'Unable to reset the password !'}
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"custom_error": "Oops!! Something went wrong"}

	@api.model
	def OTPValidation(self, args):
		contact = args.get('contact', None)
		otp = args.get('otp', None)
		if otp and contact:
			membership = self.env['membership.menu'].search(['|',('end_user.mobile', 'ilike', contact),('contact_number', 'ilike', contact),('otp','=', otp)])
			temp_membership = self.env['temporary.users'].search([('mobile_no', 'ilike', contact),('otp','=', otp)])
			if membership or temp_membership:
				return {'success': 'OTP has been validated successfully!'}
			else:
				return {'custom_error': 'Invalid OTP !'}
		else:
			return {'custom_error': 'OTP not found !'}

	@api.model
	def Vehicledetails_plate(self, args):
		plate_no = args.get('plate_no', None)
		if plate_no:
			vehicles = self.env['vehicle.plate'].search_read(
				[('vehicle_plate_no', '=', plate_no)])
			return {'vehicles': vehicles}
		else:
			return {}

	@api.model
	def VehicleList(self, args):
		details = []
		customer_id = args.get('customer_id', None)
		temp_id = args.get('temp_id', None)
		if customer_id:
			try:
				membership = self.env['membership.menu'].search(
					[('end_user', '=', customer_id)])
				if membership.type == 'bank':
					details += self.env['membership.vehicle.info'].search_read(
						[('membership_id', '=', membership.id)], ['model_id', 'make_id', 'chassis_no', 'car_plate',
																  'status'])
					for temp in details:
						if temp['make_id']:
							temp['vehicle_make'] = temp['make_id'][1]
						if temp['model_id']:
							temp['model'] = temp['model_id'][1]
				else:
					details += [
						{
							'vehicle_make': membership.vehicle_make.name,
							'model': membership.model.model,
							'chassis_no': membership.chassis_no,
							'car_plate': membership.car_plate,
							'status': 'active',
						}
					]
			except Exception as e:
				logger.exception("Error: %s;" % e)
				return {'vehicles': details}

		elif temp_id:
			temp_membership = self.env[
				'temporary.users'].search([('id', '=', temp_id)])
			if temp_membership:
				details += [
					{
						'vehicle_make': temp_membership.vehicle_make.name,
						'model': temp_membership.model.model,
						'chassis_no': temp_membership.chassis_no,
						'car_plate': temp_membership.car_plate,
					}
				]

		if details:
			return {'vehicles': details}
		return {'vehicles': []}

	@api.model
	def AddVehicle(self, args):
		car_make = args.get('car_make', None)
		car_model = args.get('car_model', None)
		year = args.get('year', None)
		plate_no = args.get('plate_no', None)
		chassis_no = args.get('chassis_no', None)
		front_img = args.get('front_img', None)
		back_img = args.get('back_img', None)
		membership_id = args.get('membership_id', None)

		if car_make and car_model:
			car_make = self.env['vehicles.master'].search([('name','=',car_make)],limit=1).id
			if not car_make:
				car_make = self.env['vehicles.master'].create({'name': car_make}).id
			car_model = self.env['vehicle.model'].search([('model','ilike',car_model),('make_id','=',car_make)],limit=1).id
			if not car_model:
				car_model = self.env['vehicle.model'].create({'model': car_model, 'make_id': car_make}).id

		try:
			if membership_id:
				add = self.env['membership.menu'].browse(membership_id).write({'vehicles': [(
					0, 0, {'chassis_no': chassis_no, 'make_id': car_make, 'mfg_year': year, 'car_plate': plate_no,
						   'model_id': car_model,
						   'front_rc': front_img, 'back_rc': back_img, 'status': 'inactive'})]})
				return {'vehicle_added': add}
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"custom_error": 'Unable to add vehicle at the moment! Please try later!'}
		return {'vehicle_added': False}

	@api.model
	def CustomerEligibleServices(self, args):
		services = []
		services_ids = []
		service_type = args.get('type', None)
		membership_id = args.get('membership_id', None)
		try:
			if service_type:
				if str(service_type) == 'era':
					services = self.env['era.avail.services'].search(
						[('membership_id', '=', membership_id)])
				elif str(service_type) == 'concierge':
					services = self.env['concierge.avail.services'].search(
						[('membership_id', '=', membership_id)])
				elif str(service_type) == 'rent-a-car':
					services = self.env['rentacar.avail.services'].search(
						[('membership_id', '=', membership_id)])
				elif str(service_type) == 'registration':
					services = self.env['carreg.avail.services'].search(
						[('membership_id', '=', membership_id)])
				else:
					services = self.env['transport.avail.services'].search(
						[('membership_id', '=', membership_id)])
				for i in services:
					services_ids.append(i.service_id.id)
				services = self.env['service.menu'].search_read(
					[('id', 'in', services_ids),('name','not in',['Third Party'])])
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"custom_error": 'Unable to get service types!'}
		return {'records': services}

	@api.model
	def CustomerEligibleTypes(self, args):
		customer_id = args.get('customer_id', None)
		temp_id = args.get('temp_id', None)
		eligible_types = {}
		try:
			if customer_id:
				membership_id = self.env['membership.menu'].search([('end_user', '=', customer_id)]).id
				era_services = self.env['era.avail.services'].search_count(
					[('membership_id', '=', membership_id)])
				con_services = self.env['concierge.avail.services'].search_count(
					[('membership_id', '=', membership_id)])
				rent_services = self.env['rentacar.avail.services'].search_count(
					[('membership_id', '=', membership_id)])
				reg_services = self.env['carreg.avail.services'].search_count(
					[('membership_id', '=', membership_id)])
				trans_services = self.env['transport.avail.services'].search_count(
					[('membership_id', '=', membership_id)])
				eligible_types.update({
					'era': era_services,
					'concierge': con_services,
					'rent-a-car': rent_services,
					'registration': reg_services,
					'transportation': trans_services,
				})
			elif temp_id:
				era_services = self.env['service.menu'].search_count(
					[('request_type', '=', 'era')])
				con_services = self.env['service.menu'].search_count(
					[('request_type', '=', 'concierge')])
				rent_services = self.env['service.menu'].search_count(
					[('request_type', '=', 'rent-a-car')])
				reg_services = self.env['service.menu'].search_count(
					[('request_type', '=', 'registration')])
				trans_services = self.env['service.menu'].search_count(
					[('request_type', '=', 'transportation')])
				eligible_types.update({
					'era': era_services,
					'concierge': con_services,
					'rent-a-car': rent_services,
					'registration': reg_services,
					'transportation': trans_services,
				})

		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"custom_error": 'Unable to get service types!'}
		result = [eligible_types]  # for mobile app appending result to list
		return {'records': result}

	@api.model
	def CustomerRequestedServices(self, args):
		customer_id = args.get('customer_id', None)
		temp_id = args.get('temp_id', None)
		eligible_types = {}
		try:
			if customer_id:
				membership_id = self.env['membership.menu'].search([('end_user', '=', customer_id)]).id
				era_services = self.env['claimed.service'].search_count(
					[('membership_id', '=', membership_id), ('request_type', '=', 'era')])
				con_services = self.env['claimed.service'].search_count(
					[('membership_id', '=', membership_id), ('request_type', '=', 'concierge')])
				rent_services = self.env['claimed.service'].search_count(
					[('membership_id', '=', membership_id), ('request_type', '=', 'rent-a-car')])
				reg_services = self.env['claimed.service'].search_count(
					[('membership_id', '=', membership_id), ('request_type', '=', 'registration')])
				trans_services = self.env['claimed.service'].search_count(
					[('membership_id', '=', membership_id), ('request_type', '=', 'transportation')])
				eligible_types.update({
					'era': era_services,
					'concierge': con_services,
					'rent-a-car': rent_services,
					'registration': reg_services,
					'transportation': trans_services,
				})
			elif temp_id:
				era_services = self.env['claimed.service'].search_count(
					[('request_type', '=', 'era'), ('temporary_user_id', '=', temp_id)])
				con_services = self.env['claimed.service'].search_count(
					[('request_type', '=', 'concierge'), ('temporary_user_id', '=', temp_id)])
				rent_services = self.env['claimed.service'].search_count(
					[('request_type', '=', 'rent-a-car'), ('temporary_user_id', '=', temp_id)])
				reg_services = self.env['claimed.service'].search_count(
					[('request_type', '=', 'registration'), ('temporary_user_id', '=', temp_id)])
				trans_services = self.env['claimed.service'].search_count(
					[('request_type', '=', 'transportation'), ('temporary_user_id', '=', temp_id)])
				eligible_types.update({
					'era': era_services,
					'concierge': con_services,
					'rent-a-car': rent_services,
					'registration': reg_services,
					'transportation': trans_services,
				})

		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {'custom_error': 'Unable to get service types!'}
		result = [eligible_types]  # for mobile app appending result to list
		return {'records': result}

	@api.model
	def CustomerAccountUpdate(self, args):
		vals = args.get('customer_vals')
		if vals:
			try:
				customer = self.env['res.partner'].search([('id', '=', vals['id'])])
				vals.pop('id', None)
				customer.write(vals)
				membership = self.env['membership.menu'].search([('end_user', '=', customer.id)])
				membership.write({"contact_number": vals['mobile']})
				vehicle_data = self.env['membership.vehicle.info'].search_read(
					[('membership_id', '=', membership.id), ('status', '=', 'active')],
					['model_id', 'make_id', 'chassis_no', 'car_plate', 'mfg_year',
					 'status'])
				mem_data = self.env['membership.menu'].search_read(
					[('end_user', '=', customer.id), ('status', 'in', ['temp', 'activate'])])
				temp_data = self.env['temporary.users'].search_read([('customer_id', '=', customer.id)])
				if mem_data:
					if vehicle_data:
						for temp in mem_data:
							temp['vehicle_make'] = vehicle_data[0]['make_id']
							temp['model'] = vehicle_data[0]['model_id']
							temp['car_plate'] = vehicle_data[0]['car_plate']
							temp['chassis_no'] = vehicle_data[0]['chassis_no']
							temp['year'] = vehicle_data[0]['mfg_year']
					for name in mem_data:
						name['customer_name'] = str(name['end_user'][1]).title()
						customer_image = self.env['res.partner'].search_read([('id', '=', membership.end_user.id)],
																			 ['image'])
						name['customer_image'] = customer_image
					return {'success': 'Login Success!', 'membership_data': mem_data}
				elif temp_data:
					for temp in temp_data:
						customer_name = str(temp['customer_id'][1]).title()
						temp['customer_name'] = customer_name
						customer_image = self.env['res.partner'].search_read([('id', '=', customer.id)],
																			 ['image'])
						temp['customer_image'] = customer_image
					return {'success': 'Membership not found! Still you can request for a service!',
							"temporary_data": temp_data}

				else:
					return {'custom_error': 'Details not found!'}
			except Exception as e:
				logger.exception("Error: %s;" % e)
				return {'custom_error': "Oops!! Something went wrong"}

		return {'custom_error': 'Membership not found!'}


CustomerAPi()

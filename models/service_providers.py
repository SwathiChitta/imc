from odoo import api, fields, models, _
from openerp.exceptions import except_orm, Warning, RedirectWarning


class service_providers(models.Model):
    _name = "service.providers"

    name = fields.Char('Name')
    given_services = fields.One2many('service.providers.access', 'service_provider', 'Service Providers')
    email_id = fields.Char('Email Id')
    gcm_dev_id = fields.Char('GCM Dev ID')
    contact_no = fields.Char('Contact No.')
    has_manager = fields.Boolean('Has a Manager?')
    user_id = fields.Many2one('res.users', 'Managed By')

    driver_ids = fields.Many2many('drivers.menu', 'subcontractor_driver_rel_id', 'subcontractor_id', 'd_id',
                                  string='Drivers', domain="[('driver_type','=','sub')]")
    image = fields.Binary('Image')
    
    @api.onchange('driver_ids')
    def _onchange_driver_ids(self):
        context = self._context.copy()
        if ('params' in context) and ('id' in context['params']): current_id = context['params']['id']

        total_drivers = []
        drivers = self.driver_ids.ids
        current_id = False
        if current_id:
            for each in self.search(
                [('id', '!=', current_id), ('driver_ids', '!=', False)]): total_drivers += each.driver_ids.ids
        else:
            for each in self.search([('driver_ids', '!=', False)]): total_drivers += each.driver_ids.ids

        if drivers:
            for driver in drivers:
                if driver in total_drivers: raise Warning(
                    _('Sorry !! This driver is already assigned to other Sub Contractor'))

class service_providers_access(models.Model):
    _name = "service.providers.access"

    service_provider = fields.Many2one('service.providers', 'Service Provider')
    service = fields.Many2one('service.menu', 'Service')
    country_id = fields.Many2one('res.country', 'Country')
    state_id = fields.Many2one('res.country.state', 'Emirates/State')
    location_id = fields.Many2one('res.state.locations', 'Location')

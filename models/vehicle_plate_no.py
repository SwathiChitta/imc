from odoo import api, fields, models


class vehicle_plate(models.Model):
    _name = 'vehicle.plate'

    _rec_name = 'vehicle_plate_no'

    name = fields.Char('Vehicle Name')
    vehicle_make = fields.Many2one('vehicles.master', 'Vehicle Make')
    model = fields.Many2one('vehicle.model','Vehicle Model')
    vehicle_plate_no = fields.Char('Plate Number')
    service_km = fields.Char('Service KM')
    last_service_done_on = fields.Date('Last Service Done')
    reg_date = fields.Date('Registration Date')
    reminder_date = fields.Date('Reminder Date')
   
    @api.multi
    def get_vehicle_id(self, args):
        vehicle_data = {}
        if 'plate_number' in args:
            vehicle_data = self.search_read([('vehicle_plate_no', '=', args['plate_number'])])
        return vehicle_data

vehicle_plate()

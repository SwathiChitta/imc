from odoo import api, fields, models
from lxml import etree
from odoo.osv.orm import setup_modifiers

class Internal_Users(models.Model):

	_inherit = 'res.users'

	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):

		res = super(Internal_Users, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		if 'type' in self._context and self._context['type'] == 'yes':
			doc = etree.XML(res['arch'])
			
			page = doc.xpath("//page[2]")
			if page: setup_modifiers(page[0], {'invisible':1})
			
			for i in ["Invisible","Employees","Administration"]:
				if i == "Invisible":
					seperator = "//separator[4]"
					test = "//field[@name='in_group_"+str(self.env['res.groups'].search([('name','=',i)]).ids[0])+"']"
				elif i == "Employees":
					seperator = "//separator[1]"
					test = "//field[@name='sel_groups_"+str(self.env['res.groups'].search([('name','=',"Employee")]).ids[0])+'_'+str(self.env['res.groups'].search([('name','=',"Officer")]).ids[0])+'_'+str(self.env['res.groups'].search([('name','=',"Manager")]).ids[0])+"']"
				elif i == "Administration":
					seperator = "//separator[1]"
					test = "//field[@name='sel_groups_"+str(self.env['res.groups'].search([('name','=',"Access Rights")]).ids[0])+'_'+str(self.env['res.groups'].search([('name','=',"Settings")]).ids[0])+"']"
				
				node = doc.xpath(test)
				if node: setup_modifiers(node[0], {'invisible':1})

				node2 = doc.xpath(seperator)
				if node2: setup_modifiers(node2[0], {'invisible':1})

			res['arch'] = etree.tostring(doc, encoding='unicode')
		return res
		


	# cust = fields.Boolean('Customer')
	# languages_known = fields.Many2many('user.lang','rel_to_languages','relid1','relid2','Languages Known')
	# department = fields.Many2one('staff.department','Department')
	# job_profile = fields.Many2one('staff.jobprofile','Job Profile')

Internal_Users()


class UserLang(models.Model):

	_name = 'user.lang'

	name = fields.Char('Language', required=True)

UserLang()

class Staff_departments(models.Model):

	_name = 'staff.department'

	name = fields.Char('Department Name', required=True)

Staff_departments()



class Staff_Jobprofile(models.Model):

	_name = 'staff.jobprofile'

	name = fields.Char('Name', required=True)

Staff_Jobprofile()


class Hr_employee_custom(models.Model):

	_inherit = 'hr.employee'

	languages_known = fields.Many2many('user.lang','rel_to_languages','relid1','relid2','Languages Known')
Hr_employee_custom()

class Uninstall(models.TransientModel):
	_inherit = 'base.module.upgrade'

	def upgrade_module(self, cr, uid, ids, context=None):
		res = super(UnInstall, self).upgrade_module(cr, uid, ids, context=context)
		ir_module = self.env['ir.module.module']

		followers  = self.env['mail.followers']

		followers_ids = followers.search([('res_model', 'in', ['membership.menu'])])
		ids = ir_module.search([('state', 'in', ['to remove'])])

		for j in ir_module.browse(cr, uid, ids):
			if j.name == 'imc':
				for i in followers.browse(cr, uid, followers_ids):
					i.unlink()
		return res
from odoo import api, fields, models


class Costs(models.Model):

    _name = "conf.cost"

    name = fields.Selection([('imc', 'IMC'),
                             ('3rd Party', 'Sub Contractor')], 'Driver Type')

    from_city = fields.Many2one('res.country.state', 'From City')
    from_location = fields.Many2one(
        'res.state.locations', string='From Location')

    to_city = fields.Many2one('res.country.state', 'To City')
    to_location = fields.Many2one('res.state.locations', string='To Location')

    cost = fields.Integer('Cost')


Costs()

class State(models.Model):

	_inherit = 'res.country.state'

	active = fields.Boolean('Active', default=True)

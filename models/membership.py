from odoo import api, fields, models, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
import datetime, time
from datetime import date
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from os.path import expanduser
import requests
from . import HTML
import xlrd
import xmlrpc.client
# xmlrpc
home = expanduser("~")
import logging
import socket

logger = logging.getLogger(__name__)

import os
from xlrd import open_workbook
from O365 import Message, Attachment
o365_auth = ('ops@motoringclub.com', 'ITPLAZA@906')
m = Message(auth=o365_auth)


# def get_ip_address():
# 	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# 	s.connect(("8.8.8.8", 80))
# 	return s.getsockname()[0]
	
# username = 'admin' #the user
# pwd = 'imcadmin'      #the password of the user
# dbname = 'IMC11'
# ip = get_ip_address()
# URL = "http://" + ip + ":8069/xmlrpc/common"
# sock_common = xmlrpc.client.ServerProxy(URL, transport=None,
# 												encoding=None, verbose=False, allow_none=False,
# 												use_datetime=False, context=None)

# overriding check_concurrency function from odoo->models.py to prevnt error while verifying temp user
from odoo.models import BaseModel

@api.multi
def _check_concurrency(self):
	update_date = False
	if not (self._log_access and self._context.get(self.CONCURRENCY_CHECK_FIELD)):
		return
	check_clause = "(id = %s AND %s < COALESCE(write_date, create_date, (now() at time zone 'UTC'))::timestamp)"
	for sub_ids in self._cr.split_for_in_conditions(self.ids):
		nclauses = 0
		params = []
		for id in sub_ids:
			id_ref = "%s,%s" % (self._name, id)
			try:
				update_date = self._context[self.CONCURRENCY_CHECK_FIELD].pop(id_ref, None)
			except:
				pass
			if update_date:
				nclauses += 1
				params.extend([id, update_date])
		if not nclauses:
			continue
		query = "SELECT id FROM %s WHERE %s" % (self._table, " OR ".join([check_clause] * nclauses))
		self._cr.execute(query, tuple(params))
		res = self._cr.fetchone()
		if res:
			raise ValidationError(_('A document was modified since you last viewed it (%s:%d)') % (self._description, res[0]))
BaseModel._check_concurrency = _check_concurrency




def luhn_checksum(card_number):
	def digits_of(n):
		return [int(d) for d in str(n)]

	digits = digits_of(card_number)
	odd_digits = digits[-1::-2]
	even_digits = digits[-2::-2]
	checksum = 0
	checksum += sum(odd_digits)
	for d in even_digits:
		checksum += sum(digits_of(d * 2))
	return checksum % 10


def is_luhn_valid(card_number):
	return luhn_checksum(card_number) == 0


REQUEST_SELECTION = [('concierge', 'Concierge'),
					 ('era', 'ERA'),
					 ('rent-a-car', 'Rent a Car'),
					 ('registration', 'Car Registration'),
					 ('transportation', 'Transportation'),
					 ('hertz', 'Hertz'),
					 ]


class Membership_Menu(models.Model):
	_name = "membership.menu"

	_order = "id desc"

	_description = "Memberships"

	_inherit = ['mail.thread', 'mail.activity.mixin']

	def claimed_services_view(self):
		res = {}
		counter = 0
		for service in self.browse():
			for i in service.transport_available_services:
				counter += i.claimed_services
			res[service.id] = counter
		return res

	def _get_claimed_services(self):
		"""
		Compute the total services.
		"""
		for obj in self:
			counter = 0
			for i in obj.transport_available_services:
				counter += i.number_of_services
			services = self.env['claimed.service'].search_count([('membership_id', '=', obj.id)])
			# max_services_id = self.env['membership.type'].search([('rel_ids', '=', obj.company_id.id), ('service_cat_id', '=',obj.membership_type.id),('max_service_count', '=', 'yes')])
			max_services_id = []
			obj.update({
				'claimed_services': services,
				'is_max_service': False,
				'total_services': counter,
				'memberships_count': 0,
				'verified_count': 0,

				'era_claimed_services': self.env['claimed.service'].search_count(
					[('membership_id', '=', obj.id), ('request_type', '=', 'era')]),
				'concierge_claimed_services': self.env['claimed.service'].search_count(
					[('membership_id', '=', obj.id), ('request_type', '=', 'concierge')]),
				'rentacar_claimed_services': self.env['claimed.service'].search_count(
					[('membership_id', '=', obj.id), ('request_type', '=', 'rent-a-car')]),
				'transport_claimed_services': self.env['claimed.service'].search_count(
					[('membership_id', '=', obj.id), ('request_type', '=', 'transportation')]),
				'carreg_claimed_services': self.env['claimed.service'].search_count(
					[('membership_id', '=', obj.id), ('request_type', '=', 'registration')]),
			})
			if max_services_id:
				obj.update({
					'is_max_service': False,
				})

	def _fetch_policy_numbers(self):
		string = ''
		for obj in self:
			formats = obj.company_id.policy_format.ids
			for i in formats:
				string += str(self.env['policy.formats'].browse(i).formats) + ' ,'
			obj.update({
				'helps': string,
			})

	name = fields.Char('ID')
	active = fields.Boolean('Active', default=True)
	type = fields.Selection([('bank', 'Bank'),
							 ('insurance', 'Insurance Company'),
							 ('transport', 'Transport Company'),
							 ('others', 'Others')], "Type")
	credit_card_number = fields.Char('Credit Card #', track_visibility='onchange', index=True)
	card_type = fields.Char('Card Type')
	membership_no = fields.Char('Membership #', track_visibility='onchange')
	contact_number = fields.Char('Contact #', track_visibility='onchange')
	contact_1 = fields.Char('Contact 1 #', track_visibility='onchange')
	contact_2 = fields.Char('Contact 2 #', track_visibility='onchange')
	insurance_number = fields.Char('Policy #', track_visibility='onchange', index=True)
	start_date = fields.Date('Start Date', track_visibility='onchange')
	expiry_date = fields.Date('Expiry Date', track_visibility='onchange')
	issued_date = fields.Date('Issue Date', track_visibility='onchange')
	end_user = fields.Many2one('res.partner', 'Customer Name')
	signatories = fields.One2many("membership.signatories", "membership_id", string="Signatories")
	vip = fields.Selection([('yes', 'Yes'),
							('no', 'No')], 'VIP')
	company_id = fields.Many2one('imc.clients', 'Company Name', track_visibility='onchange')
	vehicle_make = fields.Many2one('vehicles.master', 'Vehicle Make', track_visibility='onchange')
	car_plate_state = fields.Char('Car Plate#', track_visibility='onchange')
	car_plate_code = fields.Char('Car Plate#', track_visibility='onchange')
	car_plate = fields.Char('Car Plate#', track_visibility='onchange')
	repair_inside_agency = fields.Selection([('Y', 'YES'),
											 ('N', 'NO')], 'Agency Repair', track_visibility='onchange')
	rac = fields.Selection([('no', 'No'),
							('yes', 'Yes')], "Rent A Car Eligibility")
	road_side_asst = fields.Selection([('no', 'No'),
							('yes', 'Yes')], "Road Side Assistance")
	how_many_days = fields.Selection([('1', '1 Day'),
									  ('3', '3 Days'),
									  ('5', '5 Days'),
									  ('7', '7 Days'),
									  ('8', '8 Days'),
									  ('10', '10 Days'),
									  ('14', '14 Days'),
									  ('15', '15 Days')], "For How Many Days?")
	type_of_car = fields.Char('Car Category', track_visibility='onchange')
	year = fields.Selection([(str(num), str(num)) for num in range(1930, (datetime.now().year) + 2)],
							'Manufacture Year', track_visibility='onchange')
	model = fields.Many2one('vehicle.model', 'Vehicle Model', track_visibility='onchange')
	body_type = fields.Char('Body Type', track_visibility='onchange')
	veh_reg_no = fields.Char('Registration No.', track_visibility='onchange')
	expiry = fields.Date('Expiry', track_visibility='onchange')
	membership_type = fields.Many2one('service.type', 'Membership Type', track_visibility='onchange')
	veh_type = fields.Selection([('suv', 'SUV'),
								 ('sedan', 'Sedan'),
								 ('sports', 'Sports'),
								 ('bike', 'Bike'),
								 ('others', 'Others')], 'Vehicle Type')
	chassis_no = fields.Char('Chassis #', track_visibility='onchange', index=True)
	member_fee = fields.Char('Member Fee', track_visibility='onchange')
	branch = fields.Char('Branch', track_visibility='onchange')
	mode_of_payment = fields.Char('Mode Of Payment', track_visibility='onchange')
	dated = fields.Date('Dated', track_visibility='onchange')
	ref_date = fields.Date('Ref Date', track_visibility='onchange')
	created_datetime = fields.Datetime('Created At', default=fields.Datetime.now, track_visibility='onchange')
	created_by = fields.Many2one('res.users', 'Created By', default=lambda self: self.env.user,
								 track_visibility='onchange')
	invoice_date = fields.Date('Invoice No/Date', track_visibility='onchange')
	comments = fields.Text('Comments', track_visibility='onchange')
	claimed_services = fields.Integer(string='Claimed Services', readonly=True, compute='_get_claimed_services')
	total_services = fields.Integer("Total Services", compute='_get_total_services')
	max_service_count = fields.Integer(string="Max. Services Count", compute='claimed_services_view', readonly=True)
	is_max_service = fields.Boolean("Max. Services??", compute='_get_claimed_services')

	status = fields.Selection([('temp', 'Temp'),
							   ('activate', 'Active'),
							   ('expired', 'Expired'),
							   ('cancel', 'Cancelled'),
							   ], 'Status', default='temp', track_visibility='onchange')

	memberships_count = fields.Integer(compute="_get_claimed_services", readonly=True,
									   string="New Memberships Count")
	verified_count = fields.Integer(compute="_get_claimed_services", readonly=True,
									string="How many Verified ?")
	era_available_services = fields.One2many('era.avail.services', 'membership_id', 'Available Services',
											 track_visibility='onchange')
	concierge_available_services = fields.One2many('concierge.avail.services', 'membership_id', 'Available Services',
												   track_visibility='onchange')
	hertz_available_services = fields.One2many('hertz.avail.services', 'membership_id', 'Available Services',
											   track_visibility='onchange')
	carreg_available_services = fields.One2many('carreg.avail.services', 'membership_id', 'Available Services',
												track_visibility='onchange')
	rentacar_available_services = fields.One2many('rentacar.avail.services', 'membership_id', 'Available Services',
												  track_visibility='onchange')
	transport_available_services = fields.One2many('transport.avail.services', 'membership_id', 'Available Services',
												   track_visibility='onchange')
	vehicles = fields.One2many('membership.vehicle.info', 'membership_id', 'Vehicles')

	era_claimed_services = fields.Integer(string='ERA', readonly=True, store=False, compute='_get_claimed_services')
	concierge_claimed_services = fields.Integer(string='Concierge', readonly=True, store=False,
												compute='_get_claimed_services')
	rentacar_claimed_services = fields.Integer(string='Rent A Car', readonly=True, store=False,
											   compute='_get_claimed_services')
	transport_claimed_services = fields.Integer(string='Transport', readonly=True, store=False,
												compute='_get_claimed_services')
	carreg_claimed_services = fields.Integer(string='Car Registration', readonly=True, store=False,
											 compute='_get_claimed_services')

	helps = fields.Char('Help')
	email = fields.Char('Email')
	car_change = fields.Integer('Car Change Count')
	uploaded_year = fields.Char('Year')
	month = fields.Char('Month')
	datetime = fields.Datetime('Date & Time')
	otp = fields.Char('OTP')
	is_registered = fields.Boolean('Registered from App?')

	front_rc = fields.Binary('Front Side of RC')
	back_rc = fields.Binary('Back Side of RC')
	expired_notified = fields.Boolean('Expired Notified')
	



	@api.onchange('company_id')
	def onchange_company(self):
		if self.company_id:
			string = ''
			for i in self.company_id.policy_format:
				string = string + i.formats + ', '
			self.helps = string

	@api.multi
	def get_avaialable_services(self, args):
		if 'card_number' in args:
			v_ids = []
			card_number = args['card_number']

			# Luhn Check
			result = is_luhn_valid(card_number)
			if not result:
				raise UserError(_('Card is not valid! Kindly recheck your card number!!'))

			if card_number:
				card_ids = self.env['bin.range.numbers'].search([('number', '=', card_number)])
				# type_ids = self.env['membership.type'].search([('bin_ranges', 'in', card_ids)])
				type_ids = []

			services = self.env['membership.services'].search([('membership_id', 'in', type_ids)])

			services_data = self.env['membership.services'].read(services)

			return services_data

		if 'insurance_number' in args:
			insurance_number = args['insurance_number']
			if insurance_number:
				memb_ids = self.search([('insurance_number', '=', str(insurance_number))])
				membership_data = self.read(memb_ids)
				if membership_data:
					# services_data = self.env['membership.avail.services'].read(membership_data[0]['available_services'])
					services_data = []
					return services_data

		return []

	@api.multi
	def _update_memberships_status_data(self):
		# x = False
		# records = []
		# table_data = HTML.Table(header_row=['Customer Name','Membership Type','Company','Policy','Chassis No'])
		# manager_mails_list = self.env['res.users'].search([('groups_id.name', 'in', ['Manager','Admin']), ('groups_id.category_id.name', '=', 'Insurance Access Levels')])

		# temp_memberships = self.env['membership.menu'].search([('status', '=', 'temp'),('expired_notified','=',False)])
		temp_memberships = self.env['membership.menu'].search([('status', '=', 'temp')])

		if temp_memberships:
			for each in temp_memberships:
				created_time = datetime.strptime(each.created_datetime, "%Y-%m-%d %H:%M:%S")
				diff = datetime.now() - created_time
				if diff.total_seconds() / 3600 > 96:
					each.write({'status': 'expired'})
			# 		records += [each.id]
			# 		table_data.rows.append([each.end_user.name, each.membership_type.name, each.company_id.name, each.insurance_number, each.chassis_no])

			# if records:
			# 	for mail in manager_mails_list:

			# 		body_html = "<p>Dear %s,</p><p>Following are the details regarding membership that got expired:</p><p>%s</p><p>Regards,</p><p>IMC Motoring Club.</p>" % (mail.name, table_data)
			# 		m.setRecipients(mail.email)
			# 		m.setSubject('Expired Temporary Memberships')
			# 		m.setBodyHTML(body_html)
			# 		x = m.sendMessage()

			# if x:
			# 	for each in temp_memberships: each.write({'status': 'expired','expired_notified':True})
		return True


	def message_process(self, model, message, custom_values=None,
						save_original=False, strip_attachments=False,
						thread_id=None):

		if isinstance(message, xmlrpclib.Binary):
			message = bytes(message.data)

		if isinstance(message, pycompat.text_type):
			message = message.encode('utf-8')
		extract = getattr(email, 'message_from_bytes', email.message_from_string)
		msg_txt = extract(message)

		msg = self.message_parse(msg_txt, save_original=save_original)
		if strip_attachments:
			msg.pop('attachments', None)

		if msg.get('message_id'):   # should always be True as message_parse generate one if missing
			existing_msg_ids = self.env['mail.message'].search([('message_id', '=', msg.get('message_id'))])
			if existing_msg_ids:
				return False

		routes = self.message_route(msg_txt, msg, model, thread_id, custom_values)
		thread_id = self.message_route_process(msg_txt, msg, routes)
		return thread_id
	
	def message_new(self, msg_dict, custom_values=None):
		vals = {}
		insurances = []

		if msg_dict.get('attachments'):
			BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

			with open(home + '/Export.xlsx', 'wb') as file:
				file.write(msg_dict.get('attachments')[0][1])
				file.close()

			BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
			xl_workbook = xlrd.open_workbook(home + '/Export.xlsx')  # opening a xsl file
			sheet_names = xl_workbook.sheet_names()  # retrieving all sheet names in that xsl file

			xl_sheet0 = xl_workbook.sheet_by_name(sheet_names[0])  # first sheet
			for row in xrange(1, xl_sheet0.nrows):

				vals = {}
				types = self.env['imc.clients'].search([('name', '=', 'Client-4')])
				if types:
					company_id = types[0]
				else:
					company_id = self.env['imc.clients'].create({'name': 'Client-4'})
				vals.update({'company_id': company_id.id})

				if xl_sheet0.cell(row, 1).value:
					vals.update({'insurance_number': xl_sheet0.cell(row, 1).value})
					insurances += [xl_sheet0.cell(row, 1).value]

				if xl_sheet0.cell(row, 2).value:
					enduser = self.env['res.partner'].search([('name', '=', xl_sheet0.cell(row, 2).value)])
					if enduser:
						endusr = enduser[0]
					else:
						endusr = self.env['res.partner'].create({'name': str(xl_sheet0.cell(row, 2).value)})
					vals.update({'end_user': endusr.id})

				if xl_sheet0.cell(row, 3).value:
					dt = xl_sheet0.cell(row, 3).value
					dt = xlrd.xldate.xldate_as_datetime(dt, xl_workbook.datemode)
					vals.update({'start_date': dt.date()})

				if xl_sheet0.cell(row, 4).value:
					ex = xl_sheet0.cell(row, 4).value
					ex = xlrd.xldate.xldate_as_datetime(ex, xl_workbook.datemode)
					vals.update({'expiry_date': ex.date()})

				if xl_sheet0.cell(row, 6).value: vals.update(
					{'car_plate': str(xl_sheet0.cell(row, 6).value).rstrip('0').rstrip('.')})
				if xl_sheet0.cell(row, 7).value: vals.update({'chassis_no': xl_sheet0.cell(row, 7).value})

				if xl_sheet0.cell(row, 5).value:
					vehicle = self.env['vehicles.master'].search([('name', '=', xl_sheet0.cell(row, 5).value)])
					if vehicle:
						vehicle_id = vehicle[0]
					else:
						vehicle_id = self.env['vehicles.master'].create({'name': xl_sheet0.cell(row, 5).value})
					vals.update({'vehicle_make': vehicle_id.id})

				if xl_sheet0.cell(row, 8).value:
					membership = self.env['service.type'].search([('name', '=', xl_sheet0.cell(row, 8).value)])
					if membership:
						membership_type_id = membership[0]

						# member_type = self.env['membership.type'].search([('rel_ids', '=', company_id.id), ('service_cat_id', '=', membership[0].id)])
						member_type = []
						# member_services = self.env['membership.type'].browse(member_type).service_ids.ids
						member_services = []
						services = []
						for each in member_services:
							service_id = self.env['membership.services'].browse(each).service_id.id
							unlimited = self.env['membership.services'].browse(each).unlimited
							number_services = self.env['membership.services'].browse(each).number_services
							services += [(0, 0, {'service_id': service_id, 'number_of_services': number_services,
												 'unlimited': unlimited})]
					# if services: vals.update({'available_services': services})
					else:
						membership_type_id = self.env['service.type'].create({'name': xl_sheet0.cell(row, 8).value})
					vals.update({'membership_type': membership_type_id.id})

				if xl_sheet0.cell(row, 1).value and endusr:
					membership_Rec = self.env['membership.menu'].search(
						[('insurance_number', '=', vals['insurance_number']), ('end_user', '=', endusr.id)])
					if membership_Rec:

						if self.env['membership.menu'].browse(membership_Rec.id).status == 'cancel':
							self.env['membership.menu'].browse(membership_Rec.id).write({'status': 'activate'})
						else:
							self.env['membership.menu'].browse(membership_Rec.id).update(vals)
					else:
						vals.update({'status': 'activate', 'type': 'insurance'})
						self.env['membership.menu'].create(vals)

				# os.remove(home +'/Export.xlsx')
			for each in self.env['membership.menu'].search([]):
				rec = self.env['membership.menu'].browse(each.id)
				if rec.insurance_number not in insurances:
					rec.write({'status': 'cancel'})


	# def message_new(self, msg_dict, custom_values=None):
	# 	vals = {}
	# 	insurances = []

	# 	# xmlrpc
		
	# 	uid = sock_common.login(dbname, username, pwd)
	# 	sock = xmlrpc.client.ServerProxy("http://" + ip + ":8069/xmlrpc/" + 'object')


	# 	if msg_dict.get('attachments'):
	# 		try :
	# 			BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

	# 			with open(home + '/Export.xlsx', 'wb') as file:
	# 				file.write(msg_dict.get('attachments')[0][1])
	# 				file.close()

	# 			BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	# 			xl_workbook = xlrd.open_workbook(home + '/Export.xlsx')  # opening a xsl file
	# 			sheet_names = xl_workbook.sheet_names()  # retrieving all sheet names in that xsl file

	# 			xl_sheet0 = xl_workbook.sheet_by_name(sheet_names[0])  # first sheet
	# 			for row in xrange(1, xl_sheet0.nrows):

	# 				vals = {}
	# 				types = sock.execute(dbname, uid, pwd,'imc.clients', 'search',[['name', '=', 'Client-4']])
	# 				if types: company_id = types[0]
	# 				else: company_id = sock.execute(dbname, uid, pwd, 'imc.clients', 'create', {'name': 'Client-4'})
	# 				vals.update({'company_id': company_id})

	# 				if xl_sheet0.cell(row, 1).value:
	# 					vals.update({'insurance_number': xl_sheet0.cell(row, 1).value})
	# 					insurances += [xl_sheet0.cell(row, 1).value]

	# 				if xl_sheet0.cell(row, 2).value:
	# 					enduser = sock.execute(dbname, uid, pwd,'res.partner', 'search',[['name', '=', xl_sheet0.cell(row, 2).value]])
	# 					if enduser: endusr = enduser[0]
	# 					else: endusr = sock.execute(dbname, uid, pwd, 'res.partner', 'create', {'name': str(xl_sheet0.cell(row, 2).value)})
	# 					vals.update({'end_user': endusr})

	# 				# if xl_sheet0.cell(row, 3).value:
	# 				# 	dt = xl_sheet0.cell(row, 3).value
	# 				# 	dt = xlrd.xldate.xldate_as_datetime(dt, xl_workbook.datemode)
	# 				# 	vals.update({'start_date': dt.date()})

	# 				# if xl_sheet0.cell(row, 4).value:
	# 				# 	ex = xl_sheet0.cell(row, 4).value
	# 				# 	ex = xlrd.xldate.xldate_as_datetime(ex, xl_workbook.datemode)
	# 				# 	vals.update({'expiry_date': ex.date()})

	# 				if xl_sheet0.cell(row, 6).value: vals.update({'car_plate': str(xl_sheet0.cell(row, 6).value).rstrip('0').rstrip('.')})
	# 				if xl_sheet0.cell(row, 7).value: vals.update({'chassis_no': xl_sheet0.cell(row, 7).value})

	# 				if xl_sheet0.cell(row, 5).value:
	# 					vehicle = sock.execute(dbname, uid, pwd,'vehicles.master', 'search',[['name', '=', xl_sheet0.cell(row, 5).value]])
	# 					if vehicle: vehicle_id = vehicle[0]
	# 					else: vehicle_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': xl_sheet0.cell(row, 5).value})
	# 					vals.update({'vehicle_make': vehicle_id})

	# 				if xl_sheet0.cell(row, 8).value:
	# 					membership = sock.execute(dbname, uid, pwd,'service.type', 'search',[['name', '=', xl_sheet0.cell(row, 8).value]])
	# 					if membership:
	# 						membership_type_id = membership[0]
	# 						member_type = []
	# 						member_services = []
	# 						services = []
	# 						for each in member_services:
	# 							each_vls = sock.execute(dbname, uid, pwd,'membership.services', 'read', [each])[0]

	# 							service_id = each_vls['service_id']
	# 							# service_id = self.env['membership.services'].browse(each).service_id.id
	# 							unlimited = each_vls['service_id']['unlimited']
	# 							# unlimited = self.env['membership.services'].browse(each).unlimited
	# 							number_services = each_vls['number_services']
	# 							# number_services = self.env['membership.services'].browse(each).number_services
	# 							services += [(0, 0, {'service_id': service_id, 'number_of_services': number_services,'unlimited': unlimited})]
	# 					else:
	# 						membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': xl_sheet0.cell(row, 8).value})
	# 					vals.update({'membership_type': membership_type_id})

	# 				if xl_sheet0.cell(row, 1).value and endusr:
	# 					membership_Rec = sock.execute(dbname, uid, pwd,'membership.menu', 'search',[['end_user', '=', endusr],['insurance_number', '=', vals['insurance_number']]])
	# 					if membership_Rec:
	# 						membership_Rec = membership_Rec[0]
	# 						membership_Rec_vals = sock.execute(dbname, uid, pwd,'membership.menu', 'read', [membership_Rec])
	# 						if membership_Rec_vals:
	# 							membership_Rec_vals=membership_Rec_vals[0]
	# 							if membership_Rec_vals['status'] == 'cancel': sock.execute(dbname, uid, pwd, 'membership.menu', 'write', membership_Rec, {'status': 'activate'})
	# 							else: sock.execute(dbname, uid, pwd, 'membership.menu', 'write', membership_Rec, vals)

	# 					else:
	# 						vals.update({'status': 'activate', 'type': 'insurance'})
	# 						sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)

	# 				# os.remove(home +'/Export.xlsx')
						
	# 			for each in sock.execute(dbname, uid, pwd,'membership.menu', 'search',[]):
	# 				rec = sock.execute(dbname, uid, pwd,'membership.menu', 'read', [each])[0]
	# 				if rec['insurance_number'] not in insurances: 
	# 					sock.execute(dbname, uid, pwd, 'membership.menu', 'write', each, {'status': 'cancel'})
			
	# 		except Exception as e:
	# 			pass



	# @api.multi
	# @api.onchange('expiry_date', 'company_id')
	# def _onchange_date(self):
	#   current_date = str(datetime.now().date())
	#   if self.expiry_date:
	#       if current_date >= self.expiry_date:
	#           raise UserError(_('Please select present or future Expiry date!!'))

	#   if self.company_id:
	#       if self.company_id.customer_type == 'bank':
	#           if self.company_id.contract_expiry:
	#               self.expiry_date = self.company_id.contract_expiry
	#       if self.company_id.customer_type == 'insurance':
	#           self.expiry_date = datetime.now() + timedelta(13 * 365 / 12)

	# @api.multi
	# @api.onchange('start_date', 'expiry_date', 'company_id')
	# def _changes_in_date(self):
	# 	if self.start_date and self.expiry_date and (self.start_date > self.expiry_date):
	# 		raise UserError(_("Expiry Date Should be greater than Start Date"))

	# if self.company_id:
	#   if self.company_id.customer_type == 'bank':
	#       if self.company_id.contract_expiry:
	#           self.expiry_date = self.company_id.contract_expiry

	#   if self.company_id.customer_type == 'insurance' and self.start_date:
	#       start_date = datetime.strptime(str(self.start_date), '%Y-%m-%d')
	#       self.expiry_date = start_date + timedelta(13 * 365 / 12)

	@api.multi
	@api.onchange('expiry_date', 'type', 'start_date')
	def _changes_in_date(self):
		if self.type:
			if self.type == 'insurance' and self.expiry_date:
				expiry_date = datetime.strptime(str(self.expiry_date), '%Y-%m-%d')
				self.start_date = datetime.strftime(expiry_date - relativedelta(days=395), '%Y-%m-%d')

			if self.type == 'insurance' and self.start_date:
				start_date = datetime.strptime(str(self.start_date), '%Y-%m-%d')
				self.expiry_date = datetime.strftime(start_date + relativedelta(days=395), '%Y-%m-%d')

	@api.onchange('company_id')
	def _onchangeclient(self):
		res = {'value': {}, 'domain': {}, 'warning': 'Warning Message'}
		cate_ids = []
		if self.company_id:
			for i in self.company_id.service_cat_ids:
				cate_ids.append(i.category_id.id)
		return {'domain': {'membership_type': [('id', 'in', cate_ids)]}}

	@api.onchange('end_user')
	def onchange_customer_id(self):
		if self.end_user:
			self.email = self.end_user.email
			self.contact_number = self.end_user.mobile

	@api.multi
	def write(self, vals):
		if 'vehicles' in vals:
			for each in vals['vehicles']:
				if each[2] and each[2]['status'] == 'active':
					if self.car_change > 5:
						raise Warning(_("You have used your limit to change cars!!"))
					else:
						self.car_change += 1
		result = super(Membership_Menu, self).write(vals)
		return result

	@api.model
	def create(self, vals):
		count = 0
		if vals.get('name', _('New')) == _('New'):
			year = datetime.now().year
			vals['name'] = str(year) + '/' + self.env['ir.sequence'].next_by_code('membership.menu') or _('New')

		# if 'expiry_date' in vals and type(vals['expiry_date']) == str:
		#     if (datetime.strptime(vals['expiry_date'], '%Y-%m-%d').date() < datetime.now().date()):
		#         raise Warning(_("Expiry Date should not be less than Today's Date!!"))

		# if 'company_id' in vals:
		#   rec = self.env['imc.clients'].browse(vals['company_id'])
		#   if rec.contract_expiry and vals['expiry_date'] :
		#       if rec.contract_expiry < vals['expiry_date']: raise UserError(_('Card/Policy Expiry date is greater than client contract Expiry date!'))
		#   if rec.contract_start and vals['start_date']:
		#       if rec.contract_start > vals['start_date']: raise UserError(_('Policy Start date is greater than client contract Start date!'))

		if 'vehicles' in vals and vals['vehicles']:
			for each in vals['vehicles']:
				if each[2]['status'] == 'active': count += 1
			if count > 5:
				raise Warning(_('You have used your limit to change cars!!'))
			else:
				vals.update({'car_change': count})

		vals.update({'datetime': datetime.now()})

		result = super(Membership_Menu, self).create(vals)

		try:
			if ('company_id' in vals) and ('insurance_number' in vals) and ('contact_number' in vals) :
				comp_obj = self.env['imc.clients'].browse(vals['company_id'])
				if ('Nasco' in comp_obj.name) and vals['insurance_number'] and \
						vals['contact_number']:
					if comp_obj.send_sms and str(comp_obj.send_sms) == 'yes':
						contact = ''.join(e for e in vals['contact_number'] if e.isalnum())
						contact = '971' + contact[-9:]
						sms_content = "Your Nasco IMC Road Side Assistance Policy No. is %s. For emergency please call " \
									  "8004101. Download our app android " \
									  "https://bit.ly/2rY50Jw " \
									  "Download our iPhone app " \
									  "https://goo.gl/Up5Ygu" % (
							vals['insurance_number'])
						requests.get(
							"https://api.smsglobal.com/http-api.php?action=sendsms&user=ml2ihgqq&password=mvYSrtg8&&from=IMC&to=%s&text=%s&maxsplit=3" % (
							contact, sms_content))
		except Exception as e:
			logger.exception("Error: %s;" % e)

		return result

	@api.multi
	def get_claimed_services(self):
		claimed_services = self.env['claimed.service'].search([('membership_id', '=', self.id)])

		services_claimed = []
		for i in claimed_services:
			services_claimed.append(i.id)
		models_data = self.env['ir.model.data']
		action = self.env.ref('imc.claimed_service_tree_view').read()[0]
		action['Claimed Services'] = 'Claimed Services'
		action['res_model'] = 'claimed.service'
		action['view_mode'] = 'tree,form'
		action['domain'] = [('id', 'in', services_claimed)]
		action['type'] = 'ir.actions.act_window'
		return action

	@api.multi
	def get_total_services(self):
		claimed_services = self.env['claimed.service'].search([('membership_id', '=', self.id)])

		services_claimed = []
		for i in claimed_services:
			services_claimed.append(i.id)
		models_data = self.env['ir.model.data']
		action = self.env.ref('imc.claimed_service_tree_view').read()[0]
		action['Claimed Services'] = 'Claimed Services'
		action['res_model'] = 'claimed.service'
		action['view_mode'] = 'tree'
		action['domain'] = [('id', 'in', services_claimed)]
		action['type'] = 'ir.actions.act_window'
		return action

	@api.multi
	def confirm_membership(self):
		return self.write({'status': 'activate'})

	@api.multi
	def cancel_membership(self):
		return self.write({'status': 'cancel'})

	@api.multi
	def cancel_membership(self):
		return self.write({'status': 'cancel'})

	@api.multi
	def reset_draft(self):
		return self.write({'status': 'temp'})

	def get_claimed_services_pop(self):
		request = self._context['request']
		if request == 'era':
			tree = self.env['ir.ui.view'].search([('name', '=', 'era.test.view')])[0]
			form = self.env['ir.ui.view'].search([('name', '=', 'era.test.form.view')])[0]
		elif request == 'concierge':
			tree = self.env['ir.ui.view'].search([('name', '=', 'concierge.test.view')])[0]
			form = self.env['ir.ui.view'].search([('name', '=', 'concierge.test.form.view')])[0]
		elif request == 'rent-a-car':
			tree = self.env['ir.ui.view'].search([('name', '=', 'rent.acar.test.view')])[0]
			form = self.env['ir.ui.view'].search([('name', '=', 'rent.acar.test.form.view')])[0]
		elif request == 'registration':
			tree = self.env['ir.ui.view'].search([('name', '=', 'car.regis.test.view')])[0]
			form = self.env['ir.ui.view'].search([('name', '=', 'car.regis.test.form.view')])[0]
		elif request == 'transportation':
			tree = self.env['ir.ui.view'].search([('name', '=', 'transportation.test.view')])[0]
			form = self.env['ir.ui.view'].search([('name', '=', 'transportation.test.form.view')])[0]
		else:
			tree = self.env['ir.ui.view'].search([('name', '=', 'claimed.service.view')])[0]
			form = self.env['ir.ui.view'].search([('name', '=', 'claimed.service.form.view')])[0]
		claimed_services = self.env['claimed.service'].search(
			[('request_type', '=', request), ('membership_id', '=', self.id)]).ids
		return {
			'name': 'Claimed Services',
			'view_type': 'form',
			'view_mode': 'form',
			'target': 'current',
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', claimed_services)],
			'views': [(tree.id, 'tree'), (form.id, 'form')],
		}

	@api.onchange('company_id', 'credit_card_number', 'membership_type')
	def onchange_cardnumber(self):
		""" verifying a user's card number """
		res = {'value': {}}
		bin_numbers = []
		if self.credit_card_number:
			card_number = self.credit_card_number

			# Luhn Check
			result = is_luhn_valid(card_number)
			if not result:
				raise UserError(_('Card is not valid! Kindly recheck your card number'))

		if self.company_id:
			client_services = []
			if self.company_id.customer_type == 'bank' and self.credit_card_number:
				bins = []
				sliced_string = ''
				if self.credit_card_number:
					# if self.company_id.verification_type == 'f6+l4':
					sliced_string = str(self.credit_card_number)[0:6]
					# if self.company_id.verification_type == 'all':
					# 	sliced_string = str(self.credit_card_number)
					bin_numbers = self.env['bin.range.numbers'].search([('number', '=', sliced_string)])
					if bin_numbers:
						res['value'].update({'card_type': bin_numbers[0].card_type})
						bins = bin_numbers.ids
				client_services = self.env['membership.services'].search(
					[('client_id', '=', self.company_id.id),
					 ('bin_numbers', 'in', bins)])
				if client_services:
					self.membership_type = client_services[0].category_id.id
			if self.company_id.customer_type == 'insurance'  and self.membership_type:
				client_services = self.env['membership.services'].search(
					[('client_id', '=', self.company_id.id), ('category_id', '=', self.membership_type.id)])

			if client_services:
				client_services = client_services[0]
				era_services = client_services.era_available_services
				carreg_services = client_services.carreg_available_services
				rent_services = client_services.rentacar_available_services
				concierge_services = client_services.concierge_available_services
				transport_services = client_services.transport_available_services
				era_avail_services, concierge_avail_services, carreg_avail_services, rentacar_avail_services, transport_avail_services = [], [], [], [], []
				for i in era_services:
					era_avail_services.append(
						{'type': self.type, 'service_id': i.service_id.id, 'number_of_services': i.number_of_services,
						 'request_type': i.service_id.request_type, 'unlimited': i.unlimited}
					)
				res['value'].update({'era_available_services': era_avail_services})

				for i in carreg_services:
					carreg_avail_services.append(
						{'type': self.type, 'service_id': i.service_id.id, 'number_of_services': i.number_of_services,
						 'request_type': i.service_id.request_type, 'unlimited': i.unlimited}
					)
				res['value'].update({'carreg_available_services': carreg_avail_services})

				for i in rent_services:
					rentacar_avail_services.append(
						{'type': self.type, 'service_id': i.service_id.id, 'number_of_services': i.number_of_services,
						 'request_type': i.service_id.request_type, 'unlimited': i.unlimited}
					)
				res['value'].update({'rentacar_available_services': rentacar_avail_services})

				for i in concierge_services:
					concierge_avail_services.append(
						{'type': self.type, 'service_id': i.service_id.id, 'number_of_services': i.number_of_services,
						 'request_type': i.service_id.request_type, 'unlimited': i.unlimited}
					)
				res['value'].update({'concierge_available_services': concierge_avail_services})

				for i in transport_services:
					transport_avail_services.append(
						{'type': self.type, 'service_id': i.service_id.id, 'number_of_services': i.number_of_services,
						 'request_type': i.service_id.request_type, 'unlimited': i.unlimited}
					)
				res['value'].update({'transport_available_services': transport_avail_services})
		return res

	@api.multi
	def import_membership_data(self):

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		xl_workbook = xlrd.open_workbook(BASE_DIR + '/excels/Client 1.xlsx')  # opening a xsl file
		sheet_names = xl_workbook.sheet_names()  # retrieving all sheet names in that xsl file

		for name in sheet_names:
			xl_sheet0 = xl_workbook.sheet_by_name(name)  # accessing the first sheet of workbook
			# August Sheet
			if 'Aug' in name:
				for row in range(1, xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number: vals.update({'insurance_number': str(insurance_number).rstrip()})

						start_date = xl_sheet0.cell(row, 2).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							vals.update({'start_date': start_date.date()})

						expiry_date = xl_sheet0.cell(row, 3).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							vals.update({'expiry_date': expiry_date.date()})

						customer_name = xl_sheet0.cell(row, 4).value
						if customer_name:
							customer_id = self.env['res.partner'].search(
								[('name', '=', str(customer_name).rstrip())]).ids
							if not customer_id:
								customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
							else:
								customer_id = customer_id[0]
							vals.update({'end_user': customer_id})

						make = xl_sheet0.cell(row, 5).value
						if make:
							make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
							if not make_id:
								make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
							else:
								make_id = make_id[0]
							vals.update({'vehicle_make': make_id})

						car_plate = xl_sheet0.cell(row, 6).value
						if car_plate: vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 7).value
						if chassis_no: vals.update({'chassis_no': chassis_no})

						mem_type = xl_sheet0.cell(row, 8).value
						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = self.env['service.type'].search(
								[('name', 'ilike', str(mem_type_new).rstrip())]).ids
							if not membership_type_id: membership_type_id = [
								self.env['service.type'].create({'name': str(mem_type_new).rstrip()}).id]
							vals.update({'membership_type': membership_type_id[0]})

						company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids

						type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0][
							'service_cat_ids']
						era, concierge, rentacar, transport, carreg = [], [], [], [], []
						for types in type_ids:
							if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
								era_service_list = self.env['membership.services'].browse(
									types).era_available_services.ids
								concierge_service_list = self.env['membership.services'].browse(
									types).concierge_available_services.ids
								rentacar_service_list = self.env['membership.services'].browse(
									types).rentacar_available_services.ids
								carreg_service_list = self.env['membership.services'].browse(
									types).carreg_available_services.ids
								transport_service_list = self.env['membership.services'].browse(
									types).transport_available_services.ids
								if era_service_list:
									for each in era_service_list:
										rec = self.env['era.avail.services'].browse(each)
										era.append([0, False, {'service_id': rec.service_id.id,
															   'number_of_services': rec.number_of_services,
															   'request_type': rec.request_type,
															   'unlimited': rec.unlimited}])
								if concierge_service_list:
									for each in concierge_service_list:
										rec = self.env['concierge.avail.services'].browse(each)
										concierge.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])
								if rentacar_service_list:
									for each in rentacar_service_list:
										rec = self.env['rentacar.avail.services'].browse(each)
										rentacar.append([0, False, {'service_id': rec.service_id.id,
																	'number_of_services': rec.number_of_services,
																	'request_type': rec.request_type,
																	'unlimited': rec.unlimited}])

								if carreg_service_list:
									for each in carreg_service_list:
										rec = self.env['carreg.avail.services'].browse(each)
										carreg.append([0, False, {'service_id': rec.service_id.id,
																  'number_of_services': rec.number_of_services,
																  'request_type': rec.request_type,
																  'unlimited': rec.unlimited}])
								if transport_service_list:
									for each in transport_service_list:
										rec = self.env['transport.avail.services'].browse(each)
										transport.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])

						if era: vals.update({'era_available_services': era})
						if concierge: vals.update({'concierge_available_services': concierge})
						if rentacar: vals.update({'rentacar_available_services': rentacar})
						if transport: vals.update({'transport_available_services': transport})
						if carreg: vals.update({'carreg_available_services': carreg})

						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})
						membership_id = self.search(
							[('chassis_no', '=', chassis_no), ('insurance_number', '=', insurance_number),
							 ('end_user', '=', customer_id)]).ids
						if not membership_id:
							if insurance_number:
								membership = self.create(vals)

					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {"error": "Oops!! Something went wrong"}

			# September Sheet
			if 'Sep' in name:
				for row in range(1, xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number: vals.update({'insurance_number': str(insurance_number).rstrip()})

						start_date = xl_sheet0.cell(row, 3).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							vals.update({'start_date': start_date.date()})

						expiry_date = xl_sheet0.cell(row, 4).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							vals.update({'expiry_date': expiry_date.date()})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = self.env['res.partner'].search(
								[('name', '=', str(customer_name).rstrip())]).ids
							if not customer_id:
								customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
							else:
								customer_id = customer_id[0]
							vals.update({'end_user': customer_id})

						make = xl_sheet0.cell(row, 5).value
						if make:
							make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
							if not make_id:
								make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
							else:
								make_id = make_id[0]
							vals.update({'vehicle_make': make_id})

						year = xl_sheet0.cell(row, 6).value
						if year: vals.update({'year': str(year).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 7).value
						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = self.env['service.type'].search(
								[('name', 'ilike', str(mem_type_new).rstrip())]).ids
							if not membership_type_id: membership_type_id = [
								self.env['service.type'].create({'name': str(mem_type_new).rstrip()}).id]
							vals.update({'membership_type': membership_type_id[0]})

						company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
						type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0][
							'service_cat_ids']
						era, concierge, rentacar, transport, carreg = [], [], [], [], []
						for types in type_ids:
							if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
								era_service_list = self.env['membership.services'].browse(
									types).era_available_services.ids
								concierge_service_list = self.env['membership.services'].browse(
									types).concierge_available_services.ids
								rentacar_service_list = self.env['membership.services'].browse(
									types).rentacar_available_services.ids
								carreg_service_list = self.env['membership.services'].browse(
									types).carreg_available_services.ids
								transport_service_list = self.env['membership.services'].browse(
									types).transport_available_services.ids
								if era_service_list:
									for each in era_service_list:
										rec = self.env['era.avail.services'].browse(each)
										era.append([0, False, {'service_id': rec.service_id.id,
															   'number_of_services': rec.number_of_services,
															   'request_type': rec.request_type,
															   'unlimited': rec.unlimited}])
								if concierge_service_list:
									for each in concierge_service_list:
										rec = self.env['concierge.avail.services'].browse(each)
										concierge.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])
								if rentacar_service_list:
									for each in rentacar_service_list:
										rec = self.env['rentacar.avail.services'].browse(each)
										rentacar.append([0, False, {'service_id': rec.service_id.id,
																	'number_of_services': rec.number_of_services,
																	'request_type': rec.request_type,
																	'unlimited': rec.unlimited}])

								if carreg_service_list:
									for each in carreg_service_list:
										rec = self.env['carreg.avail.services'].browse(each)
										carreg.append([0, False, {'service_id': rec.service_id.id,
																  'number_of_services': rec.number_of_services,
																  'request_type': rec.request_type,
																  'unlimited': rec.unlimited}])
								if transport_service_list:
									for each in transport_service_list:
										rec = self.env['transport.avail.services'].browse(each)
										transport.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])

						if era: vals.update({'era_available_services': era})
						if concierge: vals.update({'concierge_available_services': concierge})
						if rentacar: vals.update({'rentacar_available_services': rentacar})
						if transport: vals.update({'transport_available_services': transport})
						if carreg: vals.update({'carreg_available_services': carreg})

						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						if not self.search(
								[('insurance_number', '=', insurance_number), ('end_user', '=', customer_id)]):
							if insurance_number:
								membership = self.create(vals)
							# cr.commit()
					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {"error": "Oops!! Something went wrong"}

			# October Sheet
			if 'Oct' in name:
				for row in range(1, xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number: vals.update({'insurance_number': str(insurance_number).rstrip()})

						start_date = xl_sheet0.cell(row, 3).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							vals.update({'start_date': start_date.date()})

						expiry_date = xl_sheet0.cell(row, 4).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							vals.update({'expiry_date': expiry_date.date()})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = self.env['res.partner'].search(
								[('name', '=', str(customer_name).rstrip())]).ids
							if not customer_id:
								customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
							else:
								customer_id = customer_id[0]
							vals.update({'end_user': customer_id})

						make = xl_sheet0.cell(row, 5).value
						if make:
							make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
							if not make_id:
								make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
							else:
								make_id = make_id[0]
							vals.update({'vehicle_make': make_id})

						chassis_no = xl_sheet0.cell(row, 6).value
						if chassis_no: vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 7).value
						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = self.env['service.type'].search(
								[('name', 'ilike', str(mem_type_new).rstrip())]).ids
							if not membership_type_id: membership_type_id = [
								self.env['service.type'].create({'name': str(mem_type_new).rstrip()}).id]
							vals.update({'membership_type': membership_type_id[0]})

						company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
						type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0][
							'service_cat_ids']
						era, concierge, rentacar, transport, carreg = [], [], [], [], []
						for types in type_ids:
							if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
								era_service_list = self.env['membership.services'].browse(
									types).era_available_services.ids
								concierge_service_list = self.env['membership.services'].browse(
									types).concierge_available_services.ids
								rentacar_service_list = self.env['membership.services'].browse(
									types).rentacar_available_services.ids
								carreg_service_list = self.env['membership.services'].browse(
									types).carreg_available_services.ids
								transport_service_list = self.env['membership.services'].browse(
									types).transport_available_services.ids
								if era_service_list:
									for each in era_service_list:
										rec = self.env['era.avail.services'].browse(each)
										era.append([0, False, {'service_id': rec.service_id.id,
															   'number_of_services': rec.number_of_services,
															   'request_type': rec.request_type,
															   'unlimited': rec.unlimited}])
								if concierge_service_list:
									for each in concierge_service_list:
										rec = self.env['concierge.avail.services'].browse(each)
										concierge.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])
								if rentacar_service_list:
									for each in rentacar_service_list:
										rec = self.env['rentacar.avail.services'].browse(each)
										rentacar.append([0, False, {'service_id': rec.service_id.id,
																	'number_of_services': rec.number_of_services,
																	'request_type': rec.request_type,
																	'unlimited': rec.unlimited}])

								if carreg_service_list:
									for each in carreg_service_list:
										rec = self.env['carreg.avail.services'].browse(each)
										carreg.append([0, False, {'service_id': rec.service_id.id,
																  'number_of_services': rec.number_of_services,
																  'request_type': rec.request_type,
																  'unlimited': rec.unlimited}])
								if transport_service_list:
									for each in transport_service_list:
										rec = self.env['transport.avail.services'].browse(each)
										transport.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])

						if era: vals.update({'era_available_services': era})
						if concierge: vals.update({'concierge_available_services': concierge})
						if rentacar: vals.update({'rentacar_available_services': rentacar})
						if transport: vals.update({'transport_available_services': transport})
						if carreg: vals.update({'carreg_available_services': carreg})

						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						if not self.search(
								[('insurance_number', '=', insurance_number), ('end_user', '=', customer_id)]):
							if insurance_number:
								membership = self.create(vals)

					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {"error": "Oops!! Something went wrong"}

			# November Sheet
			if 'Nov' in name:
				for row in range(1, xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 3).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							vals.update({'start_date': start_date.date()})

						expiry_date = xl_sheet0.cell(row, 4).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							vals.update({'expiry_date': expiry_date.date()})
						customer_name = xl_sheet0.cell(row, 2).value

						if customer_name:
							customer_id = self.env['res.partner'].search(
								[('name', '=', str(customer_name).rstrip())]).ids
							if not customer_id:
								customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 5).value

						if make:

							make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
							if not make_id:
								make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
							else:
								make_id = make_id[0]

							vals.update({'vehicle_make': make_id})

						car_plate = xl_sheet0.cell(row, 6).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 8).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = self.env['service.type'].search([
								('name', 'ilike', str(mem_type_new).rstrip())]).ids
							if not membership_type_id:
								membership_type_id = [
									self.env['service.type'].create({'name': str(mem_type_new).rstrip()}).id]
							vals.update({'membership_type': membership_type_id[0]})

						company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
						type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0][
							'service_cat_ids']
						era, concierge, rentacar, transport, carreg = [], [], [], [], []
						for types in type_ids:
							if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
								era_service_list = self.env['membership.services'].browse(
									types).era_available_services.ids
								concierge_service_list = self.env['membership.services'].browse(
									types).concierge_available_services.ids
								rentacar_service_list = self.env['membership.services'].browse(
									types).rentacar_available_services.ids
								carreg_service_list = self.env['membership.services'].browse(
									types).carreg_available_services.ids
								transport_service_list = self.env['membership.services'].browse(
									types).transport_available_services.ids
								if era_service_list:
									for each in era_service_list:
										rec = self.env['era.avail.services'].browse(each)
										era.append([0, False, {'service_id': rec.service_id.id,
															   'number_of_services': rec.number_of_services,
															   'request_type': rec.request_type,
															   'unlimited': rec.unlimited}])
								if concierge_service_list:
									for each in concierge_service_list:
										rec = self.env['concierge.avail.services'].browse(each)
										concierge.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])
								if rentacar_service_list:
									for each in rentacar_service_list:
										rec = self.env['rentacar.avail.services'].browse(each)
										rentacar.append([0, False, {'service_id': rec.service_id.id,
																	'number_of_services': rec.number_of_services,
																	'request_type': rec.request_type,
																	'unlimited': rec.unlimited}])

								if carreg_service_list:
									for each in carreg_service_list:
										rec = self.env['carreg.avail.services'].browse(each)
										carreg.append([0, False, {'service_id': rec.service_id.id,
																  'number_of_services': rec.number_of_services,
																  'request_type': rec.request_type,
																  'unlimited': rec.unlimited}])
								if transport_service_list:
									for each in transport_service_list:
										rec = self.env['transport.avail.services'].browse(each)
										transport.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])

						if era: vals.update({'era_available_services': era})
						if concierge: vals.update({'concierge_available_services': concierge})
						if rentacar: vals.update({'rentacar_available_services': rentacar})
						if transport: vals.update({'transport_available_services': transport})
						if carreg: vals.update({'carreg_available_services': carreg})

						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						if not self.search([('insurance_number', '=', insurance_number),
											('end_user', '=', customer_id)]):
							if insurance_number:
								membership = self.create(vals)
							# cr.commit()
					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {"error": "Oops!! Something went wrong"}

			# December Sheet
			if 'Dec' in name:
				for row in range(1, xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 3).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							vals.update({'start_date': start_date.date()})

						expiry_date = xl_sheet0.cell(row, 4).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							vals.update({'expiry_date': expiry_date.date()})
						customer_name = xl_sheet0.cell(row, 2).value

						if customer_name:
							customer_id = self.env['res.partner'].search([
								('name', '=', str(customer_name).rstrip())]).ids
							if not customer_id:
								customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 5).value

						if make:

							make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
							if not make_id:
								make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
							else:
								make_id = make_id[0]

							vals.update({'vehicle_make': make_id})

						car_plate = xl_sheet0.cell(row, 6).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 8).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = self.env['service.type'].search([
								('name', 'ilike', str(mem_type_new).rstrip())]).ids
							if not membership_type_id:
								membership_type_id = [
									self.env['service.type'].create({'name': str(mem_type_new).rstrip()}).id]
							vals.update({'membership_type': membership_type_id[0]})

						company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
						type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0][
							'service_cat_ids']
						era, concierge, rentacar, transport, carreg = [], [], [], [], []
						for types in type_ids:
							if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
								era_service_list = self.env['membership.services'].browse(
									types).era_available_services.ids
								concierge_service_list = self.env['membership.services'].browse(
									types).concierge_available_services.ids
								rentacar_service_list = self.env['membership.services'].browse(
									types).rentacar_available_services.ids
								carreg_service_list = self.env['membership.services'].browse(
									types).carreg_available_services.ids
								transport_service_list = self.env['membership.services'].browse(
									types).transport_available_services.ids
								if era_service_list:
									for each in era_service_list:
										rec = self.env['era.avail.services'].browse(each)
										era.append([0, False, {'service_id': rec.service_id.id,
															   'number_of_services': rec.number_of_services,
															   'request_type': rec.request_type,
															   'unlimited': rec.unlimited}])
								if concierge_service_list:
									for each in concierge_service_list:
										rec = self.env['concierge.avail.services'].browse(each)
										concierge.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])
								if rentacar_service_list:
									for each in rentacar_service_list:
										rec = self.env['rentacar.avail.services'].browse(each)
										rentacar.append([0, False, {'service_id': rec.service_id.id,
																	'number_of_services': rec.number_of_services,
																	'request_type': rec.request_type,
																	'unlimited': rec.unlimited}])

								if carreg_service_list:
									for each in carreg_service_list:
										rec = self.env['carreg.avail.services'].browse(each)
										carreg.append([0, False, {'service_id': rec.service_id.id,
																  'number_of_services': rec.number_of_services,
																  'request_type': rec.request_type,
																  'unlimited': rec.unlimited}])
								if transport_service_list:
									for each in transport_service_list:
										rec = self.env['transport.avail.services'].browse(each)
										transport.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])

						if era: vals.update({'era_available_services': era})
						if concierge: vals.update({'concierge_available_services': concierge})
						if rentacar: vals.update({'rentacar_available_services': rentacar})
						if transport: vals.update({'transport_available_services': transport})
						if carreg: vals.update({'carreg_available_services': carreg})

						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						if not self.search([('insurance_number', '=', insurance_number),
											('end_user', '=', customer_id)]):
							if insurance_number:
								membership = self.create(vals)
							# cr.commit()
					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {"error": "Oops!! Something went wrong"}

			# January Sheet
			if 'Jan' in name:
				for row in range(1, xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})

						issued_date = xl_sheet0.cell(row, 3).value
						if issued_date:
							issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
							vals.update({'issued_date': issued_date.date()})

						start_date = xl_sheet0.cell(row, 4).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							vals.update({'start_date': start_date.date()})

						expiry_date = xl_sheet0.cell(row, 5).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							vals.update({'expiry_date': expiry_date.date()})

						veh_reg_no = xl_sheet0.cell(row, 6).value
						if veh_reg_no:
							vals.update({'veh_reg_no': str(veh_reg_no).rstrip('0').rstrip('.')})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = self.env['res.partner'].search([
								('name', '=', str(customer_name).rstrip())]).ids
							if not customer_id:
								customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})

						make = xl_sheet0.cell(row, 8).value
						if make:

							make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
							if not make_id:
								make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
							else:
								make_id = make_id[0]

							vals.update({'vehicle_make': make_id})

						body_type = xl_sheet0.cell(row, 9).value

						if body_type:
							vals.update({'body_type': body_type})

						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 10).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = self.env['service.type'].search([
								('name', 'ilike', str(mem_type_new).rstrip())]).ids
							if not membership_type_id:
								membership_type_id = [
									self.env['service.type'].create({'name': str(mem_type_new).rstrip()}).id]
							vals.update({'membership_type': membership_type_id[0]})

						company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
						type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0][
							'service_cat_ids']
						era, concierge, rentacar, transport, carreg = [], [], [], [], []
						for types in type_ids:
							if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
								era_service_list = self.env['membership.services'].browse(
									types).era_available_services.ids
								concierge_service_list = self.env['membership.services'].browse(
									types).concierge_available_services.ids
								rentacar_service_list = self.env['membership.services'].browse(
									types).rentacar_available_services.ids
								carreg_service_list = self.env['membership.services'].browse(
									types).carreg_available_services.ids
								transport_service_list = self.env['membership.services'].browse(
									types).transport_available_services.ids
								if era_service_list:
									for each in era_service_list:
										rec = self.env['era.avail.services'].browse(each)
										era.append([0, False, {'service_id': rec.service_id.id,
															   'number_of_services': rec.number_of_services,
															   'request_type': rec.request_type,
															   'unlimited': rec.unlimited}])
								if concierge_service_list:
									for each in concierge_service_list:
										rec = self.env['concierge.avail.services'].browse(each)
										concierge.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])
								if rentacar_service_list:
									for each in rentacar_service_list:
										rec = self.env['rentacar.avail.services'].browse(each)
										rentacar.append([0, False, {'service_id': rec.service_id.id,
																	'number_of_services': rec.number_of_services,
																	'request_type': rec.request_type,
																	'unlimited': rec.unlimited}])

								if carreg_service_list:
									for each in carreg_service_list:
										rec = self.env['carreg.avail.services'].browse(each)
										carreg.append([0, False, {'service_id': rec.service_id.id,
																  'number_of_services': rec.number_of_services,
																  'request_type': rec.request_type,
																  'unlimited': rec.unlimited}])
								if transport_service_list:
									for each in transport_service_list:
										rec = self.env['transport.avail.services'].browse(each)
										transport.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])

						if era: vals.update({'era_available_services': era})
						if concierge: vals.update({'concierge_available_services': concierge})
						if rentacar: vals.update({'rentacar_available_services': rentacar})
						if transport: vals.update({'transport_available_services': transport})
						if carreg: vals.update({'carreg_available_services': carreg})

						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						if not self.search([('insurance_number', '=', insurance_number),
											('end_user', '=', customer_id)]):
							if insurance_number:
								membership = self.create(vals)
							# cr.commit()
					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {"error": "Oops!! Something went wrong"}

			# February Sheet
			if 'Feb' in name:
				for row in range(1, xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})

						issued_date = xl_sheet0.cell(row, 3).value
						if issued_date:
							issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
							vals.update({'issued_date': issued_date.date()})

						start_date = xl_sheet0.cell(row, 4).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							vals.update({'start_date': start_date.date()})

						expiry_date = xl_sheet0.cell(row, 5).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							vals.update({'expiry_date': expiry_date.date()})

						veh_reg_no = xl_sheet0.cell(row, 6).value
						if veh_reg_no:
							vals.update({'veh_reg_no': str(veh_reg_no).rstrip('0').rstrip('.')})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = self.env['res.partner'].search([
								('name', '=', str(customer_name).rstrip())]).ids
							if not customer_id:
								customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})

						make = xl_sheet0.cell(row, 8).value
						if make:

							make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
							if not make_id:
								make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
							else:
								make_id = make_id[0]

							vals.update({'vehicle_make': make_id})

						body_type = xl_sheet0.cell(row, 9).value

						if body_type:
							vals.update({'body_type': body_type})

						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 10).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = self.env['service.type'].search(
								[('name', 'ilike', str(mem_type_new).rstrip())]).ids
							if not membership_type_id:
								membership_type_id = [
									self.env['service.type'].create({'name': str(mem_type_new).rstrip()}).id]
							vals.update({'membership_type': membership_type_id[0]})

						company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
						type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0][
							'service_cat_ids']
						era, concierge, rentacar, transport, carreg = [], [], [], [], []
						for types in type_ids:
							if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
								era_service_list = self.env['membership.services'].browse(
									types).era_available_services.ids
								concierge_service_list = self.env['membership.services'].browse(
									types).concierge_available_services.ids
								rentacar_service_list = self.env['membership.services'].browse(
									types).rentacar_available_services.ids
								carreg_service_list = self.env['membership.services'].browse(
									types).carreg_available_services.ids
								transport_service_list = self.env['membership.services'].browse(
									types).transport_available_services.ids
								if era_service_list:
									for each in era_service_list:
										rec = self.env['era.avail.services'].browse(each)
										era.append([0, False, {'service_id': rec.service_id.id,
															   'number_of_services': rec.number_of_services,
															   'request_type': rec.request_type,
															   'unlimited': rec.unlimited}])
								if concierge_service_list:
									for each in concierge_service_list:
										rec = self.env['concierge.avail.services'].browse(each)
										concierge.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])
								if rentacar_service_list:
									for each in rentacar_service_list:
										rec = self.env['rentacar.avail.services'].browse(each)
										rentacar.append([0, False, {'service_id': rec.service_id.id,
																	'number_of_services': rec.number_of_services,
																	'request_type': rec.request_type,
																	'unlimited': rec.unlimited}])

								if carreg_service_list:
									for each in carreg_service_list:
										rec = self.env['carreg.avail.services'].browse(each)
										carreg.append([0, False, {'service_id': rec.service_id.id,
																  'number_of_services': rec.number_of_services,
																  'request_type': rec.request_type,
																  'unlimited': rec.unlimited}])
								if transport_service_list:
									for each in transport_service_list:
										rec = self.env['transport.avail.services'].browse(each)
										transport.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])

						if era: vals.update({'era_available_services': era})
						if concierge: vals.update({'concierge_available_services': concierge})
						if rentacar: vals.update({'rentacar_available_services': rentacar})
						if transport: vals.update({'transport_available_services': transport})
						if carreg: vals.update({'carreg_available_services': carreg})

						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						if not self.search([('insurance_number', '=', insurance_number),
											('end_user', '=', customer_id)]):
							if insurance_number:
								membership = self.create(vals)
							# cr.commit()
					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {"error": "Oops!! Something went wrong"}

			# March Sheet
			if 'Mar' in name:
				for row in range(1, xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})

						issued_date = xl_sheet0.cell(row, 3).value
						if issued_date:
							issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
							vals.update({'issued_date': issued_date.date()})

						start_date = xl_sheet0.cell(row, 4).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							vals.update({'start_date': start_date.date()})

						expiry_date = xl_sheet0.cell(row, 5).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							vals.update({'expiry_date': expiry_date.date()})

						veh_reg_no = xl_sheet0.cell(row, 6).value
						if veh_reg_no:
							vals.update({'veh_reg_no': str(veh_reg_no).rstrip('0').rstrip('.')})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = self.env['res.partner'].search([
								('name', '=', str(customer_name).rstrip())]).ids
							if not customer_id:
								customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})

						make = xl_sheet0.cell(row, 8).value
						if make:

							make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
							if not make_id:
								make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
							else:
								make_id = make_id[0]

							vals.update({'vehicle_make': make_id})

						body_type = xl_sheet0.cell(row, 9).value

						if body_type:
							vals.update({'body_type': body_type})

						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 10).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = self.env['service.type'].search(
								[('name', 'ilike', str(mem_type_new).rstrip())]).ids
							if not membership_type_id:
								membership_type_id = [
									self.env['service.type'].create({'name': str(mem_type_new).rstrip()}).id]
							vals.update({'membership_type': membership_type_id[0]})

						company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
						type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0][
							'service_cat_ids']
						era, concierge, rentacar, transport, carreg = [], [], [], [], []
						for types in type_ids:
							if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
								era_service_list = self.env['membership.services'].browse(
									types).era_available_services.ids
								concierge_service_list = self.env['membership.services'].browse(
									types).concierge_available_services.ids
								rentacar_service_list = self.env['membership.services'].browse(
									types).rentacar_available_services.ids
								carreg_service_list = self.env['membership.services'].browse(
									types).carreg_available_services.ids
								transport_service_list = self.env['membership.services'].browse(
									types).transport_available_services.ids
								if era_service_list:
									for each in era_service_list:
										rec = self.env['era.avail.services'].browse(each)
										era.append([0, False, {'service_id': rec.service_id.id,
															   'number_of_services': rec.number_of_services,
															   'request_type': rec.request_type,
															   'unlimited': rec.unlimited}])
								if concierge_service_list:
									for each in concierge_service_list:
										rec = self.env['concierge.avail.services'].browse(each)
										concierge.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])
								if rentacar_service_list:
									for each in rentacar_service_list:
										rec = self.env['rentacar.avail.services'].browse(each)
										rentacar.append([0, False, {'service_id': rec.service_id.id,
																	'number_of_services': rec.number_of_services,
																	'request_type': rec.request_type,
																	'unlimited': rec.unlimited}])

								if carreg_service_list:
									for each in carreg_service_list:
										rec = self.env['carreg.avail.services'].browse(each)
										carreg.append([0, False, {'service_id': rec.service_id.id,
																  'number_of_services': rec.number_of_services,
																  'request_type': rec.request_type,
																  'unlimited': rec.unlimited}])
								if transport_service_list:
									for each in transport_service_list:
										rec = self.env['transport.avail.services'].browse(each)
										transport.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])

						if era: vals.update({'era_available_services': era})
						if concierge: vals.update({'concierge_available_services': concierge})
						if rentacar: vals.update({'rentacar_available_services': rentacar})
						if transport: vals.update({'transport_available_services': transport})
						if carreg: vals.update({'carreg_available_services': carreg})

						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						if not self.search([('insurance_number', '=', insurance_number),
											('end_user', '=', customer_id)]):
							if insurance_number:
								membership = self.create(vals)
							# cr.commit()
					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {"error": "Oops!! Something went wrong"}

			# April Sheet
			if 'Apr' in name:
				for row in range(1, xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})

						issued_date = xl_sheet0.cell(row, 3).value
						if issued_date:
							issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
							vals.update({'issued_date': issued_date.date()})

						start_date = xl_sheet0.cell(row, 4).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							vals.update({'start_date': start_date.date()})

						expiry_date = xl_sheet0.cell(row, 5).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							vals.update({'expiry_date': expiry_date.date()})

						veh_reg_no = xl_sheet0.cell(row, 6).value
						if veh_reg_no:
							vals.update({'veh_reg_no': str(veh_reg_no).rstrip('0').rstrip('.')})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = self.env['res.partner'].search(
								[('name', '=', str(customer_name).rstrip())]).ids
							if not customer_id:
								customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
							else:
								customer_id = customer_id[0]
							vals.update({'end_user': customer_id[0]})

						make = xl_sheet0.cell(row, 8).value
						if make:

							make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
							if not make_id:
								make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
							else:
								make_id = make_id[0]

							vals.update({'vehicle_make': make_id})

						body_type = xl_sheet0.cell(row, 9).value

						if body_type:
							vals.update({'body_type': body_type})

						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 10).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = self.env['service.type'].search(
								[('name', 'ilike', str(mem_type_new).rstrip())]).ids
							if not membership_type_id: membership_type_id = [
								self.env['service.type'].create({'name': str(mem_type_new).rstrip()}).id]
							vals.update({'membership_type': membership_type_id[0]})

						company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
						type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0][
							'service_cat_ids']
						era, concierge, rentacar, transport, carreg = [], [], [], [], []
						for types in type_ids:
							if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
								era_service_list = self.env['membership.services'].browse(
									types).era_available_services.ids
								concierge_service_list = self.env['membership.services'].browse(
									types).concierge_available_services.ids
								rentacar_service_list = self.env['membership.services'].browse(
									types).rentacar_available_services.ids
								carreg_service_list = self.env['membership.services'].browse(
									types).carreg_available_services.ids
								transport_service_list = self.env['membership.services'].browse(
									types).transport_available_services.ids
								if era_service_list:
									for each in era_service_list:
										rec = self.env['era.avail.services'].browse(each)
										era.append([0, False, {'service_id': rec.service_id.id,
															   'number_of_services': rec.number_of_services,
															   'request_type': rec.request_type,
															   'unlimited': rec.unlimited}])
								if concierge_service_list:
									for each in concierge_service_list:
										rec = self.env['concierge.avail.services'].browse(each)
										concierge.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])
								if rentacar_service_list:
									for each in rentacar_service_list:
										rec = self.env['rentacar.avail.services'].browse(each)
										rentacar.append([0, False, {'service_id': rec.service_id.id,
																	'number_of_services': rec.number_of_services,
																	'request_type': rec.request_type,
																	'unlimited': rec.unlimited}])

								if carreg_service_list:
									for each in carreg_service_list:
										rec = self.env['carreg.avail.services'].browse(each)
										carreg.append([0, False, {'service_id': rec.service_id.id,
																  'number_of_services': rec.number_of_services,
																  'request_type': rec.request_type,
																  'unlimited': rec.unlimited}])
								if transport_service_list:
									for each in transport_service_list:
										rec = self.env['transport.avail.services'].browse(each)
										transport.append([0, False, {'service_id': rec.service_id.id,
																	 'number_of_services': rec.number_of_services,
																	 'request_type': rec.request_type,
																	 'unlimited': rec.unlimited}])

						if era: vals.update({'era_available_services': era})
						if concierge: vals.update({'concierge_available_services': concierge})
						if rentacar: vals.update({'rentacar_available_services': rentacar})
						if transport: vals.update({'transport_available_services': transport})
						if carreg: vals.update({'carreg_available_services': carreg})

						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						if not self.search(
								[('insurance_number', '=', insurance_number), ('end_user', '=', customer_id[0])]):
							if insurance_number:
								membership = self.create(vals)
							# cr.commit()
					except Exception as e:
						logger.exception("Error: %s;" % e)
						return {"error": "Oops!! Something went wrong"}
					return True

		# @api.multi
		# def import_membership_data2(self):

		#   ''' client-2 data import '''

		#   BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		#   xl_workbook = xlrd.open_workbook(BASE_DIR + '/excels/Client 2.xlsx')  # opening a xsl file
		#   sheet_names = xl_workbook.sheet_names()  # retrieving all sheet names in that xsl file

		#   for name in sheet_names:
		#       xl_sheet0 = xl_workbook.sheet_by_name(name)  # accessing the first sheet of workbook

		#       # Feb Sheet
		#       if 'Feb' in name:
		#           # for row in range(1, xl_sheet0.nrows):
		#           for row in range(1, 5):
		#               try:
		#                   vals = {}

		#                   insurance_number = xl_sheet0.cell(row, 4).value
		#                   if insurance_number:
		#                       vals.update({'insurance_number': str(insurance_number).rstrip()})
		#                   start_date = xl_sheet0.cell(row, 2).value
		#                   if start_date:
		#                       start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
		#                       vals.update({'start_date': start_date.date()})
		#                   expiry_date = xl_sheet0.cell(row, 3).value
		#                   if expiry_date:
		#                       expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
		#                       vals.update({'expiry_date': expiry_date.date()})
		#                   customer_name = xl_sheet0.cell(row, 1).value

		#                   if customer_name:
		#                       customer_id = self.env['res.partner'].search([ ('name', '=', str(customer_name))]).ids
		#                       if not customer_id:
		#                           customer_id = [self.env['res.partner'].create({'name': str(customer_name)}).id]
		#                       else:
		#                           customer_id = customer_id[0]

		#                       vals.update({'end_user': customer_id})
		#                   make = xl_sheet0.cell(row, 6).value

		#                   if make:

		#                       make_id = self.env['vehicles.master'].search([('name', '=', str(make))]).ids
		#                       if not make_id:
		#                           make_id = [self.env['vehicles.master'].create({'name': str(make)}).id]
		#                       else:
		#                           make_id = make_id[0]

		#                       vals.update({'vehicle_make': make_id})

		#                   car_plate = xl_sheet0.cell(row, 8).value

		#                   if car_plate:
		#                       vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

		#                   chassis_no = xl_sheet0.cell(row, 5).value

		#                   if chassis_no:
		#                       vals.update({'chassis_no': chassis_no})
		#                   model = xl_sheet0.cell(row, 7).value

		#                   if model:
		#                       vals.update({'model': str(model)})

		#                   membership_type_id = self.env['service.type'].search([('name', '=', 'Standard')]).ids
		#                   if not membership_type_id: membership_type_id = [self.env['service.type'].create({'name': 'Standard'}).id]
		#                   else: membership_type_id = membership_type_id[0]

		#                   company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
		#                   type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0]['service_cat_ids']
		#                   era, concierge, rentacar, transport, carreg =[], [], [], [], []
		#                   for types in type_ids:
		#                       if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
		#                           era_service_list =  self.env['membership.services'].browse(types).era_available_services.ids
		#                           concierge_service_list =  self.env['membership.services'].browse(types).concierge_available_services.ids
		#                           rentacar_service_list =  self.env['membership.services'].browse(types).rentacar_available_services.ids
		#                           carreg_service_list =  self.env['membership.services'].browse(types).carreg_available_services.ids
		#                           transport_service_list =  self.env['membership.services'].browse(types).transport_available_services.ids
		#                           if era_service_list:
		#                               for each in era_service_list:
		#                                   rec = self.env['era.avail.services'].browse(each)
		#                                   era.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])
		#                           if concierge_service_list:
		#                               for each in concierge_service_list:
		#                                   rec = self.env['concierge.avail.services'].browse(each)
		#                                   concierge.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])
		#                           if rentacar_service_list:
		#                               for each in rentacar_service_list:
		#                                   rec = self.env['rentacar.avail.services'].browse(each)
		#                                   rentacar.append([0, False,  {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])

		#                           if carreg_service_list:
		#                               for each in carreg_service_list:
		#                                   rec = self.env['carreg.avail.services'].browse(each)
		#                                   carreg.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])
		#                           if transport_service_list:
		#                               for each in transport_service_list:
		#                                   rec = self.env['transport.avail.services'].browse(each)
		#                                   transport.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])

		#                   if era: vals.update({'era_available_services':era})
		#                   if concierge: vals.update({'concierge_available_services':concierge})
		#                   if rentacar: vals.update({'rentacar_available_services':rentacar})
		#                   if transport: vals.update({'transport_available_services':transport})
		#                   if carreg: vals.update({'carreg_available_services':carreg})

		#                   vals.update({'company_id': company_id[0]})
		#                   vals.update({'type': 'insurance'})
		#                   vals.update({'membership_type': membership_type_id})

		#                   membership_id = self.search([('chassis_no', '=', chassis_no),
		#                                                         ('insurance_number', '=', insurance_number),
		#                                                         ('end_user', '=', customer_id)])
		#                   if not membership_id:
		#                       if insurance_number:
		#                           membership = self.create(vals)
		#               except Exception as e:
		#                   pass

		# # March Sheet
		# if 'Mar' in name:
		#   for row in range(1, 5):
		#   # for row in range(1, xl_sheet0.nrows):
		#       try:
		#           vals = {}

		#           insurance_number = xl_sheet0.cell(row, 4).value
		#           if insurance_number:
		#               vals.update({'insurance_number': str(insurance_number).rstrip()})
		#           start_date = xl_sheet0.cell(row, 2).value
		#           if start_date:
		#               start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
		#               vals.update({'start_date': start_date.date()})

		#           expiry_date = xl_sheet0.cell(row, 3).value
		#           if expiry_date:
		#               expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
		#               vals.update({'expiry_date': expiry_date.date()})
		#           customer_name = xl_sheet0.cell(row, 1).value

		#           if customer_name:
		#               customer_id = self.env['res.partner'].search([
		#                   ('name', '=', str(customer_name).rstrip())]).ids
		#               if not customer_id:
		#                   customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
		#               else:
		#                   customer_id = customer_id[0]

		#               vals.update({'end_user': customer_id})
		#           make = xl_sheet0.cell(row, 6).value

		#           if make:

		#               make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
		#               if not make_id:
		#                   make_id = [self.env['vehicles.master'].create(cr, uid, {'name': str(make).rstrip()}).id]
		#               else:
		#                   make_id = make_id[0]

		#               vals.update({'vehicle_make': make_id})
		#           car_plate = xl_sheet0.cell(row, 8).value

		#           if car_plate:
		#               vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

		#           chassis_no = xl_sheet0.cell(row, 5).value

		#           if chassis_no:
		#               vals.update({'chassis_no': chassis_no})
		#           model = xl_sheet0.cell(row, 7).value

		#           if model:
		#               vals.update({'model': str(model)})

		#           membership_type_id = self.env['service.type'].search([('name', '=', 'Standard')]).ids

		#           if not membership_type_id:
		#               membership_type_id = [self.env['service.type'].create({'name': 'Standard'}).id]
		#           else:
		#               membership_type_id = membership_type_id[0]

		#           company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
		#           type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0]['service_cat_ids']
		#           era, concierge, rentacar, transport, carreg =[], [], [], [], []
		#           for types in type_ids:
		#               if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
		#                   era_service_list =  self.env['membership.services'].browse(types).era_available_services.ids
		#                   concierge_service_list =  self.env['membership.services'].browse(types).concierge_available_services.ids
		#                   rentacar_service_list =  self.env['membership.services'].browse(types).rentacar_available_services.ids
		#                   carreg_service_list =  self.env['membership.services'].browse(types).carreg_available_services.ids
		#                   transport_service_list =  self.env['membership.services'].browse(types).transport_available_services.ids
		#                   if era_service_list:
		#                       for each in era_service_list:
		#                           rec = self.env['era.avail.services'].browse(each)
		#                           era.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])
		#                   if concierge_service_list:
		#                       for each in concierge_service_list:
		#                           rec = self.env['concierge.avail.services'].browse(each)
		#                           concierge.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])
		#                   if rentacar_service_list:
		#                       for each in rentacar_service_list:
		#                           rec = self.env['rentacar.avail.services'].browse(each)
		#                           rentacar.append([0, False,  {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])

		#                   if carreg_service_list:
		#                       for each in carreg_service_list:
		#                           rec = self.env['carreg.avail.services'].browse(each)
		#                           carreg.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])
		#                   if transport_service_list:
		#                       for each in transport_service_list:
		#                           rec = self.env['transport.avail.services'].browse(each)
		#                           transport.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])

		#           if era: vals.update({'era_available_services':era})
		#           if concierge: vals.update({'concierge_available_services':concierge})
		#           if rentacar: vals.update({'rentacar_available_services':rentacar})
		#           if transport: vals.update({'transport_available_services':transport})
		#           if carreg: vals.update({'carreg_available_services':carreg})

		#           vals.update({'company_id': company_id[0]})
		#           vals.update({'type': 'insurance'})
		#           vals.update({'membership_type': membership_type_id})

		#           membership_id = self.search( [('chassis_no', '=', chassis_no),
		#                                                 ('insurance_number', '=', insurance_number),
		#                                                 ('end_user', '=', customer_id)])
		#           if not membership_id:
		#               if insurance_number:
		#                   membership = self.create(vals)
		#               # else:
		#       except Exception as e:
		#           pass

		# # April Sheet
		# if 'Apr' in name:
		#   # for row in range(1, xl_sheet0.nrows):
		#   for row in range(1, 5):
		#       try:
		#           vals = {}

		#           insurance_number = xl_sheet0.cell(row, 4).value
		#           if insurance_number:
		#               vals.update({'insurance_number': str(insurance_number).rstrip()})
		#           start_date = xl_sheet0.cell(row, 2).value
		#           if start_date:
		#               start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
		#               vals.update({'start_date': start_date.date()})

		#           expiry_date = xl_sheet0.cell(row, 3).value
		#           if expiry_date:
		#               expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
		#               vals.update({'expiry_date': expiry_date.date()})
		#           customer_name = xl_sheet0.cell(row, 1).value

		#           if customer_name:
		#               customer_id = self.env['res.partner'].search(cr, uid, [
		#                   ('name', '=', str(customer_name).rstrip())]).ids
		#               if not customer_id:
		#                   customer_id = [self.env['res.partner'].create({'name': str(customer_name).rstrip()}).id]
		#               else:
		#                   customer_id = customer_id[0]

		#               vals.update({'end_user': customer_id})
		#           make = xl_sheet0.cell(row, 6).value

		#           if make:

		#               make_id = self.env['vehicles.master'].search([('name', '=', str(make).rstrip())]).ids
		#               if not make_id:
		#                   make_id = [self.env['vehicles.master'].create({'name': str(make).rstrip()}).id]
		#               else:
		#                   make_id = make_id[0]

		#               vals.update({'vehicle_make': make_id})
		#           car_plate = xl_sheet0.cell(row, 8).value

		#           if car_plate:
		#               vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

		#           chassis_no = xl_sheet0.cell(row, 5).value

		#           if chassis_no:
		#               vals.update({'chassis_no': chassis_no})
		#           model = xl_sheet0.cell(row, 7).value

		#           if model:
		#               vals.update({'model': str(model)})

		#           membership_type_id = self.env['service.type'].search([('name', '=', 'Standard')]).ids

		#           if not membership_type_id:
		#               membership_type_id = [self.env['service.type'].create({'name': 'Standard'}).id]
		#           else:
		#               membership_type_id = membership_type_id[0]

		#           company_id = self.env['imc.clients'].search([('name', '=', 'Client-1')]).ids
		#           type_ids = self.env['imc.clients'].browse(company_id[0]).read(['service_cat_ids'])[0]['service_cat_ids']
		#           era, concierge, rentacar, transport, carreg =[], [], [], [], []
		#           for types in type_ids:
		#               if membership_type_id[0] == self.env['membership.services'].browse(types).category_id.id:
		#                   era_service_list =  self.env['membership.services'].browse(types).era_available_services.ids
		#                   concierge_service_list =  self.env['membership.services'].browse(types).concierge_available_services.ids
		#                   rentacar_service_list =  self.env['membership.services'].browse(types).rentacar_available_services.ids
		#                   carreg_service_list =  self.env['membership.services'].browse(types).carreg_available_services.ids
		#                   transport_service_list =  self.env['membership.services'].browse(types).transport_available_services.ids
		#                   if era_service_list:
		#                       for each in era_service_list:
		#                           rec = self.env['era.avail.services'].browse(each)
		#                           era.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])
		#                   if concierge_service_list:
		#                       for each in concierge_service_list:
		#                           rec = self.env['concierge.avail.services'].browse(each)
		#                           concierge.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])
		#                   if rentacar_service_list:
		#                       for each in rentacar_service_list:
		#                           rec = self.env['rentacar.avail.services'].browse(each)
		#                           rentacar.append([0, False,  {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])

		#                   if carreg_service_list:
		#                       for each in carreg_service_list:
		#                           rec = self.env['carreg.avail.services'].browse(each)
		#                           carreg.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])
		#                   if transport_service_list:
		#                       for each in transport_service_list:
		#                           rec = self.env['transport.avail.services'].browse(each)
		#                           transport.append([0, False, {'service_id': rec.service_id.id, 'number_of_services': rec.number_of_services, 'request_type': rec.request_type, 'unlimited': rec.unlimited} ])

		#           if era: vals.update({'era_available_services':era})
		#           if concierge: vals.update({'concierge_available_services':concierge})
		#           if rentacar: vals.update({'rentacar_available_services':rentacar})
		#           if transport: vals.update({'transport_available_services':transport})
		#           if carreg: vals.update({'carreg_available_services':carreg})

		#           vals.update({'company_id': company_id[0]})
		#           vals.update({'type': 'insurance'})
		#           vals.update({'membership_type': membership_type_id})

		#           membership_id = self.search( [('chassis_no', '=', chassis_no),  ('insurance_number', '=', insurance_number), ('end_user', '=', customer_id)]).ids
		#           if not membership_id:
		#               if insurance_number:
		#                   membership = self.create(vals)
		#               # else:
		#       except Exception as e:
		#           pass
		return True

	# def import_membership_data3(self, cr, uid, ids,context=None):

	#     ''' client-3 data import '''

	#     BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	#     xl_workbook = xlrd.open_workbook(BASE_DIR + '/excels/Client 3.xlsx')  # opening a xsl file
	#     sheet_names = xl_workbook.sheet_names()  # retrieving all sheet names in that xsl file

	#     for name in sheet_names:
	#         xl_sheet0 = xl_workbook.sheet_by_name(name)  # accessing the first sheet of workbook

	#         # Jan Sheet
	#         if 'Jan' in name:
	#             for row in range(1, xl_sheet0.nrows):
	#                 try:
	#                     vals = {}

	#                     insurance_number = xl_sheet0.cell(row, 4).value
	#                     if insurance_number:
	#                         vals.update({'insurance_number': str(insurance_number).rstrip()})

	#                     year = xl_sheet0.cell(row, 11).value
	#                     if year:
	#                         vals.update({'year': str(year).rstrip('0').rstrip('.')})

	#                     start_date = xl_sheet0.cell(row, 5).value
	#                     if start_date:
	#                         start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
	#                         vals.update({'start_date': start_date.date()})

	#                     expiry_date = xl_sheet0.cell(row, 6).value
	#                     if expiry_date:
	#                         expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
	#                         vals.update({'expiry_date': expiry_date.date()})
	#                     customer_name = xl_sheet0.cell(row, 7).value

	#                     if customer_name:
	#                         customer_id = self.env['res.partner'].search(cr, uid, [
	#                             ('name', '=', str(customer_name).rstrip())])
	#                         if not customer_id:
	#                             customer_id = self.env['res.partner'].create(cr, uid,
	#                                                                               {'name': str(customer_name).rstrip()})
	#                         else:
	#                             customer_id = customer_id[0]

	#                         vals.update({'end_user': customer_id})
	#                     make = xl_sheet0.cell(row, 8).value

	#                     if make:

	#                         make_id = self.env['vehicles.master'].search(cr, uid,
	#                                                                           [('name', '=', str(make).rstrip())])
	#                         if not make_id:
	#                             make_id = self.env['vehicles.master'].create(cr, uid, {'name': str(make).rstrip()})
	#                         else:
	#                             make_id = make_id[0]

	#                         vals.update({'vehicle_make': make_id})
	#                     car_plate = xl_sheet0.cell(row, 10).value

	#                     if car_plate:
	#                         vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

	#                     chassis_no = xl_sheet0.cell(row, 12).value

	#                     if chassis_no:
	#                         vals.update({'chassis_no': chassis_no})

	#                     repair_inside_agency = xl_sheet0.cell(row, 13).value
	#                     if repair_inside_agency:
	#                         vals.update({'repair_inside_agency': str(repair_inside_agency)})

	#                     rac = xl_sheet0.cell(row, 14).value
	#                     if rac:
	#                         vals.update({'rac': str(rac)})

	#                     model = xl_sheet0.cell(row, 9).value

	#                     if model:
	#                         vals.update({'model': str(model)})

	#                     mem_type = xl_sheet0.cell(row, 15).value
	#                     if mem_type:
	#                         membership_type_id = self.env['service.type'].search(cr, uid, [
	#                             ('name', 'ilike', str(mem_type))])
	#                         if not membership_type_id:
	#                             membership_type_id = self.env['service.type'].create(cr, uid,
	#                                                                                       {'name': str(mem_type)})
	#                             vals.update({'membership_type': membership_type_id})
	#                         else:
	#                             membership_type_id = membership_type_id[0]
	#                             vals.update({'membership_type': membership_type_id})

	#                     company_id = self.env['imc.clients'].search(cr, uid, [('name', '=', 'Client-3')])
	#                     type_ids = self.env['imc.clients'].read(cr, uid, company_id, ['membership_types'])
	#                     com_service_ids = self.env['membership.type'].search(cr, uid,
	#                                                                               [('rel_ids', 'in', company_id), (
	#                                                                                   'service_cat_id', '=',
	#                                                                                   membership_type_id)])
	#                     service_ids = self.env['membership.type'].read(cr, uid, com_service_ids, ['service_ids'])
	#                     for i in service_ids:
	#                         service_data = self.env['membership.services'].read(cr, uid, i['service_ids'])
	#                         avail_services = []
	#                         for data in service_data:
	#                             service_obj = self.env['service.menu'].browse(cr, uid, data['service_id'][0])
	#                             avail_services.append([0, False,
	#                                                    {'service_id': data['service_id'][0],
	#                                                     'number_of_services': data['number_services'],
	#                                                     'request_type': service_obj.request_type,
	#                                                     'unlimited': data['unlimited']}
	#                                                    ])
	#                         vals.update({'available_services': avail_services})
	#                     vals.update({'company_id': company_id[0]})
	#                     vals.update({'type': 'insurance'})

	#                     membership_id = self.search(cr, uid, [('chassis_no', '=', chassis_no),
	#                                                           ('insurance_number', '=', insurance_number),
	#                                                           ('end_user', '=', customer_id)])
	#                     if not membership_id:
	#                         if insurance_number:
	#                             membership = self.create(cr, uid, vals)
	#                             cr.commit()
	#                         # else:
	#                 except Exception as e:
	#                     pass

	#         # Feb Sheet
	#         if 'Feb' in name:
	#             for row in range(1, xl_sheet0.nrows):
	#                 try:
	#                     vals = {}

	#                     insurance_number = xl_sheet0.cell(row, 4).value
	#                     if insurance_number:
	#                         vals.update({'insurance_number': str(insurance_number).rstrip()})

	#                     year = xl_sheet0.cell(row, 11).value
	#                     if year:
	#                         vals.update({'year': str(year).rstrip('0').rstrip('.')})

	#                     start_date = xl_sheet0.cell(row, 5).value
	#                     if start_date:
	#                         start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
	#                         vals.update({'start_date': start_date.date()})

	#                     expiry_date = xl_sheet0.cell(row, 6).value
	#                     if expiry_date:
	#                         expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
	#                         vals.update({'expiry_date': expiry_date.date()})
	#                     customer_name = xl_sheet0.cell(row, 7).value

	#                     if customer_name:
	#                         customer_id = self.env['res.partner'].search(cr, uid, [
	#                             ('name', '=', str(customer_name).rstrip())])
	#                         if not customer_id:
	#                             customer_id = self.env['res.partner'].create(cr, uid,
	#                                                                               {'name': str(customer_name).rstrip()})
	#                         else:
	#                             customer_id = customer_id[0]

	#                         vals.update({'end_user': customer_id})
	#                     make = xl_sheet0.cell(row, 8).value

	#                     if make:

	#                         make_id = self.env['vehicles.master'].search(cr, uid,
	#                                                                           [('name', '=', str(make).rstrip())])
	#                         if not make_id:
	#                             make_id = self.env['vehicles.master'].create(cr, uid, {'name': str(make).rstrip()})
	#                         else:
	#                             make_id = make_id[0]

	#                         vals.update({'vehicle_make': make_id})
	#                     car_plate = xl_sheet0.cell(row, 10).value

	#                     if car_plate:
	#                         vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

	#                     chassis_no = xl_sheet0.cell(row, 12).value

	#                     if chassis_no:
	#                         vals.update({'chassis_no': chassis_no})

	#                     repair_inside_agency = xl_sheet0.cell(row, 13).value
	#                     if repair_inside_agency:
	#                         vals.update({'repair_inside_agency': str(repair_inside_agency)})

	#                     rac = xl_sheet0.cell(row, 14).value
	#                     if rac:
	#                         vals.update({'rac': str(rac)})

	#                     model = xl_sheet0.cell(row, 9).value

	#                     if model:
	#                         vals.update({'model': str(model)})

	#                     mem_type = xl_sheet0.cell(row, 15).value
	#                     if mem_type:
	#                         membership_type_id = self.env['service.type'].search(cr, uid, [
	#                             ('name', 'ilike', str(mem_type))])
	#                         if not membership_type_id:
	#                             membership_type_id = self.env['service.type'].create(cr, uid,
	#                                                                                       {'name': str(mem_type)})
	#                             vals.update({'membership_type': membership_type_id})
	#                         else:
	#                             membership_type_id = membership_type_id[0]
	#                             vals.update({'membership_type': membership_type_id})

	#                     company_id = self.env['imc.clients'].search(cr, uid, [('name', '=', 'Client-3')])
	#                     type_ids = self.env['imc.clients'].read(cr, uid, company_id, ['membership_types'])
	#                     com_service_ids = self.env['membership.type'].search(cr, uid,
	#                                                                               [('rel_ids', 'in', company_id), (
	#                                                                                   'service_cat_id', '=',
	#                                                                                   membership_type_id)])
	#                     service_ids = self.env['membership.type'].read(cr, uid, com_service_ids, ['service_ids'])
	#                     for i in service_ids:
	#                         service_data = self.env['membership.services'].read(cr, uid, i['service_ids'])
	#                         avail_services = []
	#                         for data in service_data:
	#                             service_obj = self.env['service.menu'].browse(cr, uid, data['service_id'][0])
	#                             avail_services.append([0, False,
	#                                                    {'service_id': data['service_id'][0],
	#                                                     'number_of_services': data['number_services'],
	#                                                     'request_type': service_obj.request_type,
	#                                                     'unlimited': data['unlimited']}
	#                                                    ])
	#                         vals.update({'available_services': avail_services})
	#                     vals.update({'company_id': company_id[0]})
	#                     vals.update({'type': 'insurance'})

	#                     membership_id = self.search(cr, uid, [('chassis_no', '=', chassis_no),
	#                                                           ('insurance_number', '=', insurance_number),
	#                                                           ('end_user', '=', customer_id)])
	#                     if not membership_id:
	#                         if insurance_number:
	#                             membership = self.create(cr, uid, vals)
	#                             cr.commit()
	#                         # else:
	#                 except Exception as e:
	#                     pass

	#         # March Sheet
	#         if 'Mar' in name:
	#             for row in range(1, xl_sheet0.nrows):
	#                 try:
	#                     vals = {}

	#                     insurance_number = xl_sheet0.cell(row, 4).value
	#                     if insurance_number:
	#                         vals.update({'insurance_number': str(insurance_number).rstrip()})

	#                     year = xl_sheet0.cell(row, 12).value
	#                     if year:
	#                         vals.update({'year': str(year).rstrip('0').rstrip('.')})

	#                     issued_date = xl_sheet0.cell(row, 5).value
	#                     if issued_date:
	#                         issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
	#                         vals.update({'issued_date': issued_date.date()})

	#                     start_date = xl_sheet0.cell(row, 6).value
	#                     if start_date:
	#                         start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
	#                         vals.update({'start_date': start_date.date()})

	#                     expiry_date = xl_sheet0.cell(row, 7).value
	#                     if expiry_date:
	#                         expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
	#                         vals.update({'expiry_date': expiry_date.date()})
	#                     customer_name = xl_sheet0.cell(row, 8).value

	#                     if customer_name:
	#                         customer_id = self.env['res.partner'].search(cr, uid, [
	#                             ('name', '=', str(customer_name).rstrip())])
	#                         if not customer_id:
	#                             customer_id = self.env['res.partner'].create(cr, uid,
	#                                                                               {'name': str(customer_name).rstrip()})
	#                         else:
	#                             customer_id = customer_id[0]

	#                         vals.update({'end_user': customer_id})
	#                     make = xl_sheet0.cell(row, 9).value

	#                     if make:

	#                         make_id = self.env['vehicles.master'].search(cr, uid,
	#                                                                           [('name', '=', str(make).rstrip())])
	#                         if not make_id:
	#                             make_id = self.env['vehicles.master'].create(cr, uid, {'name': str(make).rstrip()})
	#                         else:
	#                             make_id = make_id[0]

	#                         vals.update({'vehicle_make': make_id})

	#                     veh_type = xl_sheet0.cell(row, 10).value

	#                     if veh_type:
	#                         vals.update({'veh_type': str(veh_type)})

	#                     car_plate = xl_sheet0.cell(row, 11).value

	#                     if car_plate:
	#                         vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

	#                     chassis_no = xl_sheet0.cell(row, 13).value

	#                     if chassis_no:
	#                         vals.update({'chassis_no': chassis_no})

	#                     repair_inside_agency = xl_sheet0.cell(row, 15).value
	#                     if repair_inside_agency:
	#                         vals.update({'repair_inside_agency': str(repair_inside_agency)})

	#                     rac = xl_sheet0.cell(row, 16).value
	#                     if rac:
	#                         vals.update({'rac': str(rac)})

	#                     model = xl_sheet0.cell(row, 9).value

	#                     if model:
	#                         vals.update({'model': str(model)})

	#                     mem_type = xl_sheet0.cell(row, 14).value
	#                     if mem_type:
	#                         membership_type_id = self.env['service.type'].search(cr, uid, [
	#                             ('name', 'ilike', str(mem_type))])
	#                         if not membership_type_id:
	#                             membership_type_id = self.env['service.type'].create(cr, uid,
	#                                                                                       {'name': str(mem_type)})
	#                             vals.update({'membership_type': membership_type_id})
	#                         else:
	#                             membership_type_id = membership_type_id[0]
	#                             vals.update({'membership_type': membership_type_id})

	#                     company_id = self.env['imc.clients'].search(cr, uid, [('name', '=', 'Client-3')])
	#                     type_ids = self.env['imc.clients'].read(cr, uid, company_id, ['membership_types'])
	#                     com_service_ids = self.env['membership.type'].search(cr, uid,
	#                                                                               [('rel_ids', 'in', company_id), (
	#                                                                                   'service_cat_id', '=',
	#                                                                                   membership_type_id)])
	#                     service_ids = self.env['membership.type'].read(cr, uid, com_service_ids, ['service_ids'])
	#                     for i in service_ids:
	#                         service_data = self.env['membership.services'].read(cr, uid, i['service_ids'])
	#                         avail_services = []
	#                         for data in service_data:
	#                             service_obj = self.env['service.menu'].browse(cr, uid, data['service_id'][0])
	#                             avail_services.append([0, False,
	#                                                    {'service_id': data['service_id'][0],
	#                                                     'number_of_services': data['number_services'],
	#                                                     'request_type': service_obj.request_type,
	#                                                     'unlimited': data['unlimited']}
	#                                                    ])
	#                         vals.update({'available_services': avail_services})
	#                     vals.update({'company_id': company_id[0]})
	#                     vals.update({'type': 'insurance'})

	#                     membership_id = self.search(cr, uid, [('chassis_no', '=', chassis_no),
	#                                                           ('insurance_number', '=', insurance_number),
	#                                                           ('end_user', '=', customer_id)])
	#                     if not membership_id:
	#                         if insurance_number:
	#                             membership = self.create(cr, uid, vals)
	#                             cr.commit()
	#                         # else:
	#                 except Exception as e:
	#                     pass

	#         # April Sheet
	#         if 'Apr' in name:
	#             for row in range(1, xl_sheet0.nrows):
	#                 try:
	#                     vals = {}

	#                     insurance_number = xl_sheet0.cell(row, 4).value
	#                     if insurance_number:
	#                         vals.update({'insurance_number': str(insurance_number).rstrip()})

	#                     year = xl_sheet0.cell(row, 11).value
	#                     if year:
	#                         vals.update({'year': str(year).rstrip('0').rstrip('.')})

	#                     start_date = xl_sheet0.cell(row, 5).value
	#                     if start_date:
	#                         start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
	#                         vals.update({'start_date': start_date.date()})

	#                     expiry_date = xl_sheet0.cell(row, 6).value
	#                     if expiry_date:
	#                         expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
	#                         vals.update({'expiry_date': expiry_date.date()})

	#                     customer_name = xl_sheet0.cell(row, 7).value
	#                     if customer_name:
	#                         customer_id = self.env['res.partner'].search(cr, uid, [
	#                             ('name', '=', str(customer_name).rstrip())])
	#                         if not customer_id:
	#                             customer_id = self.env['res.partner'].create(cr, uid,
	#                                                                               {'name': str(customer_name).rstrip()})
	#                         else:
	#                             customer_id = customer_id[0]

	#                         vals.update({'end_user': customer_id})
	#                     make = xl_sheet0.cell(row, 8).value

	#                     if make:

	#                         make_id = self.env['vehicles.master'].search(cr, uid,
	#                                                                           [('name', '=', str(make).rstrip())])
	#                         if not make_id:
	#                             make_id = self.env['vehicles.master'].create(cr, uid, {'name': str(make).rstrip()})
	#                         else:
	#                             make_id = make_id[0]

	#                         vals.update({'vehicle_make': make_id})

	#                     veh_type = xl_sheet0.cell(row, 9).value

	#                     if veh_type:
	#                         vals.update({'veh_type': str(veh_type)})

	#                     car_plate = xl_sheet0.cell(row, 10).value

	#                     if car_plate:
	#                         vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

	#                     chassis_no = xl_sheet0.cell(row, 12).value

	#                     if chassis_no:
	#                         vals.update({'chassis_no': chassis_no})

	#                     repair_inside_agency = xl_sheet0.cell(row, 15).value
	#                     if repair_inside_agency:
	#                         vals.update({'repair_inside_agency': str(repair_inside_agency)})

	#                     rac = xl_sheet0.cell(row, 16).value
	#                     if rac:
	#                         vals.update({'rac': str(rac)})

	#                     model = xl_sheet0.cell(row, 9).value

	#                     if model:
	#                         vals.update({'model': str(model)})

	#                     mem_type = xl_sheet0.cell(row, 14).value
	#                     if mem_type:
	#                         membership_type_id = self.env['service.type'].search(cr, uid, [
	#                             ('name', 'ilike', str(mem_type))])
	#                         if not membership_type_id:
	#                             membership_type_id = self.env['service.type'].create(cr, uid,
	#                                                                                       {'name': str(mem_type)})
	#                             vals.update({'membership_type': membership_type_id})
	#                         else:
	#                             membership_type_id = membership_type_id[0]
	#                             vals.update({'membership_type': membership_type_id})

	#                     company_id = self.env['imc.clients'].search(cr, uid, [('name', '=', 'Client-3')])
	#                     type_ids = self.env['imc.clients'].read(cr, uid, company_id, ['membership_types'])
	#                     com_service_ids = self.env['membership.type'].search(cr, uid,
	#                                                                               [('rel_ids', 'in', company_id), (
	#                                                                                   'service_cat_id', '=',
	#                                                                                   membership_type_id)])
	#                     service_ids = self.env['membership.type'].read(cr, uid, com_service_ids, ['service_ids'])
	#                     for i in service_ids:
	#                         service_data = self.env['membership.services'].read(cr, uid, i['service_ids'])
	#                         avail_services = []
	#                         for data in service_data:
	#                             service_obj = self.env['service.menu'].browse(cr, uid, data['service_id'][0])
	#                             avail_services.append([0, False,
	#                                                    {'service_id': data['service_id'][0],
	#                                                     'number_of_services': data['number_services'],
	#                                                     'request_type': service_obj.request_type,
	#                                                     'unlimited': data['unlimited']}
	#                                                    ])
	#                         vals.update({'available_services': avail_services})
	#                     vals.update({'company_id': company_id[0]})
	#                     vals.update({'type': 'insurance'})

	#                     membership_id = self.search(cr, uid, [('chassis_no', '=', chassis_no),
	#                                                           ('insurance_number', '=', insurance_number),
	#                                                           ('end_user', '=', customer_id)])
	#                     if not membership_id:
	#                         if insurance_number:
	#                             membership = self.create(cr, uid, vals)
	#                             cr.commit()
	#                         # else:
	#                 except Exception as e:
	#                     pass
	#     return True

	@api.multi
	def get_multiple_excels_headers_data(self):
		possible_strings = ['NEW POLICY NO', 'Policy No', 'POLNO', 'Policy No.', 'Polic #', 'Policy Number',
							'POLICY NUMBER', 'Client Policy No', 'ASSURED NAME', 'Insured Name', 'Name of the Insured',
							'Assured Name', 'Customer Name', 'CLIENT_DESC', 'Customer', 'Issue Date', 'IssueDate',
							'Insurance IssueDate', 'FROM DATE', 'From Date', 'PRDFRM', 'Policy Fm Date', 'Start Date',
							'Inception Date', 'Insurance Inception Date', 'From', 'EFFECTIVE DATE', 'Policy Start Date',
							'Policy FM Date', 'TO DATE', 'To Date', 'PRDTO', 'Policy Expiry', 'Policy To Date',
							'End Date', 'Policy Expiry', 'Expiry Date', 'Insurance Expiry Date', 'To', 'EXPIRY DATE',
							'Policy TO Date', 'Total Cover', 'MOBILE NO', 'Tel. Off/Res/  Mob.', 'Assured Phone No.',
							'Contact Number', 'Tel. Off/Res/', 'Mob.', 'Contact', 'Customer Contact', 'CHASSIS NO.',
							'Chassis Number', 'CHASISNO', 'Chassis No', 'Chassis No.', 'Chassis #', 'Car Chasis',
							'Chasis No.', 'Chassis Number', 'VEHCHAHIS', 'Chasis_No', 'VEHICLE MAKE', 'Make Name',
							'Make', 'Vehicle Name', 'Vehicle Make', 'Vehicle', 'CarMake', 'Car Make', 'MAKE',
							'Make Desc', 'VEHICLE MODEL', 'Model Name', 'Model', 'model', 'Car Model', 'Model Desc',
							'MF YEAR', 'Year', 'Model Year', 'VEHICLE TYPE', 'Body Name', 'BODY TYPE', 'Reg No',
							'Reg. No', 'Registration No.', 'Reg. Number', 'Reg Not', 'CarPlate', 'Plate No.',
							'MOTRPLAT', 'IMC membership  No.', 'IMC Membership No.', 'IMC Membership #', 'IMC Card No.',
							'IMC Membership NO']
		# possible_strings = ['NEW POLICY NO','Policy No','POLNO','Policy No.','Policy No.','Polic #','Policy No.','Policy Number','Policy Number','Policy No.','POLICY NUMBER','Client Policy No','Policy No','ASSURED NAME','Insured Name','Name of the Insured','Assured Name','Customer Name','Name of the Insured','Customer Name','Customer Name','Insured Name','CLIENT_DESC','Customer','Customer Name','Assured Name','Issue Date','Issue Date','Issue Date','IssueDate','Insurance IssueDate','Issue Date','FROM DATE','From Date','PRDFRM','Policy Fm Date','Start Date','Inception Date','Insurance Inception Date','From','EFFECTIVE DATE','Policy Start Date','Policy FM Date','TO DATE','To Date','PRDTO','Policy Expiry','Policy To Date','End Date','Policy Expiry','Expiry Date','Insurance Expiry Date','To','EXPIRY DATE','Policy TO Date','Total Cover','MOBILE NO','Tel. Off/Res/  Mob.','Assured Phone No.','Contact Number','Tel. Off/Res/', 'Mob.','Contact','Customer Contact','CHASSIS NO.','Chassis Number','CHASISNO','Chassis No','Chassis No.','Chassis #','Chassis No','Car Chasis','Car Chasis','Chasis No.','Chassis Number','VEHCHAHIS','Chasis_No','VEHICLE MAKE','Make Name','Make','Vehicle Name','Vehicle Make','Vehicle','Vehicle Name','CarMake','CarMake','Car Make','MAKE','Make','Make Desc','VEHICLE MODEL','Model Name','Model','model','Car Model','Model','Model Desc','Model','Model','MF YEAR','Year','Year','Year','Year','Year','Model Year','VEHICLE TYPE','Body Name','BODY TYPE','Reg No','Reg. No','Registration No.','Reg. Number','Reg. No','CarPlate','CarPlate','Plate No.','MOTRPLAT','Reg Not','IMC membership  No.','IMC Membership No.','IMC Membership #','IMC Card No.','IMC Membership NO']

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		excel_files = ['1.xlsx', '2.xls', '3.xlsx', '4A.xls', '4B.xlsx', '4C.xls', '4D.xlsx', '5 - RAC.xlsx',
					   '5 - ERA.xlsx', '6.xls', '7.xlsx', '8.xls', '9.xls']
		for file in excel_files:

			xl_workbook = xlrd.open_workbook(BASE_DIR + '/excels/' + str(file))
			sheet_names = xl_workbook.sheet_names()
			for each in sheet_names:
				actual_row, policy_col, cust_col, issue_col, from_col, to_col, mobile_col, chassis_col, vehmake_col, vehmodel_col, mfyear_col, vehtype_col, plateno_col, mem_no_col = False, False, False, False, False, False, False, False, False, False, False, False, False, False

				if file == '9.xls': issue_col, to_col, vehtype_col, mfyear_col, plateno_col, chassis_col = 9, 11, 14, 15, 16, 17
				if file == '5 - ERA.xlsx': mem_no_col, mfyear_col = 1, 9
				if file == '5 - RAC.xlsx': mfyear_col = 8
				if file == '4D.xlsx': from_col = 2
				if file == '4A.xls': from_col = 3
				if file == '3.xlsx': vehmake_col, vehmodel_col, mem_no_col = 11, 12, 4
				if file == '2.xls': vehmodel_col, mfyear_col = 7, 9

				xl_sheet = xl_workbook.sheet_by_name(each)

				for row in range(0, xl_sheet.nrows):
					for col in range(0, xl_sheet.ncols):

						if xl_sheet.cell(row, col).value in possible_strings: actual_row = row + 1

						if xl_sheet.cell(row, col).value in ['POLNO', 'NEW POLICY NO', 'Policy No', 'Policy No.',
															 ' Policy No.', 'Polic #', 'Policy No.', 'Policy Number',
															 'Policy Number ', 'Policy No.', 'POLICY NUMBER ',
															 'Client Policy No', 'Policy No', ]:
							policy_col = str(col)
						elif xl_sheet.cell(row, col).value in ['Customer Name', 'ASSURED NAME', 'Insured Name',
															   'Name of the Insured', 'Assured Name', 'CLIENT_DESC',
															   'Customer']:
							if file == '9.xls':
								cust_col = 8
							else:
								cust_col = str(col)
						elif xl_sheet.cell(row, col).value in ['"Issue Date\n"', 'Issue Date', 'IssueDate',
															   'Insurance IssueDate', ' IssueDate']:
							issue_col = col
						elif xl_sheet.cell(row, col).value in ['EFFECTIVE DATE ', 'FROM DATE', 'From Date', 'PRDFRM',
															   'Policy Fm Date', 'Start Date', 'Inception Date',
															   'Insurance Inception Date', ' Inception Date', 'From',
															   'EFFECTIVE DATE', 'Policy Start Date',
															   'Policy FM Date', ]:
							from_col = col
						elif xl_sheet.cell(row, col).value in ['Total Cover', '"Policy TO Date\n"', 'EXPIRY DATE ',
															   'TO DATE', 'To Date', 'PRDTO', 'Policy Expiry',
															   'Policy To Date', 'End Date', 'Policy Expiry',
															   'Expiry Date', 'Insurance Expiry Date', ' Expiry Date',
															   'To', 'EXPIRY DATE', 'Policy TO Date']:
							to_col = col
						elif xl_sheet.cell(row, col).value in ['MOBILE NO', 'Tel. Off/Res/  Mob.', 'Assured Phone No.',
															   'Contact Number', 'Tel. Off/Res/', 'Mob.', 'Contact',
															   'Customer Contact']:
							mobile_col = col
						elif xl_sheet.cell(row, col).value in ['CHASSIS NO.', 'Chassis Number', 'CHASISNO',
															   'Chassis No', 'Chassis No.', 'Chassis #', 'Car Chasis',
															   'Chasis No.', 'Chassis Number', 'VEHCHAHIS',
															   'Chasis_No', ]:
							chassis_col = col
						elif xl_sheet.cell(row, col).value in ['VEHICLE MAKE', 'Make Name', 'Make', 'Vehicle Name',
															   'Vehicle Make', 'Vehicle', 'CarMake', 'Car Make', 'MAKE',
															   'Make Desc']:
							vehmake_col = col
						elif xl_sheet.cell(row, col).value in ['VEHICLE MODEL', 'Model Name', 'Model', 'model',
															   'Car Model', 'Model Desc', ]:
							if not file in ['5 - ERA.xlsx', '5 - RAC.xlsx', '2.xls']: vehmodel_col = col
						elif xl_sheet.cell(row, col).value in ['MF YEAR', 'Year', 'Model Year', ]:
							if not file in ['9.xls', '2.xls']: mfyear_col = col
						elif xl_sheet.cell(row, col).value in ['Body Type', 'VEHICLE TYPE', 'Body Name', 'BODY TYPE', ]:
							vehtype_col = col
						elif xl_sheet.cell(row, col).value in ['Reg No', 'Reg. No', 'Registration No.', 'Reg. Number',
															   'Reg Not', 'CarPlate', 'Plate No.', 'MOTRPLAT', ]:
							plateno_col = col
						elif xl_sheet.cell(row, col).value in ['IMC membership  No.', 'IMC Membership No.',
															   'IMC Membership #', 'IMC Card No.', 'IMC Membership NO']:
							mem_no_col = str(col)
						else:
							pass

				for row in range(actual_row, xl_sheet.nrows):
					issue_date, policy_no, policy_from, policy_to, mobile, customer_name, vehicle_make, vehicle_model, vehicle_type, mfg_year, chassis_no, membership_no, car_plate = False, False, False, False, False, False, False, False, False, False, False, False, False

					vals = {}
					if policy_col: policy_no = xl_sheet.cell(row, int(policy_col)).value
					if from_col: policy_from = xl_sheet.cell(row, from_col).value
					if to_col: policy_to = xl_sheet.cell(row, to_col).value
					if mobile_col: mobile = xl_sheet.cell(row, mobile_col).value
					if cust_col:
						if type(xl_sheet.cell(row, int(cust_col)).value) == 'unicode':
							customer_name = xl_sheet.cell(row, int(cust_col)).encode('ascii', 'ignore')
						else:
							customer_name = xl_sheet.cell(row, int(cust_col)).value
					if vehmake_col: vehicle_make = xl_sheet.cell(row, vehmake_col).value
					if vehmodel_col: vehicle_model = xl_sheet.cell(row, vehmodel_col).value
					if vehtype_col: vehicle_type = xl_sheet.cell(row, vehtype_col).value
					if mfyear_col: mfg_year = xl_sheet.cell(row, mfyear_col).value
					if chassis_col: chassis_no = xl_sheet.cell(row, chassis_col).value
					if mem_no_col: membership_no = xl_sheet.cell(row, int(mem_no_col)).value
					if plateno_col: car_plate = xl_sheet.cell(row, plateno_col).value
					if issue_col: issue_date = xl_sheet.cell(row, issue_col).value

					customer, make_id, model_id, membership = False, False, False, False

					if mobile: vals.update({'contact_number': str(mobile)})
					if mfg_year: vals.update({'year': str(int(mfg_year))})
					if membership_no:
						if type(membership_no) == float: membership_no = str(membership_no).split('.')[0]
						vals.update({'membership_no': str(membership_no)})
					if chassis_no: vals.update({'chassis_no': chassis_no})
					if car_plate:
						if type(car_plate) == float: car_plate = str(car_plate).split('.')[0]
						vals.update({'car_plate': car_plate})
					if vehicle_type: vals.update({'veh_type': vehicle_type})

					if customer_name:
						customer = self.env['res.partner'].search([('name', '=', str(customer_name))]).ids
						if not customer:
							if not mobile:
								customer = self.env['res.partner'].create({'name': str(customer_name)}).id
							else:
								customer = self.env['res.partner'].create(
									{'name': str(customer_name), 'mobile': str(mobile)}).id
						else:
							customer = customer[0]
						vals.update({'end_user': customer})
					else:
						customer = False

					if vehicle_make:
						make = self.env['vehicles.master'].search([('name', '=', str(vehicle_make))]).ids
						if not make:
							make_id = self.env['vehicles.master'].create({'name': str(vehicle_make)}).id
						else:
							make_id = make[0]
						vals.update({'vehicle_make': make_id})

					if vehicle_model and make_id:
						model = self.env['vehicle.model'].search(
							[('make_id', '=', make_id), ('model', '=', str(vehicle_model))]).ids
						if not model:
							model_id = self.env['vehicle.model'].create(
								{'make_id': make_id, 'model': str(vehicle_model)}).id
						else:
							model_id = model[0]
						vals.update({'model': model_id})

					if issue_date:
						issued_date = (xlrd.xldate.xldate_as_datetime(issue_date, xl_workbook.datemode)).date()
						vals.update({'issued_date': issued_date})

					if policy_from:
						if type(policy_from) == str and file == '3.xlsx':
							policy_fromdate = datetime.strptime(policy_from, '%d-%b-%Y %H:%M:%S')
						elif type(policy_from) == str and file == '8.xls':
							policy_fromdate = datetime.strptime(policy_from, '%d-%m-%Y')
						else:
							if type(policy_from) == str:
								policy_fromdate = datetime.strptime(policy_from, '%d-%b-%Y')
							elif type(policy_from) == float:
								policy_fromdate = (
									xlrd.xldate.xldate_as_datetime(policy_from, xl_workbook.datemode)).date()
							else:
								policy_fromdate = False
						if policy_fromdate: vals.update({'start_date': policy_fromdate})

					if policy_to:
						if type(policy_to) == str and file == '3.xlsx':
							policy_todate = datetime.strptime(policy_to, '%d-%b-%Y %H:%M:%S')
						elif file == '8.xls':
							if policy_fromdate:  policy_todate = policy_fromdate + timedelta(
								days=int(policy_to.split(' ')[0]) * 360 / 12)
						else:
							if type(policy_to) == str:
								policy_todate = datetime.strptime(policy_to, '%d-%b-%Y')
							elif type(policy_to) == float:
								policy_todate = (xlrd.xldate.xldate_as_datetime(policy_to, xl_workbook.datemode)).date()
							else:
								policy_todate = False
						if policy_todate: vals.update({'expiry_date': policy_todate})

					if policy_no:
						if type(policy_no) == float: policy_no = str(policy_no).split('.')[0]
						vals.update({'type': 'insurance', 'insurance_number': str(policy_no)})
						if customer:
							membership = self.env['membership.menu'].search(
								[('end_user', '=', customer), ('insurance_number', '=', str(policy_no))]).ids
						else:
							membership = self.env['membership.menu'].search(
								[('insurance_number', '=', str(policy_no))]).ids

						if not membership:
							self.env['membership.menu'].create(vals)
						else:
							self.env['membership.menu'].browse(membership[0]).write(vals)


# @api.multi
# def get_multiple_excels_memberships_data(self):
#   BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#   excel_files = ['1.xlsx', '2.xls', '3.xlsx', '4A.xls', '4B.xlsx', '4C.xls', '4D.xlsx', '5 - RAC.xlsx', '5 - ERA.xlsx', '6.xls', '7.xlsx', '8.xls', '9.xls']
#   for file in excel_files:
#       xl_workbook = xlrd.open_workbook(BASE_DIR +'/excels/'+str(file))
#       sheet_names = xl_workbook.sheet_names()
#       for each in sheet_names:
#           xl_sheet = xl_workbook.sheet_by_name(each)
#           for row in range(1,xl_sheet.nrows):

#               if file == '4A.xls': row += 2
#               elif file =='4C.xls': row += 1
#               elif file =='4D.xlsx': row += 2
#               elif file == '5 - RAC.xlsx': row += 2
#               elif file == '5 - ERA.xlsx': row += 1
#               elif file == '6.xls': row += 10
#               elif file == '9.xls': row += 4
#               else:pass

#               vals = {}

#               if file == '1.xlsx':
#                   policy_no = xl_sheet.cell(row, 1).value
#                   policy_from = xl_sheet.cell(row, 3).value
#                   policy_to = xl_sheet.cell(row, 4).value
#                   mobile = xl_sheet.cell(row, 6).value
#                   customer_name = xl_sheet.cell(row, 5).value
#                   vehicle_make = xl_sheet.cell(row, 7).value
#                   vehicle_model = xl_sheet.cell(row, 8).value
#                   vehicle_type = xl_sheet.cell(row, 9).value
#                   mfg_year = xl_sheet.cell(row, 10).value
#                   chassis_no = xl_sheet.cell(row, 11).value
#                   membership_no = False
#                   car_plate = False

#               elif file == '2.xls':
#                   policy_no = xl_sheet.cell(row, 0).value
#                   policy_from = xl_sheet.cell(row, 3).value
#                   policy_to = xl_sheet.cell(row, 4).value
#                   mobile = False
#                   customer_name = xl_sheet.cell(row, 5).value
#                   vehicle_make = xl_sheet.cell(row, 6).value
#                   vehicle_model = xl_sheet.cell(row, 7).value
#                   vehicle_type = xl_sheet.cell(row, 8).value
#                   mfg_year = xl_sheet.cell(row, 9).value
#                   chassis_no = xl_sheet.cell(row, 11).value
#                   membership_no = False
#                   car_plate = xl_sheet.cell(row, 12).value

#               elif file == '3.xlsx':
#                   policy_no = xl_sheet.cell(row, 0).value
#                   policy_from = xl_sheet.cell(row, 2).value
#                   policy_to = xl_sheet.cell(row, 3).value
#                   mobile = False
#                   customer_name = False
#                   vehicle_make = xl_sheet.cell(row, 11).value
#                   vehicle_model = xl_sheet.cell(row, 12).value
#                   vehicle_type = xl_sheet.cell(row, 13).value
#                   mfg_year = False
#                   chassis_no = xl_sheet.cell(row, 5).value
#                   membership_no = xl_sheet.cell(row, 4).value
#                   car_plate = False

#               elif file == '4A.xls' and row < 31 :
#                   policy_no = xl_sheet.cell(row, 2).value
#                   policy_from = xl_sheet.cell(row, 3).value
#                   policy_to = xl_sheet.cell(row, 4).value
#                   mobile = False
#                   customer_name = xl_sheet.cell(row, 5).value
#                   vehicle_make = xl_sheet.cell(row, 7).value
#                   vehicle_model =False
#                   vehicle_type = False
#                   mfg_year = xl_sheet.cell(row, 10).value
#                   chassis_no = xl_sheet.cell(row, 9).value
#                   membership_no = xl_sheet.cell(row, 1).value
#                   car_plate = xl_sheet.cell(row, 8).value

#               elif file == '4B.xlsx':
#                   policy_no = xl_sheet.cell(row, 1).value
#                   policy_from = xl_sheet.cell(row, 2).value
#                   policy_to = xl_sheet.cell(row, 4).value
#                   mobile = False
#                   customer_name = xl_sheet.cell(row, 5).value
#                   vehicle_make = xl_sheet.cell(row, 7).value
#                   vehicle_model =False
#                   vehicle_type = False
#                   mfg_year = xl_sheet.cell(row, 9).value
#                   chassis_no = xl_sheet.cell(row, 8).value
#                   membership_no = xl_sheet.cell(row, 0).value
#                   car_plate = xl_sheet.cell(row, 10).value

#               elif file in ['4C.xls','4D.xlsx']:
#                   if (file == '4C.xls' and row<26) or (file=='4D.xlsx' and row<15):
#                       policy_no = xl_sheet.cell(row, 1).value
#                       policy_from = xl_sheet.cell(row, 2).value
#                       policy_to = xl_sheet.cell(row, 3).value
#                       mobile = False
#                       customer_name = xl_sheet.cell(row, 4).value
#                       vehicle_make = xl_sheet.cell(row, 5).value
#                       vehicle_model =False
#                       vehicle_type = False
#                       mfg_year = xl_sheet.cell(row, 8).value
#                       chassis_no = xl_sheet.cell(row, 7).value
#                       membership_no = xl_sheet.cell(row, 0).value
#                       car_plate = xl_sheet.cell(row, 6).value

#               elif file == '5 - RAC.xlsx' and row < 14 :
#                   policy_no = xl_sheet.cell(row, 1).value
#                   policy_from = xl_sheet.cell(row, 3).value
#                   policy_to = xl_sheet.cell(row, 4).value
#                   mobile = False
#                   customer_name = xl_sheet.cell(row, 5).value
#                   vehicle_make = xl_sheet.cell(row, 7).value
#                   vehicle_model =xl_sheet.cell(row, 8).value
#                   vehicle_type = False
#                   mfg_year = False
#                   chassis_no = xl_sheet.cell(row, 10).value
#                   membership_no = False
#                   car_plate = xl_sheet.cell(row, 9).value

#               elif file == '5 - ERA.xlsx' and row < 31:
#                   policy_no = xl_sheet.cell(row, 2).value
#                   policy_from = xl_sheet.cell(row, 4).value
#                   policy_to = xl_sheet.cell(row, 5).value
#                   mobile = False
#                   customer_name = xl_sheet.cell(row, 6).value
#                   vehicle_make = xl_sheet.cell(row, 8).value
#                   vehicle_model =xl_sheet.cell(row, 9).value
#                   vehicle_type = False
#                   mfg_year = False
#                   chassis_no = xl_sheet.cell(row, 11).value
#                   membership_no = xl_sheet.cell(row, 1).value
#                   car_plate = xl_sheet.cell(row, 10).value

#               elif file == '6.xls' and row<36:
#                   policy_no = xl_sheet.cell(row, 1).value
#                   policy_from = xl_sheet.cell(row, 2).value
#                   policy_to = xl_sheet.cell(row, 3).value
#                   mobile = False
#                   customer_name = xl_sheet.cell(row, 4).value
#                   vehicle_make = xl_sheet.cell(row, 5).value
#                   vehicle_model = False
#                   vehicle_type = False
#                   mfg_year = False
#                   chassis_no = xl_sheet.cell(row, 7).value
#                   membership_no = False
#                   car_plate = xl_sheet.cell(row, 6).value

#               elif file == '7.xlsx':
#                   policy_no = xl_sheet.cell(row, 1).value
#                   policy_from = xl_sheet.cell(row, 6).value
#                   policy_to = xl_sheet.cell(row, 7).value
#                   mobile = False
#                   customer_name = xl_sheet.cell(row, 8).value
#                   vehicle_make = xl_sheet.cell(row, 3).value
#                   vehicle_model = False
#                   vehicle_type = False
#                   mfg_year = False
#                   chassis_no = xl_sheet.cell(row, 4).value
#                   membership_no = xl_sheet.cell(row, 0).value
#                   car_plate = xl_sheet.cell(row, 5).value

#               elif file == '8.xls':
#                   policy_no = xl_sheet.cell(row, 1).value
#                   policy_from = xl_sheet.cell(row, 5).value
#                   policy_to = False
#                   mobile = False
#                   customer_name = xl_sheet.cell(row, 0).value
#                   vehicle_make = xl_sheet.cell(row, 2).value
#                   vehicle_model =  xl_sheet.cell(row, 3).value
#                   vehicle_type = False
#                   mfg_year =  xl_sheet.cell(row, 8).value
#                   chassis_no = xl_sheet.cell(row, 4).value
#                   membership_no = xl_sheet.cell(row, 0).value
#                   car_plate = xl_sheet.cell(row, 5).value

#               elif file == '9.xls' and row<25:
#                   policy_no = xl_sheet.cell(row, 7).value
#                   policy_from = xl_sheet.cell(row, 10).value
#                   policy_to = xl_sheet.cell(row, 11).value
#                   mobile = False
#                   customer_name = xl_sheet.cell(row, 8).value
#                   vehicle_make = xl_sheet.cell(row, 12).value
#                   vehicle_model =  xl_sheet.cell(row, 13).value
#                   vehicle_type =  xl_sheet.cell(row, 14).value
#                   mfg_year =  xl_sheet.cell(row, 15).value
#                   chassis_no = xl_sheet.cell(row, 17).value
#                   membership_no = False
#                   car_plate = xl_sheet.cell(row, 16).value

#               else:pass

#               if mobile: vals.update({'contact_number':str(mobile)})

#               if customer_name:
#                   customer = self.env['res.partner'].search([('name','=',str(customer_name))]).ids
#                   if not customer:
#                       if not mobile: customer = self.env['res.partner'].create({'name':str(customer_name)}).id
#                       else: customer = self.env['res.partner'].create({'name':str(customer_name),'mobile':str(mobile)}).id
#                   else: customer = customer[0]
#                   vals.update({'end_user':customer})
#               else: customer = False

#               if vehicle_make:
#                   make = self.env['vehicles.master'].search([('name','=',str(vehicle_make))]).ids
#                   if not make: make_id = self.env['vehicles.master'].create({'name':str(vehicle_make)}).id
#                   else: make_id = make[0]
#                   vals.update({'vehicle_make':make_id})

#               if vehicle_model and make_id:
#                   model = self.env['vehicle.model'].search([('make_id','=',make_id),('model','=',str(vehicle_model))]).ids
#                   if not model: model_id = self.env['vehicle.model'].create({'make_id':make_id, 'model':str(vehicle_model)}).id
#                   else: model_id = model[0]
#                   vals.update({'model':model_id})

#               if membership_no: vals.update({'membership_no':str(membership_no)})

#               if policy_no :
#                   vals.update({'type':'insurance','insurance_number':str(policy_no)})
#                   if customer: membership = self.env['membership.menu'].search([('end_user','=',customer),('insurance_number','=',str(policy_no))]).ids
#                   else: membership = self.env['membership.menu'].search([('insurance_number','=',str(policy_no))]).ids

#                   if not membership: self.env['membership.menu'].create(vals)
#                   else: self.env['membership.menu'].browse(membership[0]).write(vals)


Membership_Menu()


class Membership_era_avail_services(models.Model):

	@api.multi
	def _get_claimed_services(self):
		res = {}
		try:
			for service in self:
				services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id),
															   ('state', 'not in', ['requested', 'cancelled'])])
				if service.number_of_services - len(services) < 0:
					service_left = 0
				else:
					service_left = service.number_of_services - len(services)
				
				# booked_services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
				# 											   ('customer', '=', service.membership_id.end_user.id),
				# 											   ('service', '=', service.service_id.id)])
				service.update({
					'claimed_services': len(services),
					'avail_services': service_left,
					# 'booked_services' : len(booked_services),
				})
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"error": "Oops!! Something went wrong"}

	_name = 'era.avail.services'

	membership_id = fields.Many2one('membership.menu', 'Membership ID', ondelete='cascade')
	client_cat_id = fields.Many2one('membership.services', 'Client Memership Type')
	service_id = fields.Many2one('service.menu', 'Service', domain="[('request_type','=','era')]")
	# unlimited = fields.Boolean('Is Unlimited??')
	unlimited = fields.Selection(string="Is Unlimited??", selection=[('yes', 'Yes'), ('no', 'No')])
	number_of_services = fields.Integer('Total Services')
	services_claimed = fields.Integer('Claimed Services')
	claimed_services = fields.Integer(compute="_get_claimed_services", string="Claimed Services")
	avail_services = fields.Integer(compute="_get_claimed_services", string="Services left")
	request_type = fields.Selection(related='service_id.request_type', type="selection", selection=REQUEST_SELECTION,
									string="Request Type")
	type = fields.Selection(related='membership_id.type', string='Selection',
							selection=[('bank', 'Bank'), ('insurance', 'Insurance Company'), ('others', 'Others')])
	booked_services = fields.Integer(readonly=True, string="Booked Services")

	@api.multi
	def choose_vehicles(self):
		context = self._context.copy()
		vehicles = []
		for i in self.membership_id.vehicles:
			if i.primary == 'yes': vehicles.append(i.id)
		# if i.status == 'active': vehicles.append(i.id)
		if context is None:
			context = {}
		if not vehicles:
			raise UserError(_('No Vehicles added! Please add atleast 1 vehicle to proceed!!'))
		context.update({'m_vehicles': vehicles})

		return {
			'name': 'Choose Vehicle',
			'view_type': 'form',
			'view_mode': 'form',
			'target': 'new',
			# 'domain': "[('vehicles', 'in',%s)]" %(vehicles),
			'res_model': 'choose.vehicle',
			'type': 'ir.actions.act_window',
			'context': context,
		}

	@api.multi
	def claim_service(self):
		vals = {}
		rec = self.env['era.avail.services'].browse(self.id)

		if rec.service_id.request_type == 'hertz':

			if not rec.membership_id.chassis_no:
				raise UserError(_('Warning!'), _('Incomplete vehicle details!!'))

		if rec.membership_id.status != 'activate':
			raise UserError(_('You cannot claim this service as the membership not in Active!!'))

		membership_obj = self.env['era.avail.services'].browse(self.id).membership_id
		vals = {
			'customer': membership_obj.end_user.id,
			'service': rec.service_id.id,
			'membership_id': membership_obj.id,
			'insurance_number': membership_obj.insurance_number,
			'membership_type': membership_obj.membership_type.id,
			'membership_id': membership_obj.id,
			'type': membership_obj.type,
			'date_time': datetime.now(),
			'service_name': rec.service_id.name + rec.service_id.request_type,
			'vehicle_make': membership_obj.vehicle_make.id,
			'car_plate': membership_obj.car_plate}

		res_id = self.env['claimed.service'].create(vals)

		if rec.service_id.request_type == 'concierge':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'concierge.test.form.view')])[0]

		elif rec.service_id.request_type == 'era':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'era.test.form.view')])[0]

		elif rec.service_id.request_type == 'rent-a-car':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'rent.acar.test.form.view')])[0]

		elif rec.service_id.request_type == 'registration':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'car.regis.test.form.view')])[0]

		elif rec.service_id.request_type == 'hertz':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'hertz.test.form.view')])[0]

		elif rec.service_id.request_type == 'transportation':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'transportation.test.form.view')])[0]

		else:
			view_id = self.env['ir.ui.view'].search([('name', '=', 'claimed.service.form.view')])[0]

		return {
			'name': 'Claimed Services',
			'view_mode': 'form',
			'view_type': 'form',
			'view_id': view_id.id,
			'res_model': 'claimed.service',
			'res_id': res_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			# opening form in edit mode
			'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
		}


class Membership_hertz_avail_services(models.Model):

	@api.multi
	def _get_claimed_services(self):
		res = {}
		try:
			for service in self:
				services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id),
															   ('state', 'not in', ['requested', 'cancelled'])])
				if service.number_of_services - len(services) < 0:
					service_left = 0
				else:
					service_left = service.number_of_services - len(services)

				booked_services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id)])
				
				service.update({
					'claimed_services': len(services),
					'avail_services': service_left,
					# 'booked_services':len(booked_services),
				})
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"error": "Oops!! Something went wrong"}

	_name = 'hertz.avail.services'

	membership_id = fields.Many2one('membership.menu', 'Membership ID', ondelete='cascade')
	client_cat_id = fields.Many2one('membership.services', 'Client Memership Type')
	service_id = fields.Many2one('service.menu', 'Service', domain="[('request_type','=','hertz')]")
	# unlimited = fields.Boolean('Is Unlimited??')
	unlimited = fields.Selection(string="Is Unlimited??", selection=[('yes', 'Yes'), ('no', 'No')])
	number_of_services = fields.Integer('Total Services')
	services_claimed = fields.Integer('Claimed Services')
	claimed_services = fields.Integer(compute="_get_claimed_services", string="Claimed Services")
	avail_services = fields.Integer(compute="_get_claimed_services", string="Services left")
	request_type = fields.Selection(related='service_id.request_type', type="selection", selection=REQUEST_SELECTION,
									string="Request Type")
	type = fields.Selection(related='membership_id.type', string='Selection',
							selection=[('bank', 'Bank'), ('insurance', 'Insurance Company'), ('others', 'Others')])
	booked_services = fields.Integer(readonly=True, string="Booked Services")

	@api.multi
	def choose_vehicles(self):
		context = self._context.copy()
		vehicles = []
		for i in self.membership_id.vehicles:
			if i.primary == 'yes': vehicles.append(i.id)
		# if i.status == 'active':
		# vehicles.append(i.id)
		if context is None:
			context = {}
		if not vehicles:
			raise UserError(_('No Vehicles added! Please add atleast 1 vehicle to proceed!!'))
		context.update({'m_vehicles': vehicles})

		return {
			'name': 'Choose Vehicle',
			'view_type': 'form',
			'view_mode': 'form',
			'target': 'new',
			# 'domain': "[('vehicles', 'in',%s)]" %(vehicles),
			'res_model': 'choose.vehicle',
			'type': 'ir.actions.act_window',
			'context': context,
		}

	@api.multi
	def claim_service(self):
		vals = {}
		rec = self.env['hertz.avail.services'].browse(self.id)

		if rec.service_id.request_type == 'hertz':

			if not rec.membership_id.chassis_no:
				raise UserError(_('Warning!'), _('Incomplete vehicle details!!'))

		if rec.membership_id.status != 'activate':
			raise UserError(_('You cannot claim this service as the membership not in Active!!'))

		membership_obj = self.env['hertz.avail.services'].browse(self.id).membership_id
		vals = {
			'customer': membership_obj.end_user.id,
			'service': rec.service_id.id,
			'membership_id': membership_obj.id,
			'insurance_number': membership_obj.insurance_number,
			'membership_type': membership_obj.membership_type.id,
			'membership_id': membership_obj.id,
			'type': membership_obj.type,
			'date_time': datetime.now(),
			'service_name': rec.service_id.name + rec.service_id.request_type,
			'vehicle_make': membership_obj.vehicle_make.id,
			'car_plate': membership_obj.car_plate}

		res_id = self.env['claimed.service'].create(vals)

		if rec.service_id.request_type == 'concierge':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'concierge.test.form.view')])[0]

		elif rec.service_id.request_type == 'era':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'era.test.form.view')])[0]

		elif rec.service_id.request_type == 'rent-a-car':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'rent.acar.test.form.view')])[0]

		elif rec.service_id.request_type == 'registration':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'car.regis.test.form.view')])[0]

		elif rec.service_id.request_type == 'hertz':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'hertz.test.form.view')])[0]

		elif rec.service_id.request_type == 'transportation':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'transportation.test.form.view')])[0]

		else:
			view_id = self.env['ir.ui.view'].search([('name', '=', 'claimed.service.form.view')])[0]

		return {
			'name': 'Claimed Services',
			'view_mode': 'form',
			'view_type': 'form',
			'view_id': view_id.id,
			'res_model': 'claimed.service',
			'res_id': res_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			# opening form in edit mode
			'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
		}


class Concierge_avail_services(models.Model):

	@api.multi
	def _get_claimed_services(self):
		res = {}
		try:
			for service in self:
				services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id),
															   ('state', 'not in', ['requested', 'cancelled'])])
				if service.number_of_services - len(services) < 0:
					service_left = 0
				else:
					service_left = service.number_of_services - len(services)

				booked_services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id)])
				
				service.update({
					'claimed_services': len(services),
					'avail_services': service_left,
					# 'booked_services':len(booked_services),
				})
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"error": "Oops!! Something went wrong"}

	_name = 'concierge.avail.services'

	membership_id = fields.Many2one('membership.menu', 'Membership ID', ondelete='cascade')
	client_cat_id = fields.Many2one('membership.services', 'Client Memership Type')
	service_id = fields.Many2one('service.menu', 'Service', domain="[('request_type','=','concierge')]")
	# unlimited = fields.Boolean('Is Unlimited??')
	unlimited = fields.Selection(string="Is Unlimited??", selection=[('yes', 'Yes'), ('no', 'No')])
	number_of_services = fields.Integer('Total Services')
	services_claimed = fields.Integer('Claimed Services')
	claimed_services = fields.Integer(compute="_get_claimed_services", string="Claimed Services")
	avail_services = fields.Integer(compute="_get_claimed_services", string="Services left")
	request_type = fields.Selection(related='service_id.request_type', type="selection", selection=REQUEST_SELECTION,
									string="Request Type")
	type = fields.Selection(related='membership_id.type', string='Selection',
							selection=[('bank', 'Bank'), ('insurance', 'Insurance Company'), ('others', 'Others')])
	booked_services = fields.Integer(readonly=True, string="Booked Services")
	
	@api.model
	def _check_concierge_service(self):
		for val in self.read(['service_id']):
			if val['service_id']:
				if len(val['service_id']) > 1:
					return False
		return True

	@api.multi
	def choose_vehicles(self):
		context = self._context.copy()
		vehicles = []
		for i in self.membership_id.vehicles:
			if i.primary == 'yes': vehicles.append(i.id)
		# if i.status == 'active':
		# vehicles.append(i.id)
		if context is None:
			context = {}
		if not vehicles:
			raise UserError(_('No Vehicles added! Please add atleast 1 vehicle to proceed!!'))
		context.update({'m_vehicles': vehicles})

		return {
			'name': 'Choose Vehicle',
			'view_type': 'form',
			'view_mode': 'form',
			'target': 'new',
			# 'domain': "[('vehicles', 'in',%s)]" %(vehicles),
			'res_model': 'choose.vehicle',
			'type': 'ir.actions.act_window',
			'context': context,
		}

	@api.multi
	def claim_service(self):
		vals = {}
		rec = self.env['concierge.avail.services'].browse(self.id)

		if rec.service_id.request_type == 'hertz':

			if not rec.membership_id.chassis_no:
				raise UserError(_('Warning!'), _('Incomplete vehicle details!!'))

		if rec.membership_id.status != 'activate':
			raise UserError(_('You cannot claim this service as the membership not in Active!!'))

		membership_obj = self.env['concierge.avail.services'].browse(self.id).membership_id
		vals = {
			'customer': membership_obj.end_user.id,
			'service': rec.service_id.id,
			'membership_id': membership_obj.id,
			'insurance_number': membership_obj.insurance_number,
			'membership_type': membership_obj.membership_type.id,
			'membership_id': membership_obj.id,
			'type': membership_obj.type,
			'date_time': datetime.now(),
			'service_name': rec.service_id.name + rec.service_id.request_type,
			'vehicle_make': membership_obj.vehicle_make.id,
			'car_plate': membership_obj.car_plate}

		res_id = self.env['claimed.service'].create(vals)

		if rec.service_id.request_type == 'concierge':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'concierge.test.form.view')])[0]

		elif rec.service_id.request_type == 'era':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'era.test.form.view')])[0]

		elif rec.service_id.request_type == 'rent-a-car':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'rent.acar.test.form.view')])[0]

		elif rec.service_id.request_type == 'registration':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'car.regis.test.form.view')])[0]

		elif rec.service_id.request_type == 'hertz':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'hertz.test.form.view')])[0]

		elif rec.service_id.request_type == 'transportation':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'transportation.test.form.view')])[0]

		else:
			view_id = self.env['ir.ui.view'].search([('name', '=', 'claimed.service.form.view')])[0]

		return {
			'name': 'Claimed Services',
			'view_mode': 'form',
			'view_type': 'form',
			'view_id': view_id.id,
			'res_model': 'claimed.service',
			'res_id': res_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			# opening form in edit mode
			'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
		}


class Rentacar_avail_services(models.Model):

	@api.multi
	def _get_claimed_services(self):
		res = {}
		try:
			for service in self:
				services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id),
															   ('state', 'not in', ['requested', 'cancelled'])])
				if service.number_of_services - len(services) < 0:
					service_left = 0
				else:
					service_left = service.number_of_services - len(services)

				booked_services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id)])
				
				service.update({
					'claimed_services': len(services),
					'avail_services': service_left,
					# 'booked_services':len(booked_services),
				})

		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"error": "Oops!! Something went wrong"}

	_name = 'rentacar.avail.services'

	membership_id = fields.Many2one('membership.menu', 'Membership ID', ondelete='cascade')
	client_cat_id = fields.Many2one('membership.services', 'Client Memership Type')
	service_id = fields.Many2one('service.menu', 'Service', domain="[('request_type','=','rent-a-car')]")
	# unlimited = fields.Boolean('Is Unlimited??')
	unlimited = fields.Selection(string="Is Unlimited??", selection=[('yes', 'Yes'), ('no', 'No')])
	number_of_services = fields.Integer('Total Services')
	services_claimed = fields.Integer('Claimed Services')
	claimed_services = fields.Integer(compute="_get_claimed_services", string="Claimed Services")
	avail_services = fields.Integer(compute="_get_claimed_services", string="Services left")
	request_type = fields.Selection(related='service_id.request_type', type="selection", selection=REQUEST_SELECTION,
									string="Request Type")
	type = fields.Selection(related='membership_id.type', string='Selection',
							selection=[('bank', 'Bank'), ('insurance', 'Insurance Company'), ('others', 'Others')])
	booked_services = fields.Integer(readonly=True,string="Booked Services")

	@api.multi
	def choose_vehicles(self):
		context = self._context.copy()
		vehicles = []
		for i in self.membership_id.vehicles:
			if i.primary == 'yes': vehicles.append(i.id)
		# if i.status == 'active':
		# vehicles.append(i.id)
		if context is None:
			context = {}
		if not vehicles:
			raise UserError(_('No Vehicles added! Please add atleast 1 vehicle to proceed!!'))
		context.update({'m_vehicles': vehicles})

		return {
			'name': 'Choose Vehicle',
			'view_type': 'form',
			'view_mode': 'form',
			'target': 'new',
			# 'domain': "[('vehicles', 'in',%s)]" %(vehicles),
			'res_model': 'choose.vehicle',
			'type': 'ir.actions.act_window',
			'context': context,
		}

	@api.multi
	def claim_service(self):
		vals = {}
		rec = self.env['rentacar.avail.services'].browse(self.id)

		if rec.service_id.request_type == 'hertz':

			if not rec.membership_id.chassis_no:
				raise UserError(_('Warning!'), _('Incomplete vehicle details!!'))

		if rec.membership_id.status != 'activate':
			raise UserError(_('You cannot claim this service as the membership not in Active!!'))

		membership_obj = self.env['rentacar.avail.services'].browse(self.id).membership_id
		vals = {
			'customer': membership_obj.end_user.id,
			'service': rec.service_id.id,
			'membership_id': membership_obj.id,
			'insurance_number': membership_obj.insurance_number,
			'membership_type': membership_obj.membership_type.id,
			'membership_id': membership_obj.id,
			'type': membership_obj.type,
			'date_time': datetime.now(),
			'service_name': rec.service_id.name + rec.service_id.request_type,
			'vehicle_make': membership_obj.vehicle_make.id,
			'car_plate': membership_obj.car_plate}

		res_id = self.env['claimed.service'].create(vals)

		if rec.service_id.request_type == 'concierge':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'concierge.test.form.view')])[0]

		elif rec.service_id.request_type == 'era':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'era.test.form.view')])[0]

		elif rec.service_id.request_type == 'rent-a-car':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'rent.acar.test.form.view')])[0]

		elif rec.service_id.request_type == 'registration':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'car.regis.test.form.view')])[0]

		elif rec.service_id.request_type == 'hertz':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'hertz.test.form.view')])[0]

		elif rec.service_id.request_type == 'transportation':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'transportation.test.form.view')])[0]

		else:
			view_id = self.env['ir.ui.view'].search([('name', '=', 'claimed.service.form.view')])[0]

		return {
			'name': 'Claimed Services',
			'view_mode': 'form',
			'view_type': 'form',
			'view_id': view_id.id,
			'res_model': 'claimed.service',
			'res_id': res_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			# opening form in edit mode
			'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
		}


class Carreg_avail_services(models.Model):

	@api.multi
	def _get_claimed_services(self):
		res = {}
		try:
			for service in self:
				services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id),
															   ('state', 'not in', ['requested', 'cancelled'])])
				if service.number_of_services - len(services) < 0:
					service_left = 0
				else:
					service_left = service.number_of_services - len(services)

				booked_services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id)])
				
				service.update({
					'claimed_services': len(services),
					'avail_services': service_left,
					# 'booked_services':len(booked_services),
				})
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"error": "Oops!! Something went wrong"}

	_name = 'carreg.avail.services'

	membership_id = fields.Many2one('membership.menu', 'Membership ID', ondelete='cascade')
	client_cat_id = fields.Many2one('membership.services', 'Client Memership Type')
	service_id = fields.Many2one('service.menu', 'Service', domain="[('request_type','=','registration')]")
	# unlimited = fields.Boolean('Is Unlimited??')
	unlimited = fields.Selection(string="Is Unlimited??", selection=[('yes', 'Yes'), ('no', 'No')])
	number_of_services = fields.Integer('Total Services')
	services_claimed = fields.Integer('Claimed Services')
	claimed_services = fields.Integer(compute="_get_claimed_services", string="Claimed Services")
	avail_services = fields.Integer(compute="_get_claimed_services", string="Services left")
	request_type = fields.Selection(related='service_id.request_type', type="selection", selection=REQUEST_SELECTION,
									string="Request Type")
	type = fields.Selection(related='membership_id.type', string='Selection',
							selection=[('bank', 'Bank'), ('insurance', 'Insurance Company'), ('others', 'Others')])
	booked_services = fields.Integer(readonly=True,string="Booked Services")

	@api.multi
	def choose_vehicles(self):
		context = self._context.copy()
		vehicles = []
		for i in self.membership_id.vehicles:
			if i.primary == 'yes': vehicles.append(i.id)
		# if i.status == 'active':
		# vehicles.append(i.id)
		if context is None:
			context = {}
		if not vehicles:
			raise UserError(_('No Vehicles added! Please add atleast 1 vehicle to proceed!!'))
		context.update({'m_vehicles': vehicles})

		return {
			'name': 'Choose Vehicle',
			'view_type': 'form',
			'view_mode': 'form',
			'target': 'new',
			# 'domain': "[('vehicles', 'in',%s)]" %(vehicles),
			'res_model': 'choose.vehicle',
			'type': 'ir.actions.act_window',
			'context': context,
		}

	@api.multi
	def claim_service(self):
		vals = {}
		rec = self.env['carreg.avail.services'].browse(self.id)

		if rec.service_id.request_type == 'hertz':

			if not rec.membership_id.chassis_no:
				raise UserError(_('Warning!'), _('Incomplete vehicle details!!'))

		if rec.membership_id.status != 'activate':
			raise UserError(_('You cannot claim this service as the membership not in Active!!'))

		membership_obj = self.env['carreg.avail.services'].browse(self.id).membership_id
		vals = {
			'customer': membership_obj.end_user.id,
			'service': rec.service_id.id,
			'membership_id': membership_obj.id,
			'insurance_number': membership_obj.insurance_number,
			'membership_type': membership_obj.membership_type.id,
			'membership_id': membership_obj.id,
			'type': membership_obj.type,
			'date_time': datetime.now(),
			'service_name': rec.service_id.name + rec.service_id.request_type,
			'vehicle_make': membership_obj.vehicle_make.id,
			'car_plate': membership_obj.car_plate}

		res_id = self.env['claimed.service'].create(vals)

		if rec.service_id.request_type == 'concierge':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'concierge.test.form.view')])[0]

		elif rec.service_id.request_type == 'era':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'era.test.form.view')])[0]

		elif rec.service_id.request_type == 'rent-a-car':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'rent.acar.test.form.view')])[0]

		elif rec.service_id.request_type == 'registration':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'car.regis.test.form.view')])[0]

		elif rec.service_id.request_type == 'hertz':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'hertz.test.form.view')])[0]

		elif rec.service_id.request_type == 'transportation':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'transportation.test.form.view')])[0]

		else:
			view_id = self.env['ir.ui.view'].search([('name', '=', 'claimed.service.form.view')])[0]

		return {
			'name': 'Claimed Services',
			'view_mode': 'form',
			'view_type': 'form',
			'view_id': view_id.id,
			'res_model': 'claimed.service',
			'res_id': res_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			# opening form in edit mode
			'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
		}


class Transportation_avail_services(models.Model):

	@api.multi
	def _get_claimed_services(self):
		res = {}
		try:
			for service in self:
				services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id),
															   ('state', 'not in', ['requested', 'cancelled'])])
				if service.number_of_services - len(services) < 0:
					service_left = 0
				else:
					service_left = service.number_of_services - len(services)

				booked_services = self.env['claimed.service'].search([('membership_id', '=', service.membership_id.id),
															   ('customer', '=', service.membership_id.end_user.id),
															   ('service', '=', service.service_id.id)])
				service.update({
					'claimed_services': len(services),
					'avail_services': service_left,
					# 'booked_services':len(booked_services),
				})
		except Exception as e:
			logger.exception("Error: %s;" % e)
			return {"error": "Oops!! Something went wrong"}

	_name = 'transport.avail.services'

	membership_id = fields.Many2one('membership.menu', 'Membership ID', ondelete='cascade')
	client_cat_id = fields.Many2one('membership.services', 'Client Memership Type')
	service_id = fields.Many2one('service.menu', 'Service', domain="[('request_type','=','transportation')]")
	# unlimited = fields.Boolean('Is Unlimited??')
	unlimited = fields.Selection(string="Is Unlimited??", selection=[('yes', 'Yes'), ('no', 'No')])
	number_of_services = fields.Integer('Total Services')
	services_claimed = fields.Integer('Claimed Services')
	claimed_services = fields.Integer(compute="_get_claimed_services", string="Claimed Services")
	avail_services = fields.Integer(compute="_get_claimed_services", string="Services left")
	request_type = fields.Selection(related='service_id.request_type', type="selection", selection=REQUEST_SELECTION,
									string="Request Type")
	type = fields.Selection(related='membership_id.type', string='Selection',
							selection=[('bank', 'Bank'), ('insurance', 'Insurance Company'), ('others', 'Others')])
	booked_services = fields.Integer(readonly=True, string="Booked Services")

	@api.multi
	def choose_vehicles(self):
		context = self._context.copy()
		vehicles = []
		for i in self.membership_id.vehicles:
			if i.primary == 'yes': vehicles.append(i.id)
		# if i.status == 'active':
		# vehicles.append(i.id)
		if context is None:
			context = {}
		if not vehicles:
			raise UserError(_('No Vehicles added! Please add atleast 1 vehicle to proceed!!'))
		context.update({'m_vehicles': vehicles})

		return {
			'name': 'Choose Vehicle',
			'view_type': 'form',
			'view_mode': 'form',
			'target': 'new',
			# 'domain': "[('vehicles', 'in',%s)]" %(vehicles),
			'res_model': 'choose.vehicle',
			'type': 'ir.actions.act_window',
			'context': context,
		}

	@api.multi
	def claim_service(self):
		vals = {}
		rec = self.env['transport.avail.services'].browse(self.id)

		if rec.service_id.request_type == 'hertz':

			if not rec.membership_id.chassis_no:
				raise UserError(_('Warning!'), _('Incomplete vehicle details!!'))

		if rec.membership_id.status != 'activate':
			raise UserError(_('You cannot claim this service as the membership not in Active!!'))

		membership_obj = self.env['transport.avail.services'].browse(self.id).membership_id
		vals = {
			'customer': membership_obj.end_user.id,
			'service': rec.service_id.id,
			'membership_id': membership_obj.id,
			'insurance_number': membership_obj.insurance_number,
			'membership_type': membership_obj.membership_type.id,
			'type': membership_obj.type,
			'date_time': datetime.now(),
			'service_name': rec.service_id.name + rec.service_id.request_type,
			'vehicle_make': membership_obj.vehicle_make.id,
			'car_plate': membership_obj.car_plate}

		res_id = self.env['claimed.service'].create(vals)

		if rec.service_id.request_type == 'concierge':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'concierge.test.form.view')])[0]

		elif rec.service_id.request_type == 'era':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'era.test.form.view')])[0]

		elif rec.service_id.request_type == 'rent-a-car':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'rent.acar.test.form.view')])[0]

		elif rec.service_id.request_type == 'registration':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'car.regis.test.form.view')])[0]

		elif rec.service_id.request_type == 'hertz':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'hertz.test.form.view')])[0]

		elif rec.service_id.request_type == 'transportation':
			view_id = self.env['ir.ui.view'].search([('name', '=', 'transportation.test.form.view')])[0]

		else:
			view_id = self.env['ir.ui.view'].search([('name', '=', 'claimed.service.form.view')])[0]

		return {
			'name': 'Claimed Services',
			'view_mode': 'form',
			'view_type': 'form',
			'view_id': view_id.id,
			'res_model': 'claimed.service',
			'res_id': res_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			# opening form in edit mode
			'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
		}


class MembershipVehicles(models.Model):
	_name = 'membership.vehicle.info'

	_rec_name = 'chassis_no'

	membership_id = fields.Many2one('membership.menu', 'Membership ID', ondelete='cascade')
	client_cat_id = fields.Many2one('membership.services', 'Client Memership Type')
	make_id = fields.Many2one('vehicles.master', 'Make')
	mfg_year = fields.Selection([(str(num), str(num)) for num in range(1900, (datetime.now().year) + 1)],
								'Mfg Year')
	car_plate = fields.Char('Car Plate')
	model_id = fields.Many2one('vehicle.model', 'Model')
	chassis_no = fields.Char('Chassis No.')
	veh_type = fields.Char('Vehicle Type')
	front_rc = fields.Binary('Front Side of RC')
	back_rc = fields.Binary('Back Side of RC')
	policy_no = fields.Char('Policy No.')
	status = fields.Selection([('active', 'Active'),
							   ('inactive', 'Inactive')], 'Status', default='active')

	# primary = fields.Selection([('yes','Yes'),
	#                           ('no','No')],'Primary')
	@api.multi
	def name_get(self):
		res = []
		for record in self:
			name = record.chassis_no
			name = "%s ,%s, %s" % (name, str(record.model_id.model), str(record.make_id.name))
			res.append((record.id, name))
		return super(MembershipVehicles, self).name_get()

	@api.onchange('status')
	def onchange_vehicle_status(self):

		if self.status:
			if 'params' in self._context:
				membership_id = self._context['params']['id']
				if self.env['membership.vehicle.info'].search([('membership_id', '=', membership_id)]).ids:
					raise Warning(_('Only one vehicle is allowed to be in Active state!!'))


class Model_number(models.Model):
	_name = 'vehicle.model'
	_rec_name = 'model'

	make_id = fields.Many2one('vehicles.master', 'Make')
	model = fields.Char('Model')


class MembershipHistory(models.Model):
	_name = 'membership.history'

	_rec_name = 'membership_id'

	membership_id = fields.Many2one('membership.menu', 'Membership ID', ondelete='cascade')
	start_date = fields.Date('Start Date')
	expiry_date = fields.Date('Expiry Date')

class MembershipSignatories(models.Model):
	_name = 'membership.signatories'

	_rec_name = 'membership_id'

	membership_id = fields.Many2one('membership.menu', 'Membership ID', ondelete='cascade')
	name = fields.Char('Signatory Name')

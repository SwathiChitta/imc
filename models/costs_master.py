from odoo import api, fields, models

class Costs_Master(models.Model):

	_name ='costs.master'

	_rec_name = 'city'

	city = fields.Many2one('res.country.state','City')
	cost = fields.Float('Cost')
	
Costs_Master()
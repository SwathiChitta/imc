from odoo import api, fields, models
import json

class Insurance_Menu(models.Model):
	_inherit = 'res.partner'

	customer_type = fields.Selection([('bank', 'Bank'),
									  ('insurance', 'Insurance Company'),
									  ('others', 'Others'), ], "Customer Type")
	is_bank = fields.Boolean('is_bank')
	is_a_insurance = fields.Boolean('Is a Insurance Company')
	other = fields.Boolean('other')
	
Insurance_Menu()


# class Membership_type(models.Model):
# 	_name = "membership.type"

# 	name = fields.Char('Name')
# 	service_ids = fields.One2many(
# 		'membership.services', 'membership_id', 'Services')
# 	rel_ids = fields.Many2one('imc.clients', 'Company')
# 	service_cat_id = fields.Many2one('service.type', 'Membership Category')
# 	max_service_count = fields.Selection([('yes', 'Yes'),
# 										  ('no', 'No')], "Max Service Count")
# 	max_count = fields.Integer("Max Service Count")
# 	bin_ranges = fields.Many2many('bin.range.numbers', id="type_id", id2="number_id",
# 								  string="Bin/Card Range Numbers")

# Membership_type()

class company_contacts(models.Model):

	_name = 'company.contact.persons'

	company_id = fields.Many2one('imc.clients','Company')
	client_company_id =fields.Many2one('imc.clients','Company')
	name = fields.Char('Name')
	mobile = fields.Char('Mobile')
	phone = fields.Char('Phone')
	email = fields.Char('Email')
	image = fields.Binary('Image')
	attachment = fields.Binary('Attachment')
	filename = fields.Char('Filename')
from odoo import api, fields, models, _
import datetime
from datetime import datetime
import time
from datetime import datetime, timedelta, date
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
import requests
import urllib
import json
from . import HTML
import re
import logging

logger = logging.getLogger(__name__)
from O365 import Message, Attachment
o365_auth = ('ops@motoringclub.com', 'ITPLAZA@906')
m = Message(auth=o365_auth)
import webbrowser
import base64
import PyPDF2
import os

try:
	import urllib2 as urlreq  # Python 2.x
except:
	import urllib.request as urlreq  # Python 3.x
from os.path import expanduser
import shutil

# try:
# 	from xmlrpc import client as xmlrpclib
# except ImportError:
# 	import xmlrpclib


TYPE_SELECTION = [('bank', 'Bank'),
				  ('insurance', 'Insurance Company'),
				  ('others', 'Others')]

REQUEST_SELECTION = [('concierge', 'Concierge'),
					 ('era', 'ERA'),
					 ('rent-a-car', 'Rent a Car'),
					 ('registration', 'Car Registration'),
					 ('transportation', 'Transportation'),
					 ('hertz', 'Hertz'),
					 ]

FEEDBACK_SELECTION = [('responses', 'Responses'),
					  ('very-satisfied', 'Very Satisfied'),
					  ('somewhat-satisfied', 'Somewhat Satisfied'),
					  ('unhappy', 'Unhappy'),
					  ]

STATE_SELECTION = [
	('requested', 'Service Taken'),
	('documents-received', 'Documents Received'),
	('verified-rac-eligibility', 'Verified RAC Eligibility'),
	('dispatched', 'Dispatched'),
	('accepted', 'Driver/Vendor Accepted'),
	('rejected', 'Driver/Vendor Rejected'),
	('offhire-requested', 'Off Hire Requested'),
	('ongoing', 'On the Way to Customer'),
	('reaching', 'Reached Customer/Pickup Location'),
	('car-loaded', 'Car Loaded'),
	('leaving', 'Left Customer Location'),
	('reached-dropoff', 'Reached Drop off Location'),
	('car-off-loaded', 'Car Off Loaded'),
	('reached-rta', 'Reached RTA center'),
	('left-rta', 'Left RTA center'),
	('dropped-car', 'Dropped Car to Customer Location'),
	('feedback', 'Service Closed'),
	('service-provided', 'Service Provided'),
	('completed', 'Pending-Feedback'),
	('cancelled', 'Cancelled')]


def reverse_geocode(latlng):
	result = {}
	url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng={}&key=AIzaSyDH1IaPXc1kzxRTVu-ZNp3hAzd52RiAi-E'
	request = url.format(latlng)
	data = requests.get(request).json()
	if len(data['results']) > 0:
		result = data['results'][0]
	return result


def parse_country(geocode_data):
	if geocode_data:
		# for i in geocode_data:
		for component in geocode_data['address_components']:
			if 'country' in component['types']:
				return component['long_name']
	return None


def parse_area(geocode_data):
	area = ''
	if (geocode_data):
		for component in geocode_data['address_components']:
			if 'sublocality_level_1' in component['types']:
				area += component['long_name'] + ', '

			if 'sublocality_level_2' in component['types']:
				area += component['long_name'] + ', '

			if 'route' in component['types']:
				area += component['long_name'] + ', '
	if area:
		area = area.rstrip(', ')
	return area


def parse_city(geocode_data):
	if geocode_data:
		# for i in geocode_data:
		for component in geocode_data['address_components']:
			if 'locality' in component['types']:
				return component['long_name']
			elif 'postal_town' in component['types']:
				return component['long_name']
			elif 'administrative_area_level_1' in component['types']:
				return component['long_name']
			elif 'administrative_area_level_2' in component['types']:
				return component['long_name']

	return None


class claimed_service(models.Model):
	_name = "claimed.service"

	_inherit = ['mail.thread']

	_order = "id desc"

	_description = 'Service Request'

	@api.multi
	def _get_status(self):
		res = {}
		for obj in self:
			obj.update({

				'era_other': str(obj.state),
				'conceirge_airport': str(obj.state),
				'conceirge_chauffeur': str(obj.state),
				'rent_owndamage': str(obj.state),
				'registration_renewal': str(obj.state),
				'hertz_towing': str(obj.state),
				'transportation_towing': str(obj.state),

			})

	@api.depends('updated_time')
	def _get_status_color(self):
		res = {}
		for obj in self:

			if obj.state in ['cancelled','rejected']:
				self.update({'red_color':True,'green_color':False, 'white_color':False})
			elif obj.state in ['dispatched','reaching','accepted','rejected','ongoing','reaching','leaving','reached-rta','left-rta','dropped-car','completed','feedback','cancelled']:
				self.update({'red_color':False,'green_color':True, 'white_color':False})
			else:
				self.update({'red_color':False,'green_color':False, 'white_color':True})

	@api.multi
	def _get_kms_values(self):
		for obj in self:
			if obj.reached_km and obj.starting_km: 
				obj.update({'actual_km':obj.reached_km - obj.starting_km})
			if obj.ending_km and obj.starting_km:
				obj.update({'km_taken':obj.ending_km - obj.starting_km})
			if obj.starting_km_time and obj.reached_km_time :
				result =datetime.strptime(obj.reached_km_time,'%Y-%m-%d %H:%M:%S') - datetime.strptime(obj.starting_km_time,'%Y-%m-%d %H:%M:%S')
				obj.update({'actual_time':str(int(result.seconds/3600)) + ':' + str(str(result.seconds/60)[:2])})
			if obj.starting_km_time and obj.ending_km_time :
				result2 =datetime.strptime(obj.ending_km_time,'%Y-%m-%d %H:%M:%S') - datetime.strptime(obj.starting_km_time,'%Y-%m-%d %H:%M:%S')
				obj.update({'time_taken':str(int(result2.seconds/3600)) + ':' + str(str(result2.seconds/60)[:2])})
	
	@api.multi
	def _get_claim_list(self):
		for rec in self:
			rec.update({
				'claim_open_today': self.search_count(
					[('state', '=', 'requested'), ('scheduled_date', '<', time.strftime("%Y-%m-%d %H:%M:%S"))]),
				'services_taken_today': self.search_count(
					[('state', '=', 'completed'), ('scheduled_date', '<', time.strftime("%Y-%m-%d %H:%M:%S"))]),
				'claimed_scheduled_today': self.search_count(
					[('state', '=', 'ondway'), ('scheduled_date', '<', time.strftime("%Y-%m-%d %H:%M:%S"))]),
				'claim_completed_today': self.search_count(
					[('state', '=', 'completed'), ('scheduled_date', '<', time.strftime("%Y-%m-%d %H:%M:%S"))]),
				'ongoing_claim': self.search_count(
					[('state', '=', 'ongoing'), ('scheduled_date', '<', time.strftime("%Y-%m-%d %H:%M:%S"))]),
			})

	@api.multi
	def _get_locations_data(self):
		location = ''
		for rec in self:

			if rec.service_name in ['Registration Renewalregistration', 'Emergency Home Maintenance Serviceconcierge',
									'Gift Deliveryconcierge', 'Pull Outera', 'Battery Boostingera',
									'Off Road Recoveryera', 'Lockout Serviceera', 'Jump Startera', 'Fuel Supplyera',
									'Flat Tyreera']:
				location = str(rec.customer_state_id.name) + ', ' + str(
					rec.customer_location_add)
				rec.update({'pickup_locations': location})

			elif rec.service_name in ['Intercity Towingtransportation', 'City Limit Towingtransportation',
									  'Key Collectiontransportation', 'Key Deliverytransportation',
									  'Citylimit Towingera', 'Intercity Towingera']:
				pickup_location = str(rec.state_from_id.name) + ', ' + str(
					rec.location_from_add)
				drop_off = str(rec.state_to_id.name) + ', ' + str(
					rec.location_to_add)
				rec.update({'pickup_locations': pickup_location, 'dropoff_locations': drop_off})

			elif rec.service_name in ['Chauffeur Serviceconcierge', 'Document Delivery Serviceconcierge',
									  'Car Service (Automobile)concierge']:
				pickup_location = str(rec.state_from_id.name) + ', ' + str(
					rec.location_from_add)
				drop_off = str(rec.state_to_id.name) + ', ' + str(
					rec.location_to_add)
				rec.update({'pickup_locations': location, 'dropoff_locations': drop_off})

			elif rec.service_name in ['Third Partyrent-a-car', 'Car Replacementrent-a-car']:
				pickup_location = str(rec.state_from_id.name) + ', ' + str(
					rec.location_from_add)
				rec.update({'pickup_locations': pickup_location})

			elif rec.service_name in ['Airport Pick up and Dropconcierge', 'Meet and Greet Assistanceconcierge']:
				if rec.pickup_drop == 'pickup':
					pickup_location = str(rec.customer_state_id.name) + ', ' + str(rec.from_airport) + ', ' + str(
						rec.terminal)
					drop_off = str(rec.state_to_id.name) + ', ' + str(
						rec.location_to_add)
					rec.update({'pickup_locations': pickup_location, 'dropoff_locations': drop_off})
				elif rec.pickup_drop == 'drop':
					pickup_location = str(rec.state_from_id.name) + ', ' + str(
						rec.location_from_add)
					dropoff = str(rec.to_airport) + ', ' + str(rec.to_customer_state_id.name) + ', ' + str(
						rec.to_terminal)
					rec.update({'pickup_locations': pickup_location, 'dropoff_locations': dropoff})
				else:
					pass
				rec.update({'pickup_locations': location})

	@api.depends('customer_state_id')
	def _customer_states_in_uae_test(self):

		for rec in self:
			for each in rec.ids:
				flag = False
				record = self.browse(each)

				if record.customer_state_id and record.service_name in ['Battery Boostingera', 'Jump Startera']:

					if record.customer_state_id.code in ['AUH', 'DXB', 'SHJ'] and record.service_name == 'Jump Startera':
						flag = True
					elif record.customer_state_id.code not in ['AUH', 'DXB', 'SHJ'] and record.service_name == 'Jump Startera':
						flag = False
					elif record.service_name == 'Battery Boostingera':
						flag = True
					else:
						pass

				rec.update({'states_in_uae': flag})


	@api.depends('updated_time')
	def _get_ready_to_dispatch_value(self):
		for rec in self:
			for each in rec.ids:
				flag = False

				record = self.browse(each)
				request_type, service = record.request_type, record.service_name

				if request_type == 'era':
					if service == 'Citylimit Towingera':
						if record.country_from_id and record.state_from_id  and record.car_in_basement: flag = True

					elif service == 'Intercity Towingera':
						if record.country_from_id and record.state_from_id and record.country_to_id and record.car_in_basement: flag = True
					
					elif service in ['Mechanical Breakdown Towing Service era',
									 'Discounted International Driving Licenseera', 'Usageera',
									 'Accidental & Breakdown Towing era', 'Claims Document Delivery Serviceera',
									 'Onsite Battery Replacementera', ]:
						flag = True
					elif service in ['Fuel Supplyera']:
						if record.customer_country_id and record.customer_state_id and record.car_in_basement:  flag = True
					elif service  == 'Pull Outera':
						if record.pullout_type and record.customer_country_id and record.customer_state_id and record.car_in_basement:  flag = True
					elif service in ['Lockout Serviceera']:
						if record.customer_country_id and record.customer_state_id and record.key_inside_car:  flag = True
					elif service in ['Battery Boostingera', 'Jump Startera']:
						if record.customer_country_id and record.customer_state_id and record.car_in_basement and record.battery_replacement:  flag = True
					elif service in ['Off Road Recoveryera']:
						if record.customer_country_id and record.customer_state_id :flag = True
					elif service in ['Flat Tyreera']:
						if record.customer_country_id and record.customer_state_id and record.spare_tyre and record.tools: flag = True
					else:
						pass

				elif request_type == 'concierge':
					if service in ['Airport Pick up and Dropconcierge', 'Meet and Greet Assistanceconcierge']:
						if record.pickup_drop == 'pickup':
							if service == 'Meet and Greet Assistanceconcierge' and record.marhaba_type:
								if record.customer_country_id and record.from_airport and record.customer_state_id and record.terminal and record.country_to_id and record.state_to_id  and record.date_of_travel and record.flight_number and record.flight_time and record.car_type: flag = True
							elif service == 'Airport Pick up and Dropconcierge':
								if record.customer_country_id and record.from_airport and record.customer_state_id and record.terminal and record.country_to_id and record.state_to_id and record.date_of_travel and record.flight_number and record.flight_time and record.car_type: flag = True
							else:
								pass
						elif record.pickup_drop == 'drop':
							if service == 'Meet and Greet Assistanceconcierge' and record.marhaba_type:
								if record.country_from_id and record.state_from_id  and record.to_airport and record.to_customer_state_id and record.to_terminal and record.date_of_travel and record.flight_number and record.flight_time and record.car_type: flag = True
							elif service == 'Airport Pick up and Dropconcierge':
								if record.country_from_id and record.state_from_id and record.to_airport and record.to_customer_state_id and record.to_terminal and record.date_of_travel and record.flight_number and record.flight_time and record.car_type: flag = True
							else:
								pass
						else:
							pass
					elif service in ['Chauffeur Serviceconcierge', 'Document Delivery Serviceconcierge']:
						if record.service_time and record.country_from_id and record.state_from_id and record.country_to_id and record.state_to_id: flag = True
					elif service == 'Car Service (Automobile)concierge':
						if record.country_from_id and record.state_from_id and record.country_to_id and record.state_to_id: flag = True
					elif service == 'Gift Deliveryconcierge':
						if record.customer_country_id and record.customer_state_id and record.customer_location: flag = True
					elif service in ['Emergency Home Maintenance Serviceconcierge']:
						if record.customer_country_id and record.customer_state_id and record.customer_location and record.noc: flag = True
					else:
						pass

				elif request_type == 'rent-a-car':
					if service == 'Third Partyrent-a-car':
						if record.country_from_id and record.state_from_id and record.security_deposit and record.no_of_days and record.type_of_car and record.type_of_vehicle and record.cost_per_day and record.security_deposit_amt: flag = True
					if service == 'Car Replacementrent-a-car':
						if record.country_from_id and record.state_from_id and record.security_deposit and record.no_of_days and record.claim_insurance_number and record.security_deposit_amt: flag = True
					else:
						pass

				elif request_type == 'registration' and service == 'Registration Renewalregistration':
					if record.service_date and record.time_slots:
						flag = True
					else:
						pass

				elif request_type == 'transportation':
					if service == 'Waiting Chargestransportation':
						if record.service_req_from and record.waiting_time and record.waiting_location: flag = True
					elif service in ['Key Deliverytransportation','Key Collectiontransportation','Intercity Towingtransportation', 'City Limit Towingtransportation']:
						if record.country_from_id and record.state_from_id and record.country_to_id and record.state_to_id and record.service_req_from: flag = True
					else:
						flag = True

				else:
					pass
				rec.update({'ready_to_dispatch': flag})

	@api.model
	def get_default_country(self):
		# country = self.env['res.country'].search([('code', '=', 'AE')]).id
		country = self.env['membership.menu'].search([('id', '=', self.env.context.get('default_membership_id', None))]).company_id.country_id.id
		return country

	@api.model
	def _default_user(self):
		return self.env.context.get('service_takenby', self.env.user.id)

	@api.multi
	def _default_country(self):
		if self.membership_id.company_id.country_id.id:
			cid = self.membership_id.company_id.country_id.id
		else:
			cid = self.env['res.country'].search([('code', '=', 'AE')]).id
		return cid

	# @api.multi
	# def cancel_button(self):
	# 	return self.write({'state': 'cancelled'})

	name = fields.Char('ID')
	customer = fields.Many2one('res.partner', string='Customer Name', track_visibility='onchange')
	contact = fields.Char('Contact #', track_visibility='onchange')
	# request_type = fields.Selection('service', 'request_type',track_visibility='onchange')
	request_type = fields.Selection(related='service.request_type', type="selection", selection=REQUEST_SELECTION,
									string="Request Type", track_visibility='onchange')
	vip = fields.Selection(related="membership_id.vip", string="VIP", type="selection",
						   selection=[('yes', 'Yes'), ('no', 'No')])
	service = fields.Many2one('service.menu', string='Service Name', track_visibility='onchange')
	service_name = fields.Char('Service Name')
	assigned_driver = fields.Many2one('drivers.menu', string='Driver', track_visibility='onchange')
	driver_vehicle_id = fields.Many2one('vehicle.plate', "Driver's Vehicle")
	assigned_party = fields.Many2one('service.providers', string='Sub Contractor', track_visibility='onchange')
	assigned_party_driver = fields.Many2one('drivers.menu', string='Sub Contractor Driver', track_visibility='onchange')
	driver_type = fields.Selection([('imc', 'IMC'),
									('3rd Party', 'Sub Contractor')], 'Driver Type', track_visibility='onchange')
	calling_team = fields.Boolean('Calling Team', track_visibility='onchange')
	rejected_notes = fields.Text('Rejected Notes', track_visibility='onchange')
	manual_completion = fields.Boolean('Service will be manually completed ?')

	pickup_drop = fields.Selection([('pickup', 'Pickup'),
									('drop', 'Drop Off')], 'Pickup/Drop off', track_visibility='onchange')

	# vehicle fields
	veh_type = fields.Selection([('suv', 'SUV'),
								 ('sedan', 'Sedan'),
								 ('sports', 'Sports'),
								 ('bike', 'Bike'),
								 ('others', 'Others')], 'Vehicle Type')
	chassis_no = fields.Char('Chassis #', track_visibility='onchange')
	year = fields.Selection([(str(num), str(num)) for num in range(1950, (datetime.now().year) + 1)],
							'Manufacture Year', track_visibility='onchange')
	model = fields.Many2one('vehicle.model', 'Vehicle Model', track_visibility='onchange')
	vehicle_make = fields.Many2one('vehicles.master', 'Vehicle Make', track_visibility='onchange')
	car_plate = fields.Char('Car Plate#', track_visibility='onchange')
	car_plate_state = fields.Char('Car Plate#')
	car_plate_code = fields.Char('Car Plate#')
	# Towing Type
	towing_type = fields.Selection([('intercity', 'Inter City Towing'),
									('city', 'City Limit Towing')], 'Towing Type', track_visibility='onchange')
	car_in_basement = fields.Selection([('yes', 'Yes'),
										('no', 'No')], 'Car in Basement', track_visibility='onchange')

	# Flat tyre
	spare_tyre = fields.Selection([('yes', 'Yes'),
								   ('no', 'No')], 'Spare Tyre', track_visibility='onchange')
	tools = fields.Selection([('yes', 'Yes'),
							  ('no', 'No')], 'Tools', track_visibility='onchange')
	# customer_location = fields.Text('Customer Location',track_visibility='onchange')
	customer_location = fields.Many2one('res.state.locations', string='Customer Location', track_visibility='onchange')
	customer_country_id = fields.Many2one('res.country', string='Country', default=get_default_country, domain="[('code','in',['IN','OM','AE'])]",
										  track_visibility='onchange')
	customer_state_id = fields.Many2one('res.country.state', string='State', track_visibility='onchange')
	customer_location_add = fields.Text('Customer Address', track_visibility='onchange')
	cust_country_id = fields.Many2one('res.country', string='Country', default=get_default_country, domain="[('code','in',['IN','OM','AE'])]",
									  track_visibility='onchange')
	cust_state_id = fields.Many2one('res.country.state', string='State', track_visibility='onchange',
									domain="[('country_id','=',cust_country_id)]")
	# lockout service
	key_inside_car = fields.Selection([('yes', 'Yes'),
									   ('no', 'No')], 'Key Inside Car', track_visibility='onchange')

	# Jump Start
	battery_replacement = fields.Selection([('yes', 'Yes'),
											('no', 'No')], 'Battery Replacement', track_visibility='onchange')

	membership_id = fields.Many2one('membership.menu', string='Membership Id', track_visibility='onchange')
	type = fields.Selection([('bank', 'Bank'),
							 ('insurance', 'Insurance Company'),
							 ('others', 'Others')], "Type", track_visibility='onchange')
	credit_card_number = fields.Char(
		related='membership_id.credit_card_number', type='Char', string='Credit Card Number',
		track_visibility='onchange')

	email = fields.Char(related='membership_id.email', type='Char', string='Email', store=True)
	membership_type = fields.Many2one('service.type', string='Membership Type', track_visibility='onchange')
	insurance_number = fields.Char('Policy Number', track_visibility='onchange')
	date_time = fields.Datetime('Schedule Date & Time', track_visibility='onchange')
	comments = fields.Text('Remarks', track_visibility='onchange')
	feedback = fields.Text('Feedback', track_visibility='onchange')
	state = fields.Selection([
		('requested', 'Service Taken'),
		('documents-received', 'Documents Received'),
		('verified-rac-eligibility', 'Verified RAC Eligibility'),
		('dispatched', 'Dispatched'),
		('accepted', 'Driver/Vendor Accepted'),
		('rejected', 'Driver/Vendor Rejected'),
		('offhire-requested', 'Off Hire Requested'),
		('ongoing', 'On the Way to Customer'),
		('reaching', 'Reached Customer/Pickup Location'),
		('car-loaded', 'Car Loaded'),
		('leaving', 'Left Customer Location'),
		('reached-dropoff', 'Reached Drop off Location'),
		('car-off-loaded', 'Car Off Loaded'),
		('reached-rta', 'Reached RTA center'),
		('left-rta', 'Left RTA center'),
		('dropped-car', 'Dropped Car to Customer Location'),
		('feedback', 'Service Closed'),
		('service-provided', 'Service Provided'),
		('completed', 'Pending-Feedback'),
		('cancelled', 'Cancelled')], 'Status', default='requested', track_visibility='onchange')
	country_id = fields.Many2one('res.country', string='From Country', default=get_default_country, domain="[('code','in',['IN','OM','AE'])]",
								  track_visibility='onchange')
	state_id = fields.Many2one('res.country.state', string='State', track_visibility='onchange',
							   domain="[('country_id','=',country_id)]")

	# New fields
	call_date = fields.Datetime(string='Date and Time of Call', default=datetime.now(), track_visibility='onchange')

	location_from = fields.Many2one('res.state.locations', string='Pickup Location', track_visibility='onchange')
	location_from_add = fields.Text('Location From Address', track_visibility='onchange')
	country_from_id = fields.Many2one('res.country', string='Country From', default=get_default_country,
									  domain="[('code','in',['IN','OM','AE'])]",
									  track_visibility='onchange')
	state_from_id = fields.Many2one('res.country.state', string='State From',
									domain="[('country_id','=',country_from_id)]", track_visibility='onchange')

	# location_to = fields.Text('Drop off Location',track_visibility='onchange')
	location_to = fields.Many2one('res.state.locations', string='Drop off Location', track_visibility='onchange')
	location_to_add = fields.Text('Location To Address', track_visibility='onchange')
	country_to_id = fields.Many2one('res.country', string='Country To', default=get_default_country,
									domain="[('code','in',['IN','OM','AE'])]",
									track_visibility='onchange')
	state_to_id = fields.Many2one('res.country.state', string='State To', track_visibility='onchange',
								  domain="[('country_id','=',country_to_id)]")

	country_id_to = fields.Many2one('res.country', string='To Country', default=get_default_country,
									domain="[('code','in',['IN','OM','AE'])]",
									track_visibility='onchange')
	state_id_to = fields.Many2one('res.country.state', string='State', track_visibility='onchange')
	amount = fields.Float('Amount', track_visibility='onchange')
	claim_number = fields.Char('Claim Number', track_visibility='onchange')
	scheduled_date = fields.Datetime(string='Scheduled Date & Time',
									 track_visibility='onchange')
	service_takenby = fields.Many2one('res.users', string='Service Taken By', default=_default_user,
									  track_visibility='onchange')
	service_category = fields.Selection([('breakdown', 'Break Down'),
										 ('accident', 'Accident'),
										 ('others', 'Others')], 'Service Category', track_visibility='onchange')
	request_source = fields.Selection([('phone', 'Phone'),
									   ('email', 'Email'),
									   ('mobile-app', 'Mobile App')], 'Request Source', default='phone',
									  track_visibility='onchange')

	# Closing Fields

	trip_sheet_number = fields.Char('Trip Sheet Number', track_visibility='onchange')
	reached_time = fields.Datetime(string='Reached Time', track_visibility='onchange')
	leaving_customer_location_time = fields.Datetime(
		string='Leaving Customer location Time', track_visibility='onchange')
	service_end_time = fields.Datetime(string='Service End Time', track_visibility='onchange')
	starting_km = fields.Float('Starting KM', track_visibility='onchange')
	reached_km = fields.Float('Reached KM', track_visibility='onchange')
	ending_km = fields.Float('Ending KM', track_visibility='onchange')
	reached_rta_km = fields.Float('Reached RTA KM', track_visibility='onchange')
	cost = fields.Float('Cost', track_visibility='onchange')
	closing_remarks = fields.Text('Closing Remarks/Feedback', track_visibility='onchange')
	inv_amount = fields.Float('Invoice Amount', track_visibility='onchange')
	services_done = fields.Integer('Services Done', track_visibility='onchange')
	reached_customer_time = fields.Datetime(
		string='Reached Customer Location Time', track_visibility='onchange')
	reached_registration_center = fields.Datetime(
		string='Reached Registration Center Time', track_visibility='onchange')
	left_registration_center = fields.Datetime(
		string='Left Registration Center Time', track_visibility='onchange')
	difference = fields.Datetime(
		string='Reached Customer Location Time to drop off car', track_visibility='onchange')
	fuel_level = fields.Char('Fuel Level', track_visibility='onchange')
	car_loading_time = fields.Datetime(string='Car Loading Time', track_visibility='onchange')
	hertz_km_reading = fields.Float('Hertz Car KM reading', track_visibility='onchange')
	hertz_fuel_reading = fields.Float('Hertz Car Fuel Level', track_visibility='onchange')
	passport_no = fields.Char('Passport No.', track_visibility='onchange')
	emirates_id = fields.Char('Emirates ID No.', track_visibility='onchange')
	driv_licence = fields.Char('Driving Licence No.', track_visibility='onchange')
	closing_img = fields.Binary('Photo', track_visibility='onchange')
	signature = fields.Binary('Signature', track_visibility='onchange')
	car_handover_to_checkin = fields.Char('Vehicle Handed Over to Checkin', track_visibility='onchange')
	car_handover_to = fields.Char('Vehicle Handed Over to', track_visibility='onchange')
	rejected_notified = fields.Boolean("Rejected Notified", track_visibility='onchange')
	cancelled_notified = fields.Boolean("Cancelled Notified", track_visibility='onchange')

	# Tripsheet vehicle images
	checkin_veh_img1 = fields.Binary('Check-in Vehicle Image-1')
	checkin_veh_img2 = fields.Binary('Check-in Vehicle Image-2')
	checkin_veh_img3 = fields.Binary('Check-in Vehicle Image-3')
	checkin_veh_img4 = fields.Binary('Check-in Vehicle Image-4')
	checkin_veh_img5 = fields.Binary('Check-in Vehicle Image-5')
	checkin_veh_img6 = fields.Binary('Check-in Vehicle Image-6')

	checkout_veh_img1 = fields.Binary('Check-out Vehicle Image-1')
	checkout_veh_img2 = fields.Binary('Check-out Vehicle Image-2')
	checkout_veh_img3 = fields.Binary('Check-out Vehicle Image-3')
	checkout_veh_img4 = fields.Binary('Check-out Vehicle Image-4')
	checkout_veh_img5 = fields.Binary('Check-out Vehicle Image-5')
	checkout_veh_img6 = fields.Binary('Check-out Vehicle Image-6')

	# Remarks
	step1_remarks = fields.Text('Step 1 Remarks')
	step2_remarks = fields.Text('Step 2 Remarks')
	step3_remarks = fields.Text('Step 3 Remarks')
	step4_remarks = fields.Text('Step 4 Remarks')

	# Checkins Images and fields
	checkin_img = fields.Binary('Check-in')
	checkout_img = fields.Binary('Check-out')
	c_signature = fields.Binary('Vehicle received by IMC in above condition')
	h_signature = fields.Binary('Vehicle handed over by IMC in above condition')
	d_signature = fields.Binary('Driver Signature')
	r_signature = fields.Binary('Representative Signature')
	id_type = fields.Selection([('emirates-id', 'Emirates ID'),
										('passport', 'Passport'),
										('driving-licence', 'Driving Licence'),
										], 'Driving Licence', track_visibility='onchange')
	id_value = fields.Char('ID Number')
	check_items = fields.One2many(
		'claimed.service.check.items', 'claim_service_id', 'Items', track_visibility='onchange')
	document_items = fields.One2many(
		'claimed.service.document.items', 'claim_service_id', 'Document Items', track_visibility='onchange')
	cash_advance = fields.Float('Cash Advance', track_visibility='onchange')
	fines = fields.Float('Fines', track_visibility='onchange')
	dept_charges = fields.Float('Department Charges', track_visibility='onchange')
	others = fields.Float('Others', track_visibility='onchange')
	total = fields.Float('Total', track_visibility='onchange')
	balance = fields.Float('Balance', track_visibility='onchange')
	other_charge_details = fields.Text('Other Charge Details')

	# Car Regi. Renewal
	document_types = fields.Char('Document Types', track_visibility='onchange')
	service_time = fields.Datetime(string='Service Date/Time', track_visibility='onchange')
	# time_slots = fields.Selection([('8-10', '08-10'),
	# 							   ('10-12', '10-12'),
	# 							   ('12-14', '12-14'),
	# 							   ('14-16', '14-16'),
	# 							   ('16-18', '16-18'),
	# 							   ], 'Time Slots',track_visibility='onchange')
	time_slots = fields.Many2one('time.slot', 'Time Slots')
	service_date = fields.Date('Service Date', track_visibility='onchange')

	# Feedback questions
	overall_feedback = fields.Selection(
		FEEDBACK_SELECTION, 'Overall, how satisfied are you with the service', track_visibility='onchange')
	agents_feedback = fields.Selection(
		FEEDBACK_SELECTION, 'How satisfied are you with the call center agent who took your service',
		track_visibility='onchange')
	drivers_feedback = fields.Selection(
		FEEDBACK_SELECTION, 'How satisfied are you with our services on the road and the behavior of the Driver',
		track_visibility='onchange')
	drivers_arrival = fields.Selection(
		[('yes', 'YES'), ('no', 'NO')], 'Did the driver reach within 60 minutes', track_visibility='onchange')
	additional_suggestions = fields.Text(
		'Any additional suggestions to improve our service ?', track_visibility='onchange')
	reason_cancel = fields.Text('Reason For Cancel ?', track_visibility='onchange')

	# conceirge fields
	date_of_travel = fields.Datetime(string='Date/Time of Travel', track_visibility='onchange')
	from_airport = fields.Char('From Airport', track_visibility='onchange')
	to_airport = fields.Char('To Airport', track_visibility='onchange')
	to_customer_state_id = fields.Many2one('res.country.state', 'To State', track_visibility='onchange')
	to_terminal = fields.Char('To Terminal', track_visibility='onchange')
	flight_number = fields.Char('Flight Number', track_visibility='onchange')
	customer_names = fields.One2many('claimed.service.customers', 'claim_service_id', 'Customer Names',
									 track_visibility='onchange')
	car_type = fields.Selection([('sedan', 'Sedan'),
								 ('suv', 'SUV')], 'Car Type', track_visibility='onchange')
	no_passengers = fields.Integer('No. of Passengers', track_visibility='onchange')
	service_dispatched_to = fields.Char('Service Dispatched to', track_visibility='onchange')
	documents = fields.One2many('documents.info', 'doc_id', 'Documents', track_visibility='onchange')
	adult_no = fields.Integer('No. of Adults', track_visibility='onchange')
	child_no = fields.Integer('No. of Children', track_visibility='onchange')
	infant_no = fields.Integer('No. of Infants', track_visibility='onchange')
	flight_time = fields.Datetime(string='Flight Time', track_visibility='onchange')
	to_airport = fields.Char('To Airport', track_visibility='onchange')
	marhaba_type = fields.Selection([('gold', 'Gold'),
									 ('silver', 'Silver'),
									 ('bronze', 'Bronze')], 'Marhaba Type', track_visibility='onchange')
	terminal = fields.Char('Terminal')

	pickup_datetime = fields.Datetime('Pickup Date&Time')

	# trasnportations
	# from_location = fields.Text('From Location',track_visibility='onchange')
	from_location = fields.Many2one('res.state.locations', string='From Location', track_visibility='onchange')
	from_location_add = fields.Text('From Address', track_visibility='onchange')
	from_country_id = fields.Many2one('res.country', track_visibility='onchange', string='Country From',
									  default=get_default_country,
									  domain="[('code','in',['IN','OM','AE'])]")
	from_state_id = fields.Many2one('res.country.state', string='State From', track_visibility='onchange')

	# to_location = fields.Text('To Location',track_visibility='onchange')
	to_location = fields.Many2one('res.state.locations', string='To Location', track_visibility='onchange')
	to_location_add = fields.Text('To Address', track_visibility='onchange')
	to_country_id = fields.Many2one('res.country', track_visibility='onchange', string='Country To',
									default=get_default_country,domain="[('code','in',['IN','OM','AE'])]")
	to_state_id = fields.Many2one('res.country.state', string='State To', domain="[('country_id','=',to_country_id)]")

	# Rent a car
	type_of_car = fields.Char('Car Category', track_visibility='onchange')
	type_of_vehicle = fields.Char('Type of Vehicle', track_visibility='onchange')
	cost_per_day = fields.Float('Cost per Day', track_visibility='onchange')
	no_of_days = fields.Integer('No. of days Car Required', track_visibility='onchange')
	off_hire_date = fields.Date('Off hire Date')
	security_deposit = fields.Selection([('cash', 'Cash'),
										 ('card', 'Card')], 'Security Deposit', track_visibility='onchange')

	security_deposit_amt = fields.Float('Security Deposit Amt.', track_visibility='onchange')
	days_eligible = fields.Integer('No. of days eligible', track_visibility='onchange')
	claim_insurance_number = fields.Char('Claim Insurance No.', track_visibility='onchange')
	vendor_ref_no = fields.Char('Vendor Ref. Number', track_visibility='onchange')
	police_report = fields.Selection([('yes', 'YES'),
									  ('no', 'NO')],
									 'Police Report', track_visibility='onchange')
	police_report_received = fields.Selection([('yes', 'YES'),
											   ('no', 'NO')],
											  'Police Report Received??', track_visibility='onchange')

	verified_rac_eligibility = fields.Selection([('yes', 'YES'),
												 ('no', 'NO')],
												'Verified RAC Eligibility??', track_visibility='onchange')

	insurance_company = fields.Many2one(
		related='membership_id.company_id', relation="imc.clients", string='Insurance Company',
		track_visibility='onchange')

	insurance_company_temp = fields.Many2one(
		related='temporary_user_id.client', relation="imc.clients", string='Insurance Company')
	car_required = fields.Datetime(string='DateTime Car Required', track_visibility='onchange')

	# Hertz
	service_req_from = fields.Text('Service Request From', track_visibility='onchange')
	waiting_time = fields.Float('Waiting Time', track_visibility='onchange')
	# waiting_location = fields.Char('Waiting Location',track_visibility='onchange')
	waiting_location = fields.Many2one('res.state.locations', string='Waiting Location', track_visibility='onchange')

	# Driver Ending fields
	driver_ending_km = fields.Float('Drivers Ending KM', track_visibility='onchange')
	driver_feedback = fields.Text('Driver Feedback', track_visibility='onchange')

	# functional fields
	claim_open_today = fields.Integer(
		'Claim Open Today', compute='_get_claim_list')
	services_taken_today = fields.Integer(
		'Services Taken Today', compute='_get_claim_list')
	claimed_scheduled_today = fields.Integer(
		'Claim Scheduled Today', compute='_get_claim_list')
	claim_completed_today = fields.Integer(
		'Claim Completed Today', compute='_get_claim_list')
	ongoing_claim = fields.Integer(
		'Ongoing Claim', compute='_get_claim_list')

	# car service fields
	agency = fields.Selection([('yes', 'YES'),
							   ('no', 'NO')], 'Agency', track_visibility='onchange')

	# Home maintainance service
	noc = fields.Selection([('yes', 'YES'),
							('no', 'NO')], 'NOC', track_visibility='onchange')

	# Airport Transfer fields

	documents_received = fields.Selection([('yes', 'YES'),
										   ('no', 'NO')], 'Documents Received?', track_visibility='onchange')

	# App fields
	is_new = fields.Boolean('is New?', track_visibility='onchange')
	updated_time = fields.Datetime(string='Updated Time', track_visibility='onchange')
	lat = fields.Char('Pickup Latitude', track_visibility='onchange')
	lng = fields.Char('Pikup Longitude', track_visibility='onchange')

	dropoff_lat = fields.Char('Dropoff Latitude', track_visibility='onchange')
	dropoff_lng = fields.Char('Dropoff Longitude', track_visibility='onchange')

	# services status bar fields

	era_other = fields.Selection(compute='_get_status', string="ERA other", default=_get_status,
								 selection=STATE_SELECTION)
	conceirge_airport = fields.Selection(compute='_get_status', default=_get_status, selection=STATE_SELECTION)
	conceirge_chauffeur = fields.Selection(compute='_get_status', default=_get_status, selection=STATE_SELECTION)
	rent_owndamage = fields.Selection(compute='_get_status', readonly=True, default=_get_status,
									  selection=STATE_SELECTION)
	registration_renewal = fields.Selection(compute='_get_status', readonly=True, default=_get_status,
											selection=STATE_SELECTION)
	hertz_towing = fields.Selection(compute='_get_status', readonly=True, selection=STATE_SELECTION)
	transportation_towing = fields.Selection(compute='_get_status', readonly=True, selection=STATE_SELECTION)

	dispatched_time = fields.Datetime(string='Dispatched Time')
	dispatch_reminder_time = fields.Datetime(string='Dispatched Reminder Time')
	dispatch_notified = fields.Boolean("Dispatched Notified")
	dispatch_sms_notified = fields.Boolean("Dispatched SMS Notified")
	dispatch_mail_tosend = fields.Boolean("Dispatched Mail")
	dispatch_mail_sent = fields.Boolean('Dispatched Mail Sent')
	ready_to_dispatch = fields.Boolean('Ready To Dispatch ?', compute='_get_ready_to_dispatch_value', store=True)
	# towing additional fields
	# era_pickup_add = fields.Text('Pickup Address')
	# era_drop_add = fields.Text('Drop Address')

	# GPS fields
	reached_location_lat = fields.Char('Reached Location Lat', track_visibility='onchange')
	reached_location_lng = fields.Char('Reached Location Long', track_visibility='onchange')

	left_cus_location_lat = fields.Char('Left Customer Location Lat', track_visibility='onchange')
	left_cus_location_lng = fields.Char('Left Customer Location Long', track_visibility='onchange')

	dropped_location_lat = fields.Char('Dropped Location Lat', track_visibility='onchange')
	dropped_location_lng = fields.Char('Dropped Location Long', track_visibility='onchange')

	closed_location_lat = fields.Char('Closed Location Lat', track_visibility='onchange')
	closed_location_lng = fields.Char('Closed Location Long', track_visibility='onchange')

	flatbed = fields.Selection([('yes', 'Yes'), ('no', 'No')], 'Flat Bed ?')
	states_in_uae = fields.Boolean('In UAE?', compute='_customer_states_in_uae_test')
	eticket = fields.Binary('E-Ticket')
	eticket_name = fields.Char('Eticket Name')
	pullout_type = fields.Selection([('road', 'Roadside Pull Out'), ('basement', 'Basement Pull Out')], 'Pull Out Type')
	police_report_attachment = fields.Binary('Police Report Attachment')
	police_report_filename = fields.Char('Police Report Filename')

	vehicle_km = fields.Float('Vehicle KM')
	checkitem_remarks1 = fields.Text('Closing Remarks 1')
	checkitem_remarks2 = fields.Text('Closing Remarks 2')
	is_service_completed = fields.Boolean('Is Service Completed?')
	incomplete_reason = fields.Char('Reason for Incomplete')
	driver_cancel_options = fields.Selection([('cancel-direct', 'Cancel-Direct'),
											  ('cancel-ontheway', 'Cancel-On The Way'),
											  ('cancel-reached', 'Cancel-Reached')], 'Driver Cancel Options')

	offhire_24hrs = fields.Boolean('Offhire val')
	offhire_24hrs_edit = fields.Datetime('Offhire val Edited')
	pickup_locations = fields.Text('Pickup/Customer Location', compute='_get_locations_data')
	dropoff_locations = fields.Text('Dropoff Location', compute='_get_locations_data')

	# notification trigger signal fields
	service_taken_sms = fields.Boolean('SMS Sent on service taken?')
	service_closed_sms = fields.Boolean('SMS Sent on service Closed?')
	service_closed_mail = fields.Boolean('Mail Sent on service Closed?')

	temporary_user_id = fields.Many2one('temporary.users', string='Temporary Id', track_visibility='onchange')
	checkin_customer = fields.Char('Checkin Customer')
	vehicle_handed_by = fields.Char('Vehicle Handed By',track_visibility='onchange')
	inspection_fees = fields.Float('Inspection Fees')
	imc_charge = fields.Float('IMC Charge')

	# rent a car closing fields
	invoice_number = fields.Char('Invoice Number')
	days_car_given = fields.Integer('Days Car Given')
	billed_days = fields.Integer('Billed Days')
	late_offhire_charge = fields.Float('Late Offhire Charge')
	delivery_charge = fields.Float('Delivery Charge')
	car_given_date = fields.Date('Car Given Date')
	car_returned_date = fields.Date('Car Returned Date')

	white_color = fields.Boolean('White',compute='_get_status_color', store=True)
	red_color = fields.Boolean('Red',compute='_get_status_color', store=True)
	green_color = fields.Boolean('Green',compute='_get_status_color', store=True)
	# time_slot_exists = fields.Boolean('Timeslot Value Exists', default=False)
	token = fields.Char('Token')

	starting_km_time = fields.Datetime('Starting KM Time')
	reached_km_time = fields.Datetime('Reached KM Time')
	ending_km_time = fields.Datetime('Ending KM Time')
	km_taken = fields.Integer('KM Taken',compute='_get_kms_values')
	actual_km = fields.Integer('Actual KM',compute='_get_kms_values')
	time_taken = fields.Char('Time Taken',compute='_get_kms_values')
	actual_time = fields.Char('Actual Time',compute='_get_kms_values')
	offhire_notified = fields.Boolean('Off hire notified')
	rating = fields.Selection([(0,0),(1,1),(2,2),(3,3),(4,4),(5,5)],'Rating')
	customer_veh_start_km = fields.Float('Customer Vehicle Start KM')
	customer_veh_end_km = fields.Float('Customer Vehicle End KM')




	# @api.onchange('customer_state_id')
	# def _onchange_customer_state_id(self):
	# 	if self.customer_state_id:

	# 		if self.customer_state_id.code in ['AUH','DXB','SHJ'] and self.service_name == 'Jump Startera':
	# 			self.states_in_uae = True
	# 		elif self.customer_state_id.code not in ['AUH','DXB','SHJ'] and self.service_name == 'Jump Startera':
	# 			self.states_in_uae = False
	# 		elif self.service_name == 'Battery Boostingera':
	# 			self.states_in_uae = True
	# 		else:
	# 			pass


	# @api.multi
	# def _offhire_notify_service_providers(self):
	# 	x = False
	# 	for each in self.search([('offhire_notified','!=',True),('off_hire_date','=',str(datetime.now().date()))]):
	# 		table_data_offhire = False
	# 		table_data_offhire = HTML.Table(header_row=['Customer Name','Contact','Service','Scheduled Date','No. of Days','Claim Insurance No.'])
	# 		table_data_offhire.rows.append([each.customer.name, each.contact, each.service.name, each.scheduled_date, each.no_of_days, each.claim_insurance_number])
	# 		body_html = "<p>Dear %s,</p><p>Details of Car to be picked are given below:</p><p>%s</p><p>Regards,</p><p>Team IMC.</p>" % (each.assigned_party.name, table_data_offhire)
	# 		m.setRecipients(each.assigned_party.email_id)
	# 		m.setSubject('Pickup Car')
	# 		m.setBodyHTML(body_html)
	# 		x = m.sendMessage()
	# 		if x:  each.write({'offhire_notified':True})
	# 	return True

	@api.model
	def city_check(self):
		for data in self:
			service_name = data.service_name
			fromm = data.state_from_id.id
			to = data.state_to_id.id
			if service_name:
				service_name = str(service_name)
				if service_name in ['Car Service (Automobile)concierge', 'Document Delivery Serviceconcierge']:
					if fromm and to and fromm != to:
						return False
					else:
						return True

				elif service_name in ['Intercity Towingera', 'Intercity Towingtransportation']:
					if fromm and to and fromm == to:
						return False
					else:
						return True

				elif service_name in ['Citylimit Towingera', 'Citylimit Towingtransportation']:
					if fromm and to and fromm != to:
						return False
					else:
						return True
				else:
					return True
		return {}


	_constraints = [
		(city_check, 'Please check the cities/emirates !', ['service_name', 'state_from_id','state_to_id']),
	]

		
	@api.model
	def _cancelled_policies_services(self):
		table_data, table_data_count = [], []
		table_data2 = {}
		claimed_services = []
		recipients = []

		table_data_count = HTML.Table(header_row=[ 'Service', 'No. of Services Claimed'])
		table_data = HTML.Table(header_row=['Request Type', 'Service', 'Customer','Membership Type','Credit Card Number/Policy Number', 'State'])
		manager_mails_list = self.env['res.users'].search([('groups_id.name', '=', 'Admin'), ('groups_id.category_id.name', '=', 'Insurance Access Levels')])
		ops_mails_list = self.env['res.users'].search([('groups_id.name', '=', 'Operations Team'),('groups_id.category_id.name', '=', 'Insurance Access Levels')])
		recipients = manager_mails_list + ops_mails_list

		cancelled_memberships = self.env['membership.menu'].search([('status','=','cancel')])
		for each in cancelled_memberships:
			claimed_services += self.search([('membership_id','=',each.id)])
			for service in self.search([('membership_id','=',each.id)]):
				if service.request_type == 'concierge': state = 'Concierge'
				elif service.request_type == 'era': state = 'ERA'
				elif service.request_type == 'rent-a-car': state = 'Rent a Car'
				elif service.request_type == 'registration': state = 'Car Registration'
				elif service.request_type == 'transportation': state = 'Transportation'
				else: state = ''
				table_data.rows.append([state, str(service.service.name), str(service.customer.name),str(service.membership_id.type),str(service.membership_id.credit_card_number)+str(service.membership_id.insurance_number),str(service.state)])
				if service.service.name in table_data2:	table_data2[service.service.name] += 1
				else: table_data2[service.service.name] = 1

		for ech in table_data2:
			table_data_count.rows.append([ech,table_data2[ech]])
		if claimed_services and recipients:

			for mail in recipients:
				body_html = "<p>Dear %s,</p><p>Following Services belong to cancelled Memberships:</p><p>%s</p><p>%s</p><p>Regards,</p><p>Team IMC.</p>" % (mail.name,table_data_count,table_data)
				m.setRecipients(mail.email)
				m.setSubject('Cancelled Membership Services')
				m.setBodyHTML(body_html)
				m.sendMessage()

		return True

	@api.model
	def _open_link(self):
		template = False
		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		era_template = self.env['ir.model.data'].xmlid_to_res_id('imc.email_template_era_completed_id')
		carreg_template = self.env['ir.model.data'].xmlid_to_res_id('imc.email_template_carreg_completed_id')
		i = 0
		for each in self.env['claimed.service'].search([('state', '=', 'feedback'), ('service_closed_mail', '=', False)]):
			try:
				i += 1
				if each.request_type in ['era', 'registration']:

					home = expanduser("~")
					dir_name = home + '/temp/%s' % (str(i))
					if os.path.exists(dir_name):
						pass
					else:
						os.makedirs(dir_name)
					os.chdir(dir_name)

					m = Message(auth=o365_auth)
					if each.request_type == 'era' and era_template:
						template = era_template
					elif each.request_type == 'registration' and carreg_template:
						template = carreg_template
					else:
						template = False

					if template: mail_mail = self.env['mail.template'].browse(template).with_context(
						base_url=base_url).send_mail(each.id, force_send=False)

					if mail_mail:
						record = self.env['mail.mail'].browse(mail_mail)
						recepients = []
						recepients.append('ops@motoringclub.com')
						# if each.assigned_driver.related_user:
						# 	recepients.append(str(each.assigned_driver.related_user.login))

						if each.customer.email:
							recepients.append(each.customer.email)
							# m.setRecipients(each.customer.email)
						elif each.membership_id.email:
							recepients.append(each.membership_id.email)
							# m.setRecipients(each.membership_id.email)
						m.setSubject(record.subject)
						m.setBodyHTML(record.body_html)
						attachment = self.env['ir.attachment'].browse(record.attachment_ids.ids[0]).datas
						file = open('TripSheet.pdf', 'wb')
						file.write(base64.b64decode(attachment))
						file.close()
						path = os.path.abspath("TripSheet.pdf")
						att = Attachment(path=path)
						att.save(path)
						append = m.attachments.append(att)
						self.env['mail.mail'].browse(mail_mail).unlink()
						os.remove(path)
						shutil.rmtree(dir_name)

						for recepient in recepients:
							m.setRecipients(recepient)
							final = m.sendMessage()
						# if final:
						try:
							os.unlink("TripSheet.pdf")
						except OSError:
							# logger.exception("Error:%s", e)
							pass
						self.env['mail.mail'].browse(mail_mail).unlink()
						if final: self.browse(each.id).write({'service_closed_mail': True, "is_notification": True})
			except Exception as e:
				self.browse(each.id).write({'service_closed_mail': True, "is_notification": True})
				logger.exception("Error:%s", e)
				pass
				# return {"error": "Oops!! Something went wrong"}

		return True


	# @api.onchange('scheduled_date')
	# def _onchange_scheduled_date(self):
	# 	context = self._context
	# 	self.customer = context.get('customer', False)
	# 	self.service = context.get('service', False)
	# 	self.membership_id = context.get('membership_id', False)
	# 	self.contact = context.get('contact', False)
	# 	self.insurance_number = context.get('insurance_number', False)
	# 	self.membership_type = context.get('membership_type', False)
	# 	self.membership_id = context.get('membership_id', False)
	# 	self.type = context.get('type', False)
	# 	self.date_time = context.get('date_time', False)
	# 	self.service_name = context.get('service_name', False)
	# 	self.calling_team = context.get('calling_team', False)
	# 	self.vehicle_make = context.get('vehicle_make', False)
	# 	self.car_plate = context.get('car_plate', False)
	# 	self.model = context.get('model', False)
	# 	self.year = context.get('year', False)
	# 	self.days_eligible = context.get('days_eligible', False)
	# 	self.veh_type = context.get('veh_type', False)
	# 	self.chassis_no = context.get('chassis_no', False)

	@api.onchange('customer_state_id')
	def _customer_state_test(self):
		for record in self:
			flag = False
			if record.customer_state_id and record.service_name in ['Battery Boostingera', 'Jump Startera']:
				if record.customer_state_id.code in ['AUH', 'DXB', 'SHJ'] and record.service_name == 'Jump Startera':
					flag = True
				elif record.customer_state_id.code not in ['AUH', 'DXB', 'SHJ'] and record.service_name == 'Jump Startera':
					flag = False
				elif record.service_name == 'Battery Boostingera':
					flag = True
				else:
					pass

	@api.onchange('country_from_id', 'state_from_id')
	def _onchange_country_state(self):

		if self.service_name == "Citylimit Towinghertz" or self.service_name == "Citylimit Towingera":
			if self.state_from_id:
				self.state_to_id = self.state_from_id
			if self.country_from_id:
				self.country_to_id = self.country_from_id

		if self.service_name == "Intercity Towinghertz" or self.service_name == "Intercity Towingera":
			if self.country_from_id:
				self.country_to_id = self.country_from_id

		if self.service_name in ['Car Service (Automobile)concierge',
								 'Document Delivery Serviceconcierge'] and self.state_from_id:
			self.state_to_id = self.state_from_id.id

	# @api.onchange('time_slots')
	# def _onchange_timeslots_value(self):
	# 	test = self.env['claimed.service'].search([('service_date','=',self.service_date),('time_slots','=',self.time_slots.id)]).ids
	# 	if len(test) >= 3:
	# 		raise Warning(_('This time slot has been booked. Please choose another timeslot!! '))

	def get_membership_record(self):
		if self.membership_id:
			res_id = self.env['membership.menu'].search([('id', '=', self.membership_id.id)])
			model = 'membership.menu'
		else:
			res_id = self.env['temporary.users'].search([('id', '=', self.temporary_user_id.id)])
			model = 'temporary.users'

		return {
			'name': 'Membership',
			"view_mode": 'form',
			'res_model': model,
			'res_id': res_id.id,
			'type': 'ir.actions.act_window',
			'target': 'current',
		}


	@api.multi
	def display_timeslots_info(self):
		context = self._context.copy()
		if context is None: context = {}

		service_date = self.service_date
		dictt = {}
		slots, last = [], []
		
		if self.customer_state_id:
			records = self.env['claimed.service'].search(
			[('service_date', '=', service_date), ('customer_state_id', '=', self.customer_state_id.id),
			 ('time_slots', '!=', False)]).ids
		else:
			records = self.env['claimed.service'].search(
			[('service_date', '=', service_date),
			 ('time_slots', '!=', False)]).ids

		if self.customer_state_id:
			value = self.env['slot.values'].search([('state','=',self.customer_state_id.id)]).value
		else:
			default_state = self.env['res.country.state'].search([('code','=','DXB')],limit=1).id
			value = self.env['slot.values'].search([('state','=',default_state)]).value

		if records:
			for each in records:
				rec = self.env['claimed.service'].browse(each)
				slots += [rec.time_slots.name]

			for each in slots:
				for timesl in self.env['time.slot'].search([]).ids:
					timeslot = self.env['time.slot'].browse(timesl).name
					if not timeslot in slots:
						dictt.update({timeslot: 0})
					else:
						dictt.update({each: slots.count(each)})

			for dit in dictt:
				vals = {}
				if dictt[dit] < value:
					vals.update({'slot': dit,'sort':dit.split('-')[0], 'available': value - dictt[dit], 'booked': dictt[dit]})
				else:
					vals.update({'slot': dit,'sort':dit.split('-')[0], 'available': 0, 'booked': value})
				last += [(0, 0, vals)]
		else:
			for each in self.env['time.slot'].search([]).ids:
				timeslot = self.env['time.slot'].browse(each)
				last += [(0, 0, {'slot': timeslot.name,'sort':timeslot.name.split('-')[0], 'booked': 0, 'available': value})]

		if self.customer_state_id:
			res_id = self.env['display.current.timeslots'].create({'state':self.customer_state_id.id,'service_date': service_date, 'info': last, 'claim_id': self.id})
		else:
			res_id = self.env['display.current.timeslots'].create({'state':default_state,'service_date': service_date, 'info': last, 'claim_id': self.id})

		context.update({'active_id': self.id})

		return {
			'name': 'Timeslots',
			"view_mode": 'form',
			'res_model': 'display.current.timeslots',
			'res_id': res_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'context': context,
		}



	@api.model
	def create(self, vals):
		context = self._context
		from_app = vals.get('from_app', None)

		if 'customer_names' in vals:
			if type(vals['customer_names']) == 'unicode':
				vals.update({'customer_names': eval(vals['customer_names'])})
			else:
				vals.update({'customer_names': vals['customer_names']})

		url = 'https://maps.googleapis.com/maps/api/geocode/json?address='
		if 'service' in vals and vals['service']:
			vals['service_name'] = (self.env['service.menu'].browse(vals['service']).name) + str(
				self.env['service.menu'].browse(vals['service']).request_type)

		if vals.get('name', _('New')) == _('New'):
			vals['name'] = self.env['ir.sequence'].next_by_code('claimed.service') or _('New')

		if vals['service_name'] in ['Citylimit Towingera', 'Intercity Towingera', 'Car Regi. Renewalconcierge',
									'Car Service (Automobile)concierge', 'Chauffeur Serviceconcierge',
									'Document Delivery Serviceconcierge', 'Airport Pick up and Dropconcierge',
									'Meet and Greet Assistanceconcierge', 'Third Partyrent-a-car',
									'Car Replacementrent-a-car']:
			membership_id = vals.get('membership_id', None)
			if membership_id:
				vals['country_from_id'] = self.env['membership.menu'].browse(
					vals['membership_id']).company_id.country_id.id

		if vals['service_name'] in ['Emergency Home Maintenance Serviceconcierge', 'Flat Tyreera', 'Fuel Supplyera',
									'Jump Startera', 'Lockout Serviceera', 'Off Road Recoveryera', 'Pull Outera',
									'Battery Boostingera']:
			membership_id = vals.get('membership_id', None)
			if membership_id:
				vals['customer_country_id'] = self.env['membership.menu'].browse(
					vals['membership_id']).company_id.country_id.id

		# if 'service_name' in vals and vals['service_name'] in ['Registration Renewalregistration']:
		# 	time_slots = vals.get('timeslots', None)
		# 	if not time_slots:
		# 		raise Warning(_('Time slots should not be empty!!'))

		# if 'no_of_days' in vals:
		# 	no_of_days = vals.get('no_of_days', None)
		# 	if no_of_days <= 0:
		# 		raise UserError(_('No. of days should be non zero value!!'))

		
		lat = vals.get('lat', None)
		lng = vals.get('lng', None)
		dropoff_lat = vals.get('dropoff_lat', None)
		dropoff_lng = vals.get('dropoff_lng', None)
		if from_app:

			if lat and lng:
				res = reverse_geocode(str(lat) + ',' + str(lng))
				res_country = parse_country(res)
				res_city = parse_city(res)
				res_area = parse_area(res)
				logger.info("Pickup Locations: %s/%s/%s;" % (res_country, res_city, res_area))
				if res_country:
					country_id = self.env['res.country'].search([('name', '=', res_country)]).id
					vals.update({'country_from_id': country_id, 'customer_country_id': country_id})
				if res_city:
					city_id = self.env['res.country.state'].search([('name', 'ilike', res_city)])
					if city_id:
						city_id = city_id[0].id
					else:
						city_id = self.env['res.country.state'].create(
							{'name': res_city, 'country_id': country_id}).id
					vals.update({'state_from_id': city_id, 'customer_state_id': city_id})
				if res_area:
					area_id = self.env['res.state.locations'].search(
						[('name', '=', res_area), ('state_id', '=', city_id)])
					if not area_id:
						area_id = self.env['res.state.locations'].create(
							{'name': res_area, 'state_id': city_id, 'country_id': country_id})
						area_id = area_id.id
					else:
						area_id = area_id[0].id

					vals.update({'location_from': area_id, 'customer_location': area_id})

			if dropoff_lat and dropoff_lng:
				res = reverse_geocode(str(dropoff_lat) + ',' + str(dropoff_lng))
				res_country = parse_country(res)
				res_city = parse_city(res)
				res_area = parse_area(res)
				logger.info("Drop off Locations: %s/%s/%s;" % (res_country, res_city, res_area))
				if res_country:
					country_id = self.env['res.country'].search([('name', '=', res_country)]).id
					vals.update({'country_to_id': country_id})
				if res_city:
					city_id = self.env['res.country.state'].search([('name', 'ilike', res_city)])
					if not city_id:
						city_id = self.env['res.country.state'].create(
							{'name': res_city, 'country_id': country_id}).id
					else:
						city_id = city_id[0].id
					vals.update({'state_to_id': city_id})
				if res_area:
					area_id = self.env['res.state.locations'].search(
						[('name', '=', res_area), ('state_id', '=', city_id)])
					if not area_id:
						area_id = self.env['res.state.locations'].create(
							{'name': res_area, 'state_id': city_id, 'country_id': country_id})
						area_id = area_id.id
					else:
						area_id = area_id[0].id
					vals.update({'location_to': area_id})

				service_name = vals.get('service_name', None)

				if 'state_from_id' in vals:
					fromm = vals['state_from_id']
				if 'state_to_id' in vals:
					to = vals['state_to_id']
				if service_name:
					service_name = str(service_name)
					if service_name in ['Car Service (Automobile)concierge', 'Document Delivery Serviceconcierge']:
						if fromm and to and fromm != to:
							raise UserError(_('City Should be the Same!!'))
						# return {'error': 'City Should be the Same!!'}

					if service_name in ['Intercity Towingera', 'Intercity Towingtransportation']:
						if fromm and to and fromm == to:
							raise UserError(_('City Should not be the Same!!'))

					if service_name in ['Citylimit Towingera', 'Citylimit Towingtransportation']:
						if fromm and to and fromm != to:
							raise UserError(_('City Should be the Same!!'))

			vals.pop('from_app')

		vals.update({'call_date': datetime.now()})
		result = super(claimed_service, self).create(vals)
		if result:
			if ('sub_id' and 'sub_model' in context) and context['sub_id'] and context['sub_model']: 
				sub_rec = self.env[context['sub_model']].browse(context['sub_id'])
				sub_rec.write({'booked_services':self.search_count([('membership_id', '=', vals['membership_id']),('customer', '=', sub_rec.membership_id.end_user.id),('service', '=', sub_rec.service_id.id)])})
			if from_app:
				try:
					if 'contact' in vals and vals['contact']:
						contact = ''.join(e for e in vals['contact'] if e.isalnum())
						contact = '971' + contact[-9:]
						sms_content = "Your service request has been logged. Our team will get back to you shortly.\n" \
									  "Team IMC"
						r = requests.get(
							"https://api.smsglobal.com/http-api.php?action=sendsms&user=ml2ihgqq&password=mvYSrtg8"
							"&&from=IMC&to=%s&text=%s" % (
								contact, sms_content))
						# if r.status_code == 200:
						# 	vals.update({'service_closed_sms': True})
				except Exception as e:
					
					logger.exception("Error: %s;" % e)

		self.env['bus.bus'].sendone('refresh', [self.env.cr.dbname, self._name, self._uid]) # for auto reload
					
		return result

	@api.model
	def _send_rejected_notification_to_ops(self):
		table_data = []
		cancelled_services = self.search(
			[('state', '=', 'rejected'), ('assigned_party', '!=', False), ('cancelled_notified', '=', False)])
		if cancelled_services:
			table_data = HTML.Table(header_row=['End User', 'Membership Type', 'Service', 'Insurance/Bank No.'])
		for each in cancelled_services:

			rec = self.browse(each.id)
			if rec.state == 'rejected':

				if rec.type == 'bank':
					table_data.rows.append(
						[str(rec.customer.name), str(rec.membership_type.name), str(rec.service.name),
						 str(rec.credit_card_number)])
				elif rec.type == 'insurance':
					table_data.rows.append(
						[str(rec.customer.name), str(rec.membership_type.name), str(rec.service.name),
						 str(rec.insurance_number)])
				else:
					table_data.rows.append(
						[str(rec.customer.name), str(rec.membership_type.name), str(rec.service.name),
						 str(str(rec.insurance_number) + ' / ' + str(rec.credit_card_number))])

				rec.write({'cancelled_notified': True, 'is_notification': True})

		for each in self.env['res.users'].search([('groups_id.name', '=', 'Operations Team'),
												  ('groups_id.category_id.name', '=', 'Insurance Access Levels')]):
			values = {}
			values['subject'] = 'Cancelled Services'
			values['email_to'] = self.env['res.users'].browse(each.id).email
			values['email_from'] = 'info@motoringclub.com'
			values[
				'body_html'] = "<p>Dear %s,</p><p>Following Services have been cancelled:</p><p>%s</p><p>Regards,</p><p>Team IMC.</p>" % (
				self.env['res.users'].browse(each.id).name, table_data)
			values['body'] = 'body_html',

			if len(cancelled_services.ids) != 0:
				# self.env['mail.mail'].create(values).send()
				m.setRecipients(self.env['res.users'].browse(each.id).email)
				m.setSubject('Cancelled Services')
				m.setBodyHTML(values['body_html'])
				m.sendMessage()

	@api.multi
	def write(self, vals):
		if 'assigned_party' in vals and vals['assigned_party']: vals.update({'assigned_driver':False})
		if 'assigned_driver' in vals and vals['assigned_driver']: vals.update({'assigned_party':False})
		
		url = 'https://maps.googleapis.com/maps/api/geocode/json?address='
		cost_id = self.env['conf.cost']
		total_cost = total_km = 0
		service_name = False

		key_inside_car = vals.get('key_inside_car', None)
		rejected_notified = vals.get('rejected_notified', None)
		dispatch_mail_sent = vals.get('dispatch_mail_sent', None)
		dispatch_mail_tosend = vals.get('dispatch_mail_tosend', None)
		service_taken_sms = vals.get('service_taken_sms', None)
		is_notification = vals.get('is_notification', None)

		if 'service' in vals:
			service_name = str(self.env['service.menu'].browse(vals['service']).name) + str(
				self.env['service.menu'].browse(vals['service']).request_type)
			vals.update({'service_name': service_name})
		else:
			service_name = self.service_name

		if key_inside_car:
			if key_inside_car == 'no':
				raise Warning(_('Unable to Save (Key Inside Car is selected as No) !!'))

		if not is_notification:
			if service_name == 'Car Replacementrent-a-car':
				if 'no_of_days' in vals and 'days_eligible' in vals and (vals['no_of_days'] > vals['days_eligible']):
					raise Warning(_('You are exceeding eligible days!!'))
				elif 'no_of_days' in vals and self.days_eligible and (vals['no_of_days'] > self.days_eligible):
					raise Warning(_('You are exceeding eligible days!!'))
				elif 'days_eligible' in vals and self.no_of_days and (self.no_of_days > vals['days_eligible']):
					raise Warning(_('You are exceeding eligible days!!'))
				
				elif 'no_of_days' in vals:
					if vals['no_of_days'] <= 0:
						raise Warning(_('No. of days car required should be non zero!!'))
				else:
					if self.no_of_days <= 0:
						raise Warning(_('No. of days car required should be non zero!!'))
					if self.no_of_days and self.days_eligible and self.no_of_days > self.days_eligible:
						raise Warning(
						_('You are exceeding eligible days!!'))

		if 'check_items' in vals:
			# if type(vals['check_items']) == str: vals.update({'check_items':json.loads(vals['check_items'])})
			vals.update({'check_items': eval(vals['check_items'])})
		if 'document_items' in vals:
			# if type(vals['document_items']) == str: vals.update({'document_items':json.loads(vals['document_items'])})
			vals.update({'document_items': eval(vals['document_items'])})

		if 'starting_km' in vals:
			vals.update({'starting_km_time':datetime.now()})

		if 'reached_km' in vals:
			vals.update({'reached_km_time':datetime.now()})

		if 'ending_km' in vals:
			vals.update({'ending_km_time':datetime.now()})
			starting_km = self.starting_km
			total_km = float(vals['ending_km']) - starting_km

		if self.assigned_party.id:
			rec = cost_id.search([('name', '=', '3rd Party')])
			if rec:
				cost = cost_id.browse(rec).cost

				if total_km and cost: total_cost = total_km * cost

				if self.assigned_driver.id:
					rec = cost_id.search([('name', '=', 'imc')])
					cost = cost_id.browse(rec).cost

					if total_km and cost: total_cost = total_km * cost

				if total_cost:
					if total_cost < 0:
						total_cost = 0
					vals.update({'cost': total_cost})

		status = vals.get('state', None)
		if status:
			if status == 'reaching':
				vals.update({'reached_time': datetime.now()})

			if status == 'leaving':
				vals.update({'leaving_customer_location_time': datetime.now()})

			if status == 'accepted':
				vals.update({'trip_sheet_number': self.env['ir.sequence'].next_by_code('trip.sheet') or _('New')})
	
			if status == 'dispatched':
				vals.update({'is_new': True})

			if status == 'feedback':
				try:
					# if self.driver_type == '3rd Party' and (self.assigned_party_driver or self.assigned_party):
					# 	for each in self.env['res.users'].search([('groups_id.name', '=', 'Operations Team'), ('groups_id.category_id.name', '=', 'Insurance Access Levels')]):
					# 		subject = '%s - Completed' % self.service.name
					# 		body = "<p>Dear %s,</p><p>Following are the details regarding service completed by the Suncontractor:</p><p>1. Service Name &nbsp;: &nbsp;%s</p><p>2. &nbsp;Customer &nbsp;: %s</p><p>3. &nbsp;Sub Contractor &nbsp;: %s</p><p>3. &nbsp;Sub Contractor Email &nbsp;: %s</p><p>3. &nbsp;Sub Contractor Contact &nbsp;: %s</p><p>Regards,</p><p>IMC Motoring Club.</p>" % (each.name, self.service.name, self.customer.name, self.assigned_party.name, self.assigned_party.email_id, self.assigned_party.contact_no)
					# 		m.setRecipients(each.email)
					# 		m.setSubject(subject)
					# 		m.setBodyHTML(body)
					# 		m.sendMessage()


					if self.contact and self.service_closed_sms == False:
						contact = ''.join(e for e in self.contact if e.isalnum())
						contact = '971' + contact[-9:]
						sms_content = "Thank you for contacting IMC for your roadside assistance service. Should you " \
									  "have any feedback or suggestions about the service please call us on 043876605. "
						r = requests.get(
							"https://api.smsglobal.com/http-api.php?action=sendsms&user=ml2ihgqq&password=mvYSrtg8"
							"&&from=IMC&to=%s&text=%s" % (
								contact, sms_content))
						if r.status_code == 200:
							vals.update({'service_closed_sms': True})
				except Exception as e:
					pass
					logger.exception("Error: %s;" % e)

		vals.update({'updated_time': datetime.now()})

		adult_no = vals.get('adult_no', None)
		child_no = vals.get('child_no', None)
		infant_no = vals.get('infant_no', None)

		if not is_notification:

			passengers = 0
			if adult_no:
				passengers += adult_no
			else:
				passengers += self.adult_no
			if child_no:
				passengers += child_no
			else:
				passengers += self.child_no
			if infant_no:
				passengers += infant_no
			else:
				passengers += self.infant_no

			if passengers < 1 and service_name in ['Airport Pick up and Dropconcierge',
												   'Meet and Greet Assistanceconcierge'] and 'temporary_user_id' not in vals:
				raise Warning(_('Invalid Passenger Count!!'))

		if 'state_from_id' in vals:
			fromm = vals['state_from_id']
		else:
			fromm = self.state_from_id.id
		if 'state_to_id' in vals:
			to = vals['state_to_id']
		else:
			to = self.state_to_id.id

		if not is_notification:

			if service_name in ['Car Service (Automobile)concierge', 'Document Delivery Serviceconcierge']:
				if fromm and to and fromm != to: raise Warning(_('City Should be the Same!!'))

			if service_name in ['Intercity Towingera', 'Intercity Towingtransportation']:
				if fromm and to and fromm == to: raise Warning(_('City Should not be the Same!!'))

			if service_name in ['Citylimit Towingera', 'Citylimit Towingtransportation']:
				if fromm and to and fromm != to: raise Warning(_('City Should be the Same!!'))

		if is_notification:
			vals.pop('is_notification', None)
		updated = super(claimed_service, self).write(vals)
		self.env['bus.bus'].sendone('refresh', [self.env.cr.dbname, self._name, self._uid]) # for auto reload
		return updated

	@api.model
	def _send_dispatch_sms_notification(self):
		
		dispatched_servs = self.search([('dispatched_time','!=',False),('state','=','dispatched'),('manual_completion','=', False)])
		if dispatched_servs:
			for each in dispatched_servs:
				if (datetime.now() - datetime.strptime(each.dispatched_time, "%Y-%m-%d %H:%M:%S") ).seconds/60 > 10:
					each.write({'state':'rejected'})
		
		required_services = self.search([('state', '=', 'dispatched'), ('dispatched_time', '!=', False), ('dispatch_sms_notified', '=', False)])
		for each in required_services:
			minutes_total = (datetime.now() - datetime.strptime(each.dispatched_time, "%Y-%m-%d %H:%M:%S")).seconds / 60

			if minutes_total >= 30:
				customer = each.customer.name
				# contact = self.phone_format(str(each.contact))
				contact = str(each.contact)
				if each.assigned_driver:
					driver, driver_contact = each.assigned_driver.name, each.assigned_driver.contact_no
				elif each.assigned_party and each.assigned_party_driver:
					driver, driver_contact = each.assigned_party_driver.name, each.assigned_party_driver.contact_no
				else:
					driver = False
				try:
					if contact and driver:
						sms_content = "Dear %s, Your service request with ID %s has been dispatched to %s, contact %s. " % (
							customer, each.name, driver, driver_contact)
						contact = ''.join(e for e in contact if e.isalnum())
						contact = '971' + contact[-9:]
						r = requests.get(
							"https://api.smsglobal.com/http-api.php?action=sendsms&user=ml2ihgqq&password=mvYSrtg8&&from=IMC&to=%s&text=%s" % (
								contact, sms_content))
						if r.status_code == 200:
							self.browse(each.id).write({'dispatch_sms_notified': True, 'is_notification': True})
				except Exception as e:
					logger.exception("Error: %s;" % e)

		return True

	@api.model
	def _send_service_taken_sms_client(self):
		sms_to_send_ids = self.search([('service_taken_sms','=', False),('ready_to_dispatch', '=', True)])
		for service in sms_to_send_ids:
			try:
				if service.contact:
					contact = ''.join(e for e in service.contact if e.isalnum())
					contact = '971' + contact[-9:]
					company = ''
					if service.membership_id:
						company = service.membership_id.company_id.name
					elif service.temporary_user_id:
						company = service.temporary_user_id.client.name

					if company and service.request_type == 'era':
						sms_content = "Your breakdown service courtesy of %s has been logged. The service agent is on his way " \
									  "to assist you and will contact you in a short time." % str(company)
						r = requests.get(
							"https://api.smsglobal.com/http-api.php?action=sendsms&user=ml2ihgqq&password=mvYSrtg8&&from=IMC"
							"&to=%s&text=%s" % (
								contact, sms_content))
						# if r.status_code == 200:
				service.write({'service_taken_sms': True, 'is_notification': True})
			except Exception as e:
				logger.exception("Error: %s;" % e)
				service.write({'service_taken_sms': True, 'is_notification': True})
				# pass
		return True

	@api.model
	def _send_rejected_driver_notification(self):
		values = {}
		total_emails = []
		manager_mails_list = self.env['res.users'].search(
			[('groups_id.name', '=', 'Admin'), ('groups_id.category_id.name', '=', 'Insurance Access Levels')])
		ops_mails_list = self.env['res.users'].search([('groups_id.name', '=', 'Operations Team'),
													   ('groups_id.category_id.name', '=', 'Insurance Access Levels')])

		for inn in manager_mails_list.ids + ops_mails_list.ids:
			total_emails += [self.env['res.users'].browse(inn).email]

		rejected_recs = self.search(
			[('state', '=', 'rejected'), ('assigned_driver', '!=', False), ('rejected_notified', '=', False)])
		for i in rejected_recs:
			service_name = i.service.name
			customer = i.customer.name
			rejected_notes = i.rejected_notes

			for each in total_emails:
				subject = '%s - Rejected' % service_name
				# values['email_to'] = each
				# values['email_from'] = 'ops@motoringclub.com'
				body = "<p>Dear User,</p><p>Following are the details regarding service for which driver has rejected:</p><p>1. Service Name &nbsp;: &nbsp;%s</p><p>2. &nbsp;Customer &nbsp;: %s</p><p>3. &nbsp;Driver &nbsp;: %s</p><p>4. &nbsp;Rejected Reason &nbsp;: &nbsp;%s</p><p>Regards,</p><p>IMC Motoring Club.</p>" % (
					service_name, customer, i.assigned_driver.name, rejected_notes)
				# values['body'] = 'body_html'
				# self.env['mail.mail'].create(values).send()

				m.setRecipients(each)
				m.setSubject(subject)
				m.setBodyHTML(body)
				m.sendMessage()

			self.browse(i.id).write({'rejected_notified': True, 'is_notification': True})

		return True

	@api.model
	def _updating_service_offhire(self):
		offhire_records = self.search([('off_hire_date', '>', datetime.now().date())]).ids
		for each in offhire_records:
			rec = self.browse(each)
			if (datetime.strptime(rec.off_hire_date, "%Y-%m-%d").date() - datetime.now().date()).days == 1:
				rec.write({'offhire_24hrs': 1, 'offhire_24hrs_edit': datetime.now(), 'is_notification': True})
			else:
				rec.write({'offhire_24hrs': 0, 'offhire_24hrs_edit': datetime.now(), 'is_notification': True})

		# notifying sub contractor on offhire day to pick car
		x = False
		for each in self.search([('offhire_notified','!=',True),('off_hire_date','=',str(datetime.now().date()))]):
			table_data_offhire = False
			table_data_offhire = HTML.Table(header_row=['Customer Name','Contact','Service','Scheduled Date','No. of Days','Claim Insurance No.'])
			table_data_offhire.rows.append([each.customer.name, each.contact, each.service.name, each.scheduled_date, each.no_of_days, each.claim_insurance_number])
			body_html = "<p>Dear %s,</p><p>Details of Car to be picked are given below:</p><p>%s</p><p>Regards,</p><p>Team IMC.</p>" % (each.assigned_party.name, table_data_offhire)
			m.setRecipients(each.assigned_party.email_id)
			m.setSubject('Pickup Car')
			m.setBodyHTML(body_html)
			x = m.sendMessage()
			if x:  each.write({'offhire_notified':True})

		return True
	

	@api.onchange('service', 'request_type')
	def _onchange_service(self):
		if self.service:
			if self.service.request_type:
				self.service_name = self.service.name + self.service.request_type
			else:
				self.service_name = self.service.name

		if self.request_type == 'era' and 'towing' not in (self.service.name).lower():
			if self.country_to_id:
				self.customer_country_id = self.country_to_id.id
			if self.state_to_id:
				self.customer_state_id = self.state_to_id.id
			if self.location_to:
				self.customer_location = self.location_to.id
			self.customer_location_add = self.location_to_add

	@api.onchange('no_of_days')
	def _onchange_days(self):
		if self.no_of_days > 0:
			self.off_hire_date = datetime.strptime(self.scheduled_date, '%Y-%m-%d %H:%M:%S') + timedelta(days=self.no_of_days)

	@api.onchange('security_deposit', 'security_deposit_amt')
	def _onchange_security_deposit(self):
		if self.security_deposit:
			if self.security_deposit == 'cash':
				self.security_deposit_amt = 1500
			else:
				self.security_deposit_amt = 1000

	@api.onchange('police_report_received')
	def _onchange_police_report_received(self):
		if self.police_report_received:
			if self.police_report_received == 'yes':
				self.rent_owndamage = 'documents-received'
				self.state = 'documents-received'
			else:
				self.rent_owndamage = 'requested'
				self.state = 'requested'
		else:
			self.rent_owndamage = 'requested'
			self.state = 'requested'

		self.verified_rac_eligibility = False

	@api.onchange('verified_rac_eligibility')
	def _onchange_verified_rac_eligibility(self):
		if self.verified_rac_eligibility:
			if self.verified_rac_eligibility == 'yes':
				if self.police_report_received != 'yes':
					self.rent_owndamage = 'requested'
					self.state = 'requested'
					raise UserError(_('Police Report Not Received !!'))
				self.rent_owndamage = 'verified-rac-eligibility'
				self.state = 'verified-rac-eligibility'
			else:
				self.rent_owndamage = 'documents-received'
				self.state = 'documents-received'
		else:
			self.rent_owndamage = 'documents-received'
			self.state = 'documents-received'

	@api.onchange('documents_received')
	def _onchange_documents_received(self):
		if self.documents_received:
			if self.documents_received == 'yes':
				self.conceirge_airport = 'documents-received'
				self.state = 'documents-received'
			else:
				self.conceirge_airport = 'requested'
				self.state = 'requested'
		else:
			self.conceirge_airport = 'requested'
			self.state = 'requested'

	@api.model
	def _sending_reminder_notification_subcon(self):
		vals = values = {}

		dispatch_notify_list = self.search(
			[('dispatch_mail_sent', '!=', True), ('dispatch_mail_tosend', '=', True), ('state', '=', 'dispatched')])
		for each in dispatch_notify_list:
			items = []
			email = ""
			html = """<HTML>
					<body>
					<p>Service details are as follows:
						<table border="1" align="center">
							{0}
						</table>
					</body>
					</HTML>"""
			tr = "<tr>{0}</tr>"
			td = "<td>{0}</td>"

			rec = self.browse(each.id)
			service_name = rec.service_name
			if rec.assigned_party.id:

				if service_name in ['Citylimit Towingera', 'Intercity Towingera']:
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)], ['PickUp Location', str(
							rec.country_from_id.name) + ', ' + str(rec.state_from_id.name) + ', ' + str(
							rec.location_from.name) + ', ' + str(rec.location_from_add)], ['DropOff Location', str(
							rec.country_to_id.name) + ', ' + str(rec.state_to_id.name) + ', ' + str(
							rec.location_to) + ', ' + str(rec.location_to_add)], ['Flat Bed ?', str(rec.flatbed)],
							 ['Car In Basement', str(rec.car_in_basement)]]

				elif service_name == 'Flat Tyreera':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)], ['Customer Location', str(
							rec.customer_country_id.name) + ', ' + str(rec.customer_state_id.name) + ', ' + str(
							rec.customer_location.name) + ', ' + str(rec.customer_location_add)],
							 ['Spare Tyre', str(rec.spare_tyre)], ['Tools', str(rec.tools)]]

				elif service_name == ['Battery Boostingera', 'Jump Startera', 'Fuel Supplyera']:
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)], ['Customer Location', str(
							rec.customer_country_id.name) + ', ' + str(rec.customer_state_id.name) + ', ' + str(
							rec.customer_location.name) + ', ' + str(rec.customer_location_add)],
							 ['Car In Basement', str(rec.car_in_basement)]]

				elif service_name == 'Lockout Serviceera':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)], ['Customer Location', str(
							rec.customer_country_id.name) + ', ' + str(rec.customer_state_id.name) + ', ' + str(
							rec.customer_location.name) + ', ' + str(rec.customer_location_add)],
							 ['Key Inside Car', str(rec.key_inside_car)]]

				elif service_name == 'Off Road Recoveryera':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)], ['Customer Location', str(
							rec.customer_country_id.name) + ', ' + str(rec.customer_state_id.name) + ', ' + str(
							rec.customer_location.name) + ', ' + str(rec.customer_location_add)]]

				elif service_name == 'Pull Outera':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)], ['Customer Location', str(
							rec.customer_country_id.name) + ', ' + str(rec.customer_state_id.name) + ', ' + str(
							rec.customer_location.name) + ', ' + str(rec.customer_location_add)],
							 ['Car In Basement', str(rec.car_in_basement)], ['Pull Out Type', str(rec.pullout_type)]]

				elif service_name == 'Emergency Home Maintenance Serviceconcierge':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)],
							 ['Scheduled Date', str(rec.scheduled_date)], ['Customer Location', str(
							rec.customer_country_id.name) + ', ' + str(rec.customer_state_id.name) + ', ' + str(
							rec.customer_location.name) + ', ' + str(rec.customer_location_add)], ['NOC', str(rec.noc)]]

				elif service_name in ['Meet and Greet Assistanceconcierge',
									  'Airport Pick up and Dropconcierge'] and rec.pickup_drop == 'pickup':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)],
							 ['Scheduled Date', str(rec.scheduled_date)], ['From Airport', str(
							rec.customer_country_id.name) + ', ' + str(rec.customer_state_id.name) + ', ' + str(
							rec.from_airport) + ', ' + str(rec.terminal)], ['Drop Off Location',
																			str(rec.country_to_id.name) + ', ' + str(
																				rec.state_to_id.name) + ', ' + str(
																				rec.location_to.name) + ', ' + str(
																				rec.location_to_add)],
							 ['Date/Time of Travel', str(rec.date_of_travel)], ['Marhaba Type', str(rec.marhaba_type)],
							 ['Flight No.', str(rec.flight_number)], ['Flight Time', str(rec.flight_time)],
							 ['Car Type', str(rec.car_type)], ['Customer Name', str(rec.customer_names)],
							 ['No. of Passengers', str(rec.adult_no + rec.child_no + rec.infant_no)]]

				elif service_name in ['Meet and Greet Assistanceconcierge',
									  'Airport Pick up and Dropconcierge'] and rec.pickup_drop == 'drop':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)],
							 ['Scheduled Date', str(rec.scheduled_date)], ['Location From',
																		   str(rec.country_from_id.name) + ', ' + str(
																			   rec.state_from_id.name) + ', ' + str(
																			   rec.location_from.name) + ', ' + str(
																			   rec.location_from_add)], ['To Airport',
																										 str(
																											 rec.to_airport) + ', ' + str(
																											 rec.to_customer_state_id.name) + ', ' + str(
																											 to_terminal)],
							 ['Date/Time of Travel', str(rec.date_of_travel)], ['Marhaba Type', str(rec.marhaba_type)],
							 ['Flight No.', str(rec.flight_number)], ['Flight Time', str(rec.flight_time)],
							 ['Car Type', str(rec.car_type)], ['Customer Name', str(rec.customer_names)],
							 ['No. of Passengers', str(rec.adult_no + rec.child_no + rec.infant_no)]]

				elif service_name in ['Document Delivery Serviceconcierge', 'Chauffeur Serviceconcierge',
									  'Car Service (Automobile)concierge']:
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)], ['PickUp Location', str(
							rec.country_from_id.name) + ', ' + str(rec.state_from_id.name) + ', ' + str(
							rec.location_from.name) + ', ' + str(rec.location_from_add)], ['DropOff Location', str(
							rec.country_to_id.name) + ', ' + str(rec.state_to_id.name) + ', ' + str(
							rec.location_to) + ', ' + str(rec.location_to_add)],
							 ['Date of Service', str(rec.service_time)]]

				elif service_name == 'Gift Deliveryconcierge':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)], ['Customer Location', str(
							rec.customer_country_id.name) + ', ' + str(rec.customer_state_id.name) + ', ' + str(
							rec.customer_location.name) + ', ' + str(rec.customer_location_add)]]

				elif service_name == 'Car Replacementrent-a-car':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Scheduled Date', str(rec.scheduled_date)],
							 ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)],
							 ['DateTime Car Required', str(rec.car_required)], ['PickUp Location', str(
							rec.country_from_id.name) + ', ' + str(rec.state_from_id.name) + ', ' + str(
							rec.location_from.name) + ', ' + str(rec.location_from_add)],
							 ['Security Deposit', str(rec.security_deposit)], ['Number of days', str(rec.no_of_days)],
							 ['Days Eligible', str(rec.days_eligible)],
							 ['Claim Insurance No.', str(rec.claim_insurance_number)],
							 ['Verified RAC Eligiblity?', str(rec.verified_rac_eligibility)],
							 ['Security Deposit Amt.	', str(rec.security_deposit_amt)],
							 ['Police Report', str(rec.police_report)],
							 ['Police Report Received', str(rec.police_report_received)]]

				elif service_name == 'Third Partyrent-a-car':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Scheduled Date', str(rec.scheduled_date)],
							 ['Service Category', str(rec.service_category)],
							 ['Membership Type', str(rec.membership_type.name)],
							 ['DateTime Car Required', str(rec.car_required)], ['PickUp Location', str(
							rec.country_from_id.name) + ', ' + str(rec.state_from_id.name) + ', ' + str(
							rec.location_from.name) + ', ' + str(rec.location_from_add)],
							 ['Security Deposit', str(rec.security_deposit)], ['Number of days', str(rec.no_of_days)],
							 ['Car Category', str(rec.type_of_car)], ['Type of Vehicle', str(rec.type_of_vehicle)],
							 ['Cost per Day', str(rec.cost_per_day)],
							 ['Security Deposit Amt.	', str(rec.security_deposit_amt)]]

				elif service_name == 'Registration Renewalregistration':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Scheduled Date', str(rec.scheduled_date)],
							 ['Membership Type', str(rec.membership_type.name)], ['Customer Location', str(
							rec.customer_country_id.name) + ', ' + str(rec.customer_state_id.name) + ', ' + str(
							rec.customer_location.name) + ', ' + str(rec.customer_location_add)],
							 ['Service Date', str(rec.service_date)], ['Time Slots', str(rec.time_slots)]]

				elif service_name in ['Key Deliverytransportation', 'Key Collectiontransportation',
									  'Intercity Towingtransportation', 'City Limit Towingtransportation']:
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Scheduled Date', str(rec.scheduled_date)],
							 ['Membership Type', str(rec.membership_type.name)], ['PickUp Location', str(
							rec.country_from_id.name) + ', ' + str(rec.state_from_id.name) + ', ' + str(
							rec.location_from.name) + ', ' + str(rec.location_from_add)], ['DropOff Location', str(
							rec.country_to_id.name) + ', ' + str(rec.state_to_id.name) + ', ' + str(
							rec.location_to) + ', ' + str(rec.location_to_add)],
							 ['Service Request From', str(rec.service_req_from)]]

				elif service_name == 'Key Deliverytransportation':
					items = [['ID', str(rec.name)], ['End User', str(rec.customer.name)],
							 ['Service', str(rec.service.name)], ['Service Category', str(rec.service_category)],
							 ['Scheduled Date', str(rec.scheduled_date)],
							 ['Membership Type', str(rec.membership_type.name)],
							 ['Service Request From', str(rec.service_req_from)],
							 ['Waiting Time', str(rec.waiting_time)],
							 ['Waiting Location', str(rec.waiting_location.name)]]

				else:
					pass

				# sending dispatched status mail to service provider
				email_id = rec.assigned_party.email_id
				if items and email_id:
					table_data = [tr.format(''.join([td.format(a) for a in item])) for item in items]
					body_html = html.format("".join(table_data))
					# vals = { 'body': 'body_html','res_id': False, 'subject' : 'Claimed Service', 'body_html': body_html, 'email_from' : 'info@motoringclub.com', 'email_to' : email_id, }
					# self.env['mail.mail'].create(vals).send()

					m.setRecipients(email_id)
					m.setSubject('Service Request')
					m.setBodyHTML(body_html)
					m.sendMessage()
					rec.write({'dispatch_mail_sent': 1, 'dispatch_mail_tosend': 0, 'is_notification': True})
			else:
				pass

		dispatch_reminder_list = self.search(
			[('dispatch_notified', '!=', True), ('service_name', '=', 'Third Partyrent-a-car'),
			 ('state', '=', 'dispatched'), ('dispatch_reminder_time', '!=', False)])
		for i in dispatch_reminder_list.ids:
			# sending remainder mail to assigned party within 20 minutes from service being dispatched
			if datetime.now() > datetime.strptime(self.browse(i).dispatch_reminder_time, '%Y-%m-%d %H:%M:%S'):
				email = self.browse(i).assigned_party.email_id
				name = self.browse(i).assigned_party.name
				values['subject'] = 'Service Dispatch Reminder'
				values['email_to'] = email
				values['email_from'] = 'no-reply@imc.com'
				# values['body_html'] = "Dear %s,<div><br>This is a reminder mail regarding a service being dispatched before 20 minutes<br></div><div>Regards,</div><div>Team IMC.</div>"%name
				body_html = "Dear %s,<div><br>This is a reminder mail regarding a service being dispatched before 20 minutes<br></div><div>Regards,</div><div>Team IMC.</div>" % name
				values['body'] = 'body_html'

				m.setRecipients(email)
				m.setSubject('Dispatch Reminder')
				m.setBodyHTML(body_html)
				m.sendMessage()
				# self.env['mail.mail'].create(values).send()
				self.write({'dispatch_notified': 1, 'is_notification': True})
		return True

	def phone_format(self, mobile):
		clean_phone_number = re.sub('[^0-9]+', '', str(mobile))
		formatted_phone_number = re.sub("(\d)(?=(\d{3})+(?!\d))", r"\1-", "%d" % int(clean_phone_number[:-1])) + \
								 clean_phone_number[-1]
		return formatted_phone_number

	@api.multi
	def open_dispatch(self):
		drivers = []
		context = self._context.copy()
		request_type = self.request_type
		if context is None: context = {}
		vals = {}

		# try:
		# 	if self.contact and self.service_taken_sms == False:
		# 		contact = ''.join(e for e in self.contact if e.isalnum())
		# 		contact = '971' + contact[-9:]
		# 		company = ''
		# 		if self.membership_id:
		# 			company = self.membership_id.company_id.name
		# 		elif self.temporary_user_id:
		# 			company = self.temporary_user_id.client.name

		# 		if company:
		# 			sms_content = "Your breakdown service courtesy of %s has been logged. The service agent is on his way " \
		# 						  "to assist you and will contact you in a short time." % str(company)
		# 			r = requests.get(
		# 				"https://api.smsglobal.com/http-api.php?action=sendsms&user=ml2ihgqq&password=mvYSrtg8&&from=IMC"
		# 				"&to=%s&text=%s" % (
		# 					contact, sms_content))
		# 			if r.status_code == 200:
		# 				vals.update({'service_taken_sms': True})
		# except Exception as e:
		# 	logger.exception("Error: %s;" % e)

		# self.write(vals)
		if str(request_type) == 'rent-a-car':
			context.update({'default_driver_type': '3rd Party'})

		if str(request_type) == 'registration':
			if not self.time_slots:
				raise Warning(_('Time slots should not be empty!!'))
			# context.update({'default_driver_type': '3rd Party'})

		for driver in self.env['drivers.menu'].search([]):
			rec = self.env['drivers.menu'].browse(driver.id)
			locations = rec.wrkng_locations.ids
			services = rec.skills.ids
			if locations and services:
				if str(request_type) in ['era', 'rent-a-car', 'concierge'] and self.service.name in ['Citylimit Towing',
																									 'Intercity Towing',
																									 'Third Party',
																									 'Car Replacement',
																									 'Document Delivery Service',
																									 'Chauffeur Service',
																									 'Car Service (Automobile)',
																									 '']:
					if self.state_from_id.id in locations and self.service.id in services:  drivers += [driver.id]
				elif str(request_type) in ['era', 'rent-a-car', 'concierge'] and self.service.name in [
					'Battery Boosting', 'Pull Out', 'Off Road Recovery', 'Lockout Service', 'Jump Start', 'Fuel Supply',
					'Flat Tyre', 'Gift Delivery', 'Emergency Home Maintenance Service', ]:
					if self.customer_state_id.id in locations and self.service.id in services:  drivers += [driver.id]
				elif str(request_type) == 'concierge' and self.service.name in ['Airport Pick up and Drop',
																				'Meet and Greet Assistance'] and self.pickup_drop == 'pickup':
					if self.customer_state_id.id in locations and self.service.id in services:  drivers += [driver.id]
				elif str(request_type) == 'concierge' and self.service.name in ['Airport Pick up and Drop',
																				'Meet and Greet Assistance'] and self.pickup_drop == 'drop':
					if self.state_from_id.id in locations and self.service.id in services:  drivers += [driver.id]
				else:
					drivers += [driver.id]
			elif services and not locations:
				if str(request_type) in ['era', 'rent-a-car', 'concierge'] and self.service.name in ['Citylimit Towing',
																									 'Intercity Towing',
																									 'Third Party',
																									 'Car Replacement',
																									 'Document Delivery Service',
																									 'Chauffeur Service',
																									 'Car Service (Automobile)',
																									 '']:
					if self.service.id in services:  drivers += [driver.id]
				elif str(request_type) in ['era', 'rent-a-car', 'concierge'] and self.service.name in [
					'Battery Boosting', 'Pull Out', 'Off Road Recovery', 'Lockout Service', 'Jump Start', 'Fuel Supply',
					'Flat Tyre', 'Gift Delivery', 'Emergency Home Maintenance Service', ]:
					if self.service.id in services:  drivers += [driver.id]
				elif str(request_type) == 'concierge' and self.service.name in ['Airport Pick up and Drop',
																				'Meet and Greet Assistance'] and self.pickup_drop == 'pickup':
					if self.service.id in services:  drivers += [driver.id]
				elif str(request_type) == 'concierge' and self.service.name in ['Airport Pick up and Drop',
																				'Meet and Greet Assistance'] and self.pickup_drop == 'drop':
					if self.service.id in services:  drivers += [driver.id]
				else:
					drivers += [driver.id]
			else:
				drivers += [driver.id]
		context.update({'active_id': self.id, 'default_type':self.type,'default_company':self.membership_id.company_id.id,'drivers': drivers})
		return {
			'name': 'Service Dispatch',
			'view_type': 'form',
			'view_mode': 'form',
			'target': 'new',
			'res_model': 'dispatched.claim.service',
			'type': 'ir.actions.act_window',
			'context': context,
		}

	@api.multi
	def open_time_slots(self):
		context = self._context.copy()
		service_date = self.service_date
		timeslots = []
		cate_ids = []
		# self.env['display.current.timeslots'].search([]).unlink()
		if not service_date: raise UserError(_('Please enter service date !!'))

		if context is None: context = {}

		for reg in self.env['claimed.service'].search([('service_date', '=', service_date)]).ids:
			if self.env['claimed.service'].browse(reg).time_slots: timeslots += [
				self.env['claimed.service'].browse(reg).time_slots.name]

		for each in timeslots:
			if each:
				date = datetime.combine(datetime.strptime(service_date, '%Y-%m-%d'), datetime.min.time())
				datefrom = date + timedelta(hours=int(each.split('-')[0]) - 5.5)
				dateto = date + timedelta(hours=int(each.split('-')[-1]) - 5.5)
				slot = self.env['time.slot'].search([('name', '=', each), ('from_time_slot', '=', each.split('-')[0]),
													 ('to_time_slot', '=', each.split('-')[-1])]).ids
				if not slot:
					timeslot = self.env['time.slot'].create(
						{'name': each, 'from_time_slot': each.split('-')[0], 'to_time_slot': each.split('-')[-1]}).id
				else:
					timeslot = slot[0]

				if self.customer_state_id:
					value = self.env['slot.values'].search([('state','=',self.customer_state_id.id)]).value
				else:
					default_state = self.env['res.country.state'].search([('code','=','DXB')],limit=1).id
					value = self.env['slot.values'].search([('state','=',default_state)]).value


				display = self.env['display.current.timeslots'].search(
					[('name', '=', timeslot), ('customer', '=', self.customer.id), ('datefrom', '=', str(datefrom)),
					 ('dateto', '=', str(dateto))]).ids
				if not display:
					cate_ids += [self.env['display.current.timeslots'].create(
						{'name': timeslot, 'service_date': datefrom.date(), 'customer': self.customer.id,
						 'datefrom': datefrom, 'dateto': dateto}).id]
				else:
					cate_ids += [display[0]]
		context.update({'default_datefrom': service_date, 'active_id': self.id, })
		return {
			'name': 'Time Slots',
			'view_type': 'form',
			'view_mode': 'calendar',
			'target': 'current',
			'res_model': 'display.current.timeslots',
			# 'view_id': view_id.id,
			'type': 'ir.actions.act_window',
			'context': context,
			'domain': [('id', 'in', cate_ids)]
		}

	@api.model
	def _mail_regarding_car_return(self):
		values = {}
		for each in self.search([('request_type', '=', 'rent-a-car')]).ids:
			rec = self.browse(each)
			servicename = rec.service_name
			date_taken = rec.car_required
			total_days = rec.no_of_days
			cost_per_day = rec.cost_per_day
			total_cost = cost_per_day * total_days

			if date_taken and total_days:
				expiry_date = (datetime.strptime(date_taken, '%Y-%m-%d %H:%M:%S')) + timedelta(days=(total_days - 1))

				if date.today() == expiry_date.date() and not rec.calling_team:
					if servicename == 'Third Partyrent-a-car':
						body_html = '''<p>Dear {0},&nbsp;</p>
						<div>Details of the car rented are as follows:</div>
						<div>&nbsp;</div>
						<div>
						<ul>
						<li><strong>Date Time Car Taken &nbsp; &nbsp;: {1}</strong></li>
						<li><strong>Pickup Location &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:&nbsp;<strong>{2}</strong></strong></li>
						<li><strong>Security Deposit &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :&nbsp;<strong>{3}</strong></strong></li>
						<li><strong>Security Deposit Amt. &nbsp;: {4}</strong></li>
						<li><strong>Number of days &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: {5} &nbsp;</strong></li>
						<li><strong>Car Category &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : {6}</strong></li>
						<li><strong>Type of Vehicle &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: {7}</strong></li>
						<li><strong>Cost per Day &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: {8}</strong></li>
						<li><strong>Total Cost &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : {9}</strong></li>
						</ul>
						</div>
						<div>&nbsp;</div>
						<div>Regards,</div>
						<div>Team IMC.</div>
						<p>&nbsp;</p>'''.format(rec.customer.name, date_taken, rec.location_from.name, rec.security_deposit,
												rec.security_deposit_amt, total_days, rec.type_of_car,
												rec.type_of_vehicle, cost_per_day, total_cost)
					else:
						body_html = '''<p>Dear {0},&nbsp;</p>
						<div>Details of the car rented are as follows:</div>
						<div>&nbsp;</div>
						<div>
						<ul>
						<li><strong>Date Time Car Taken &nbsp; &nbsp;: {1}</strong></li>
						<li><strong>Pickup Location &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:&nbsp;<strong>{2}</strong></strong></li>
						<li><strong>Security Deposit &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :&nbsp;<strong>{3}</strong></strong></li>
						<li><strong>Security Deposit Amt. &nbsp;: {4}</strong></li>
						<li><strong>Number of days &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: {5} &nbsp;</strong></li>
						</ul>
						</div>
						<div>&nbsp;</div>
						<div>Regards,</div>
						<div>Team IMC.</div>
						<p>&nbsp;</p>'''.format(rec.customer.name, date_taken, rec.location_from.name,
												rec.security_deposit, rec.security_deposit_amt, total_days)

					values['subject'] = 'Car Return Notification'
					values['email_from'] = 'info@motoringclub.com'
					values['email_to'] = rec.customer.email
					values['body_html'] = body_html
					values['body'] = "body_html"
					values['res_id'] = False

					m.setRecipients(rec.customer.email)
					m.setSubject('Car Return Notification')
					m.setBodyHTML(body_html)
					m.sendMessage()
					# self.env['mail.mail'].create(values).send()
					self.write({'calling_team': '1', 'is_notification': True})
		return True

	# # Web Push

	# @api.cr_uid_ids_context
	# def push_web_notification(self, cr, uid, context=None):
	# 	notification_data = {}
	# 	# current_date = datetime.utcnow()
	# 	# delta = timedelta(seconds=10)
	# 	# before_time = (current_date - delta).strftime("%Y-%m-%d %H:%M:%S")
	# 	# booking_ids1 = self.search(cr, uid, [('modified_date', '>=', before_time), ('modified_date', '<', current_date.strftime(
	# 	# 	"%Y-%m-%d %H:%M:%S")), ('is_notified', '=', False)], context=context)

	# 	# booking_ids2 = self.search(cr, uid, [('created_date', '>=', before_time), ('created_date', '<', current_date.strftime(
	# 	# 	"%Y-%m-%d %H:%M:%S")), ('is_notified', '=', False)], context=context)

	# 	# booking_ids = booking_ids1 + booking_ids2
	# 	# # print booking_ids
	# 	# if booking_ids:
	# 	# 	user_id = self.pool['res.users'].browse(
	# 	# 		cr, SUPERUSER_ID, uid, context=context)
	# 	# 	domain = [('user_id', '=', user_id.id)]
	# 	# 	for notif_id in self.browse(cr, uid, booking_ids, context=context):
	# 	notification_data[1] = {
	# 		'id': 1,
	# 		'email_from': 'IMC CRM',
	# 		'subject': 'Service has been declined by the driver',
	# 		'company_id': 1 or False
	# 	}
	# 	return notification_data or {}
	# 	# return {}

	# reverse geocoding
	# @api.onchange('location_to', 'location_to_add', 'state_to_id', 'country_to_id')
	# def _onchange_get_latitude_longitude(self):
	#     # url ='https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDjpL1857rMGhrSi8luYT5GGMPr1xpqCc4&sensor=false&address='
	#     if self.request_source != 'mobile-app':
	#         url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDH1IaPXc1kzxRTVu-ZNp3hAzd52RiAi-E&address='
	#         try:
	#             if self.country_to_id:
	#                 url += self.country_to_id.name + ','

	#             if self.state_to_id:
	#                 url += self.state_to_id.name + ','

	#             if self.location_to:
	#                 url += self.location_to.name + ','

	#             if self.location_to_add:
	#                 url += self.location_to_add

	#             logger.info(url)

	#             response = requests.get(url)
	#             resp_json_payload = response.json()

	#             res = (resp_json_payload['results'][0]['geometry']['location'])
	#             self.dropoff_lat = res['lat']
	#             self.dropoff_lng = res['lng']
	#             logger.info(res)
	#             logger.info("Lat/Lng: %s/%s;" % (res['lat'], res['lng']))
	#         except Exception as e:
	#             logger.exception("Error: %s;" % e)
	#             pass

	# @api.onchange('location_from', 'location_from_add', 'state_from_id', 'country_from_id')
	# def _onchange_get_latitude_longitude_dropoff(self):
	#     if self.request_source != 'mobile-app':
	#         url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDH1IaPXc1kzxRTVu-ZNp3hAzd52RiAi-E&address='
	#         try:
	#             if self.country_from_id:
	#                 url += self.country_from_id.name + ','

	#             if self.state_from_id:
	#                 url += self.state_from_id.name + ','

	#             if self.location_from:
	#                 url += self.location_from.name + ','

	#             if self.location_to_add:
	#                 url += self.location_from_add

	#             logger.info(url)

	#             response = requests.get(url)
	#             resp_json_payload = response.json()

	#             res = (resp_json_payload['results'][0]['geometry']['location'])
	#             self.lat = res['lat']
	#             self.lng = res['lng']
	#             logger.info(res)
	#             logger.info("Lat/Lng: %s/%s;" % (res['lat'], res['lng']))
	#         except Exception as e:
	#             logger.exception("Error: %s;" % e)
	#             pass

	# @api.onchange('customer_location', 'customer_state_id', 'customer_location_add', 'customer_country_id')
	# def _onchange_get_latitude_longitude_customer_location(self):
	#     if self.request_source != 'mobile-app':
	#         url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDH1IaPXc1kzxRTVu-ZNp3hAzd52RiAi-E&address='
	#         try:
	#             if self.customer_country_id:
	#                 url += self.customer_country_id.name + ','

	#             if self.customer_state_id:
	#                 url += self.customer_state_id.name + ','

	#             if self.customer_location:
	#                 url += self.customer_location.name + ','

	#             if self.customer_location_add:
	#                 url += self.customer_location_add
	#             logger.info(url)

	#             response = requests.get(url)
	#             resp_json_payload = response.json()

	#             res = (resp_json_payload['results'][0]['geometry']['location'])
	#             self.lat = res['lat']
	#             self.lng = res['lng']
	#             logger.info(res)
	#             logger.info("Lat/Lng: %s/%s;" % (res['lat'], res['lng']))
	#         except Exception as e:
	#             logger.exception("Error: %s;" % e)
	#             pass


claimed_service()


class claimed_service_check_items(models.Model):
	_name = 'claimed.service.check.items'

	claim_service_id = fields.Many2one(
		'claimed.service', string='Claimed Service')
	item_id = fields.Many2one('trip.sheet.check.items', string='Item')
	in_option = fields.Selection([('yes', 'YES'),
								  ('no', 'NO')], 'In')

	out_option = fields.Selection([('yes', 'YES'),
								   ('no', 'NO')], 'Out')


class claimed_service_doc_items(models.Model):
	_name = 'claimed.service.document.items'

	claim_service_id = fields.Many2one(
		'claimed.service', string='Claimed Service')
	doc_id = fields.Many2one('trip.sheet.doc.items', string='Document')
	in_option = fields.Selection([('yes', 'YES'),
								  ('no', 'NO')], 'In')

	out_option = fields.Selection([('yes', 'YES'),
								   ('no', 'NO')], 'Out')


class tripsheet_checklist_items(models.Model):
	_name = 'trip.sheet.check.items'

	_rec_name = 'item_name'

	item_name = fields.Char('Item', required=True)
	request_type = fields.Selection([('concierge', 'Concierge'),
                                     ('era', 'ERA'),
                                     ('rent-a-car', 'Rent a Car'),
                                     ('registration', 'Car Registration'),
                                     ('transportation', 'Transportation'),
                                     ('hertz', 'Hertz'),
                                     ], 'Request Type')


class tripsheet_checklist_doc_items(models.Model):
	_name = 'trip.sheet.doc.items'

	_rec_name = 'doc_name'

	doc_name = fields.Char('Doc Name', required=True)
	required_text = fields.Char('Required')


class Documents_recieved_claimedservices(models.Model):
	_name = "documents.info"

	document = fields.Binary('Document')
	name = fields.Char('Name')
	doc_id = fields.Many2one(
		'claimed.service', string='Rel to claimed service')


class claimed_service_customers(models.Model):
	_name = 'claimed.service.customers'

	claim_service_id = fields.Many2one(
		'claimed.service', string='Claimed Service')
	name = fields.Char('Name')


class Res_state_locations(models.Model):
	_name = 'res.state.locations'
	name = fields.Char('Location Name', required=True)
	state_id = fields.Many2one('res.country.state', string="State", required=True)
	country_id = fields.Many2one('res.country', string="Country")

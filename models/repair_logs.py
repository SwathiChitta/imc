from odoo import api, fields, models

class Repair_Logs(models.Model):
	_name="repair.logs"
	
	
	driver = fields.Many2one('drivers.menu','Driver')
	plate_number = fields.Many2one('vehicle.plate','Vehicle Number')
	checkin_time = fields.Datetime('Date and Time')
	performance = fields.Char('Performance')
	place = fields.Text('Place')
	total_price = fields.Float('Total Price')
	odometer_value = fields.Float('Odometer Reading')
	highlight_repair = fields.Text('Highlight of Repair')

Repair_Logs()

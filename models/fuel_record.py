from odoo import api, fields, models


class Fuel_record(models.Model):

    _name = "fuel.record"

    driver_id = fields.Many2one('drivers.menu', 'Driver')
    vehicle_id = fields.Many2one('vehicle.plate', 'Vehicle')
    checkin_time = fields.Datetime('Date and Time')
    place = fields.Text('Place')
    odometer_value = fields.Float('Odometer Reading')
    fuel_type = fields.Char('Fuel Type')
    quantity = fields.Float('Quantity')
    cost = fields.Float('Cost')

Fuel_record()

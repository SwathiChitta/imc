from odoo import api, fields, models

class bin_range_numbers(models.Model):
	_name = 'bin.range.numbers'

	_rec_name = 'number'

	number = fields.Char('Number', required=True)
	card_type = fields.Char('Card Type')

	@api.model
	def _check_length(self):
			for val in self.read(['number']):
					if val['number']:
							if len(val['number']) < 4:
									return False
			return True

	_constraints = [
			(_check_length, 'Number must have more than 4 characters.', ['number'])
	]

bin_range_numbers()
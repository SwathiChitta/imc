from odoo import api, fields, models
from O365 import Message
from odoo.http import request

o365_auth = ('ops@motoringclub.com', 'ITPLAZA@906')
m = Message(auth=o365_auth)


class temporary_users(models.Model):
    _name = "temporary.users"

    _inherit = ['mail.thread']

    _order = "id desc"

    name = fields.Char('Name', track_visibility='onchange')
    email_id = fields.Char('Email Id', track_visibility='onchange')
    mobile_no = fields.Char('Mobile No', track_visibility='onchange')
    policy_no = fields.Char('Policy No', track_visibility='onchange')
    credit_no = fields.Char('Credit Card No', track_visibility='onchange')
    status = fields.Selection([('unverified', 'Unverified'),
                               ('verified', 'Verified'),
                               ], 'Status',default='unverified')
    vehicle_make = fields.Many2one('vehicles.master', 'Vehicle Make', track_visibility='onchange')
    model = fields.Many2one('vehicle.model', 'Model', track_visibility='onchange')
    # veh_reg_no = fields.Char('Registration No.', track_visibility='onchange')
    expiry = fields.Date('Policy Expiry', track_visibility='onchange')
    veh_type = fields.Char('Vehicle Type', track_visibility='onchange')
    chassis_no = fields.Char('Chassis no', track_visibility='onchange')
    car_plate = fields.Char('Car Plate No.', track_visibility='onchange')
    card_issue = fields.Date('Card Issue Date', track_visibility='onchange')
    card_expiry = fields.Date('Card Expiry Date', track_visibility='onchange')
    customer_id = fields.Many2one('res.partner', domain="[('customer', '=', True)]")
    client = fields.Many2one('imc.clients', 'Client', track_visibility='onchange')
    front_rc = fields.Binary('Front Side of RC')
    back_rc = fields.Binary('Back Side of RC')
    otp = fields.Char('OTP', size=4)

    def mail_regarding_temp_user_creation(self, admin_mails_list, values):
        mail_body = "<p>Dear Admin,</p><p>Following are the details of new temporary Membership:</p><p>1. Name &nbsp;: &nbsp;%s</p><p>2. Email Id &nbsp;: &nbsp;%s</p><p>3. Mobile No &nbsp;: &nbsp;%s</p><p>4. Client	 &nbsp;: &nbsp;%s</p><p>5. Policy No &nbsp;: &nbsp;%s</p><p>6. Credit Card No	 &nbsp;: &nbsp;%s</p><p>7. Card Issue Date	 &nbsp;: &nbsp;%s</p><p>8. Card Expiry Date	 &nbsp;: &nbsp;%s</p><p><strong>Vehicle Details</strong></p><p>1. Vehicle Make &nbsp;: &nbsp;%s</p><p>2. Model &nbsp;: &nbsp;%s</p><p>3. Registration No. &nbsp;: &nbsp;%s</p><p>4. Expiry &nbsp;: &nbsp;%s</p><p>5. Vehicle Type	 &nbsp;: &nbsp;%s</p><p>6. Chassis no &nbsp;: &nbsp;%s</p><p>Regards,</p><p>IMC Motoring Club.</p>" % (
        values['name'], values['email'], values['mobile'], values['client'], values['policy_no'], values['credit_no'],
        values['card_issue'], values['card_expiry'], values['vehicle_make'], values['model'], values['veh_reg_no'],
        values['expiry'], values['veh_type'], values['chassis_no'])

        for each in admin_mails_list:
            m.setRecipients(each.email)
            m.setSubject('New Temporary User Created')
            m.setBodyHTML(mail_body)
            m.sendMessage()
        return True

    @api.model
    def create(self, vals):
        result = super(temporary_users, self).create(vals)
        values = {}
        if result:
            admin_mails_list = self.env['res.users'].search([('groups_id.name', '=', 'Admin'), ('groups_id.category_id.name', '=', 'Insurance Access Levels')])

            values.update({
                'name': vals['name'],
                'email': vals['email_id'],
                'mobile': vals['mobile_no'],
                'policy_no': result.policy_no,
                'credit_no': result.credit_no,
                'card_issue': result.card_issue,
                'card_expiry': result.card_expiry,
                'vehicle_make': result.vehicle_make.name,
                'model': result.model.model,
                'veh_reg_no': result.car_plate,
                'expiry': result.expiry,
                'veh_type': result.veh_type,
                'chassis_no': result.chassis_no})
           
            client = vals.get('client', None)

            if client:
                values.update({'client': self.env['imc.clients'].browse(vals['client']).name})
            else:
                values.update({'client': '-'})

        if values and admin_mails_list : self.mail_regarding_temp_user_creation(admin_mails_list,values)

        return result

    @api.multi
    def verify_user(self):
        vals = {}
        values = {'contact_number': self.mobile_no, 'company_id': self.client.id}
        vals['name'] = self.name
        vals['email'] = self.email_id
        vals['mobile'] = self.mobile_no
        if not self.customer_id:
            end_user = self.env['res.partner'].create(vals)
            values['end_user'] = end_user.id
        else:
            values['end_user'] = self.customer_id.id

        if self.policy_no:
            values['type'] = 'insurance'
            values['insurance_number'] = self.policy_no
            values['expiry_date'] = self.expiry
            values['vehicle_make'] = self.vehicle_make.id
            values['model'] = self.model.id
            values['car_plate'] = self.car_plate
            values['veh_type'] = self.veh_type
            values['chassis_no'] = self.chassis_no
            values['front_rc'] = self.front_rc
            values['back_rc'] = self.back_rc
        if self.credit_no:
            values['type'] = 'bank'
            values['credit_card_number'] = self.credit_no
            values['expiry_date'] = self.card_expiry

        res_id = self.env['membership.menu'].create(values)

        if self.credit_no:
            vehicle_values = {
                'make_id': self.vehicle_make.id,
                'model_id': self.model.id,
                'car_plate': self.car_plate,
                'front_rc': self.front_rc,
                'back_rc': self.back_rc,
                'chassis_no': self.chassis_no,
                'membership_id': res_id.id,
                'status': 'active',
            }
            self.env['membership.vehicle.info'].create(vehicle_values)

        self.write({'status': 'verified'})
        temp_requests = self.env['claimed.service'].search([('temporary_user_id','=', self.id)])
        for rec in temp_requests:
            rec.write({'membership_id': res_id.id, 'temporary_user_id': False})
        return {
            'name': 'Creating Membership',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'membership.menu',
            'res_id': res_id.id,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }



temporary_users()

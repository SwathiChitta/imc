from odoo import api, fields, models, _,tools
from odoo.exceptions import UserError, AccessError
import time
import logging
import socket
import xmlrpc.client

logger = logging.getLogger(__name__)
	
username = 'admin' #the user
pwd = 'imcadmin'      #the password of the user
dbname = 'IMC11'    #the database

def get_ip_address():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8", 80))
	return s.getsockname()[0]
	
class Imc_Clients(models.Model):

	_name = "imc.clients"

	@api.model
	def default_country(self):
		country = self.env['res.country'].search([('code', '=', 'AE')]).id
		return country


	
	logo = fields.Binary('Logo',attachment=True)
	image_medium = fields.Binary("Medium-sized photo", attachment=True)
	image_small = fields.Binary("Small-sized photo", attachment=True)
	name = fields.Char('Name')
	website = fields.Char('Website')
	color = fields.Integer('Color')
	email = fields.Char('Email')
	phone = fields.Char('Phone')
	description = fields.Char('Description')
	short_name = fields.Char('Short Name')
	customer_type = fields.Selection([('bank', 'Bank'),
									  ('insurance', 'Insurance Company'),
									  ('transport', 'Transport Company'),
									  ('others', 'Other')], " Client Category")
	street = fields.Char('Street')
	street2 = fields.Char('street2')
	zip = fields.Char('zip')
	city = fields.Char('city')
	state_id = fields.Many2one('res.country.state', 'Fed. State')
	country_id = fields.Many2one('res.country', 'Country', default= default_country )
	exception_policy = fields.Selection([('yes', 'Yes'),
										 ('no', 'No')], 'Exception Policy')
	send_sms = fields.Selection([('yes', 'Yes'),
										 ('no', 'No')], 'Send SMS')

	verification_type = fields.Selection([('f6+l4', 'Credit Card: First 6 digits + last 4 digits'),
											('all', 'Credit Card: All 16 Digits'),
											('reference', 'Reference Number')], 'Verification Type')
	reference = fields.Char('Reference Number')

	# ins_verification_type = fields.Selection([('policy_no', 'Policy Number'),
	#                                           ('car_plate', 'Car Plate No.'),
	#                                           ('chassis', 'Chassis No.'),
	#                                           ('mobile_no', 'Mobile No.'),
	#                                           ], 'Verification Type')

	insurance_verification_types = fields.Many2many('verification.type', id="client_id", id2="verification_id", string="Verification Types")
	services_offering = fields.Many2many('client.services.offered', id="client_id", id2="service_id", string="Services Offered")

	child_ids = fields.One2many(
		'company.contact.persons', 'client_company_id', 'Contact Persons')

	# Dashboard View Fields
	color = fields.Integer('Color Index')

	use_quotations = fields.Boolean(
		'Quotations', help="Check this box to manage quotations in this sales team.")

	contract_expiry = fields.Date('Contract Expiry')
	contract_start = fields.Date('Contract Start')

	# membership type fields

	service_offered = fields.One2many(
		'membership.services', 'client_id', 'Services')
	service_cat_id = fields.Many2one('service.type', 'Membership Category')
	
	# bin_ranges = fields.Many2many('bin.range.numbers', id="client_company_id", id2="number_id",
	#                               string="Bin Numbers")

	# service_cat_ids = fields.Many2many('service.type', id="client_id", id2="category_id",
								  # string="Membership Types")
	service_cat_ids = fields.One2many('membership.services', 'client_id', string="Membership Types")
   
	policy_format = fields.Many2many('policy.formats','rel_to_policy_formats','rel1_id','rel2_id','Policy Formats')
	coverage_countries = fields.Many2many('res.country','rel_to_countries','country1_id','country2_id','Coverage Countries')

	# invisible attr field
	services_offering_boolean = fields.Boolean('Internal attr')

	@api.model
	def create(self, vals):

		vals.update({'image':vals['logo']})
		tools.image_resize_images(vals)
		return super(Imc_Clients, self).create(vals)

	@api.multi
	def write(self, vals):
		if 'logo' in vals and vals['logo']: vals.update({'image':vals['logo']})
		tools.image_resize_images(vals)
		return super(Imc_Clients, self).write(vals)
		
	@api.onchange('contract_start', 'contract_expiry')
	def _changes_in_date(self):
		if self.contract_start and self.contract_expiry and (self.contract_start > self.contract_expiry):
			raise UserError(
				_(" Contract Expiry Date Should be greater than Contract Start Date"))

	@api.onchange('services_offering')
	def _onchange_services_offering(self):
		if self.services_offering:
			for i in self.services_offering:
				if 'trans' in str(i.name).lower():
					self.services_offering_boolean = True


	@api.multi
	def _update_client_membership_services(self):
		start_time = time.time()

		ip = get_ip_address()
		URL = "http://" + ip + ":8069/xmlrpc/common"
		sock_common = xmlrpc.client.ServerProxy(URL, transport=None,
												encoding=None, verbose=False, allow_none=False,
												use_datetime=False, context=None)
		uid = sock_common.login(dbname, username, pwd)
		sock = xmlrpc.client.ServerProxy("http://" + ip + ":8069/xmlrpc/" + 'object')


		for client in self.env['imc.clients'].search([]).ids:
			for category in self.env['imc.clients'].browse(client).service_cat_ids.ids:
				rex = self.env['membership.services'].browse(category)
				category_id = rex.category_id.id

				client_services = self.env['membership.services'].search([('client_id', '=', client),('category_id','=',category_id)])
				
				era_servs,concierge_servs, rentacar_servs, transport_servs, carreg_servs = [],[],[],[],[]

				era_servs = [(0,0,{'request_type':i.request_type,'service_id':i.service_id.id,'unlimited':i.unlimited, 'number_of_services':i.number_of_services})for i in rex.era_available_services]
				carreg_servs = [(0,0,{'request_type':i.request_type,'service_id':i.service_id.id,'unlimited':i.unlimited, 'number_of_services':i.number_of_services})for i in rex.carreg_available_services]
				rentacar_servs = [(0,0,{'request_type':i.request_type,'service_id':i.service_id.id,'unlimited':i.unlimited, 'number_of_services':i.number_of_services})for i in rex.rentacar_available_services]
				concierge_servs = [(0,0,{'request_type':i.request_type,'service_id':i.service_id.id,'unlimited':i.unlimited, 'number_of_services':i.number_of_services})for i in rex.concierge_available_services]
				transport_servs = [(0,0,{'request_type':i.request_type,'service_id':i.service_id.id,'unlimited':i.unlimited, 'number_of_services':i.number_of_services})for i in rex.transport_available_services]
				
				for each in self.env['membership.menu'].search([('membership_type','=',category_id),('company_id','=',client)]):

					eras,concs,carregs,tranports,rntacar = [],[],[],[],[]
					test=[]

					membership_era = each.era_available_services.ids
					membership_con = each.concierge_available_services.ids
					membership_rentcar = each.rentacar_available_services.ids
					membership_carreg = each.carreg_available_services.ids
					membership_transport  = each.transport_available_services.ids

					for i,j in [(membership_era,era_servs),(membership_con,concierge_servs),(membership_carreg,carreg_servs),(membership_rentcar,rentacar_servs),(membership_transport,transport_servs)]:		
						if i==membership_era and j==era_servs :  model,test  =  'era.avail.services',eras
						elif i==membership_con and j==concierge_servs :  model,test  =  'concierge.avail.services',concs
						elif i==membership_carreg and j==carreg_servs :  model,test  =  'carreg.avail.services',carregs
						elif i==membership_rentcar and j==rentacar_servs : model,test  =  'rentacar.avail.services',rntacar
						elif i==membership_transport and j==transport_servs :  model,test  =  'transport.avail.services',tranports
						else : pass

						if len(j)!=0:
							if len(i) < len(j): 
								for mem_serv in i:
									for eachh in j[0:len(i)]:
										if j.index(eachh) == i.index(mem_serv):
											test += [(1,mem_serv,eachh[2])]
								for rest in j[len(i):]:
									test += [(0,0,rest[2])]
							elif len(i) > len(j):
								for mem_serv in i[0:len(j)]:
									for eachh in j:
										if j.index(eachh) == i.index(mem_serv):
											test += [(1,mem_serv,eachh[2])]
								self.env[model].browse(i[len(j):]).unlink()
							else: 
								for mem_serv in i:
									for eachh in j:
										if j.index(eachh) == i.index(mem_serv):
											test += [(1,mem_serv,eachh[2])]

					sock.execute(dbname, uid, pwd, 'membership.menu', 'write', each.id, {'era_available_services':eras, 'concierge_available_services':concs, 'carreg_available_services':carregs, 'rentacar_available_services':rntacar,'transport_available_services': tranports})
					# self.env['membership.menu'].browse(each.id).write({'era_available_services':eras, 'concierge_available_services':concs, 'carreg_available_services':carregs, 'rentacar_available_services':rntacar,'transport_available_services': tranports})

		logger.info("Time Taken: %s seconds;" % ((time.time() - start_time)))
						
		return True
				

Imc_Clients()

class services_offered(models.Model):

	_name = 'client.services.offered'

	name = fields.Char('Name', required=True)


class Policy_formats(models.Model):
	_name = 'policy.formats'
	_rec_name = 'formats'
	formats = fields.Char('Format')

Policy_formats()
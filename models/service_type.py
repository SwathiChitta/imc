from odoo import api, fields, models


class Service_type(models.Model):
    _name = "service.type"

    name = fields.Char('Name')
Service_type()


class Service_menu(models.Model):
    _name = "service.menu"

    logo = fields.Binary('Service Sticker')
    name_customer = fields.Char('Name Of Customer')
    mobile_no = fields.Char('Mobile No')
    service_code = fields.Char('Service Code')
    service_category = fields.Many2one('service.type', 'Service Category')
    request_type = fields.Selection([('concierge', 'Concierge'),
                                     ('era', 'ERA'),
                                     ('rent-a-car', 'Rent a Car'),
                                     ('registration', 'Car Registration'),
                                     ('transportation', 'Transportation'),
                                     ('hertz', 'Hertz'),
                                     ], 'Request Type')
    claim_number = fields.Char('Claim Number')
    # rel_id = fields.Many2one('membership.type', 'Rel')
    name = fields.Char('Service Name')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    security_deposit = fields.Float('Security Deposit')
    active = fields.Boolean('Is Active?', default=True)

Service_menu()


# class service_bin_ranges(models.Model):

#     _name = 'service.bin.ranges'

#     service_id = fields.Many2one('service.menu', "Service ID")
#     bin_card_number = fields.Char("BIN range/Card Number")


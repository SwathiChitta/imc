from odoo import api, fields, models,_
from openerp.exceptions import except_orm, Warning, RedirectWarning

class Time_Slot(models.Model):
	_name="time.slot"
	_rec_name = "name"
	name=fields.Char('Name')
	from_time_slot = fields.Selection([('1', '1'),
								   ('2', '2'),
								   ('3', '3'),
								   ('4', '4'),
								   ('5', '5'),  
								   ('6', '6'),
								   ('7', '7'),
								   ('8', '8'),
								   ('9', '9'),  
								   ('10', '10'),
								   ('11', '11'),
								   ('12', '12'),
								   ('13', '13'),  
								   ('14', '14'),
								   ('15', '15'),
								   ('16', '16'),
								   ('17', '17'), 
								   ('18', '18'),
								   ('19', '19'),
								   ('20', '20'),
								   ('21', '21'),  
								   ('22', '22'),
								   ('23', '23'),
								   ('24', '24')], 'From Time Slot',required="1")
	to_time_slot = fields.Selection([('1', '1'),
								   ('2', '2'),
								   ('3', '3'),
								   ('4', '4'),
								   ('5', '5'),  
								   ('6', '6'),
								   ('7', '7'),
								   ('8', '8'),
								   ('9', '9'),  
								   ('10', '10'),
								   ('11', '11'),
								   ('12', '12'),
								   ('13', '13'),  
								   ('14', '14'),
								   ('15', '15'),
								   ('16', '16'),
								   ('17', '17'), 
								   ('18', '18'),
								   ('19', '19'),
								   ('20', '20'),
								   ('21', '21'),  
								   ('22', '22'),
								   ('23', '23'),
								   ('24', '24')], 'To Time Slot',required="1")

	@api.multi
	@api.depends('name', 'code')
	def name_get(self):
		res = []
		for record in self:
			name = str(record.from_time_slot) + '-' + str(record.to_time_slot)
			res.append((record.id, name))
		return res

	@api.onchange('from_time_slot', 'to_time_slot')
	def _onchange_timeslots(self):
		if self.from_time_slot and self.to_time_slot:
			if int(self.from_time_slot) >= int(self.to_time_slot):
				raise Warning(_('From timeslot should be less than To timeslot!!'))
	@api.model
	def create(self, vals):
		from_time_slot = vals.get('from_time_slot', None)
		to_time_slot = vals.get('to_time_slot', None)

		if from_time_slot and to_time_slot:
			if int(from_time_slot) >= int(to_time_slot):
				raise Warning(_('From timeslot should be less than To timeslot!!'))

		if 'name' in vals:
			vals.update({'to_time_slot':vals['name'].split('-')[-1],'from_time_slot':vals['name'].split('-')[0]})
		if vals['to_time_slot'] and vals['from_time_slot'] and 'name' not in vals: 
			vals['name']=str(vals['from_time_slot'])+'-'+str(vals['to_time_slot'])
		
		rec = self.search([('from_time_slot','=',vals['from_time_slot']),('to_time_slot','=',vals['to_time_slot'])])
		if not rec:
			return super(Time_Slot, self).create(vals)
		else: return rec[0]



	@api.multi
	def write(self, vals):

		if 'to_time_slot' in vals and not 'from_time_slot' in vals:
			vals['name'] = str(self.from_time_slot) + '-' + str(vals['to_time_slot'])
		elif 'from_time_slot' in vals and not 'to_time_slot' in vals:
			vals['name'] = str(vals['from_time_slot']) + '-' + str(self.to_time_slot)
		elif 'to_time_slot' in vals and 'from_time_slot' in vals:
			vals['name'] = str(vals['from_time_slot']) + '-' + str(vals['to_time_slot'])
		else: pass

		return super(Time_Slot, self).write(vals)

Time_Slot()

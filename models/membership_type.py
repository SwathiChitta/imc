from odoo import api, fields, models

REQUEST_SELECTION = [('concierge', 'Concierge'),
										 ('era', 'ERA'),
										 ('rent-a-car', 'Rent a Car'),
										 ('registration', 'Car Registration'),
										 ('transportation', 'Transportation'),
										 ('hertz', 'Hertz'),
										 ]

class membership_services(models.Model):
		_name = 'membership.services'

		category_id = fields.Many2one('service.type', 'Category')

		bin_numbers = fields.Many2many('bin.range.numbers', id="client_cat_id", id2="number_id",
																	string="Bin Numbers")
		max_service_count = fields.Selection([('yes', 'Yes'),
										  ('no', 'No')], "Max Service Count")
		max_count = fields.Integer("Max Service Count")
		client_id = fields.Many2one('imc.clients', "Client")
		era_available_services = fields.One2many('era.avail.services', 'client_cat_id', 'Available Services',)
		concierge_available_services = fields.One2many('concierge.avail.services', 'client_cat_id', 'Available Services',)
		hertz_available_services = fields.One2many('hertz.avail.services', 'client_cat_id', 'Available Services',)
		carreg_available_services = fields.One2many('carreg.avail.services', 'client_cat_id', 'Available Services',)
		rentacar_available_services = fields.One2many('rentacar.avail.services', 'client_cat_id', 'Available Services',)
		transport_available_services = fields.One2many('transport.avail.services', 'client_cat_id', 'Available Services',)

	
		@api.onchange('category_id')
		def onchangeCategory(self):
			res = {'value': {}}
			client_id = False
			services_offered = []
			context = self._context.copy()

			if 'services_offered' in context and context['services_offered']:
				if type(context['services_offered'][0]) == list: services_value = context['services_offered'][0][2]
				else: services_value = context['services_offered']
				for each in services_value:
				# for each in context['services_offered']:
					services_offered += [(self.env['client.services.offered'].browse(each).name).lower()]

			era_avail_services, hertz_avail_services, concierge_avail_services, rentacar_avail_services, carreg_avail_services, transport_avail_services=[],[],[],[],[],[]
			
			if self.category_id and services_offered:
				era_services = self.env['service.menu'].search([('request_type', '=', 'era')])
				hertz_services = self.env['service.menu'].search([('request_type', '=', 'hertz')])
				concierge_services = self.env['service.menu'].search([('request_type', '=', 'concierge')])
				rentacar_services = self.env['service.menu'].search([('request_type', '=', 'rent-a-car')])
				carreg_services = self.env['service.menu'].search([('request_type', '=', 'registration')])
				transport_services = self.env['service.menu'].search([('request_type', '=', 'transportation')])
				avail_services = []
				membership_type = ''
				
				if 'era' in services_offered :
					for i in era_services: era_avail_services.append({'service_id': i.id, 'number_of_services': 0})
				if 'hertz' in services_offered : 
					for i in hertz_services: hertz_avail_services.append({'service_id': i.id, 'number_of_services': 0})
				if 'concierge' in services_offered : 
					for i in concierge_services: concierge_avail_services.append({'service_id': i.id, 'number_of_services': 0})
				if 'rent a car' in services_offered : 
					for i in rentacar_services: rentacar_avail_services.append({'service_id': i.id, 'number_of_services': 0})
				if 'car registration' in services_offered : 
					for i in carreg_services: carreg_avail_services.append({'service_id': i.id, 'number_of_services': 0})
				if 'transportation' in services_offered : 
					for i in transport_services: transport_avail_services.append({'service_id': i.id, 'number_of_services': 0})
				
				res['value'].update({'era_available_services': era_avail_services,
									'concierge_available_services': concierge_avail_services,
									'hertz_available_services': hertz_avail_services,
									'carreg_available_services': carreg_avail_services,
									'rentacar_available_services': rentacar_avail_services,
									'transport_available_services': transport_avail_services})
			return res


membership_services()


class verification_type(models.Model):
    _name = 'verification.type'

    name = fields.Char('Name', required=True)
verification_type()


class membership_upload_logs(models.Model):
	
	_name = "membership.upload.log"

	datetime = fields.Datetime('Upload Datetime')
	created = fields.Integer('Created')
	updated = fields.Integer('Updated')
	duplicates = fields.Integer('Duplicates Identified')
	mail = fields.Char('Mail Delivered ?')
	error= fields.Text('Error Details')

membership_upload_logs()
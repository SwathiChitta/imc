from odoo import api, fields, models
from odoo.http import request
import datetime
from datetime import datetime,timedelta
from datetime import date
import time
import socket
import xmlrpc.client

def get_ip_address():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8", 80))
	return s.getsockname()[0]
	
dbname = "IMC11"
pwd = 'imcadmin'
username = 'admin'
ip = get_ip_address()
URL = "http://" + ip + ":8069/xmlrpc/common"

class Dashboard_Menu(models.Model):

	_name = "dashboard.dashboard"

	name = fields.Char('Name')
	pending_after_dispatch = fields.Integer('Test')
	received_services = fields.Integer('Test')
	dispatched_services = fields.Integer('Test')
	driver_completed = fields.Integer('Test')
	subcon_completed = fields.Integer('Test')
	citylimit_towing = fields.Integer('Test')
	flat_tyre = fields.Integer('Test')
	fuel_supply = fields.Integer('Test')
	jump_start = fields.Integer('Test')
	lockout_service = fields.Integer('Test')
	offroad_rec = fields.Integer('Test')
	pull_out = fields.Integer('Test')
	intercity_towing = fields.Integer('Test')
	battery_boosting = fields.Integer('Test')
	onsitebat_replace = fields.Integer('Test')
	airport_pickdrop = fields.Integer('Test')
	car_service = fields.Integer('Test')
	chauffeur_service = fields.Integer('Test')
	emergency_home = fields.Integer('Test')
	meet_greet = fields.Integer('Test')
	doc_delivery = fields.Integer('Test')
	gift_delivery = fields.Integer('Test')
	own_damage = fields.Integer('Test')
	third_party = fields.Integer('Test')
	reg_renewal = fields.Integer('Test')
	transport_citylimit = fields.Integer('Test')
	transport_intercity = fields.Integer('Test')
	transport_waiting = fields.Integer('Test')
	transport_kycol = fields.Integer('Test')
	trabnsport_keyd = fields.Integer('Test')
	clientwise_active_memberships = fields.Integer('Test')
	clientwise_temp_users = fields.Integer('Test')
	dailywsise_services_count = fields.Integer('Test')
	

	@api.model
	def _get_claimed_services_data(self):
		sock_common = xmlrpc.client.ServerProxy(URL, transport=None,
										encoding=None, verbose=False, allow_none=False,
										use_datetime=False, context=None)
		uid = sock_common.login(dbname, username, pwd)
		sock = xmlrpc.client.ServerProxy("http://" + ip + ":8069/xmlrpc/" + 'object')


		data ={}
		employee_id = [{}]
		clientwise_active_memberships =[]
		clientwise_temp_users = []
		dailywsise_services_count = []

		today = datetime.strftime(datetime.now().date(),'%Y-%m-%d %H:%M:%S')
		min_todays_date = datetime.strptime(today,'%Y-%m-%d %H:%M:%S')
		max_todays_date = datetime.strptime(today,'%Y-%m-%d %H:%M:%S')+timedelta(hours=23)+timedelta(minutes=59)+timedelta(seconds=59)
	
		# pending_after_dispatch=0
		# received_services=0
		# dispatched_services=0
		# driver_completed=0
		# subcon_completed=0
		# citylimit_towing=0
		# flat_tyre=0
		# fuel_supply=0
		# jump_start=0
		# lockout_service=0
		# offroad_rec=0
		# pull_out=0
		# intercity_towing=0
		# battery_boosting=0
		# onsitebat_replace=0
		# airport_pickdrop=0
		# car_service=0
		# chauffeur_service=0
		# emergency_home=0
		# meet_greet=0
		# doc_delivery=0
		# gift_delivery=0 
		# own_damage=0
		# third_party=0
		# reg_renewal=0
		# transport_citylimit=0
		# transport_intercity=0
		# transport_waiting =0
		# transport_kycol=0
		# trabnsport_keyd=0
		# clientwise_active_memberships =0
		# clientwise_temp_users =0
		# dailywsise_services_count = 0

		# data.update({
		# 	'pending_after_dispatch' : self.env['claimed.service'].search_count([('state','in',['accepted','rejected','ongoing','reaching','leaving','reached-rta','left-rta','dropped-car']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'received_services' : self.env['claimed.service'].search_count([('state','=','requested'),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'dispatched_services' : self.env['claimed.service'].search_count([('state','=','dispatched'),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'driver_completed' : self.env['claimed.service'].search_count([('assigned_driver','!=',False),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'subcon_completed' : self.env['claimed.service'].search_count([('assigned_party','!=',False),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'citylimit_towing' : self.env['claimed.service'].search_count([('service.name','=','Citylimit Towing'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'flat_tyre' : self.env['claimed.service'].search_count([('service.name','=','Flat Tyre'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'fuel_supply' :  self.env['claimed.service'].search_count([('service.name','=','Fuel Supply'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'jump_start' : self.env['claimed.service'].search_count([('service.name','=','Jump Start'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'lockout_service' : self.env['claimed.service'].search_count([('service.name','=','Lockout Service'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'offroad_rec' : self.env['claimed.service'].search_count([('service.name','=','Off Road Recovery'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'pull_out' : self.env['claimed.service'].search_count([('service.name','=','Pull Out'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'intercity_towing' : self.env['claimed.service'].search_count([('service.name','=','Intercity Towing'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'battery_boosting' : self.env['claimed.service'].search_count([('service.name','=','Battery Boosting'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'onsitebat_replace' : self.env['claimed.service'].search_count([('service.name','=','Onsite Battery Replacement'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'battery_boost' : self.env['claimed.service'].search_count([('service.name','=','Battery Boosting'),('request_type','=','era'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'airport_pickdrop' : self.env['claimed.service'].search_count([('service.name','=','Airport Pick up and Drop'),('request_type','=','concierge'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'car_service' : self.env['claimed.service'].search_count([('service.name','=','Car Service (Automobile)'),('request_type','=','concierge'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'chauffeur_service' : self.env['claimed.service'].search_count([('service.name','=','Chauffeur Service'),('request_type','=','concierge'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'emergency_home' : self.env['claimed.service'].search_count([('service.name','=','Emergency Home Maintenance Service'),('request_type','=','concierge'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'meet_greet' : self.env['claimed.service'].search_count([('service.name','=','Meet and Greet Assistance'),('request_type','=','concierge'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'doc_delivery' : self.env['claimed.service'].search_count([('service.name','=','Document Delivery Service'),('request_type','=','concierge'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'gift_delivery' : self.env['claimed.service'].search_count([('service.name','=','Gift Delivery'),('request_type','=','concierge'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'own_damage' : self.env['claimed.service'].search_count([('service.name','=','Car Replacement'),('request_type','=','rent-a-car'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'third_party' : self.env['claimed.service'].search_count([('service.name','=','Third Party'),('request_type','=','rent-a-car'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'reg_renewal' : self.env['claimed.service'].search_count([('service.name','=','Registration Renewal'),('request_type','=','registration'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'transport_citylimit' : self.env['claimed.service'].search_count([('service.name','=','City Limit Towing'),('request_type','=','transportation'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'transport_intercity' : self.env['claimed.service'].search_count([('service.name','=','Intercity Towing'),('request_type','=','transportation'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'transport_waiting' : self.env['claimed.service'].search_count([('service.name','=','Waiting Charges'),('request_type','=','transportation'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'transport_kycol' : self.env['claimed.service'].search_count([('service.name','=','Key Collection'),('request_type','=','transportation'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		# 	'trabnsport_keyd' : self.env['claimed.service'].search_count([('service.name','=','Key Delivery'),('request_type','=','transportation'),('state','not in',['feedback','service-provided','completed','cancelled']),('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
			

		# 	})
		# for each in self.env['imc.clients'].search([]):

		# 	clientwise_active_memberships += [str(each.name) + ' - ' + str(self.env['claimed.service'].search_count([('membership_id.company_id','=',each.id),('membership_id','!=',False),('temporary_user_id','=',False)])) + ' ']
		# 	clientwise_temp_users += [str(each.name) + ' - ' + str(self.env['claimed.service'].search_count([('membership_id.company_id','=',each.id),('membership_id','=',False),('temporary_user_id','!=',False)])) + ' ']
		# 	dailywsise_services_count += [str(each.name) + ' - ' + str(self.env['claimed.service'].search_count([('membership_id.company_id','=',each.id),('scheduled_date','>',str(min_todays_date)),('scheduled_date','<',str(max_todays_date))])) + ' ']

		# data.update({
		# 	'clientwise_active_memberships':self.env['claimed.service'].search_count([('membership_id','!=',False),('temporary_user_id','=',False)]),
		# 	'clientwise_temp_users':self.env['claimed.service'].search_count([('membership_id','=',False),('temporary_user_id','!=',False)]),
		# 	'dailywsise_services_count':self.env['claimed.service'].search_count([('call_date','<',str(max_todays_date)),('call_date','>',str(min_todays_date))]),
		
			
		# })

		services = sock.execute(dbname, uid, pwd, 'claimed.service', 'search', [['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]])

		# for each in services:
		# 	vals = sock.execute(dbname,uid,pwd,'claimed.service','read',each,['membership_id','temporary_user_id','service','assigned_party','assigned_driver','state','request_type'])[0]
		# 	if vals['state'] in ['accepted','rejected','ongoing','reaching','leaving','reached-rta','left-rta','dropped-car'] :pending_after_dispatch+1
		# 	elif vals['state'] in ['requested'] :received_services+=1
		# 	elif vals['state'] in ['dispatched'] :dispatched_services+=1
		# 	if vals['assigned_driver']:driver_completed +=1
		# 	if vals['assigned_party']:subcon_completed +=1
		# 	if vals['state'] not in ['feedback','service-provided','completed','cancelled']:
		# 		if vals['request_type'] == 'era': 
		# 			if vals['service'][1] =='Citylimit Towing': citylimit_towing+=1
		# 			if vals['service'][1] =='Flat Tyre': flat_tyre+=1
		# 			if vals['service'][1] =='Fuel Supply': fuel_supply+=1
		# 			if vals['service'][1] =='Jump Start': jump_start+=1
		# 			if vals['service'][1] =='Lockout Service': lockout_service+=1
		# 			if vals['service'][1] =='Off Road Recovery': offroad_rec+=1
		# 			if vals['service'][1] =='Pull Out': pull_out+=1
		# 			if vals['service'][1] =='Intercity Towing': intercity_towing+=1
		# 			if vals['service'][1] =='Battery Boosting': battery_boosting+=1
		# 			if vals['service'][1] =='Onsite Battery Replacement': onsitebat_replace+=1
		# 		elif vals['request_type'] == 'concierge': 
		# 			if vals['service'][1] =='Airport Pick up and Drop': airport_pickdrop+=1
		# 			if vals['service'][1] =='Car Service (Automobile)': car_service+=1
		# 			if vals['service'][1] =='Chauffeur Service': chauffeur_service+=1
		# 			if vals['service'][1] =='Emergency Home Maintenance Service': emergency_home+=1
		# 			if vals['service'][1] =='Meet and Greet Assistance': meet_greet+=1
		# 			if vals['service'][1] =='Document Delivery Service': doc_delivery+=1
		# 			if vals['service'][1] =='Gift Delivery': gift_delivery+=1
		# 		elif vals['request_type'] == 'rent-a-car': 
		# 			if vals['service'][1] =='Car Replacement': own_damage+=1
		# 			if vals['service'][1] =='Third Party': third_party+=1
		# 		elif vals['request_type'] == 'registration': 
		# 			if vals['service'][1] =='Registration Renewal': reg_renewal+=1
		# 		else:
		# 			if vals['service'][1] =='City Limit Towing': transport_citylimit+=1
		# 			if vals['service'][1] =='Intercity Towing': transport_intercity+=1
		# 			if vals['service'][1] =='Waiting Charges': transport_waiting+=1
		# 			if vals['service'][1] =='Key Collection': transport_kycol+=1
		# 			if vals['service'][1] =='Key Delivery': trabnsport_keyd+=1

		# 	if vals['membership_id'] == True and vals['temporary_user_id'] == False:clientwise_active_memberships+=1
		# 	if vals['membership_id']==False and vals['temporary_user_id']==True:clientwise_temp_users+=1
			
		# data.update({
		# 	'pending_after_dispatch' : pending_after_dispatch,
		# 	'received_services' : received_services,
		# 	'dispatched_services' :dispatched_services,
		# 	'driver_completed' : driver_completed,
		# 	'subcon_completed' : subcon_completed,
		# 	'citylimit_towing':citylimit_towing,
		# 	'flat_tyre':flat_tyre,
		# 	'fuel_supply':fuel_supply,
		# 	'jump_start':jump_start,
		# 	'lockout_service':lockout_service,
		# 	'offroad_rec':offroad_rec,
		# 	'pull_out':pull_out,
		# 	'intercity_towing':intercity_towing,
		# 	'battery_boosting':battery_boosting,
		# 	'onsitebat_replace':onsitebat_replace,
		# 	'airport_pickdrop':airport_pickdrop,
		# 	'car_service':car_service,
		# 	'chauffeur_service':chauffeur_service,
		# 	'emergency_home':emergency_home,
		# 	'meet_greet':meet_greet,
		# 	'doc_delivery':doc_delivery,
		# 	'gift_delivery':gift_delivery,
		# 	'own_damage':own_damage,
		# 	'third_party':third_party,
		# 	'reg_renewal':reg_renewal,
		# 	'transport_citylimit':transport_citylimit,
		# 	'transport_intercity':transport_intercity,
		# 	'transport_waiting':transport_waiting,
		# 	'transport_kycol':transport_kycol,
		# 	'trabnsport_keyd':trabnsport_keyd,
		# 	'clientwise_active_memberships':clientwise_active_memberships,
		# 	'clientwise_temp_users':clientwise_temp_users,
		# 	'dailywsise_services_count':len(services),

		
		# 	})
		data.update({
			'pending_after_dispatch' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['state','in',['accepted','rejected','ongoing','reaching','leaving','reached-rta','left-rta','dropped-car']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'received_services' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['state','=','requested'],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'dispatched_services' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['state','=','dispatched'],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'driver_completed' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['assigned_driver','!=',False],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'subcon_completed' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['assigned_party','!=',False],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'citylimit_towing' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Citylimit Towing'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'flat_tyre' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Flat Tyre'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'fuel_supply' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Fuel Supply'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'jump_start' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Jump Start'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'lockout_service' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Lockout Service'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'offroad_rec' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Off Road Recovery'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'pull_out' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Pull Out'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'intercity_towing' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Intercity Towing'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'battery_boosting' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Battery Boosting'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'onsitebat_replace' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Onsite Battery Replacement'],['request_type','=','era'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'airport_pickdrop' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Airport Pick up and Drop'],['request_type','=','concierge'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'car_service' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Car Service (Automobile)'],['request_type','=','concierge'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'chauffeur_service' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Chauffeur Service'],['request_type','=','concierge'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'emergency_home' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Emergency Home Maintenance Service'],['request_type','=','concierge'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'meet_greet' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Meet and Greet Assistance'],['request_type','=','concierge'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'doc_delivery' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Document Delivery Service'],['request_type','=','concierge'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'gift_delivery' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Gift Delivery'],['request_type','=','concierge'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'own_damage' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Car Replacement'],['request_type','=','rent-a-car'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'third_party' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Third Party'],['request_type','=','rent-a-car'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'reg_renewal' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Registration Renewal'],['request_type','=','registration'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'transport_citylimit' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','City Limit Towing'],['request_type','=','transportation'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'transport_intercity' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Intercity Towing'],['request_type','=','transportation'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'transport_waiting' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Waiting Charges'],['request_type','=','transportation'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'transport_kycol' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Key Collection'],['request_type','=','transportation'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'trabnsport_keyd' : sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['service.name','=','Key Delivery'],['request_type','=','transportation'],['state','not in',['feedback','service-provided','completed','cancelled']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
			'clientwise_active_memberships':sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['membership_id','!=',False],['temporary_user_id','=',False]]),
			'clientwise_temp_users':sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['membership_id','=',False],['temporary_user_id','!=',False]]),
			'dailywsise_services_count':sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
		
			})
		if self.search([]).ids:
			self.browse(self.search([]).ids[0]).write(data)
		else:
			self.create(data)
		# services = sock.execute(dbname, uid, pwd, 'claimed.service', 'search', [['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]])
		# x = sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['id','in',services],['state','in',['accepted','rejected','ongoing','reaching','leaving','reached-rta','left-rta','dropped-car']],['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
		# data.update({
		# 	'pending_after_dispatch' : 18,
		# 	'received_services' : 13,
		# 	'dispatched_services' :25,
		# 	'driver_completed' : 36,
		# 	'subcon_completed' : 25,
		# 	'citylimit_towing':32,
		# 	'flat_tyre':23,
		# 	'fuel_supply':18,
		# 	'jump_start':21,
		# 	'lockout_service':9,
		# 	'offroad_rec':3,
		# 	'pull_out':9,
		# 	'intercity_towing':24,
		# 	'battery_boosting':29,
		# 	'onsitebat_replace':31,
		# 	'airport_pickdrop':7,
		# 	'car_service':0,
		# 	'chauffeur_service':0,
		# 	'emergency_home':0,
		# 	'meet_greet':2,
		# 	'doc_delivery':0,
		# 	'gift_delivery':0,
		# 	'own_damage':2,
		# 	'third_party':0,
		# 	'reg_renewal':6,
		# 	'transport_citylimit':1,
		# 	'transport_intercity':3,
		# 	'transport_waiting':0,
		# 	'transport_kycol':0,
		# 	'trabnsport_keyd':1,
		# 	'clientwise_active_memberships':5120,
		# 	'clientwise_temp_users':122,
		# 	'dailywsise_services_count':82,

		
		# 	})


		# for each in sock.execute(dbname, uid, pwd, 'imc.clients', 'search', []):
		# 	rec = sock.execute(dbname, uid, pwd, 'imc.clients', 'read', [each])[0]
		# 	clientwise_active_memberships += [str(rec['name']) + ' - ' + str(sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['membership_id.company_id','=',rec['id']],['membership_id','!=',False],['temporary_user_id','=',False]])) + ' ']
		# 	clientwise_temp_users += [str(rec['name']) + ' - ' + str(sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['membership_id.company_id','=',rec['id']],['membership_id','=',False],['temporary_user_id','!=',False]])) + ' ']
		# 	dailywsise_services_count += [str(rec['name']) + ' - ' + str(sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['membership_id.company_id','=',rec['id']],['scheduled_date','>',str(min_todays_date)],['scheduled_date','<',str(max_todays_date)]])) + ' ']

		# data.update({
		# 	'clientwise_active_memberships':sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['membership_id','!=',False],['temporary_user_id','=',False]]),
		# 	'clientwise_temp_users':sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['membership_id','=',False],['temporary_user_id','!=',False]]),
		# 	'dailywsise_services_count':sock.execute(dbname, uid, pwd, 'claimed.service', 'search_count', [['call_date','<',str(max_todays_date)],['call_date','>',str(min_todays_date)]]),
		
			
		# })
		
		
		# employee_id[0].update(data)
		# return employee_id
		return True

Dashboard_Menu()
from odoo import api, fields, models
import datetime
from datetime import datetime
from O365 import Message, Attachment
o365_auth = ('ops@motoringclub.com', 'ITPLAZA@906')
m = Message(auth=o365_auth)
from . import HTML

class Imc_complaints(models.Model):

    _name = 'imc.complaints'

    _rec_name = 'subject'

    @api.model
    def _default_user(self):
        return self.env.user.id

    @api.multi
    def progress(self):
        return self.write({'status': 'under_review'})

    @api.multi
    def close(self):
        return self.write({'status': 'close'})


    subject = fields.Char('Subject')
    complaint = fields.Text('Complaint')
    customer_id = fields.Many2one('res.partner', 'Customer', domain="[('customer', '=', True)]")
    created_date = fields.Datetime('Created Date',default=datetime.now())
    created_by = fields.Many2one('res.users','Created By',default=_default_user,)
    status = fields.Selection([('open','Open'),('under_review','Under Review'),('close','Closed')],'Status',default="open")
    sla = fields.Datetime('SLA')
    mail_notified = fields.Boolean('Notified')

    @api.model
    def _sla_expired_complaints(self):
        x = False
        table_data = HTML.Table(header_row=['Customer', 'Subject', 'SLA', 'Complaint'])
        manager_mails_list = self.env['res.users'].search([('groups_id.name', '=', 'Manager'), ('groups_id.category_id.name', '=', 'Insurance Access Levels')])
       
        complaints = self.search([('status','=','open'),('sla','!=',False),('mail_notified','=',False)])
        for each in complaints:
            if datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S") > datetime.strptime(each.sla, "%Y-%m-%d %H:%M:%S"):
                table_data.rows.append([each.customer_id.name, each.subject, each.sla, each.complaint])

        if complaints and manager_mails_list:
            for mail in manager_mails_list:
                body_html = "<p>Dear %s,</p><p>SLA for following complaints has expired:</p><p>%s</p><p>Regards,</p><p>Team IMC.</p>" % (mail.name,table_data)
                m.setRecipients(mail.email)
                m.setSubject('SLA Expired Complaints')
                m.setBodyHTML(body_html)
                x = m.sendMessage()

            if x:
                for each in complaints: each.write({'mail_notified':True})

        return True

Imc_complaints()


from odoo import api, fields, models, _
from datetime import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning

class drivers_menu(models.Model):

	_name = 'drivers.menu'
	

	name = fields.Char('Name')
	gcm_dev_id = fields.Char('GCM Dev ID')
	contact_no = fields.Char('Contact Number')
	related_user = fields.Many2one('res.users','Related User',domain=lambda self: [("groups_id", "=", self.env.ref( "imc.group_driver" ).id)])
	driver_type = fields.Selection([('imc','IMC'),
		('sub','Sub Contractor')], 'Driver Type', default='imc')
	status = fields.Selection([('inactive','Inactive'),
		('engaged','Engaged'),
		('free','Free'),
		],'Status')
	wrkng_locations = fields.Many2many('res.country.state',id='driver_id',id2='state_id',string='Working Locations')
	skills = fields.Many2many('service.menu',id='driver_id',id2='service_id',string='Skills/Expertise')
	hypertrack_id = fields.Char('Hypertrack ID')
	gcm_updated_time = fields.Datetime('GCM Updated Time')

	@api.onchange('related_user')
	def _onchange_related_user(self):
		total_users = []
		for each in self.search([('related_user','!=',False)]):
			total_users += [each.related_user.id]

		users = self.env['res.users'].search([('groups_id.name', '=', 'Driver'), ('groups_id.category_id.name', '=', 'Insurance Access Levels')]).ids
		return {'domain': {'related_user':[('id','in',list(set(users)-set(total_users)))]}}


	@api.model
	def create(self, vals):
		gcm_dev_id = vals.get('gcm_dev_id', None)
		if gcm_dev_id:
			vals.update({'gcm_updated_time': datetime.now()})
		result = super(drivers_menu, self).create(vals)
		if vals['related_user']:
			count = self.env['drivers.menu'].search_count([('related_user','=',vals['related_user'])])
			if count > 1: raise Warning(_('This User has already been assigned to another Driver.'))
		return result

	@api.multi
	def write(self, vals):
		gcm_dev_id = vals.get('gcm_dev_id', None)
		if gcm_dev_id:
			vals.update({'gcm_updated_time': datetime.now()})
		result = super(drivers_menu, self).write(vals)
		if 'related_user' in vals and vals['related_user']:
			count = self.env['drivers.menu'].search_count([('related_user','=',vals['related_user'])])
			if count > 1: raise Warning(_('This User has already been assigned to another Driver.'))
		return result


drivers_menu()	


class Driver_break(models.Model):

	_name = 'driver.break'

	_rec_name = 'driver_id'

	_order = "id desc"

	driver_id = fields.Many2one('drivers.menu','Driver')
	assigned_party = fields.Many2one('service.providers', string='Sub Contractor')
	inbreak = fields.Boolean('In Break ?')
	description = fields.Text('Description')
	inbreak_timestamp = fields.Datetime('In Time')
	outbreak_timestamp = fields.Datetime('Out Time')
	break_type = fields.Selection([('in','In'),
								('out','Out')],'Break Type')
	type_of_break = fields.Selection([('food','Food'),
									('prayer','Prayer'),
									('washroom','Washroom'),
									('wait','Waiting for Customer'),
									('others','Others')],'Break Type')
	driver_type = fields.Selection(related='driver_id.driver_type', type="selection", selection=[('imc','IMC'),
		('sub','Sub Contractor')],string="Driver Type", store=True)
	duration = fields.Char('Break Duration')


	@api.model
	def create(self, vals):
		vals.update({'inbreak_timestamp':datetime.now()})
		result = super(Driver_break, self).create(vals)
		return result

	@api.multi
	def write(self, vals):
		if 'break_type' in vals:
			if vals['break_type'] == 'out':
				vals['outbreak_timestamp']= datetime.now()
		result = super(Driver_break, self).write(vals)
		return result

Driver_break()
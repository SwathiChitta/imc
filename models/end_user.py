from odoo import api, fields, models


class End_Users(models.Model):

	_inherit = 'res.partner'
	
	@api.model
	def default_country(self):
		country = self.env['res.country'].search([('code', '=', 'AE')]).id
		return country

	company = fields.Many2one('imc.clients','Company')
	nationality = fields.Char('Nationality')
	dob = fields.Date('Date of Birth')
	country_id = fields.Many2one('res.country', string='Country',default= default_country )

	
End_Users()


class Custom_email_tempate(models.Model):
	_inherit = "mail.template"

	@api.multi
	def send_mail(self, res_id, force_send=False, raise_exception=False, email_values=None):
		self.ensure_one()
		Mail = self.env['mail.mail']
		Attachment = self.env['ir.attachment']  # TDE FIXME: should remove dfeault_type from context

		# create a mail_mail based on values, without attachments
		values = self.generate_email(res_id)
		values['recipient_ids'] = [(4, pid) for pid in values.get('partner_ids', list())]
		values.update(email_values or {})
		attachment_ids = values.pop('attachment_ids', [])
		attachments = values.pop('attachments', [])
		# add a protection against void email_from
		if 'email_from' in values and not values.get('email_from'):
			values.pop('email_from')
		mail = Mail.create(values)

		# manage attachments
		for attachment in attachments:
			attachment_data = {
				'name': attachment[0],
				'datas_fname': attachment[0],
				'datas': attachment[1],
				'type': 'binary',
				'res_model': 'mail.message',
				'res_id': mail.mail_message_id.id,
			}
			attachment_ids.append(Attachment.create(attachment_data).id)
		if attachment_ids:
			values['attachment_ids'] = [(6, 0, attachment_ids)]
			mail.write({'attachment_ids': [(6, 0, attachment_ids)]})

		# if force_send:
		#     mail.send(raise_exception=raise_exception)
		return mail.id 

Custom_email_tempate()
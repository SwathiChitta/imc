from odoo import api, fields, models
from datetime import datetime
from O365 import Message
o365_auth = ('ops@motoringclub.com', 'ITPLAZA@906')
m = Message(auth=o365_auth)
from . import HTML

class Vehicle_maintenance(models.Model):

    _name = "vehicle.maintenance"

    service_type = fields.Selection([('oil change', 'Oil Change'),
                                    ('service forecast', 'Service Forecast'),
                            		('tyre change', 'Tyre Change'),
                                    ], 'Service Type')
    vehicle_id = fields.Many2one('vehicle.plate', 'Vehicle')
    service_date = fields.Date('Service Date')
    description = fields.Text('Description')

    mail_notified = fields.Boolean('Notified')

    @api.model
    def _maintenance_reminder(self):
        x = False
        table_data = HTML.Table(header_row=['Vehicle', 'Service Type', 'Description'])
        manager_mails_list = self.env['res.users'].search([('groups_id.name', '=', 'Manager'), ('groups_id.category_id.name', '=', 'Insurance Access Levels')])
       
        vehicles = self.search([('mail_notified','=',False),('service_date','=', datetime.now().date())])
        for each in vehicles:
        	table_data.rows.append([each.vehicle_id.vehicle_plate_no, each.service_type, each.description])

        if vehicles and manager_mails_list:
            for mail in manager_mails_list:
                body_html = "<p>Dear %s,</p><p>Below mentioned vehicles are due for service maintenance:</p><p>%s</p><p>Regards,</p><p>Team IMC.</p>" % (mail.name,table_data)
                m.setRecipients(mail.login)
                m.setSubject('Vehicles Due For Maintenance')
                m.setBodyHTML(body_html)
                x = m.sendMessage()

            if x:
                for each in vehicles: each.write({'mail_notified':True})

        return True
    


Vehicle_maintenance()
from odoo import api, fields, models
import datetime
from datetime import datetime, timedelta, date


class vehicles_menu(models.Model):

	_name = 'vehicles.menu'
	_rec_name = 'vehicle_type'

	vehicle_type = fields.Char('Vehicle Type')
	rel_id = fields.Many2one('membership.menu', 'Vehicle')
	vehicle_make = fields.Many2one('vehicles.master', 'Vehicle make')
	model = fields.Many2one('vehicle.model','Model')
	veh_reg_no = fields.Char('Veh reg no')
	expiry = fields.Char('Expiry')
	veh_type = fields.Char('Veh type')
	chassis_no = fields.Char('Chassis no')
vehicles_menu()


class vehicles_master(models.Model):

	_name = 'vehicles.master'

	name = fields.Char('Make Name', required=True)

	# @api.model
	# def _check_name(self):
	#     print self.search([('name', 'ilike', self.name)])
	#     if self.search([('name', 'ilike', self.name)]):
	#         return False
	#     return True

	# _constraints = [
	#         (_check_name, 'Make name already exists.', ['name'])
	# ]

vehicles_master()


class Vehicle_type(models.Model):

	_name = "vehicle.type"

	_rec_name = 'name'

	name = fields.Char("vehicle Type")

Vehicle_type()


class VehicleMake(models.Model):
	_name = 'make.info'

	name = fields.Char('Name')


class VehicleModel(models.Model):
	_name = 'model.info'

	name = fields.Char('Name')
	make_id = fields.Many2one('make.info', 'Make')


# for mobile app

class VehicleMakeMobileapp(models.Model):
	_name = 'make.info.mob'

	name = fields.Char('Name', required="1")

	_sql_constraints = [
		('name_uniq', 'UNIQUE (name)',  'You can not have two makes with the same name !')
	]


class VehicleModelMobileapp(models.Model):
	_name = 'model.info.mob'

	name = fields.Char('Name')
	make_id = fields.Many2one('make.info.mob', 'Make')

	_sql_constraints = [('unique_model', 'unique(make_id, name)', 'Cannot Use same model name for this Make!')]


from odoo import api, fields, models
from datetime import datetime

class Driver_Checkins(models.Model):

	_name ='driver.checkins'

	_rec_name = 'driver'

	_order = 'checkin_time desc'
	
	driver = fields.Many2one('drivers.menu','Driver')
	plate_number = fields.Many2one('vehicle.plate','Plate Number')
	checkin_time = fields.Datetime('Checkin Time', default=datetime.now())
	checkout_time = fields.Datetime('Checkout Time')
	starting_km = fields.Float('Starting KM')
	ending_km = fields.Float('Ending KM')

	@api.multi
	def write(self, values):
		values.update({'checkout_time': datetime.now()})
		result = super(Driver_Checkins, self).write(values)
		return result
Driver_Checkins()	
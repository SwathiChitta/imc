from odoo import api, fields, models

class Typechange_Records(models.Model):
	_name="typechange.records"
	
	
	driver = fields.Many2one('drivers.menu','Driver')
	plate_number = fields.Many2one('vehicle.plate','Vehicle Number')
	checkin_time = fields.Datetime('Date and Time')
	place = fields.Text('Place')
	odometer_value = fields.Float('Odometer Reading')
	position_of_tyre = fields.Char('Position of Tyre')
	number_of_tyre = fields.Integer('Number of Tyres')
	reason_for_change = fields.Text('Reason for Tyre Change')


Typechange_Records()

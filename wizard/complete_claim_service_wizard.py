from odoo import api, fields, models,_

import logging
logger = logging.getLogger(__name__)

from O365 import Message
o365_auth = ('ops@motoringclub.com','ITPLAZA@906')
m = Message(auth=o365_auth)


class complete_claim_service(models.TransientModel):

	
	_name = "complete.claim.service"


	trip_sheet_number= fields.Char('Trip Sheet Number')
	reached_time= fields.Datetime('Reached Time')
	leaving_customer_location_time= fields.Datetime('Leaving Customer location Time')
	service_start_time= fields.Datetime('Service Start Time')
	service_end_time= fields.Datetime('Service End Time')
	starting_km= fields.Float('Starting KM')
	reached_km= fields.Float('Reached KM')
	ending_km= fields.Float('Ending KM')
	cost= fields.Float('Cost')
	closing_remarks= fields.Text('Closing Remarks')
	inv_amount= fields.Float('Invoice Amount')
	services_done=fields.Integer('Services Done')
	reached_customer_time=fields.Datetime('Reached Customer Location Time')
	reached_registration_center=fields.Datetime('Reached Registration Center Time')
	left_registration_center=fields.Datetime('Left Registration Center Time')
	difference=fields.Datetime('Reached Customer Location Time to dropp off car')
	fuel_level= fields.Char('Fuel Level')
	car_loading_time=fields.Datetime('Car Loading Time')
	hertz_km_reading=fields.Float('Hertz Car KM reading')
	hertz_fuel_reading=fields.Float('Hertz Car Fuel Level')

	service_name= fields.Char('Service Name')
	request_type= fields.Char('Request Type')

	# Tripsheet vehicle images
	checkin_veh_img1 = fields.Binary('Check-in Vehicle Image-1')
	checkin_veh_img2 = fields.Binary('Check-in Vehicle Image-2')
	checkin_veh_img3 = fields.Binary('Check-in Vehicle Image-3')
	checkin_veh_img4 = fields.Binary('Check-in Vehicle Image-4')
	checkin_veh_img5 = fields.Binary('Check-in Vehicle Image-5')
	checkin_veh_img6 = fields.Binary('Check-in Vehicle Image-6')

	checkout_veh_img1 = fields.Binary('Check-out Vehicle Image-1')
	checkout_veh_img2 = fields.Binary('Check-out Vehicle Image-2')
	checkout_veh_img3 = fields.Binary('Check-out Vehicle Image-3')
	checkout_veh_img4 = fields.Binary('Check-out Vehicle Image-4')
	checkout_veh_img5 = fields.Binary('Check-out Vehicle Image-5')
	checkout_veh_img6 = fields.Binary('Check-out Vehicle Image-6')

	# Remarks
	step1_remarks = fields.Text('Step 1 Remarks')
	step2_remarks = fields.Text('Step 2 Remarks')
	step3_remarks = fields.Text('Step 3 Remarks')
	step4_remarks = fields.Text('Step 4 Remarks')

	vehicle_km = fields.Float('Vehicle KM')
	checkitem_remarks1 = fields.Text('Closing Remarks 1')
	checkitem_remarks2 = fields.Text('Closing Remarks 2')
	
	invoice_number = fields.Char('Invoice Number')
	days_car_given = fields.Integer('Days Car Given')
	billed_days = fields.Integer('Billed Days')
	late_offhire_charge = fields.Float('Late Offhire Charge')
	delivery_charge = fields.Float('Delivery Charge')
	car_given_date = fields.Date('Car Given Date')
	car_returned_date = fields.Date('Car Returned Date')


	@api.model
	def default_get(self,default_fields):
		ret = super(complete_claim_service,self).default_get(default_fields)
		service = self._context.get('active_id',False)
		if service:
			rec =  self.env['claimed.service'].browse(service)
			ret['service_name'] = rec.service_name
			ret['request_type'] = rec.request_type
			ret['car_given_date'] = rec.car_given_date
			ret['car_returned_date'] = rec.car_returned_date
		return ret

	
	@api.multi
	def get_results(self):
		input_data = self.read()[0]
		context = self._context.copy()
		
		claim_obj = self.env['claimed.service'].browse(context.get(('active_id')))
		service_provider = claim_obj.assigned_party.id
		if service_provider:
			rec_id = self.env['conf.cost'].search([('name','=','3rd Party')])
			service_provider_cost = rec_id.cost
	
		driver = claim_obj.assigned_driver.id
		if driver:
			rec_id = self.env['conf.cost'].search([('name','=','imc')])
			driver_cost = rec_id.cost
		
		if 'type' in context and context['type'] == 'complete':
			input_data.update({'state': 'completed'})
		claim_obj.write(input_data)

		status = claim_obj.state
		if driver and status == "completed":
			claim_obj.write({'trip_sheet_number': self.env['ir.sequence'].next_by_code('trip.sheet')})
			# try:
			# 	m.setRecipients(self.env['res.users'].browse(each.id).email)
			# 	m.setSubject('Cancelled Services')
			# 	m.setBodyHTML(values['body_html'])
			# 	m.sendMessage()
				
			# except Exception as e:
			# 	logger.info("Error: %s;" % e)
		

		return True

	@api.onchange('starting_km', 'ending_km')
	def _onchange_kms(self):
		if self.starting_km and self.ending_km:
			self.total_kms = self.ending_km - self.starting_km 
			
complete_claim_service()

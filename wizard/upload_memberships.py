from odoo import api, fields, models, _, SUPERUSER_ID
from odoo.exceptions import UserError
import xlrd
import os
from xlrd import open_workbook
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
import xmlrpc.client
import logging
import socket
import time

logger = logging.getLogger(__name__)
from . import HTML
import re
from O365 import Message
from datetime import datetime

import multiprocessing

o365_auth = ('ops@motoringclub.com', 'ITPLAZA@906')
m = Message(auth=o365_auth)


def get_ip_address():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8", 80))
	return s.getsockname()[0]
	
username = 'admin' #the user
pwd = 'imcadmin'      #the password of the user
dbname = 'IMC11'    #the database


class UploadMemberships(models.TransientModel):
	_name = "upload.memberships"

	client_type = fields.Selection([('insurance','Insurance'),('bank','Bank')], 'Client Type')
	upload_document = fields.Binary('Upload Document')
	filename = fields.Char('Filename')

	def Mail_regarding_memberships_updation(self, admin_mail_list, new_uploaded, updated, table_data, filename):
		delivery = True
		body_html = "<p>Dear Admin,</p><p>Following are the modifications and errors in %s memberships data:</p> <table style='border:1px solid black;'><tr style='border:1px solid black;'><th  style='border:1px solid black;text-align:center;padding:5 5 5 5;'>Created Memberships</th><td  style='border:1px solid black;text-align:center;padding:5 5 5 5;'>%s</td></tr><tr style='border:1px solid black;'><th  style='border:1px solid black;text-align:center;padding:5 5 5 5;'>Updated Memberships</th><td  style='border:1px solid black;text-align:center;padding:5 5 5 5;'>%s</td></tr><tr style='border:1px solid black;'><th  style='border:1px solid black;text-align:center;padding:5 5 5 5;'>Duplicates Identified</th><td  style='border:1px solid black;text-align:center;padding:5 5 5 5;'>%s</td></tr></table>" % (
			filename, new_uploaded, updated, updated)
		body_html = body_html + '<br>' + str(table_data)
		for each in admin_mail_list:
			m.setRecipients(each)
			m.setSubject('Uploaded Memberships Notification')
			m.setBodyHTML(body_html)
			val = m.sendMessage()
			delivery = delivery * val
		return delivery

	def _update_membership(self, vals, membership_id, sock, uid):
		sock.execute(dbname, uid, pwd, 'membership.menu', 'write', membership_id, vals)

	def _create_membership(self, vals, sock, uid):
		sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)

	def phone_format(self, mobile):
		clean_phone_number = re.sub('[^0-9]+', '', str(mobile))
		formatted_phone_number = re.sub("(\d)(?=(\d{3})+(?!\d))", r"\1-", "%d" % int(clean_phone_number[:-1])) + \
								 clean_phone_number[-1]
		return formatted_phone_number

	@api.multi
	def upload_master_memberships(self):
		table_data_error = ''

		admin_mail_list = []
		new_uploaded = 0
		updated = 0
		admin_list = self.env['res.users'].search(
			[('groups_id.name', '=', 'Admin'), ('groups_id.category_id.name', '=', 'Insurance Access Levels')]).ids
		for i in admin_list: admin_mail_list += [str(self.env['res.users'].browse(i).email)]

		mail_vals = {}
		ip = get_ip_address()
		URL = "http://" + ip + ":8069/xmlrpc/common"
		sock_common = xmlrpc.client.ServerProxy(URL, transport=None,
												encoding=None, verbose=False, allow_none=False,
												use_datetime=False, context=None)
		uid = sock_common.login(dbname, username, pwd)
		sock = xmlrpc.client.ServerProxy("http://" + ip + ":8069/xmlrpc/" + 'object')
		# table_data = HTML.Table(header_row=['Customer Name', 'Membership No.', 'Policy No.', 'Chassis No.'])
		table_data = HTML.Table(header_row=['Error Row no.', 'Error Description'])

		input_data = self.read()[0]
		filename = input_data['filename']
		if '.xls' in filename or '.xlsx' in filename:
			if 'cancel' in filename.lower():
				status = 'cancel'
			else:
				status = 'activate'
		else:
			raise Warning(_('Please choose .xls/.xlsx files only!!'))

		if input_data['client_type'] == 'insurance':
			possible_strings = ['Policy Number', 'Client Name', 'Branch', 'Policy Type', 'Customer Name',
								'Policy Issue Date', 'Policy Start Date', 'Policy End Date', 'Customer Contact #',
								'Chassis #', 'Car Make', 'Car Model', 'Car Mfg Yr', 'Car Plate #', 'Membership #','Road Side Assistance','Rent a Car', 'Rent a car Category', 'Rent a car Days']

			xl_workbook = open_workbook(file_contents=base64.decodestring(input_data['upload_document']))

			sheet_names = xl_workbook.sheet_names()
			start_time = time.time()
			try:
				for each in sheet_names:
					actual_row, policy_col, client_col, cmp_branch_col, memtype_col, cust_col, issue_col, from_col, to_col, mobile_col, chassis_col, vehmake_col, vehmodel_col, mfyear_col, plateno_col, mem_no_col, rac, rac_cat, rac_days, month_col, year_col, agency_repair_col = False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False
					policy_col, client_col, cmp_branch_col, memtype_col, cust_col, issue_col, from_col, to_col, mobile_col, chassis_col, vehmake_col, vehmodel_col, mfyear_col, plateno_col, mem_no_col = False, False, False, False, False, False, False, False, False, False, False, False, False, False, False
					road_side_assistance_col = []
					xl_sheet = xl_workbook.sheet_by_name(each)

					for row in xrange(0, xl_sheet.nrows):
						for col in xrange(0, xl_sheet.ncols):

							if xl_sheet.cell(row, col).value in possible_strings: actual_row = row + 1

							if xl_sheet.cell(row, col).value in ['Policy Number', ]:
								policy_col = col
							elif xl_sheet.cell(row, col).value in ['Client Name']:
								client_col = col
							elif xl_sheet.cell(row, col).value in ['Branch']:
								cmp_branch_col = col
							elif xl_sheet.cell(row, col).value in ['Policy Type', ]:
								memtype_col = col
							elif xl_sheet.cell(row, col).value in ['Customer Name']:
								cust_col = col
							elif xl_sheet.cell(row, col).value in ['Policy Issue Date']:
								issue_col = col
							elif xl_sheet.cell(row, col).value in ['Policy Start Date']:
								from_col = col
							elif xl_sheet.cell(row, col).value in ['Policy End Date']:
								to_col = col
							elif xl_sheet.cell(row, col).value in ['Customer Contact #']:
								mobile_col = col
							elif xl_sheet.cell(row, col).value in ['Chassis #']:
								chassis_col = col
							elif xl_sheet.cell(row, col).value in ['Car Make']:
								vehmake_col = col
							elif xl_sheet.cell(row, col).value in ['Car Model', ]:
								vehmodel_col = col
							elif xl_sheet.cell(row, col).value in ['Car Mfg Yr']:
								mfyear_col = col
							elif xl_sheet.cell(row, col).value in ['Car Plate #']:
								plateno_col = col
							elif xl_sheet.cell(row, col).value in ['Membership #']:
								mem_no_col = col
							elif xl_sheet.cell(row, col).value in ['Rent a Car']:
								rac = col
							elif xl_sheet.cell(row, col).value in ['Rent a car Category']:
								rac_cat = col
							elif xl_sheet.cell(row, col).value in ['Rent a car Days']:
								rac_days = col
							elif xl_sheet.cell(row, col).value in ['Month']:
								month_col = col
							elif xl_sheet.cell(row, col).value in ['Year']:
								year_col = col
							elif xl_sheet.cell(row, col).value in ['Agency Repair']:
								agency_repair_col = col
							elif xl_sheet.cell(row, col).value in ['Road Side Assistance']:
								road_side_assistance_col = col
							else:
								pass

					for row in xrange(actual_row, xl_sheet.nrows):
						try:
							policy_fromdate, policy_todate = None, None
							policy_no, client_name, client_branch, mem_type, customer_name, issue_date, policy_from, policy_to, mobile, chassis_no, vehicle_make, vehicle_model, mfg_year, car_plate, membership_no, rac_elg, rac_category, rac_elgible_days, month, year, agency_repair = False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False
							road_side_assistance = False
							vals = {'status': status}
							era_servs, concierge_servs, rentacar_servs, transport_servs, carreg_servs = [], [], [], [], []
							vals.update({'era_available_services': era_servs,
														 'concierge_available_services': concierge_servs,
														 'carreg_available_services': carreg_servs,
														 'rentacar_available_services': rentacar_servs,
														 'transport_available_services': transport_servs})
							if policy_col: policy_no = xl_sheet.cell(row, int(policy_col)).value
							if client_col: client_name = xl_sheet.cell(row, int(client_col)).value
							if cmp_branch_col: client_branch = xl_sheet.cell(row, int(cmp_branch_col)).value
							if memtype_col: mem_type = xl_sheet.cell(row, int(memtype_col)).value
							if cust_col: customer_name = xl_sheet.cell(row, int(cust_col)).value
							if issue_col: issue_date = xl_sheet.cell(row, issue_col).value
							if from_col: policy_from = xl_sheet.cell(row, from_col).value
							if to_col: policy_to = xl_sheet.cell(row, to_col).value
							if mobile_col: mobile = xl_sheet.cell(row, mobile_col).value
							if chassis_col: chassis_no = xl_sheet.cell(row, chassis_col).value
							if vehmake_col: vehicle_make = xl_sheet.cell(row, vehmake_col).value
							if vehmodel_col: vehicle_model = xl_sheet.cell(row, vehmodel_col).value
							if mfyear_col: mfg_year = xl_sheet.cell(row, mfyear_col).value
							if plateno_col: car_plate = xl_sheet.cell(row, plateno_col).value
							if mem_no_col: membership_no = xl_sheet.cell(row, int(mem_no_col)).value
							if rac: rac_elg = xl_sheet.cell(row, rac).value
							if rac_cat: rac_category = xl_sheet.cell(row, rac_cat).value
							if rac_days: rac_elgible_days = xl_sheet.cell(row, rac_days).value
							if month_col: month = xl_sheet.cell(row, month_col).value
							if year_col: year = xl_sheet.cell(row, year_col).value
							if year_col: year = xl_sheet.cell(row, year_col).value
							if agency_repair_col: agency_repair = xl_sheet.cell(row, agency_repair_col).value
							if road_side_assistance_col: road_side_assistance = xl_sheet.cell(row, road_side_assistance_col).value

							if rac_elg:
								if 'yes' in str(rac_elg).lower():
									vals.update({'rac': 'yes'})

									if rac_category:
										vals.update({'type_of_car': str(rac_category)})

									if rac_elgible_days:
										if type(rac_elgible_days) == float:
											rac_elgible_days = str(rac_elgible_days).split('.')[0]
										vals.update({'how_many_days': str(rac_elgible_days)})
								else:
									vals.update({'rac': 'no'})

							if road_side_assistance:
								if 'yes' in str(road_side_assistance).lower():
									vals.update({'road_side_asst': 'yes'})
								elif 'no' in str(road_side_assistance).lower():
									vals.update({'road_side_asst': 'no'})

							# if client_name:
							# 	category_ids = []
							# 	client = self.env['imc.clients'].search([('name','ilike', str(client_name).strip())]).ids
							# 	if client:
							# 		vals.update({'company_id': client[0]})

							if mobile:
								if type(mobile) == float: mobile = str(mobile).split('.')[0]
								# mobile = self.phone_format(mobile)
								vals.update({'contact_number': str(mobile).strip()})
							else:
								mobile = ''

							if year:
								if type(year) == float: year = str(year).split('.')[0]
								vals.update({'uploaded_year': str(year)})

							if agency_repair:
								if str(agency_repair).lower() == 'yes':
									agency_repair = 'Y'
								else:
									agency_repair = 'N'
								vals.update({'repair_inside_agency': agency_repair})

							if month:
								vals.update({'month': str(month)})

							if mfg_year: vals.update({'year': str(int(mfg_year))})

							if membership_no:
								if type(membership_no) == float: membership_no = str(membership_no).strip().split('.')[0]
								vals.update({'membership_no': str(membership_no)})
								# if membership_no:
								# 	mail_vals.update({'membership_no': str(membership_no)})
							# else:
							# 	mail_vals.update({'membership_no': '-'})

							if chassis_no:
								if type(chassis_no) == float: chassis_no = str(chassis_no).strip().split('.')[0]
								vals.update({'chassis_no': str(chassis_no)})
								# if chassis_no:
								# 	mail_vals.update({'chassis_no': str(chassis_no)})
								# else:
								# 	mail_vals.update({'chassis_no': '-'})
							else:
								chassis_no = ''

							if car_plate:
								if type(car_plate) == float: car_plate = str(car_plate).split('.')[0]
								vals.update({'car_plate': str(car_plate)})

							if customer_name:
								name = str(customer_name).strip()
								mobile = str(mobile).strip()

								# if mobile:
								customer = self.env['res.partner'].search([('mobile', '=', mobile),('name', '=', name)]).ids
								# elif name:
								# 	customer = self.env['res.partner'].search([('name', '=', name)]).ids
								
								if not customer:
									# customer = self.env['res.partner'].create(
									# 	{'name': name, 'mobile': mobile}).id
									customer = sock.execute(dbname, uid, pwd, 'res.partner', 'create', {'name': name, 'mobile': mobile})
								else:
									if type(customer) == list:
										customer = customer[0]
							else:
								customer = False
							vals.update({'end_user': customer})
								# mail_vals.update({'cust_name': '-'})

							if vehicle_make:
								make = self.env['vehicles.master'].search([('name', '=', str(vehicle_make).strip())]).ids
								if not make:
									# make_id = self.env['vehicles.master'].create({'name': str(vehicle_make).strip()}).id
									make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(vehicle_make).strip()})
								else:
									make_id = make[0]

							else:
								make_id = False
							vals.update({'vehicle_make': make_id})

							if vehicle_model:
								if type(vehicle_model) == float: vehicle_model = str(vehicle_model).strip().split('.')[0]
								model = self.env['vehicle.model'].search(
									[('make_id', '=', make_id), ('model', '=', str(vehicle_model))]).ids
								if not model:
									# model_id = self.env['vehicle.model'].create(
									# 	{'make_id': make_id, 'model': str(vehicle_model)}).id
									model_id = sock.execute(dbname, uid, pwd, 'vehicle.model', 'create', {'make_id': make_id, 'model': str(vehicle_model)})
								else:
									model_id = model[0]
								vals.update({'model': model_id})
							else:
								vals.update({'model': False})


							if issue_date:
								if type(issue_date) == str:
									issued_date = datetime.strptime(str(issue_date).strip(), '%d/%m/%Y')
								elif type(issue_date) == float:
									issued_date = (xlrd.xldate.xldate_as_datetime(issue_date, xl_workbook.datemode)).date()
								elif type(issue_date) == unicode:
									issue_date = str(issue_date)
									issued_date = datetime.strptime(issue_date, '%d/%m/%Y')
								else:
									issued_date = False
								vals.update({'issued_date': str(issued_date)})

							if policy_from:
								if type(policy_from) == str:
									policy_fromdate = datetime.strptime(str(policy_from).strip(), '%d/%m/%Y')
								elif type(policy_from) == float:
									policy_fromdate = (
										xlrd.xldate.xldate_as_datetime(policy_from, xl_workbook.datemode)).date()
								else:
									policy_fromdate = False
								if policy_fromdate: vals.update({'start_date': str(policy_fromdate)})

							if policy_to:
								if type(policy_to) == str:
									policy_todate = datetime.strptime(str(policy_to).strip(), '%d/%m/%Y')
								elif type(policy_to) == float:
									policy_todate = (xlrd.xldate.xldate_as_datetime(policy_to, xl_workbook.datemode)).date()
								elif type(policy_to) == unicode:
									policy_todate = datetime.strptime(str(policy_to).strip(), '%d/%m/%Y')
								else:
									policy_todate = False

								if policy_todate:
									if policy_todate < datetime.now().date():
										if status != 'cancel':
											vals.update({'status': 'expired'})
								vals.update({'expiry_date': str(policy_todate)})

							if policy_no:
								if type(policy_no) == float:
									policy_no = str(policy_no).strip().split('.')[0]
								vals.update({'type': 'insurance', 'insurance_number': str(policy_no).strip()})

								# if policy_no:
								# 	mail_vals.update({'policy_no': str(policy_no)})
								# else:
								# 	mail_vals.update({'policy_no': '-'})

								membership = self.env['membership.menu'].search(
									[('insurance_number', '=', str(policy_no).strip()), ('chassis_no', '=', str(chassis_no)),('expiry_date','=', policy_todate)])

								if not membership:
									# membership = self.env['membership.menu'].create(vals)

									if mem_type:

										type_ids = self.env['service.type'].search([('name', 'ilike', str(mem_type).strip())]).ids
										if client_name:
											membership_types = self.env['membership.services'].search(
												[('category_id', 'in', type_ids), ('client_id.name', 'ilike', str(client_name).strip())])
											vals.update({'membership_type': membership_types[0].category_id.id, 'company_id': membership_types[0].client_id.id})
											if membership_types:
												rex = membership_types[0]

												for i in rex.era_available_services:
													era_servs += [(0, 0,
																   {'request_type': i.request_type, 'service_id': i.service_id.id,
																	'unlimited': i.unlimited,
																	'number_of_services': i.number_of_services})]

												for i in rex.concierge_available_services:
													concierge_servs += [(0, 0, {'request_type': i.request_type,
																				'service_id': i.service_id.id,
																				'unlimited': i.unlimited,
																				'number_of_services': i.number_of_services})]

												for i in rex.carreg_available_services:
													carreg_servs += [(0, 0, {'request_type': i.request_type,
																			 'service_id': i.service_id.id,
																			 'unlimited': i.unlimited,
																			 'number_of_services': i.number_of_services})]

												for i in rex.rentacar_available_services:
													rentacar_servs += [(0, 0, {'request_type': i.request_type,
																			   'service_id': i.service_id.id,
																			   'unlimited': i.unlimited,
																			   'number_of_services': i.number_of_services})]

												for i in rex.transport_available_services:
													transport_servs += [(0, 0, {'request_type': i.request_type,
																				'service_id': i.service_id.id,
																				'unlimited': i.unlimited,
																				'number_of_services': i.number_of_services})]

											vals.update({'era_available_services': era_servs,
														 'concierge_available_services': concierge_servs,
														 'carreg_available_services': carreg_servs,
														 'rentacar_available_services': rentacar_servs,
														 'transport_available_services': transport_servs})
									membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
									# p1 = multiprocessing.Process(target=self._create_membership, args=(vals, sock, uid))
									# p1.start()
									# p1.join()
									# table='membership_menu'    
									# columns_string= '('+','.join(vals.keys())+')'    
									# values_string = '('+','.join(map(str,vals.values()))+')'    
									# sql = """INSERT INTO %s %s VALUES %s"""%(table, columns_string,values_string)
									# self.env.cr.execute(sql)
									# logger.info("SQL: %s;" % sql)
									new_uploaded += 1
									# if mail_vals:
									# 	table_data.rows.append(
									# 		[str(mail_vals['cust_name']), str(mail_vals['membership_no']),
									# 		 str(mail_vals['policy_no']), str(mail_vals['chassis_no'])])

								else:
									eras, concs, carregs, tranports, rntacar = [], [], [], [], []
									test = []
									# if policy_todate and policy_fromdate:
									# 	if str(membership[0].expiry_date) == str(policy_todate) and str(
									# 			membership[0].start_date) == str(policy_fromdate):
									# 		pass
									# 	else:
									# 		self.env['membership.history'].create({
									# 			'membership_id': membership[0].id,
									# 			'start_date': datetime.strptime(str(membership[0].start_date), '%Y-%m-%d'),
									# 			'expiry_date': datetime.strptime(str(membership[0].expiry_date),
									# 											 '%Y-%m-%d'),
									# 		})
									# membership_write = self.env['membership.menu'].browse(membership[0].id)
									membership_write = membership[0]

									# membership_era = membership_write.era_available_services.ids
									# membership_con = membership_write.concierge_available_services.ids
									# membership_carreg = membership_write.carreg_available_services.ids
									# membership_rentcar = membership_write.rentacar_available_services.ids
									# membership_transport = membership_write.transport_available_services.ids

									# era_ones = vals.get('era_available_services', [])
									# con_ones = vals.get('concierge_available_services', [])
									# carreg_ones = vals.get('carreg_available_services', [])
									# rentcar_ones = vals.get('rentacar_available_services', [])
									# transport_ones = vals.get('transport_available_services', [])

									# for i, j in [(membership_era, era_ones), (membership_con, con_ones),
									# 			 (membership_carreg, carreg_ones), (membership_rentcar, rentcar_ones),
									# 			 (membership_transport, transport_ones)]:

									# 	if i == membership_era and j == era_ones:
									# 		model, test = 'era.avail.services', eras
									# 	elif i == membership_con and j == con_ones:
									# 		model, test = 'concierge.avail.services', concs
									# 	elif i == membership_carreg and j == carreg_ones:
									# 		model, test = 'carreg.avail.services', carregs
									# 	elif i == membership_rentcar and j == rentcar_ones:
									# 		model, test = 'rentacar.avail.services', rntacar
									# 	elif i == membership_transport and j == transport_ones:
									# 		model, test = 'transport.avail.services', tranports
									# 	else:
									# 		pass

									# 	# if len(j) != 0:
									# 	if len(i) < len(j):
									# 		for mem_serv in i:
									# 			for each in j[0:len(i)]:
									# 				if j.index(each) == i.index(mem_serv):
									# 					test += [(1, mem_serv, each[2])]
									# 		for rest in j[len(i):]:
									# 			test += [(0, 0, rest[2])]
									# 	elif len(i) > len(j):
									# 		for mem_serv in i[0:len(j)]:
									# 			for each in j:
									# 				if j.index(each) == i.index(mem_serv):
									# 					test += [(1, mem_serv, each[2])]
									# 		self.env[model].browse(i[len(j):]).unlink()
									# 	else:
									# 		for mem_serv in i:
									# 			for each in j:
									# 				if j.index(each) == i.index(mem_serv):
									# 					test += [(1, mem_serv, each[2])]

									vals.pop('era_available_services', None)
									vals.pop('concierge_available_services', None)
									vals.pop('carreg_available_services', None)
									vals.pop('rentacar_available_services', None)
									vals.pop('transport_available_services', None)
									# vals.update({'era_available_services': eras, 'concierge_available_services': concs,
									# 			 'carreg_available_services': carregs,
									# 			 'rentacar_available_services': rntacar,
									# 			 'transport_available_services': tranports})
									# membership_write.write(vals)
									sock.execute(dbname, uid, pwd, 'membership.menu', 'write', membership_write.id, vals)
									# p2 = multiprocessing.Process(target=self._update_membership, args=(vals, membership_write.id, sock,uid, ))
									# p2.start()
									# p2.join()
									# format_string = ','.join(['%s'] * len(vals))
									# sql = "INSERT IGNORE INTO membership_menu ({0}) VALUES ({1})".format(", ".join(vals.keys()),format_string),(vals.values())
									# logger.info("SQL: %s;" %sql)
									# self.env.cr.execute(sql)
									updated += 1

									# if mail_vals:
									# 	table_data.rows.append(
									# 		[str(mail_vals['cust_name']), str(mail_vals['membership_no']),
									# 		 str(mail_vals['policy_no']), str(mail_vals['chassis_no'])])
								logger.info("Insurance Row: %s;" % row)


						except Exception as e:
							logger.exception("Error: %s;" % e)
							logger.info("Insurance Row: %s;" % row)
							table_data.rows.append([row,e])
							table_data_error += 'row - '+ str(row) +':'+str(e) + ', '
							pass
				logger.info("Time Taken: %s seconds;" % ((time.time() - start_time)))
				try:
					if admin_mail_list  and (new_uploaded or updated):
						ret = self.Mail_regarding_memberships_updation(admin_mail_list, new_uploaded, updated, table_data, filename)
						if ret:

							self.env['membership.upload.log'].create({'error':table_data_error,'datetime':datetime.now(),'created':new_uploaded,'updated':updated,'duplicates':updated,'mail':'Mail Sent'})
							res_id = self.env['display.data'].create({'created': new_uploaded, 'updated': updated})
							return {
								'name': 'New Wizard Info',
								"view_mode": 'form',
								'res_model': 'display.data',
								'res_id': res_id.id,
								'type': 'ir.actions.act_window',
								'target': 'new',
							}

						else:
							self.env['membership.upload.log'].create({'error':table_data_error,'datetime':datetime.now(),'created':new_uploaded,'updated':updated,'duplicates':updated,'mail':'Mail Delivery Failed'})
							res_id = self.env['display.data'].create({'error':"Mail delivery Failed. Please check your mail server.",'created': new_uploaded, 'updated': updated})
							return {
								'name': 'New Wizard Info',
								"view_mode": 'form',
								'res_model': 'display.data',
								'res_id': res_id.id,
								'type': 'ir.actions.act_window',
								'target': 'new',
							}
				except Exception as e:
					logger.exception("Error: %s;" % e)
					return True

			except Exception as e:
				logger.exception("Error: %s;" % e)
				return True

		if input_data['client_type'] == 'bank':
			possible_strings = ['Mailing Address','Cif Id' ,'Plan','Company name','CRN#','Date  of Activation', 'Card Number (First Four & last four digits)', 'Contact # 1','Contact # 2', 'Contact # 3','Contact # 4','Contact # 5','Address','Name','ref #','Card Number (First Four & last four digits)','Customer Reference', 'Customer Full Name', 'Package Type', 'Date Of Birth', 'EMAIL',
								'Mobile No', 'Enrollment Date', 'Signatory Name 1', 'Signatory Name 2', 'Signatory Name 3', 'Signatory Name 4', 'Signatory Name 5', 'PO Box']

			xl_workbook = open_workbook(file_contents=base64.decodestring(input_data['upload_document']))

			sheet_names = xl_workbook.sheet_names()
			try:
				for each in sheet_names:
					actual_row = 0
					po_box_col, activate_date_col, f4_card_col, add_col, f6_card_col, ref_col, cust_name_col, pkg_type_col, dob_col, email_col, mob_col, enrl_col, sign_1_col, sign_2_col, sign_3_col, sign_4_col, sign_5_col = False, False,  False, False, False, False, False, False, False, False, False, False, False, False, False, False, False
					cont1_col, cont2_col, cont3_col, cont4_col, cont5_col = False, False, False, False, False
					xl_sheet = xl_workbook.sheet_by_name(each)

					for row in xrange(0, xl_sheet.nrows):
						for col in xrange(0, xl_sheet.ncols):
							if xl_sheet.cell(row, col).value in possible_strings:
								actual_row = row + 1
							if xl_sheet.cell(row, col).value in ['Customer Reference','ref #', 'CRN#', 'Cif Id']:
								ref_col = str(col)
							elif xl_sheet.cell(row, col).value in ['Card Number (First Six & last four digits)']:
								f6_card_col = str(col)
							elif xl_sheet.cell(row, col).value in ['Date  of Activation']:
								activate_date_col = str(col)
							elif xl_sheet.cell(row, col).value in ['Card Number (First Four & last four digits)']:
								f4_card_col = str(col)
							elif xl_sheet.cell(row, col).value in ['Customer Name', 'Customer Full Name', 'Name', 'Company name']:
								cust_name_col = col
							elif xl_sheet.cell(row, col).value in ['Current Package Type', 'Plan']:
								pkg_type_col = col
							elif xl_sheet.cell(row, col).value in ['Date Of Birth']:
								dob_col = col
							elif xl_sheet.cell(row, col).value in ['EMAIL']:
								email_col = col
							elif xl_sheet.cell(row, col).value in ['Address', 'Mailing Address']:
								add_col = col
							elif xl_sheet.cell(row, col).value in ['Mobile Number', 'Mobile No']:
								mob_col = col
							elif xl_sheet.cell(row, col).value in ['Enrollment Date', 'Enrollment date']:
								enrl_col = col
							elif xl_sheet.cell(row, col).value in ['Signatory Name 1']:
								sign_1_col = col
							elif xl_sheet.cell(row, col).value in ['Signatory Name 2']:
								sign_2_col = col
							elif xl_sheet.cell(row, col).value in ['Signatory Name 3']:
								sign_3_col = col
							elif xl_sheet.cell(row, col).value in ['Signatory Name 4']:
								sign_4_col = col
							elif xl_sheet.cell(row, col).value in ['Signatory Name 5']:
								sign_5_col = col

							elif xl_sheet.cell(row, col).value in ['Contact # 1']:
								cont1_col = col
							elif xl_sheet.cell(row, col).value in ['Contact # 2']:
								cont2_col = col
							elif xl_sheet.cell(row, col).value in ['Contact # 3']:
								cont3_col = col
							elif xl_sheet.cell(row, col).value in ['Contact # 4']:
								cont4_col = col
							elif xl_sheet.cell(row, col).value in ['Contact # 5']:
								cont5_col = col
							elif xl_sheet.cell(row, col).value in ['PO Box', 'Po Box']:
								po_box_col = col
							else:
								pass
					for row in xrange(actual_row, xl_sheet.nrows):
						try:
							membership = []
							signees = []
							# contacts = []
							membership_types = []
							era_servs, concierge_servs, rentacar_servs, transport_servs, carreg_servs = [], [], [], [], []
							activate_date, cont_1, cont_2, cont_3, cont_4, cont_5,address,f4_card_no, f6_card_no,ref_no, customer_name, pkg_type, dob, email, mobile, enrl_date, sign_1, sign_2, sign_3, sign_4, sign_5 = False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False
							po_box = ''
							vals = {'status': 'activate', 'type': 'bank'}
							if ref_col:
								ref_no = xl_sheet.cell(row, int(ref_col)).value
								if type(ref_no) == float: ref_no = str(ref_no).strip().split('.')[0]
								vals.update({'membership_no': str(ref_no).strip('')})

							if activate_date_col:
								activate_date = xl_sheet.cell(row, int(activate_date_col)).value
								if type(activate_date) == str:
									activate_date = datetime.strptime(activate_date, '%d/%m/%Y')
								elif type(activate_date) == float:
									activate_date = (xlrd.xldate.xldate_as_datetime(activate_date, xl_workbook.datemode)).date()
								elif type(activate_date) == unicode:
									activate_date = str(activate_date)
									activate_date = datetime.strptime(activate_date, '%d/%m/%Y')
								vals.update({'issued_date': activate_date})

							if f6_card_col:
								f6_card_no = xl_sheet.cell(row, int(f6_card_col)).value
								if f6_card_no:
									if type(f6_card_no) == float: f6_card_no = str(f6_card_no).strip().split('.')[0]
									membership_types = self.env['membership.services'].search([('bin_numbers.number','=', str(f6_card_no).strip()[:6])])
									vals.update({'credit_card_number': str(f6_card_no).strip('')})
							if f4_card_col:
								f6_card_no = xl_sheet.cell(row, int(f4_card_col)).value
								if f6_card_no:
									if type(f6_card_no) == float: f6_card_no = str(f6_card_no).strip().split('.')[0]
									membership_types = self.env['membership.services'].search([('bin_numbers.number','=', str(f6_card_no).strip()[:4])])
									vals.update({'credit_card_number': str(f6_card_no).strip('')})
							if sign_1_col:
								sign_1 = xl_sheet.cell(row, int(sign_1_col)).value
								if sign_1:
									signees.append(str(sign_1).strip())
							if sign_2_col:
								sign_2 = xl_sheet.cell(row, int(sign_2_col)).value
								if sign_2:
									signees.append(str(sign_2).strip())
							if sign_3_col:
								sign_3 = xl_sheet.cell(row, int(sign_3_col)).value
								if sign_3:
									signees.append(str(sign_3).strip())
							if sign_4_col:
								sign_4 = xl_sheet.cell(row, int(sign_4_col)).value
								if sign_4:
									signees.append(str(sign_4).strip())
							if sign_5_col:
								sign_5 = xl_sheet.cell(row, int(sign_5_col)).value
								if sign_5:
									signees.append(str(sign_5).strip())

							if cont1_col:
								cont_1 = xl_sheet.cell(row, int(cont1_col)).value
								if cont_1:
									# contacts.append(str(cont_1).strip(' ').strip('.0'))
									vals.update({'contact_number': str(cont_1).strip(' ').strip('.0')})
							if cont2_col:
								cont_2 = xl_sheet.cell(row, int(cont2_col)).value
								if cont_2:
									# contacts.append(str(cont_2).strip(' ').strip('.0'))
									vals.update({'contact_1': str(cont_2).strip(' ').strip('.0')})
							# if cont3_col:
							# 	cont_3 = xl_sheet.cell(row, int(cont3_col)).value
							# 	if cont_3:
							# 		contacts.append(str(cont_3).strip(' ').strip('.0'))
							# if cont4_col:
							# 	cont_4 = xl_sheet.cell(row, int(cont4_col)).value
							# 	if cont_4:
							# 		contacts.append(str(cont_4).strip(' ').strip('.0'))
							# if cont5_col:
							# 	cont_5 = xl_sheet.cell(row, int(cont5_col)).value
							# 	if cont_5:
							# 		contacts.append(str(cont_5).strip(' ').strip('.0'))
							if dob_col:
								dob = xl_sheet.cell(row, int(dob_col)).value
								if type(dob) == str:
									dob = datetime.strptime(dob, '%d/%m/%Y')
								elif type(dob) == float:
									dob = (xlrd.xldate.xldate_as_datetime(dob, xl_workbook.datemode)).date()
								elif type(dob) == unicode:
									dob = str(dob)
									dob = datetime.strptime(dob, '%d/%m/%Y')
								else:
									dob = False

							if email_col:
								email = xl_sheet.cell(row, int(email_col)).value
								vals.update({'email': str(email)})
							if add_col:
								address = xl_sheet.cell(row, int(add_col)).value
							if mob_col:
								mobile = xl_sheet.cell(row, int(mob_col)).value
								vals.update({'contact_number': str(mobile)})

							if po_box_col:
								po_box = str(xl_sheet.cell(row, int(po_box_col)).value)

							if cust_name_col:
								customer_name = xl_sheet.cell(row, int(cust_name_col)).value
								if customer_name:
									customer = self.env['res.partner'].search([('name', '=', str(customer_name).strip()),('dob','=',dob)],limit=1).id
									if not customer:
										# customer = self.env['res.partner'].create(
										# 	{'name': str(customer_name), 'mobile': str(mobile), 'dob': dob, 'email': str(email)}).id
										customer = sock.execute(dbname, uid, pwd, 'res.partner', 'create', {'name': str(customer_name).strip(), 'mobile': mobile, 'street': address, 'zip': po_box.strip()})
									
									# for contact in contacts:
										# if not self.env['res.partner'].search([('name', '=', str(customer_name)),('mobile','=',contact),('parent_id','=',customer)]):
										# 	sock.execute(dbname, uid, pwd, 'res.partner', 'create', {'name': customer_name, 'mobile': contact, 'parent_id': customer, 'type': 'contact'})
									vals.update({'end_user': customer})
							if pkg_type_col:
								pkg_type = xl_sheet.cell(row, int(pkg_type_col)).value
								if pkg_type:
									membership_types = self.env['membership.services'].search(
										[('category_id.name', '=', str(pkg_type).strip())])
							if membership_types:
								vals.update({'membership_type': membership_types[0].category_id.id, 'company_id': membership_types[0].client_id.id})
								rex = membership_types[0]

								for i in rex.era_available_services:
									era_servs += [(0, 0,
												   {'request_type': i.request_type, 'service_id': i.service_id.id,
													'unlimited': i.unlimited,
													'number_of_services': i.number_of_services})]

								for i in rex.concierge_available_services:
									concierge_servs += [(0, 0, {'request_type': i.request_type,
																'service_id': i.service_id.id,
																'unlimited': i.unlimited,
																'number_of_services': i.number_of_services})]

								for i in rex.carreg_available_services:
									carreg_servs += [(0, 0, {'request_type': i.request_type,
															 'service_id': i.service_id.id,
															 'unlimited': i.unlimited,
															 'number_of_services': i.number_of_services})]

								for i in rex.rentacar_available_services:
									rentacar_servs += [(0, 0, {'request_type': i.request_type,
															   'service_id': i.service_id.id,
															   'unlimited': i.unlimited,
															   'number_of_services': i.number_of_services})]

								for i in rex.transport_available_services:
									transport_servs += [(0, 0, {'request_type': i.request_type,
																'service_id': i.service_id.id,
																'unlimited': i.unlimited,
																'number_of_services': i.number_of_services})]

							vals.update({'era_available_services': era_servs,
										 'concierge_available_services': concierge_servs,
										 'carreg_available_services': carreg_servs,
										 'rentacar_available_services': rentacar_servs,
										 'transport_available_services': transport_servs})
				
							if enrl_col:
								enrl_date = xl_sheet.cell(row, int(enrl_col)).value
								if type(enrl_date) == str:
									issued_date = datetime.strptime(enrl_date, '%d/%m/%Y')
								elif type(enrl_date) == float:
									issued_date = (xlrd.xldate.xldate_as_datetime(enrl_date, xl_workbook.datemode)).date()
								elif type(enrl_date) == unicode:
									enrl_date = str(enrl_date)
									issued_date = datetime.strptime(enrl_date, '%d/%m/%Y')
								else:
									issued_date = False
								vals.update({'issued_date': str(issued_date)})

							if ref_no:
								membership = self.env['membership.menu'].search(
										[('membership_no', '=', str(ref_no).strip()),('end_user','=', vals['end_user'])])
							if po_box:
								membership = self.env['membership.menu'].search(
										['|',('end_user.zip', '=', po_box.strip()),('end_user','=', vals['end_user']),('membership_type','=',vals['membership_type'])])
							if not membership:
								# membership = self.env['membership.menu'].create(vals)
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								for i in signees:
									signatory = self.env['membership.signatories'].search([('name', 'ilike', str(i)),('membership_id','=', membership)])
									if not signatory:
										sock.execute(dbname, uid, pwd, 'membership.signatories', 'create', {'name': str(i), 'membership_id': membership})
								# 	if not customer:
								# 		customer = self.env['res.partner'].create(
								# 			{'name': str(i)}).id
								# 	else:
								# 		customer = customer[0]
								# 	vals.update({'end_user': customer})
								# 	membership = self.env['membership.menu'].create(vals)
								new_uploaded += 1

							else:
								eras, concs, carregs, tranports, rntacar = [], [], [], [], []
								test = []
								for mem in membership:
									membership_write = mem

									membership_era = membership_write.era_available_services.ids
									membership_con = membership_write.concierge_available_services.ids
									membership_carreg = membership_write.carreg_available_services.ids
									membership_rentcar = membership_write.rentacar_available_services.ids
									membership_transport = membership_write.transport_available_services.ids

									era_ones = vals.get('era_available_services', [])
									con_ones = vals.get('concierge_available_services', [])
									carreg_ones = vals.get('carreg_available_services', [])
									rentcar_ones = vals.get('rentacar_available_services', [])
									transport_ones = vals.get('transport_available_services', [])

									for i, j in [(membership_era, era_ones), (membership_con, con_ones),
												 (membership_carreg, carreg_ones), (membership_rentcar, rentcar_ones),
												 (membership_transport, transport_ones)]:

										if i == membership_era and j == era_ones:
											model, test = 'era.avail.services', eras
										elif i == membership_con and j == con_ones:
											model, test = 'concierge.avail.services', concs
										elif i == membership_carreg and j == carreg_ones:
											model, test = 'carreg.avail.services', carregs
										elif i == membership_rentcar and j == rentcar_ones:
											model, test = 'rentacar.avail.services', rntacar
										elif i == membership_transport and j == transport_ones:
											model, test = 'transport.avail.services', tranports
										else:
											pass

										# if len(j) != 0:
										if len(i) < len(j):
											for mem_serv in i:
												for each in j[0:len(i)]:
													if j.index(each) == i.index(mem_serv):
														test += [(1, mem_serv, each[2])]
											for rest in j[len(i):]:
												test += [(0, 0, rest[2])]
										elif len(i) > len(j):
											for mem_serv in i[0:len(j)]:
												for each in j:
													if j.index(each) == i.index(mem_serv):
														test += [(1, mem_serv, each[2])]
											self.env[model].browse(i[len(j):]).unlink()
										else:
											for mem_serv in i:
												for each in j:
													if j.index(each) == i.index(mem_serv):
														test += [(1, mem_serv, each[2])]

									vals.pop('era_available_services', None)
									vals.pop('concierge_available_services', None)
									vals.pop('carreg_available_services', None)
									vals.pop('rentacar_available_services', None)
									vals.pop('transport_available_services', None)
									vals.update({'era_available_services': eras, 'concierge_available_services': concs,
												 'carreg_available_services': carregs,
												 'rentacar_available_services': rntacar,
												 'transport_available_services': tranports})
									# membership_write.write(vals)
									# vals2 = vals.copy()
									sock.execute(dbname, uid, pwd, 'membership.menu', 'write', membership_write.id, vals)
									updated += 1
									for i in signees:
										signatory = self.env['membership.signatories'].search([('name', 'ilike', str(i)),('membership_id','=', membership_write.id)])
										if not signatory:
											sock.execute(dbname, uid, pwd, 'membership.signatories', 'create', {'name': str(i), 'membership_id': membership_write.id})
							logger.info("Bank Row: %s;" % row)
						except Exception as e:
							logger.exception("Error: %s;" % e)
							table_data.rows.append([row])
							table_data_error += 'row - '+ str(row) +':'+str(e) + ', '

							pass
				try:
					if admin_mail_list and (new_uploaded or updated):

						ret = self.Mail_regarding_memberships_updation(admin_mail_list, new_uploaded, updated, table_data, filename)
						
						if ret:
							self.env['membership.upload.log'].create({'error':table_data_error,'datetime':datetime.now(),'created':new_uploaded,'updated':updated,'duplicates':updated,'mail':'Mail Sent'})
							res_id = self.env['display.data'].create({'created': new_uploaded, 'updated': updated})
							return {
								'name': 'Upload Info',
								"view_mode": 'form',
								'res_model': 'display.data',
								'res_id': res_id.id,
								'type': 'ir.actions.act_window',
								'target': 'new',
							}
						else:
							self.env['membership.upload.log'].create({'error':table_data_error,'datetime':datetime.now(),'created':new_uploaded,'updated':updated,'duplicates':updated,'mail':'Mail Delivery Failed'})
							res_id = self.env['display.data'].create({'error':"Mail delivery Failed. Please check your mail server.",'created': new_uploaded, 'updated': updated})
							return {
								'name': 'Upload Info',
								"view_mode": 'form',
								'res_model': 'display.data',
								'res_id': res_id.id,
								'type': 'ir.actions.act_window',
								'target': 'new',
							}

				except Exception as e:
					logger.exception("Error: %s;" % e)
					pass
			except Exception as e:
				logger.exception("Error: %s;" % e)
				pass
		return True


class Display_wizard_data(models.TransientModel):
	_name = "display.data"

	created = fields.Char('Created Memberships')
	updated = fields.Char('Updated Memberships')
	error = fields.Text('Error Message')


Display_wizard_data()

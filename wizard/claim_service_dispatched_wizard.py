from odoo import api, fields, models, _

# import HTML
import datetime, time
from datetime import date
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError
from pyfcm import FCMNotification
import json
import socket
import random
import string
import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

from O365 import Message
o365_auth = ('ops@motoringclub.com', 'ITPLAZA@906')
m = Message(auth=o365_auth)

def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]

push_service = FCMNotification(
    api_key="AAAAerQCSIc:APA91bEBUnZdEu-lOzXsLEn4OXR0Vy-jUNyGugziAd9mP7mk-q2FbmfJbuPmQaLKVLQ9NalcOCfCiRfvqI17OshHI90hPSDcwVypkX5HwnPJr9f5M8Hjhz2385vzAX7CPW3b7TeSCajN")


class dispatched_claim_service(models.TransientModel):
    _name = "dispatched.claim.service"

    # @api.model
    # def default_get(self, fields):
    #     yesterday = datetime.now() - timedelta(1)
    #     res = super(dispatched_claim_service, self).default_get(fields)

    #     if 'assigned_drivers' in fields:
    #         assigned = self.env['drivers.menu'].search([('status', '=', 'engaged'),('gcm_dev_id', '!=', False),('gcm_updated_time', '>=', str(yesterday))]).ids
    #         if assigned: res.update({'assigned_drivers': len(assigned), 'drivers_assigned': [(6, 0, assigned)]})

    #     if 'free_drivers' in fields:
    #         busy_drivers = []
    #         current_services = self.env['claimed.service'].search([('state', 'not in',
    #                                                                 ['requested', 'feedback', 'service-provided',
    #                                                                  'completed', 'cancelled', 'rejected']),
    #                                                                ('assigned_driver', '!=', False)])
    #         for i in current_services:
    #             busy_drivers.append(i.assigned_driver.id)
    #         free = self.env['drivers.menu'].search([('id', 'not in', busy_drivers),('gcm_dev_id', '!=', False),('gcm_updated_time', '>=', str(yesterday))]).ids
    #         if free: res.update({'free_drivers': len(free), 'drivers_free': [(6, 0, free)]})

    #     if 'services_count' in fields:
    #         # services = self.env['claimed.service'].search([('assigned_driver', '!=', False)]).ids
    #         res.update({'services_count': len(current_services)})

    #     return res

    user = fields.Many2one('drivers.menu', 'Driver', domain="[('driver_type','=','imc')]")
    manual_completion = fields.Boolean('Service will be manually completed ?')
    sub_cont_driver = fields.Many2one('drivers.menu', 'Sub Contractor Driver', domain="[('driver_type','=','sub')]")
    service_provider_id = fields.Many2one('service.providers', 'Service Provider')
    driver_type = fields.Selection([('imc', 'IMC'),
                                    ('3rd Party', 'Sub Contractor')], 'Driver Type', default="imc")

    free_drivers = fields.Integer("Free Drivers")
    assigned_drivers = fields.Integer("Assigned Drivers")
    services_count = fields.Integer("No. of Services")
    drivers_assigned = fields.Many2many('drivers.menu', 'rel_to_drivers', 'assigned_drivers', 'name',
                                        "Assigned Drivers")
    drivers_free = fields.Many2many('drivers.menu', 'rel_to_free_drivers', 'assigned_drivers', 'name', "Free Drivers")
    type = fields.Selection([('bank', 'Bank'),
                             ('insurance', 'Insurance Company'),
                             ('others', 'Others')], "Type")
    company = fields.Many2one('imc.clients','Company')

    @api.onchange('driver_type', 'service_provider_id')
    def _onchange_driver_type(self):

        yesterday = datetime.now() - timedelta(1)
        if self.driver_type == '3rd Party' and self.service_provider_id:
            assigned_drivers = []
            free_drivers = []
            busy_drivers = []

            sub_contractor = self.service_provider_id.id
            all_drivers = self.service_provider_id.driver_ids

            current_services = self.env['claimed.service'].search([('state', 'not in',
                                                                    ['requeted', 'feedback', 'service-provided',
                                                                     'completed', 'cancelled', 'rejected']),
                                                                   ('assigned_party', '=', sub_contractor)])
            if current_services:
                for i in current_services:
                    if i.assigned_party_driver and i.assigned_party_driver.gcm_dev_id:
                        assigned_drivers.append(i.assigned_party_driver.id)

            for driver in all_drivers:
                if assigned_drivers:
                    if driver.driver_type == 'sub' and driver.id not in assigned_drivers: free_drivers += [driver.id]
                else:
                    free_drivers += [driver.id]

            self.services_count = len(current_services)

            self.assigned_drivers = len(assigned_drivers)
            self.drivers_assigned = [(6, 0, assigned_drivers)]
            self.free_drivers = len(free_drivers)
            self.drivers_free = [(6, 0, free_drivers)]

        else:
            busy_drivers = []
            current_services = self.env['claimed.service'].search([('state', 'not in',
                                                                    ['requeted', 'feedback', 'service-provided',
                                                                     'completed', 'cancelled', 'rejected']),
                                                                   ('assigned_driver', '!=', False)])
            for i in current_services: busy_drivers.append(i.assigned_driver.id)
            free = self.env['drivers.menu'].search([('driver_type', '=', 'imc'), ('id', 'not in', busy_drivers),('gcm_dev_id', '!=', False),('gcm_updated_time', '>=', str(yesterday))]).ids
            if free:
                self.free_drivers = len(free)
                self.drivers_free = [(6, 0, free)]

            self.services_count = len(current_services)

            assigned = self.env['drivers.menu'].search([('driver_type', '=', 'imc'),('gcm_dev_id', '!=', False),('gcm_updated_time', '>=', str(yesterday)),('id', 'not in', free)]).ids
            if assigned:
                self.assigned_drivers = len(assigned)
                self.drivers_assigned = [(6, 0, assigned)]

    @api.multi
    def get_results(self):
        input_data = self.read()[0]
        email = ''
        dispatch_vals = {'state': 'dispatched', 'dispatch_mail_tosend': 1,
                         'dispatched_time': datetime.now(), 'manual_completion': input_data['manual_completion']}  # updating dict
        context = self._context.copy()
        claim_obj = self.env['claimed.service'].browse(context['active_id'])

        if input_data['driver_type'] == 'imc':
            dispatch_vals.update({'driver_type': 'imc'})
            dispatch_vals.update({'assigned_driver': input_data['user'][0]})
            driver_obj = self.env['drivers.menu'].browse(input_data['user'][0])

            # break check
            break_ids = self.env["driver.break"].search([('driver_id', '=', driver_obj.id)]).ids
            if break_ids:
                last_id = break_ids and max(break_ids)
                if self.env["driver.break"].browse(last_id).break_type == 'in':
                    raise UserError(
                        _('%s is on break ! Please wait for sometime or try with another driver') % (driver_obj.name))

            data_message = {
                'data': json.dumps({
                    'Job_id': claim_obj.id,
                    'Title': "New Job Received"
                })
            }
            if not input_data['manual_completion']:
                result = push_service.notify_single_device(registration_id=str(driver_obj.gcm_dev_id),
                                                           data_message=data_message)

                body_html = "<p>Dear %s,</p><p>Following are the details of service that is assigned to you:</p><p><strong>Service Type & Name: %s/%s </strong></p><p><strong>Customer Name: %s </strong></p><p><strong>Contact: %s</strong></p><p><strong>Pickup Location: %s</strong></p><p><strong>Dropoff Location: %s</strong></p><p>Team IMC</p>"%(driver_obj.name,str(claim_obj.request_type).upper(), claim_obj.service.name, claim_obj.customer.name,claim_obj.contact, claim_obj.pickup_locations,claim_obj.dropoff_locations)

                m.setRecipients(driver_obj.related_user.login)
                m.setSubject('Service has been Assigned')
                m.setBodyHTML(body_html)
                m.sendMessage()
        else:
            rec_id = input_data['service_provider_id'][0]
            dispatch_vals.update({'driver_type': '3rd Party'})
            service_provider = self.env['service.providers'].browse(rec_id)
            if service_provider.email_id:
                email = service_provider.email_id
            else:
                raise UserError(_('Please provide an email id for service provider.'))

            # Sub contracto check
            sub_cont_driver = input_data.get('sub_cont_driver', False)
            if not sub_cont_driver:
                dispatch_vals.update({'assigned_party': input_data['service_provider_id'][0]})
                gcm_dev_id = str(service_provider.gcm_dev_id)
            else:
                dispatch_vals.update({'assigned_party_driver': input_data['sub_cont_driver'][0]})
                sub_driver_obj = self.env['drivers.menu'].browse(input_data['sub_cont_driver'][0])
                break_ids = self.env["driver.break"].search([('driver_id', '=', sub_driver_obj.id)]).ids
                if break_ids:
                    last_id = break_ids and max(break_ids)
                    if self.env["driver.break"].browse(last_id).break_type == 'in':
                        raise UserError(_('%s is on break ! Please wait for sometime or try with another driver') % (
                            sub_driver_obj.name))

                gcm_dev_id = str(sub_driver_obj.gcm_dev_id)

            data_message = {
                'data': json.dumps({
                    'Job_id': claim_obj.id,
                    'Title': "New Job Received"
                })
            }
            if not input_data['manual_completion']:
                result = push_service.notify_single_device(registration_id=gcm_dev_id, data_message=data_message)
                dispatch_vals.update({'dispatch_reminder_time': datetime.now() + timedelta(minutes=20)})
                allowed_chars = ''.join((string.ascii_letters, string.digits))
                unique_id = ''.join(random.choice(allowed_chars) for _ in range(64))
                dispatch_vals.update({'token': unique_id})
                # body_html = "<p>Dear %s,</p><p>Following are the details of service that is assigned to you:</p><p><strong>Service Type & Name: %s/%s </strong></p><p><strong>Customer Name: %s </strong></p><p><strong>Contact: %s</strong></p><p><strong>Pickup Location: %s</strong></p><p><strong>Dropoff Location: %s</strong></p><p>Team IMC</p>"%(sub_driver_obj.name,str(claim_obj.request_type).upper(), claim_obj.service.name, claim_obj.customer.name,claim_obj.contact, claim_obj.pickup_locations,claim_obj.dropoff_locations)
                ip = get_ip_address()
                URL = "http://" + ip + ":8069"

                # getting user timezone
                local_tz = pytz.timezone(self.env.user.tz or pytz.utc)

                body_html = '''<!DOCTYPE html>
                        <html>
                        <head>
                            <title>IMC</title>
                            <style>
                                .container{
                                    max-width: 1000px;
                                    margin: auto;
                                    background: #fff;
                                    min-height: 150px;
                                    padding: 25px;
                                    padding-bottom: 100px;
                                }
                                img{
                                    margin-top: 50px !important;;
                                    margin: auto;
                                    display: table;
                                }
                                p{
                                    text-align: justify;
                                    line-height: 28px;
                                    font-size: 20px;
                                    padding: 25px;
                                }
                                body{
                                    padding: 50px;
                                    background: #f5f5f5;
                                }
                                .buttons{
                                    margin: auto;
                                    display: table;
                                }
                                .button1{
                                    padding: 15px 20px;
                                    background: #28A745;
                                    color: #fff;
                                    text-decoration: none;
                                    letter-spacing: 1px;
                                    margin-right: 25px;
                                }
                                .button2{
                                    padding: 15px 20px;
                                    background: #DC3545;
                                    color: #fff;
                                    text-decoration: none;
                                    letter-spacing: 1px;
                                    margin-right: 25px;
                                }
                            </style>
                        </head>
                        <body>
                            <div class="container">
                                <img src="http://www.motoringclub.com/images/logo.png" alt="">
                                <p>
                                    Dear User,<br> Below service has been assigned to you. Please click on Accept to accept the service or Decline to reject it.<br>

                                    <b>Service Type :</b> %s (%s)<br>
                                    <b>Customer:</b> %s<br>
                                    <b>Contact:</b> %s<br>
                                    <b>Date and Time of Service:</b> %s<br>


                                </p>
                                <div class="buttons">
                                    <a class="button1" href="%s/accept/%s/%s">ACCEPT</a>
                                    <a class="button2" href="%s/reject/%s/%s">DECLINE</a>
                                </div>
                            </div>
                        </body>
                        </html>''' %(str(claim_obj.request_type).upper(), claim_obj.service.name, claim_obj.customer.name,claim_obj.contact,datetime.strftime(pytz.utc.localize(datetime.strptime(claim_obj.scheduled_date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(local_tz),"%Y-%m-%d %H:%M:%S") , URL, claim_obj.id, dispatch_vals['token'], URL, claim_obj.id, dispatch_vals['token'])


                m.setRecipients(email)
                m.setSubject('New Service has been Assigned')
                m.setBodyHTML(body_html)
                m.sendMessage()
        claim_obj.write(dispatch_vals)

        if claim_obj.request_type == 'concierge':
            view_ref = self.env['ir.model.data'].get_object_reference('imc', 'concierge_test_form_view')
        elif claim_obj.request_type == 'era':
            view_ref = self.env['ir.model.data'].get_object_reference('imc', 'era_test_form_view')
        elif claim_obj.request_type == 'rent-a-car':
            view_ref = self.env['ir.model.data'].get_object_reference('imc', 'rent_a_car_test_form_view')
        elif claim_obj.request_type == 'registration':
            view_ref = self.env['ir.model.data'].get_object_reference('imc', 'car_regis_test_form_view')
        elif claim_obj.request_type == 'hertz':
            view_ref = self.env['ir.model.data'].get_object_reference('imc', 'hertz_test_form_view')
        else:
            view_ref = self.env['ir.model.data'].get_object_reference('imc', 'transportation_test_form_view')

        view_id = view_ref[1] if view_ref else False
        return {
            'name': 'Claimed Services',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': view_id,
            'res_model': 'claimed.service',
            'res_id': claim_obj.id,
            'type': 'ir.actions.act_window',
            'context': context,
            'target': 'current',
            'tag': 'reload',
        }


dispatched_claim_service()


class claim_service_dialog(models.TransientModel):
    _name = 'claim.service.dialog'

    @api.multi
    def claim_service(self):
        vals = {}
        sub_model = False
        active_model = self._context.get('active_model', None)

        if str(active_model) == 'concierge.avail.services':
            rec = self.env['concierge.avail.services'].browse(self._context.get('active_id', False))
            sub_model = 'concierge.avail.services'

        if str(active_model) == 'era.avail.services':
            rec = self.env['era.avail.services'].browse(self._context.get('active_id', False))
            sub_model = 'era.avail.services'

            if str(rec.membership_id.road_side_asst) == 'no':
                raise UserError(_('This membership is not eligible for ERA services!!'))

        if str(active_model) == 'rentacar.avail.services':
            rec = self.env['rentacar.avail.services'].browse(self._context.get('active_id', False))
            sub_model = 'rentacar.avail.services'

            if str(rec.membership_id.rac) == 'no':
                raise UserError(_('This membership is not eligible for Rent A Car services!!'))

        if str(active_model) == 'hertz.avail.services':
            rec = self.env['hertz.avail.services'].browse(self._context.get('active_id', False))
            sub_model = 'hertz.avail.services'

        if str(active_model) == 'carreg.avail.services':
            rec = self.env['carreg.avail.services'].browse(self._context.get('active_id', False))
            sub_model = 'carreg.avail.services'
      
        if str(active_model) == 'transport.avail.services':
            rec = self.env['transport.avail.services'].browse(self._context.get('active_id', False))
            sub_model = 'transport.avail.services'

        if rec.service_id.request_type == 'hertz':

            if not rec.membership_id.chassis_no:
                raise UserError(_('Incomplete vehicle details!!'))

        if rec.membership_id.status in ['expired', 'cancel']:
            raise UserError(_('Membership should be in Active/Temp status!!'))

        vals = {
            'default_customer': rec.membership_id.end_user.id,
            'default_service': rec.service_id.id,
            'default_membership_id': rec.membership_id.id,
            'default_contact': rec.membership_id.contact_number,
            'default_insurance_number': rec.membership_id.insurance_number,
            'default_membership_type': rec.membership_id.membership_type.id,
            'default_type': rec.membership_id.type,
            'default_date_time': datetime.now(),
            'default_service_name': rec.service_id.name + rec.service_id.request_type,
        }

        if rec.service_id.name + rec.service_id.request_type in ['Third Partyrent-a-car','Car Replacementrent-a-car']:
            vals.update({'default_days_eligible':rec.membership_id.how_many_days})

        if rec.service_id.request_type in ['rent-a-car', 'registration'] or rec.service_id.name in ['Airport Pick up',
                                                                                                    'Airport Drop']: vals.update(
            {'calling_team': 1})

        services = self.env['claimed.service'].search_count([('membership_id', '=', rec.membership_id.id)])
        if rec.membership_id.status == 'temp' and services >= 1:
            raise UserError(
                _('A service has already been claimed. To Claim other services please activate the membership!!'))
        else:
            pass

        if rec.service_id.request_type == 'concierge':
            view_id = self.env['ir.ui.view'].search([('name', '=', 'concierge.test.form.view')])[0]

        elif rec.service_id.request_type == 'era':
            view_id = self.env['ir.ui.view'].search([('name', '=', 'era.test.form.view')])[0]
            if rec.type == 'insurance':
                vals.update({'default_vehicle_make': rec.membership_id.vehicle_make.id,
                             'default_car_plate': rec.membership_id.car_plate,
                             'default_car_plate_state': rec.membership_id.car_plate_state,
                             'default_car_plate_code': rec.membership_id.car_plate_code,
                             'default_veh_type': rec.membership_id.veh_type,
                             'default_model': rec.membership_id.model.id,
                             'default_year': rec.membership_id.year,
                             'default_chassis_no': rec.membership_id.chassis_no,
                             })

            if rec.type == 'bank':
                vehicles = self.env['membership.vehicle.info'].search(
                    [('membership_id', '=', rec.membership_id.id), ('status', '=', 'active')])
                if not vehicles:
                    raise UserError(_('No vehicles added!! Please add atleast 1 active vehicle'))

                vehicle = vehicles[0]
                vals.update({'default_vehicle_make': vehicle.make_id.id,
                             'default_car_plate': vehicle.car_plate,
                             'default_car_plate_state': rec.membership_id.car_plate_state,
                             'default_car_plate_code': rec.membership_id.car_plate_code,
                             'default_veh_type': vehicle.veh_type,
                             'default_model': vehicle.model_id.id,
                             'default_year': vehicle.mfg_year, 'default_chassis_no': vehicle.chassis_no,
                             })

        elif rec.service_id.request_type == 'rent-a-car':
            if rec.membership_id.rac == 'yes':
                vals.update({
                    'days_eligible': int(rec.membership_id.how_many_days)
                })
            view_id = self.env['ir.ui.view'].search([('name', '=', 'rent.acar.test.form.view')])[0]

        elif rec.service_id.request_type == 'registration':

            if rec.type == 'bank':
                vehicles = self.env['membership.vehicle.info'].search(
                    [('membership_id', '=', rec.membership_id.id), ('status', '=', 'active')])
                if not vehicles:
                    raise UserError(_('No vehicles added!! Please add atleast 1 active vehicle'))

                vehicle = vehicles[0]
                vals.update({'default_vehicle_make': vehicle.make_id.id,
                             'default_car_plate': vehicle.car_plate,
                             'default_car_plate_state': rec.membership_id.car_plate_state,
                             'default_car_plate_code': rec.membership_id.car_plate_code,
                             'default_veh_type': vehicle.veh_type,
                             'default_model': vehicle.model_id.id,
                             'default_year': vehicle.mfg_year, 'default_chassis_no': vehicle.chassis_no,
                             })

            else:

                vals.update({'default_vehicle_make': rec.membership_id.vehicle_make.id,
                             'default_car_plate': rec.membership_id.car_plate,
                             'default_car_plate_state': rec.membership_id.car_plate_state,
                             'default_car_plate_code': rec.membership_id.car_plate_code,
                             'default_veh_type': rec.membership_id.veh_type,
                             'default_model': rec.membership_id.model.id,
                             'default_year': rec.membership_id.year, 'default_chassis_no': rec.membership_id.chassis_no,
                             })
            view_id = self.env['ir.ui.view'].search([('name', '=', 'car.regis.test.form.view')])[0]

        elif rec.service_id.request_type == 'hertz':
            view_id = self.env['ir.ui.view'].search([('name', '=', 'hertz.test.form.view')])[0]

        elif rec.service_id.request_type == 'transportation':
            view_id = self.env['ir.ui.view'].search([('name', '=', 'transportation.test.form.view')])[0]
            if rec.type == 'insurance':
                vals.update({'default_vehicle_make': rec.membership_id.vehicle_make.id,
                             'default_car_plate': rec.membership_id.car_plate,
                             'default_car_plate_state': rec.membership_id.car_plate_state,
                             'default_car_plate_code': rec.membership_id.car_plate_code,
                             'default_veh_type': rec.membership_id.veh_type,
                             'default_model': rec.membership_id.model.id,
                             'default_year': rec.membership_id.year, 'default_chassis_no': rec.membership_id.chassis_no,
                             })

            if rec.type == 'bank':
                vehicles = self.env['membership.vehicle.info'].search(
                    [('membership_id', '=', rec.membership_id.id), ('status', '=', 'active')])
                if not vehicles:
                    raise UserError(_('No vehicles added!! Please add atleast 1 active vehicle'))

                vehicle = vehicles[0]
                vals.update({'default_vehicle_make': vehicle.make_id.id,
                             'default_car_plate': vehicle.car_plate,
                             'default_car_plate_state': rec.membership_id.car_plate_state,
                             'default_car_plate_code': rec.membership_id.car_plate_code,
                             'default_veh_type': vehicle.veh_type,
                             'default_model': vehicle.model_id.id,
                             'default_year': vehicle.mfg_year, 'default_chassis_no': vehicle.chassis_no,
                             })

        else:
            view_id = self.env['ir.ui.view'].search([('name', '=', 'claimed.service.form.view')])[0]

        vals.update({'form_view_initial_mode': 'edit', 'force_detailed_view': 'true','sub_id':rec.id,'sub_model':sub_model})
        # res_id = self.env['claimed.service'].create(vals)
        return {
            'name': 'Service Request',
            'view_mode': 'form',
            # 'view_type': 'form',
            'view_id': view_id.id,
            'res_model': 'claimed.service',
            # 'res_id': res_id.id,
            'type': 'ir.actions.act_window',
            'target': 'current',
            # opening form in edit mode
            # 'flags': {'form': {'options': {'mode': 'edit'}}},
            'context': vals,
            # 'flags': {'initial_mode': 'edit'},
        }


        
from odoo import api, fields, models,_
import datetime
from datetime import datetime,timedelta
import time
from openerp.exceptions import except_orm, Warning, RedirectWarning


def date_range(start, end):
    r = (datetime.strptime(end, "%Y-%m-%d")-datetime.strptime(start, "%Y-%m-%d")+timedelta(days=1)).days
    return [datetime.strptime(start, "%Y-%m-%d")+timedelta(days=i) for i in range(r)]


class Subcontractors_report(models.TransientModel):

	_name = "subcontractors.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	contractor = fields.Many2one('service.providers','SubContractor')
	request_type = fields.Selection([('concierge', 'Concierge'),
                                     ('era', 'ERA'),
                                     ('rent-a-car', 'Rent a Car'),
                                     ('registration', 'Car Registration'),
                                     ('transportation', 'Transportation'),
                                     ('hertz', 'Hertz'),
                                     ], 'Request Type')

	@api.multi
	def print_subcontractors_report(self):
		vals = self.read()[0]
		context = self._context
		datas = {'form': vals}
		
		return {'type': 'ir.actions.report','report_name': 'imc.subcontractors_report','report_type':"qweb-pdf",'data': datas,}

	@api.multi
	def print_subcontractors_excel_report(self):
		vals = self.read()[0]
		context = self._context
		request_type = False

		if vals['contractor']: contractor = vals['contractor'][0]
		if vals['request_type']: request_type = vals['request_type']

		if contractor and request_type:
			services = self.env['claimed.service'].search([('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date']),('request_type','=',request_type),('assigned_party','=',contractor)]).ids
		else:
			services = self.env['claimed.service'].search([('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date']),('assigned_party','=',contractor)]).ids

		
		tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.view')])[0]
		
		return {
			'name': 'Claimed Services',
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services)],
			'views': [(tree.id, 'tree')],
		}


class subcontractors_report_wizard(models.AbstractModel):
	_name = 'report.imc.subcontractors_report'

	@api.multi
	def get_report_values(self, docids, data=None):
		data = dict(data or {})
		subcon = {}
		allservs = []
		dateList = date_range(data['form']['from_date'],data['form']['to_date'])
		
		for each in dateList:
			nextt = False
			if dateList.index(each)+1 < len(dateList): nextt = str(dateList[dateList.index(each)+1])
			else: nextt = str(dateList[-1])
			date = str(each)

			if data['form']['contractor'] : domain = [('assigned_party','=',data['form']['contractor'][0]),('scheduled_date','>=',date),('scheduled_date','<=',nextt)]
			else: domain = [('assigned_party','!=',False),('scheduled_date','>=',date),('scheduled_date','<=',nextt)]

			total = self.env['claimed.service'].search(domain)
			for ech in total:
				if ech.assigned_party:
					if ech.assigned_party in subcon: subcon[ech.assigned_party.name]+=[ech]
					else: subcon[ech.assigned_party.name]=[ech]

			for drv in subcon:
				inter,city,battery,flat,carreg,offroad=0,0,0,0,0,0
				for i in subcon[drv]:
					if i.service.name == 'Intercity Towing': inter += 1
					elif i.service.name in ['Citylimit Towing','City Limit Towing']: city += 1
					elif i.service.name == 'Battery Boosting': battery += 1
					elif i.service.name == 'Flat Tyre': flat += 1
					elif i.service.name == 'Registration Renewal': carreg += 1
					elif i.service.name == 'Off Road Recovery': offroad += 1
				allservs += [{'total':len(total),'date':each.date().strftime('%d-%m-%Y'), 'subcon':drv,'inter':inter, 'city':city, 'battery':battery, 'flat':flat,'carreg':carreg, 'offroad':offroad}]

		data.update({'final':allservs})
		return data



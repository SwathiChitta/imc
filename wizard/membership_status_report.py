from odoo import api, fields, models,_


class Membership_status_report(models.TransientModel):

	_name = "membership.status.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	status = fields.Selection([('activate', 'Active'),
							   ('inactive', 'Inactive (Temp, Expired, Cancelled)')], 'Status')
 
	@api.multi
	def print_memberships_status(self):
		vals = self.read()[0]
		status = vals['status']

		if status == 'activate':
			name = 'Active Memberships'
			memberships = self.env['membership.menu'].search([('created_datetime','>=',vals['from_date']),('created_datetime','<=',vals['to_date']),('status','=','activate')]).ids
		else:
			name = 'Inactive Memberships'
			memberships = self.env['membership.menu'].search([('created_datetime','>=',vals['from_date']),('created_datetime','<=',vals['to_date']),('status','!=','activate')]).ids
		
		tree = self.env['ir.ui.view'].search([('name', '=', 'membership.menu.tree.view')])[0]

		return {
			'name': name,
			'res_model': 'membership.menu',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', memberships)],
			'views': [(tree.id, 'tree')],
		}
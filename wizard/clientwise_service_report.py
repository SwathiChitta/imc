from odoo import api, fields, models,_
import datetime
from datetime import datetime
import time


class Clientwise_report_wizard(models.TransientModel):

	
	_name = "clientwise.report.wizard"
	
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	client = fields.Many2one('imc.clients','Client')
	request_type = fields.Selection([('concierge', 'Concierge'),
                                     ('era', 'ERA'),
                                     ('rent-a-car', 'Rent a Car'),
                                     ('registration', 'Car Registration'),
                                     ('transportation', 'Transportation'),
                                     ('hertz', 'Hertz'),
                                     ], 'Request Type')

	@api.multi
	def print_clientwise_report(self):
		vals = self.read()[0]
		context = self._context
		datas = {'form': vals}
		return {'type': 'ir.actions.report','report_name': 'imc.clientwise_report_mainid','report_type':"qweb-pdf",'data': datas,}

	@api.multi
	def print_clientwise_excel_report(self):
		services = []
		request_type = False
		vals = self.read()[0]
		context = self._context
		if 'request_type' in vals and vals['request_type']: request_type = vals['request_type']

		if 'client' in vals and vals['client']:
			if request_type: services = self.env['claimed.service'].search([('request_type','=',request_type),('insurance_company','=',vals['client'][0]),('scheduled_date','>=',vals['from_date'])]).ids
			else: services = self.env['claimed.service'].search([('insurance_company','=',vals['client'][0]),('scheduled_date','>=',vals['from_date'])]).ids
		else:
			if request_type: services = self.env['claimed.service'].search([('request_type','=',request_type),('scheduled_date','>=',vals['from_date'])]).ids
			else: services = self.env['claimed.service'].search([('scheduled_date','>=',vals['from_date'])]).ids

		tree = self.env['ir.ui.view'].search([('name', '=', 'clientwise.excel.report.view')])[0]

		return {
			'name': 'Claimed Services',
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services)],
			'views': [(tree.id, 'tree')],
		}

class all_services_report_wizard(models.AbstractModel):
	_name = 'report.imc.clientwise_report_mainid'

	@api.multi
	def get_report_values(self, docids, data=None):
		companies = {}
		domains = []
		data = dict(data or {})

		if data['form']['client']:
			domains += [[('insurance_company','=',data['form']['client'][0]),('scheduled_date','>=',data['form']['from_date']),('scheduled_date','<=',data['form']['to_date'])]]
			domains += [[('insurance_company_temp','=',data['form']['client'][0]),('scheduled_date','>=',data['form']['from_date']),('scheduled_date','<=',data['form']['to_date'])]]
		else:
			domains += [[('scheduled_date','>=',data['form']['from_date']),('scheduled_date','<=',data['form']['to_date'])]]

		for domain in domains:
			for each in self.env['claimed.service'].search(domain):
				
				if each.membership_id.membership_no: pol = each.membership_id.membership_no
				elif each.membership_id.insurance_number:  pol = each.membership_id.insurance_number
				else: pol ="None"

				if each.insurance_company: company = each.insurance_company.name
				elif each.insurance_company_temp: company = each.insurance_company_temp.name
				else:company = "None"
				
				if company in companies: 
					companies[company] += [{'client_name':company, 'service_date':datetime.strptime(each.scheduled_date,'%Y-%m-%d %H:%M:%S').date().strftime('%d-%m-%Y'), 'service_time':each.service_end_time, 'reached_time':each.reached_time, 'completed_time':each.service_time, 'policy_no':pol, 'customer_name':each.customer.name, 'mobile':each.contact, 'from_city':each.from_state_id.name, 'from_loc':each.from_location.name, 'to_city':each.to_state_id.name, 'to_loc':each.to_location.name, 'service_name':each.service.name, 'veh_make':each.vehicle_make.name, 'veh_model':each.model.model, 'veh_reg_no':each.car_plate, 'status':each.state}]
				else:
					companies.update({company:[{'client_name':company, 'service_date':datetime.strptime(each.scheduled_date,'%Y-%m-%d %H:%M:%S').date().strftime('%d-%m-%Y'), 'service_time':each.service_end_time, 'reached_time':each.reached_time, 'completed_time':each.service_time, 'policy_no':pol, 'customer_name':each.customer.name, 'mobile':each.contact, 'from_city':each.from_state_id.name, 'from_loc':each.from_location.name, 'to_city':each.to_state_id.name, 'to_loc':each.to_location.name, 'service_name':each.service.name, 'veh_make':each.vehicle_make.name, 'veh_model':each.model.model, 'veh_reg_no':each.car_plate, 'status':each.state}]})

		data.update({'companies':companies})
		return data



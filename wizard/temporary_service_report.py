from odoo import api, fields, models,_
# import datetime
# from datetime import datetime,timedelta

class Temporary_services_report(models.TransientModel):

	_name = "temp.services.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	request_type = fields.Selection([('concierge', 'Concierge'),
                                     ('era', 'ERA'),
                                     ('rent-a-car', 'Rent a Car'),
                                     ('registration', 'Car Registration'),
                                     ('transportation', 'Transportation')], 'Request Type')
	service = fields.Many2one('service.menu','Name',domain="[('request_type','=',request_type)]")

	@api.multi
	def print_allservices_excel_report(self):
		vals = self.read()[0]
		service= False
		allservs = []
		request_type = vals['request_type']
		if vals['service']: service = vals['service'][1]

		if request_type and service:
			services = self.env['claimed.service'].search([('temporary_user_id','!=',False),('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date']),('request_type','=',request_type),('service','=',service)]).ids
		else:
			services = self.env['claimed.service'].search([('temporary_user_id','!=',False),('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date']),('request_type','=',request_type)]).ids

		
		if request_type == 'rent-a-car':
			name = 'Rent A Car Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.rentacar')])[0]
		elif request_type == 'registration':
			name = 'Car Registration Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.registration')])[0]
		elif request_type == 'concierge':
			name = 'Concierge Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.concierge')])[0]
		elif request_type == 'era':
			name = 'ERA Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.era')])[0]
		elif request_type == 'transportation':
			name = 'Transportation Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.transportation')])[0]
		else:
			name = 'All Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.view')])[0]

		return {
			'name': name,
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services)],
			'views': [(tree.id, 'tree')],
		}
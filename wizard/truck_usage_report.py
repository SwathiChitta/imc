from odoo import api, fields, models,_

class Truck_usage_report(models.TransientModel):

	_name = "truck.usage.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	truck_id = fields.Many2one('vehicle.plate',"Truck")

	@api.multi
	def print_truck_usage_report(self):
		vals = self.read()[0]
	
		truck_id = vals['truck_id']

		services = self.env['claimed.service'].search([('driver_vehicle_id','=',truck_id[0]),('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date'])]).ids
	
		tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.view')])[0]

		return {
			'name': 'Truck Usage',
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services)],
			'views': [(tree.id, 'tree')],
		}



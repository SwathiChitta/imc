from odoo import api, fields, models,_
import datetime
from datetime import datetime,timedelta
import time
from openerp.exceptions import except_orm, Warning, RedirectWarning


def date_range(start, end):
    r = (datetime.strptime(end, "%Y-%m-%d")-datetime.strptime(start, "%Y-%m-%d")+timedelta(days=1)).days
    return [datetime.strptime(start, "%Y-%m-%d")+timedelta(days=i) for i in range(r)]


class Drivers_report(models.TransientModel):

	_name = "drivers.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	driver = fields.Many2one('drivers.menu','Driver')
	request_type = fields.Selection([('concierge', 'Concierge'),
                                     ('era', 'ERA'),
                                     ('rent-a-car', 'Rent a Car'),
                                     ('registration', 'Car Registration'),
                                     ('transportation', 'Transportation'),
                                     ('hertz', 'Hertz'),
                                     ], 'Request Type')

	@api.multi
	def print_drivers_report(self):
		vals = self.read()[0]
		context = self._context
		datas = {'form': vals}
		
		return {'type': 'ir.actions.report','report_name': 'imc.drivers_report','report_type':"qweb-pdf",'data': datas,}


	@api.multi
	def print_drivers_excel_report(self):
		vals = self.read()[0]
		context = self._context
		driver, request_type = False, False

		if vals['driver']: driver = vals['driver'][0]
		if vals['request_type']: request_type = vals['request_type']

		if driver and request_type:
			services = self.env['claimed.service'].search([('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date']),('request_type','=',request_type),('assigned_driver','=',driver)]).ids
		else:
			services = self.env['claimed.service'].search([('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date']),('assigned_driver','=',driver)]).ids
		
		tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.view')])[0]
		
		return {
			'name': 'Claimed Services',
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services)],
			'views': [(tree.id, 'tree')],
		}
		

class drivers_report_wizard(models.AbstractModel):
	_name = 'report.imc.drivers_report'

	@api.multi
	def get_report_values(self, docids, data=None):
		data = dict(data or {})

		drivers = {}
		allservs = []
		dateList = date_range(data['form']['from_date'],data['form']['to_date'])
		

		for each in dateList:
			nextt = False
			if dateList.index(each)+1 < len(dateList): nextt = str(dateList[dateList.index(each)+1])
			else: nextt = str(dateList[-1])
			date = str(each)

			if data['form']['driver']: domain = [('assigned_driver','=',data['form']['driver'][0]),('scheduled_date','>=',date),('scheduled_date','<=',nextt)]
			else: domain = [('assigned_driver','!=',False),('scheduled_date','>=',date),('scheduled_date','<=',nextt)]
		
			total = self.env['claimed.service'].search(domain)
			for ech in total:
				if ech.assigned_driver:
					if ech.assigned_driver in drivers: drivers[ech.assigned_driver.name]+=[ech]
					else: drivers[ech.assigned_driver.name]=[ech]

			for drv in drivers:
				inter,city,battery,flat,carreg,offroad=0,0,0,0,0,0
				for i in drivers[drv]:
					if i.service.name == 'Intercity Towing': inter += 1
					elif i.service.name in ['Citylimit Towing','City Limit Towing']: city += 1
					elif i.service.name == 'Battery Boosting': battery += 1
					elif i.service.name == 'Flat Tyre': flat += 1
					elif i.service.name == 'Registration Renewal': carreg += 1
					elif i.service.name == 'Off Road Recovery': offroad += 1
				allservs += [{'total':len(total),'date':each.date().strftime('%d-%m-%Y'), 'driver':drv,'inter':inter, 'city':city, 'battery':battery, 'flat':flat,'carreg':carreg, 'offroad':offroad}]

		data.update({'final':allservs})
		return data



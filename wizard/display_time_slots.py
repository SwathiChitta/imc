from odoo import api, fields, models,_
import datetime
from datetime import datetime
import time
from datetime import datetime, timedelta, date

class display_time_slots(models.TransientModel):

	
	_name = "display.current.timeslots"

	name = fields.Many2one('time.slot', 'Time Slots')
	customer = fields.Many2one('res.partner','Customer')
	datefrom=fields.Datetime('From')
	dateto=fields.Datetime('To')
	service_date=fields.Date('Service Date')
	info=fields.One2many('display.timeslots.info','relid','Info')
	claim_id = fields.Many2one('claimed.service','Claim ID')	
	state = fields.Many2one('res.country.state', string='State',domain="[('country_id.code','=','AE')]")
	display = fields.Boolean('Display Timeslots')

	@api.onchange('service_date')
	def _onchange_service_date(self):
		dictt={}
		claimed_service, cust_state = False, False
		slots, last = [], []
		if 'params' in self._context and self._context['active_model'] == 'claimed.service': claimed_service = self._context['active_id']
		elif 'active_id' in self._context and self._context['active_model']=='claimed.service': claimed_service = self._context['active_id']
		elif 'active_ids' in self._context and self._context['active_model']=='claimed.service': claimed_service = self._context['active_ids'][0]
		else: pass

		if self.state: cust_state= self.state.id
		elif claimed_service: cust_state = self.env['claimed.service'].browse(claimed_service).customer_state_id.id
		else: cust_state = self.env['res.country.state'].search([('code','=','DXB')],limit=1).id
		
		# if self.service_date and cust_state:
		records = self.env['claimed.service'].search([('service_date','=',self.service_date),('customer_state_id','=',cust_state),('time_slots','!=',False)]).ids
		# else:
			# records = self.env['claimed.service'].search([('service_date','=',self.service_date),('time_slots','!=',False)]).ids
		
		if records:
			for each in records:
				rec =self.env['claimed.service'].browse(each)
				slots +=[rec.time_slots.name]
			
			for each in slots:
				for timesl in self.env['time.slot'].search([]).ids:
					timeslot=self.env['time.slot'].browse(timesl).name
					if not timeslot in slots: dictt.update({timeslot:0})
					else: dictt.update({each:slots.count(each)})

			for dit in dictt:
				vals={}
				if dictt[dit]<3: vals.update({'slot':dit, 'sort':dit.split('-')[0],'available':3-dictt[dit],'booked':dictt[dit]})
				else: vals.update({'slot':dit, 'sort':dit.split('-')[0],'available':0,'booked':3})
				last += [(0,0,vals)]
		else:
			for each in self.env['time.slot'].search([]).ids:
				timeslot=self.env['time.slot'].browse(each)
				last += [(0,0,{'slot':timeslot.name, 'sort':timeslot.name.split('-')[0], 'booked':0,'available':3})]
		self.info=last
		self.display= False

	@api.onchange('state')
	def _onchange_states(self):

		if self.state:
			if 'params' in self._context and self._context['active_model'] == 'claimed.service': claimed_service = self._context['active_id']
			elif 'active_id' in self._context and self._context['active_model']=='claimed.service': claimed_service = self._context['active_id']
			elif 'active_ids' in self._context and self._context['active_model']=='claimed.service': claimed_service = self._context['active_ids'][0]
			else: pass

			dictt = {}
			slots,last = [],[]
			value = self.env['slot.values'].search([('state','=',self.state.id)]).value
			
			if self.service_date: 
				records = self.env['claimed.service'].search([('service_date','=',self.service_date),('time_slots','!=',False)]).ids
			else: 
				date = self.env['claimed.service'].browse(claimed_service).service_date
				records = self.env['claimed.service'].search([('service_date','=',date),('time_slots','!=',False)]).ids
			
			if records:
				for each in records:
					rec =self.env['claimed.service'].browse(each)
					slots +=[rec.time_slots.name]
				
				for each in slots:
					for timesl in self.env['time.slot'].search([]).ids:
						timeslot=self.env['time.slot'].browse(timesl).name
						if not timeslot in slots: dictt.update({timeslot:0})
						else: dictt.update({each:slots.count(each)})

				for dit in dictt:
					vals={}
					if dictt[dit]<value: vals.update({'slot':dit, 'sort':dit.split('-')[0],'available':value-dictt[dit],'booked':dictt[dit]})
					else: vals.update({'slot':dit, 'sort':dit.split('-')[0],'available':0,'booked':value})
					last += [(0,0,vals)]
			else:
				for each in self.env['time.slot'].search([]).ids:
					timeslot=self.env['time.slot'].browse(each)
					last += [(0,0,{'slot':timeslot.name, 'sort':timeslot.name.split('-')[0], 'booked':0,'available':value})]
			
			self.info = last
			self.display= False


	@api.onchange('datefrom', 'dateto')
	def _onchange_dates(self):
		if self.datefrom and self.dateto:
			fromm = datetime.strptime(self.datefrom,'%Y-%m-%d %H:%M:%S').hour
			to =datetime.strptime(self.dateto,'%Y-%m-%d %H:%M:%S').hour
			name = str(fromm)+'-'+str(to)
			time = self.env['time.slot'].search([('from_time_slot','=',fromm),('to_time_slot','=',to),('name','=',name)]).ids
			if not time: timeslot = self.env['time.slot'].create({'name':name,'to_time_slot':to,'from_time_slot':fromm}).id
			else: timeslot=time[0]
			self.name = timeslot


	@api.multi
	def write(self, vals):
		self = self.with_context(claim_id=self.with_context().claim_id.id)
		if '__contexts' in self._context: idd = self._context['__contexts'][0].get('active_id',False)
		else: idd = self._context['active_id']

		if 'dateto' in vals and 'datefrom' in vals:
			fromm = (datetime.strptime(vals['datefrom'],'%Y-%m-%d %H:%M:%S')+timedelta(hours=5.5)).hour
			to = (datetime.strptime(vals['dateto'],'%Y-%m-%d %H:%M:%S')+timedelta(hours=5.5)).hour
			
			rec = self.env['time.slot'].search([('name','=',str(fromm)+'-'+str(to)),('to_time_slot','=',to),('from_time_slot','=',fromm)]).ids
			if not rec: timeslot = self.env['time.slot'].create({'name':str(fromm)+'-'+str(to),'to_time_slot':to,'from_time_slot':fromm}).id
			else:timeslot = rec[0]
			
			vals.update({'name':timeslot})
			if idd: 
				recs = self.env['claimed.service'].browse(idd)
				recs.write({'time_slots':timeslot})
				vals.update({'customer':recs.customer.id})

		vals.update({'claim_id': self.env.context['active_id']})
		record =  super(display_time_slots, self).write(vals)
		return record

	@api.model
	def create(self, vals):
		name=''
		idd = False
		if 'params' in self._context:
			params = self._context['params']
			if 'id' in params: idd = params['id']

		if 'name' in vals:
			if type(vals['name']) == int:  
				name = self.env['time.slot'].browse(vals['name']).name
			elif type(vals['name']) == str: 
				name= vals['name']
				time = self.env['time.slot'].search([('name','=',name),('to_time_slot','=',name.split('-')[-1]),('from_time_slot','=',name.split('-')[0])]).ids
				if not time : timeslot = self.env['time.slot'].create({'name':name,'to_time_slot':name.split('-')[-1],'from_time_slot':name.split('-')[0]}).id
				else : timeslot = time[0]
				
				vals.update({"name":timeslot})
			else:vals.update({"name":False})

		if name and idd:
			if type(vals['datefrom']) == str:
				vals.update({'datefrom': datetime.combine(datetime.strptime(vals['datefrom'],'%Y-%m-%d %H:%M:%S').date(),datetime.min.time()) +timedelta(hours=int(name.split('-')[0])-5.5)})
				vals.update({'dateto': datetime.combine(datetime.strptime(vals['dateto'],'%Y-%m-%d %H:%M:%S').date(),datetime.min.time()) +timedelta(hours=int(name.split('-')[-1])-5.5)})
				claim = self.env['claimed.service'].browse(idd)
				claim.write({'time_slots':timeslot})
				vals.update({'customer':claim.customer.id})
		result = super(display_time_slots, self).create(vals)
		return result

	@api.multi
	def save_slot_info(self):
		service_date=self.service_date
		dictt={}
		slots, last = [], []
		
		claimed_service, cust_state = False, False
		if 'active_ids' in self._context and self._context['active_model']=='claimed.service': claimed_service = self._context['active_ids'][0]
		elif 'active_id' in self._context and self._context['active_model']=='claimed.service': claimed_service = self._context['active_id']
		elif 'params' in self._context and 'model' in self._context['params'] and self._context['params']['model'] == 'claimed.service': claimed_service = self._context['params']['id']
		else: pass
		
		if claimed_service: cust_state = self.env['claimed.service'].browse(claimed_service).customer_state_id.id
		
		value = self.env['slot.values'].search([('state','=',self.state.id)]).value

		if cust_state and service_date: 
			records = self.env['claimed.service'].search([('service_date','=',service_date),('customer_state_id','=',cust_state),('time_slots','!=',False)]).ids
		else: 
			records = self.env['claimed.service'].search([('service_date','=',service_date),('time_slots','!=',False)]).ids
		
		if records:
			for each in records:
				rec =self.env['claimed.service'].browse(each)
				slots +=[rec.time_slots.name]
			
			for each in slots:
				for timesl in self.env['time.slot'].search([]).ids:
					timeslot=self.env['time.slot'].browse(timesl).name
					if not timeslot in slots: dictt.update({timeslot:0})
					else: dictt.update({each:slots.count(each)})
			for dit in dictt:
				vals={}
				if dictt[dit]<value: vals.update({'slot':dit, 'sort':dit.split('-')[0],'available':value-dictt[dit],'booked':dictt[dit]})
				else: vals.update({'slot':dit, 'sort':dit.split('-')[0],'available':0,'booked':value})
				last += [(0,0,vals)]
		else:
			for each in self.env['time.slot'].search([]).ids:
				timeslot=self.env['time.slot'].browse(each)
				last += [(0,0,{'slot':timeslot.name, 'sort':timeslot.name.split('-')[0], 'booked':0,'available':value})]
			

		res_id = self.env['display.current.timeslots'].create({'display':True,'service_date':service_date,'state':self.state.id,'info':last, 'claim_id': self.env.context['active_id']})
		return {
			'name': 'Timeslots',
			"view_mode": 'form',
			'res_model': 'display.current.timeslots',
			'res_id' : res_id.id,
			'type' : 'ir.actions.act_window',
			'target' : 'new',
			'context':self._context,
		}


class view_time_slots(models.TransientModel):

	
	_name = "display.timeslots.info"

	slot=fields.Char('Slot')
	booked=fields.Integer('Booked')
	available=fields.Integer('Available')
	relid=fields.Many2one('display.current.timeslots','rel')
	sort = fields.Integer('Sort')

	
	@api.multi
	def book_customer_timeslot(self):
		state = False
		claim = self.relid.claim_id.id
		if self.relid.state: state = self.relid.state.id
		if not claim:
			if not 'params' in self._context:
				if 'active_id' in self._context: claim = self._context['active_id']
			else:
				if 'params' in self._context: 
					if 'id' in  self._context['params']: claim = self._context['params']['id']
		if claim:
			slot = self.slot
			slot_id = self.env['time.slot'].search([('from_time_slot','=',slot.split('-')[0]),('to_time_slot','=',slot.split('-')[-1])]).id
			rec = self.env['claimed.service'].browse(claim).write({'customer_state_id':state,'service_date':self.env['display.current.timeslots'].browse(self.relid.id).service_date,'time_slots':slot_id})
			return rec
	
view_time_slots()
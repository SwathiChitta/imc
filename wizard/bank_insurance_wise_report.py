from odoo import api, fields, models,_

class Bank_insurance_wise_services(models.TransientModel):

	_name = "bank.insurance.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	company_type = fields.Selection([('bank', 'Bank'),
									 ('insurance', 'Insurance Company'),
									 ('transport', 'Transport Company'),
									 ('others', 'Others')], "Type")
	request_type = fields.Selection([('concierge', 'Concierge'),
	                                     ('era', 'ERA'),
	                                     ('rent-a-car', 'Rent a Car'),
	                                     ('registration', 'Car Registration'),
	                                     ('transportation', 'Transportation')], 'Request Type')

	@api.multi
	def print_bank_insurance_report(self):
		vals = self.read()[0]
		company_type = vals['company_type']
		request_type = vals['request_type']
		
		if company_type and request_type:
			services = self.env['claimed.service'].search([('membership_id.type','=',company_type),('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date']),('request_type','=',request_type)]).ids
		else:
			services = self.env['claimed.service'].search([('membership_id.type','=',company_type),('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date'])]).ids

		if request_type:
			if request_type == 'rent-a-car':
				name = 'Rent A Car Services'
				tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.rentacar')])[0]
			elif request_type == 'registration':
				name = 'Car Registration Services'
				tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.registration')])[0]
			elif request_type == 'concierge':
				name = 'Concierge Services'
				tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.concierge')])[0]
			elif request_type == 'era':
				name = 'ERA Services'
				tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.era')])[0]
			elif request_type == 'transportation':
				name = 'Transportation Services'
				tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.transportation')])[0]
		else:
			name = 'All Claimed Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.view')])[0]

		if company_type == 'bank':final_name = 'Bank '+name
		elif company_type == 'insurance':final_name = 'Insurance '+name
		else: final_name = 'Other '+name
		
		return {
			'name': final_name,
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services)],
			'views': [(tree.id, 'tree')],
		}

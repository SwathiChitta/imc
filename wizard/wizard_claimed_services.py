from odoo import api, fields, models

FEEDBACK_SELECTION = [
    ('very-satisfied', 'Very Satisfied'),
    ('somewhat-satisfied', 'Somewhat Satisfied'),
    ('unhappy', 'Unhappy'),
]


class Feedback_Wizard(models.TransientModel):
    _name = "feedback.wizard"

    overall_feedback = fields.Selection(FEEDBACK_SELECTION, 'Overall, how satisfied are you with the service')
    agents_feedback = fields.Selection(FEEDBACK_SELECTION,
                                       'How satisfied are you with the call center agent who took your service')
    drivers_feedback = fields.Selection(FEEDBACK_SELECTION,
                                        'How satisfied are you with our services on the road and the behavior of the Driver')
    drivers_arrival = fields.Selection([('yes', 'YES'), ('no', 'NO')], 'Did the driver reach within 60 minutes')
    additional_suggestions = fields.Text('Any additional suggestions to improve our service ?')

    @api.multi
    def save_feedback_details(self):
        data = self.read()[0]
        context = self._context.copy()
        claim_obj = self.env['claimed.service'].browse(context['active_id'])
        claim_obj.write({'state': 'feedback',
                         'overall_feedback': data['overall_feedback'],
                         'agents_feedback': data['agents_feedback'],
                         'drivers_feedback': data['drivers_feedback'],
                         'drivers_arrival': data['drivers_arrival'],
                         'additional_suggestions': data['additional_suggestions']})
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }


Feedback_Wizard()


class Cancelation_Reason(models.TransientModel):
    _name = "cancelation.reason"

    reason_cancel = fields.Text('Reason For Cancel?', required=True)

    @api.multi
    def save_cancel_details(self):
        data = self.read()[0]
        context = self._context.copy()
        claim_obj = self.env['claimed.service'].browse(context['active_id'])
        claim_obj.write({'state': 'cancelled',
                         'reason_cancel': data['reason_cancel']
                         })
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }


Cancelation_Reason()

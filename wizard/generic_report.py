from odoo import api, fields, models,_

class Generic_report(models.TransientModel):

	_name = "generic.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	company_id = fields.Many2one('imc.clients','Company')
	card_type = fields.Char('Card Type')
	service_agent = fields.Many2one('drivers.menu', 'Service Agent')
	from_city = fields.Many2one('res.country.state','From City')
	to_city = fields.Many2one('res.country.state','To City')
	call_center_agent = fields.Many2one('res.users','Call Center Agent')
	country = fields.Many2one('res.country','Country')

	service_type = fields.Selection([('concierge', 'Concierge'),
                                     ('era', 'ERA'),
                                     ('rent-a-car', 'Rent a Car'),
                                     ('registration', 'Car Registration'),
                                     ('transportation', 'Transportation')], 'Request Type')

	membership_status = fields.Selection([('temp', 'Temp'),
									   ('activate', 'Active'),
									   ('expired', 'Expired'),
									   ('cancel', 'Cancelled')],'Membership Status')

	@api.multi
	def print_generic_report(self):
		vals = self.read()[0]
		domain = []
		domain += [('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date'])]
		
		if vals['membership_status']: domain += [('membership_id.status','=',vals['membership_status'])]
		if vals['company_id']: domain += [('insurance_company','=',vals['company_id'][0])]
		if vals['service_agent'] : domain += [('assigned_party','=',vals['service_agent'][0])]
		if vals['from_city'] : domain += [('from_state_id','=',vals['from_city'][0])]
		if vals['to_city'] : domain += [('to_state_id','=',vals['to_city'][0])]
		if vals['call_center_agent'] : domain += [('service_takenby','=',vals['call_center_agent'][0])]
		if vals['country'] : domain += [('from_country_id','=',vals['country'][0])]
		if vals['service_type'] : domain += [('request_type','=',vals['service_type'])]
		
		services = self.env['claimed.service'].search(domain+[('state','!=','cancelled')]).ids
		driver_cancel = self.env['claimed.service'].search(domain+[('state','=','cancelled'),('assigned_driver','!=',False),('starting_km','!=',False),('reason_cancel','!=',False)]).ids

		tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.view')])[0]

		return {
			'name': 'Services',
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services + driver_cancel)],
			'views': [(tree.id, 'tree')],
		}
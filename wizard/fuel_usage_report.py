from odoo import api, fields, models,_

class Fuel_usage_report(models.TransientModel):

	_name = "fuel.usage.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	request_type = fields.Selection([('concierge', 'Concierge'),
                                     ('era', 'ERA'),
                                     ('rent-a-car', 'Rent a Car'),
                                     ('registration', 'Car Registration'),
                                     ('transportation', 'Transportation'),
                                     ('hertz', 'Hertz'),
                                     ], 'Request Type')
	driver = fields.Many2one('drivers.menu','Driver')

	@api.multi
	def print_fuel_usage_report(self):
		vals = self.read()[0]
		request_type, driver = False, False

		if 'request_type' in vals and  vals['request_type']: request_type = vals['request_type']
		if 'driver' in vals and vals['driver']: driver = vals['driver']

		if request_type and driver:
			services = self.env['claimed.service'].search([('request_type','=',request_type),('assigned_driver','=',driver[0]),('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date'])]).ids
		elif request_type or driver:
			if request_type: services = self.env['claimed.service'].search([('request_type','=',request_type),('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date'])]).ids
			else: services = self.env['claimed.service'].search([('assigned_driver','=',driver[0]),('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date'])]).ids
		else:
			services = self.env['claimed.service'].search([('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date'])]).ids

		tree = self.env['ir.ui.view'].search([('name', '=', 'fuel.excel.report.view')])[0]

		return {
			'name': 'Fuel Usage',
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services)],
			'views': [(tree.id, 'tree')],
		}
from odoo import api, fields, models,_
import datetime
from datetime import datetime, timedelta
from openerp.exceptions import except_orm, Warning, RedirectWarning
from O365 import Message
o365_auth = ('ops@motoringclub.com', 'ITPLAZA@906')
m = Message(auth=o365_auth)

class Extend_days_wizard(models.TransientModel):

	
	_name = "extend.days"

	days = fields.Integer('No.of Days')

	@api.multi
	def submit_days_no(self):
		data = self.read()[0]
		context = self._context.copy()
		if 'active_id' in context:
			claim_obj = self.env['claimed.service'].browse(context['active_id'])
			if data['days'] < 1: 
				raise Warning(_('Days should be more than 0!!'))
			else:
				total_days = claim_obj.no_of_days + data['days']
				if total_days > claim_obj.days_eligible:
					raise Warning(_('Days should not be more than no. of days eligible!!'))
				else:
					claim_obj.write({'no_of_days':total_days ,'off_hire_date':datetime.strptime(claim_obj.scheduled_date, '%Y-%m-%d %H:%M:%S') + timedelta(days=int(total_days))})
					if claim_obj.assigned_party and claim_obj.assigned_party.email_id:
						body_html = "<p>Hello, </p><p>Rent a car days are extend for %s. Service details are, </p><p><strong>Service Type & Name: %s/%s </strong></p><p><strong>Customer Name: %s </strong></p><p><strong>Contact: %s</strong></p><p><strong>Pickup Location: %s</strong></p><p><strong>Dropoff Location: %s</strong></p><p>Team IMC</p>"%(str(data['days']),str(claim_obj.request_type).upper(), claim_obj.service.name, claim_obj.customer.name,claim_obj.contact, claim_obj.pickup_locations,claim_obj.dropoff_locations)

						m.setRecipients(claim_obj.assigned_party.email_id)
						m.setSubject('Rent A Car days have been extended')
						m.setBodyHTML(body_html)
						m.sendMessage()
			# if data['days'] > rec.days_eligible :
			# 	raise Warning(_('Days should not be more than no. of days eligible!!'))
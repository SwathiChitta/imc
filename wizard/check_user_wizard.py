from odoo import api, fields, models,_

class check_user(models.TransientModel):


	_name = "check.user.membership"

	@api.model
	def _default_user(self):
		return self.env.context.get('user_id', self.env.user.id)
		
	type = fields.Selection([('bank', 'Bank'),
							  ('insurance', 'Insurance Company'),
							  ('others', 'Others')], "Type")
	name = fields.Char('Customer Name')
	email_id = fields.Char('Email Id')
	mobile_no = fields.Char('Mobile No')
	membership_type = fields.Many2one('service.type','Membership Type')
	policy_no = fields.Char('Policy/Membership #')
	credit_no = fields.Char('Credit Card No')
	# reference = fields.Char('Reference No.')
	chassis_no = fields.Char('Chassis No.')
	veh_reg_no = fields.Char('Car Plate No.')
	trip_sheet_number = fields.Char('Trip Sheet No.')
	membership_no = fields.Char('Reference/Membership #')
	company_id = fields.Many2one('imc.clients', 'Company')
	membership_type_id = fields.Many2one('service.type', 'Membership Type')
	expiry_date = fields.Date('Expiry Date')
	status = fields.Selection([('temp', 'Temp'),
								('activate', 'Active'),
								('expired', 'Expired'),
								('cancel', 'Cancelled'),
								], 'Status')
	helps = fields.Char('Help')
	pobox = fields.Char('P.O.Box')
	user_id = fields.Many2one('res.users','Searched User',default=_default_user)
	
	

	# @api.onchange('name')
	# def onchange_customer_name(self):
	# 	domain={}
	# 	partner = self.env['res.partner'].search([('name','=',self.name)]).ids
	# 	if partner:
	# 		membership = self.env['membership.menu'].search([('end_user','=',partner[0])]).ids
	# 		if membership: 
	# 			rec = self.env['membership.menu'].browse(membership[0])
	# 			if rec.vehicles: return {'domain':{'chassis_no':[('id','in',rec.vehicles.ids)]}}
	# 			else: return {'domain':{'chassis_no':[('id','in',[])]}}

	@api.model
	def default_get(self,default_fields):
		req_id =  False
		ret = super(check_user,self).default_get(default_fields)
		search = self.search([('user_id','=',self.env.user.id)])
		if search: req_id = search[-1]
		if req_id:
			ret['policy_no'] =req_id.policy_no
			ret['mobile_no'] =req_id.mobile_no
			ret['name'] =req_id.name
			ret['chassis_no'] =req_id.chassis_no
			ret['pobox'] =req_id.pobox
			ret['company_id'] =req_id.company_id.id
			ret['trip_sheet_number'] =req_id.trip_sheet_number
			ret['veh_reg_no'] =req_id.veh_reg_no
			ret['credit_no'] =req_id.credit_no
			ret['display_name'] =req_id.display_name
		return ret


	@api.onchange('company_id')
	def onchange_company(self):
		if self.company_id:
			string = ''
			for i in self.company_id.policy_format:
				string = string + i.formats + ', '
			self.helps = string
		
				
	@api.multi
	def get_results(self):
		input_data = self.read()[0]

		args = []
		claimed_service_args = []
		customer_args = []
		temp_args = []
		or_args = []

		# if input_data['membership_no']:
		# 	args.append(('membership_no', '=', input_data['membership_no']))
		if input_data['pobox']:
			args.append(('end_user.zip','ilike',input_data['pobox']))

		if input_data['membership_type_id']:
			args.append(('membership_type', '=', input_data['membership_type_id'][0]))

		if input_data['expiry_date']:
			args.append(('expiry_date', '=', input_data['expiry_date']))

		if input_data['status']:
			args.append(('status', '=', input_data['status']))

		if input_data['veh_reg_no']:
			vehicle_id = self.env['membership.vehicle.info'].search([('car_plate', 'ilike', input_data['veh_reg_no'])])
			if vehicle_id:
				mem_ids = []
				for i in vehicle_id:
					mem_ids.append(i.membership_id.id)
				args.append(('id', 'in', mem_ids))
			else:
				args.append(('car_plate', 'ilike', input_data['veh_reg_no']))

		if input_data['trip_sheet_number']:
			claimed_service_args.append(('trip_sheet_number', '=', input_data['trip_sheet_number']))

		if input_data['name']:
			customer_args.append(('name', 'ilike', input_data['name']))
			temp_args.append(('name', 'ilike', input_data['name']))
		
		if input_data['mobile_no']:
			# customer_args.append(('mobile', 'ilike', input_data['mobile_no']))
			temp_args.append(('mobile_no', 'ilike', input_data['mobile_no']))
			# endusers = self.env['res.partner'].search(customer_args)
			# if not endusers:
			args.append(('contact_number', 'ilike', str(input_data['mobile_no'])))

		if input_data['policy_no']:
			or_args.append('|')
			or_args.append(('insurance_number', 'ilike', input_data['policy_no']))
			or_args.append(('membership_no', 'ilike', input_data['policy_no']))
			temp_args.append(('policy_no', 'ilike', input_data['policy_no']))

		if input_data['credit_no']:
			args.append(('credit_card_number', 'ilike', input_data['credit_no']))
			temp_args.append(('credit_no', 'ilike', input_data['credit_no']))

		if input_data['chassis_no']:
			vehicle_id = self.env['membership.vehicle.info'].search([('chassis_no', 'ilike', input_data['chassis_no'])])
			if vehicle_id:
				mem_ids = []
				for i in vehicle_id:
					mem_ids.append(i.membership_id.id)
				args.append(('id', 'in', mem_ids))
			else:
				args.append(('chassis_no', 'ilike', input_data['chassis_no']))
				temp_args.append(('chassis_no', 'ilike', input_data['chassis_no']))

		if input_data['company_id']:
			args.append(('company_id', '=', input_data['company_id'][0]))
			temp_args.append(('client', '=', input_data['company_id'][0]))

		if customer_args:
		
			endusers = self.env['res.partner'].search(customer_args)
			if endusers:
				args.append(('end_user', 'in', endusers.ids))

		if claimed_service_args:

			claimed_id = self.env['claimed.service'].search(claimed_service_args)

			if claimed_id:
				mem_ids = []
				for i in claimed_id:
					mem_ids.append(i.membership_id.id)
				args.append(('id', 'in', mem_ids))

		for i in range(1, len(args)):
			args.insert(0, '&')

		args.extend(or_args)
		for i in range(1, len(temp_args)):
			temp_args.insert(0, '&')

		if args: 
			membership_ids = self.env['membership.menu'].search(args)
		else:
			membership_ids = False
		

		if not membership_ids:
			if temp_args:
				temp_ids = self.env['temporary.users'].search(temp_args)

				if temp_ids:
					return {
					'name': 'Temporary Users',
					'view_type': 'form',
					"view_mode": 'tree,form',
					'res_model': 'temporary.users',
					'type': 'ir.actions.act_window',
					# 'search_view_id': view_id,
					'domain': "[('id', 'in',%s)]" % (temp_ids.ids),
					}

				else:
					return {
						'name': 'Temp User',
						'view_mode': 'form',
						'view_id': False,
						'view_type': 'form',
						'res_model': 'temp.user.dialog',
						'type': 'ir.actions.act_window',
						'context': input_data,
						'target': 'new',
						# opening form in edit mode
						# 'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
					}
			else:
				return {
					'name': 'Temp User',
					'view_mode': 'form',
					'view_id': False,
					'view_type': 'form',
					'res_model': 'temp.user.dialog',
					'type': 'ir.actions.act_window',
					'context': input_data,
					'target': 'new',
					# opening form in edit mode
					# 'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
				}
		
		models_data = self.env['ir.model.data']

		view_id = models_data._get_id('imc', 'membership_menu_tree_view')
		model = 'membership.menu'


		return {
			'name': 'Memberships',
			'view_type': 'form',
			"view_mode": 'tree,form',
			'res_model': model,
			'type': 'ir.actions.act_window',
			'search_view_id': view_id,
			'domain': "[('id', 'in',%s)]" % (membership_ids.ids),
		}

	@api.multi
	def go_to_membership_form(self):
		input_data = self.read()[0]
		vals = {}

		view_ref = self.env['ir.model.data'].get_object_reference('imc', 'membership_menu_form_view')
		view_id = view_ref[1] if view_ref else False
		
		if input_data['credit_no']: vals.update({'default_type':'bank','default_credit_card_number':input_data['credit_no']})
		if input_data['policy_no']: vals.update({'default_type':'insurance','default_insurance_number':input_data['policy_no']})
		if input_data['company_id']:vals.update({'default_company_id':input_data['company_id'][0]})
		if input_data['name']:
			existing = self.env['res.partner'].search([('name','ilike',input_data['name']),('mobile','=',input_data['mobile_no'])],limit=1)
			if not existing:
				idd = self.env['res.partner'].create({'name':input_data['name'], 'zip':input_data['pobox'],'mobile':input_data['mobile_no']}).id
			else: 
				idd = existing.id
			vals.update({'default_end_user':idd,'default_contact_number':input_data['mobile_no']})
		if input_data['chassis_no'] : vals.update({'default_chassis_no':input_data['chassis_no']})
		if input_data['veh_reg_no'] : vals.update({'default_car_plate':input_data['veh_reg_no']})

		return {
			'name': 'Memberhsip',
			'view_mode': 'form',
			'view_type': 'form',
			'view_id': view_id,
			'res_model': 'membership.menu',
			'type': 'ir.actions.act_window',
			'target': 'current',
			'context' : vals,
		}
		
check_user()

class temp_user_dialog_wizard(models.TransientModel):

	_name = 'temp.user.dialog'

	def create_temp_membership(self,context=None):
		vals = {}

		if context['policy_no']: vals.update({'default_type':'insurance', 'default_credit_card_number':context.get('credit_no', False)})
		if context['credit_no']: vals.update({'default_type':'bank', 'default_insurance_number':context.get('policy_no', False)})
		if context['company_id']:vals.update({'default_company_id' :context.get('company_id', False)[0]})
		
		if context['name'] and context['mobile_no']: 
			if self.env['res.partner'].search([('name','like',context.get('name',False)),('mobile','like',context.get('mobile_no',False))]):
				end_user = self.env['res.partner'].search([('name','like',context.get('name',False)),('mobile','like',context.get('mobile_no',False))])[0]
			else: end_user = self.env['res.partner'].create({'name':context.get('name', False), 'mobile':context.get('mobile_no', False)})
			vals.update({'default_end_user':end_user.id, 'default_contact_number':context.get('mobile_no', False)})
		elif context['name']: 
			if self.env['res.partner'].search([('name','like',context.get('name',False))]):
				end_user = self.env['res.partner'].search([('name','like',context.get('name',False))])[0]
			else: end_user = self.env['res.partner'].create({'name':context.get('name', False)})
			vals.update({'default_end_user':end_user.id})
		else:pass

		vals.update({
			'default_customer': context.get('customer', False),
			'default_chassis_no':context.get('chassis_no',False),
		})

		# view_id = self.env['ir.ui.view'].search([('name', '=', 'membership.menu.view')])[0]
		vals.update({'form_view_initial_mode': 'edit', 'force_detailed_view': 'true'})
		return {
			'name': 'Temp Membership',
			'view_mode': 'form',
			'view_type':'form',
			# 'view_id': view_id.id,
			'res_model': 'membership.menu',
			'type': 'ir.actions.act_window',
			'target': 'current',
			'context': vals,
		}



	def create_temp_user(self,context=None):
		models_data = self.env['ir.model.data']
		temp_vals = {
			'default_name': context['name'],
			'default_email_id': context['email_id'],
			'default_mobile_no': context['mobile_no'],
			'default_policy_no': context['policy_no'],
			'default_credit_no': context['credit_no'],
		}

		if context['company_id']:
			excp_policy = self.env['imc.clients'].browse(context['company_id'][0]).exception_policy
			if excp_policy:
				if excp_policy == 'no':
					raise osv.except_osv(_('oops!'), _('This customer does not allow exception policy!!'))
		
		# user_id = self.env['temporary.users'].create(temp_vals)
		# view_id = models_data._get_id('imc', 'temporary_users_tree_view')
		temp_vals.update({'form_view_initial_mode': 'edit', 'force_detailed_view': 'true'})

		return {
			'name': 'Temporary User',
			'view_mode': 'form',
			# 'view_id': False,
			'view_type': 'form',
			'res_model': 'temporary.users',
			# 'res_id': user_id.id,
			'type': 'ir.actions.act_window',
			'context': temp_vals,
			'target': 'current',
			# opening form in edit mode
			# 'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
		}
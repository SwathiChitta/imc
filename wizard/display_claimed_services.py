from odoo import api, fields, models,_
import datetime

class Display_claimed_services_wizard(models.TransientModel):

	
	_name = "display.services"

	service_type = fields.Selection([('concierge', 'Concierge'),
									 ('era', 'ERA'),
									 ('rent-a-car', 'Rent a Car'),
									 ('registration', 'Car Registration'),
									 ('transportation', 'Transportation')],'Service Type')

	def display_respective_view(self,context=None):
		models_data = self.env['ir.model.data']
		input_data = self.read()[0]

		service_type = input_data['service_type']
		if service_type == 'concierge': 
			view_id = self.env['ir.ui.view'].search([('name','=','concierge.test.view')])[0]
			view_id2 = self.env['ir.ui.view'].search([('name','=','concierge.test.form.view')])[0]
		elif service_type == 'era':
			view_id = self.env['ir.ui.view'].search([('name','=','era.test.view')])[0]
			view_id2 = self.env['ir.ui.view'].search([('name','=','era.test.form.view')])[0]
		elif service_type == 'rent-a-car': 
			view_id = self.env['ir.ui.view'].search([('name','=','rent.acar.test.view')])[0]
			view_id2 = self.env['ir.ui.view'].search([('name','=','rent.acar.test.form.view')])[0]
		elif service_type == 'registration': 
			view_id = self.env['ir.ui.view'].search([('name','=','car.regis.test.view')])[0]
			view_id2 = self.env['ir.ui.view'].search([('name','=','car.regis.test.form.view')])[0]
	
		elif service_type == 'transportation': 
			view_id = self.env['ir.ui.view'].search([('name','=','transportation.test.view')])[0]
			view_id2 = self.env['ir.ui.view'].search([('name','=','transportation.test.form.view')])[0]
		else: 
			view_id = self.env['ir.ui.view'].search([('name','=','claimed.service.view')])[0]
			view_id2 = self.env['ir.ui.view'].search([('name','=','claimed.service.form.view')])[0]

		membership_id = context['active_ids'][0]
		claimed_services = self.env['claimed.service'].search([('request_type','=',service_type),('membership_id','=',membership_id)]).ids

		return {
				'name': 'Claimed Services',
				'view_type': 'form',
         	   'view_mode': 'form',
				'target': 'current',
				'res_model': 'claimed.service',
				'type': 'ir.actions.act_window',
				'domain':[('id','in',claimed_services)],
				'views': [(view_id.id, 'tree'),(view_id2.id, 'form')],
			}
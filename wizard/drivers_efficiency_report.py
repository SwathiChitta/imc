from odoo import api, fields, models,_

class Drivers_efficiency_report(models.TransientModel):

	_name = "drivers.efficiency.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	
	@api.multi
	def print_drivers_efficiency_report(self):
		vals = self.read()[0]
	
		services = self.env['claimed.service'].search([('state','=','completed'),('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date'])]).ids

		tree = self.env['ir.ui.view'].search([('name', '=', 'drivers.efficiency.report.view')])[0]

		return {
			'name': 'Drivers Efficiency',
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services)],
			'views': [(tree.id, 'tree')],
		}


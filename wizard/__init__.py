from . import check_user_wizard
from . import complete_claim_service_wizard
from . import wizard_claimed_services
from . import claim_service_dispatched_wizard
from . import display_time_slots
from . import documents_recieved
from . import extend_days
from . import choose_vehicle
from . import display_claimed_services
from . import upload_memberships
from . import driver_wizard
# from . import clientwise_service_report
# from . import all_services_report
# from . import drivers_report
# from . import subcontractors_report
from . import service_usage
# from . import temporary_service_report
# from . import membership_status_report
# from . import bank_insurance_wise_report
# from . import drivers_efficiency_report
# from . import truck_usage_report
# from . import fuel_usage_report
from . import generic_report
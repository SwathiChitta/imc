from odoo import api, fields, models,_
import datetime
from datetime import datetime
from odoo.exceptions import UserError


class choose_vehicle_wizard(models.TransientModel):

	_name = "choose.vehicle"

	vehicles= fields.Many2many('membership.vehicle.info', 'wizard_id', 'vehicle_id', string='Vehicles')

	@api.multi
	def claim_service(self):
		# vals = []
		context = self._context.copy()
		data = self.read()[0]

		if len(data['vehicles']) < 1:
			raise UserError(_('Please choose atleast one vehicle!!'))

		if len(data['vehicles']) > 1:
			raise UserError(_('Please choose one vehicle at a time!!'))
	
		vals={}

		active_model = self._context.get('active_model', None)

		if str(active_model) == 'concierge.avail.services':
			rec = self.env['concierge.avail.services'].browse(self._context.get('active_id',False))

		if str(active_model) == 'era.avail.services':
			rec = self.env['era.avail.services'].browse(self._context.get('active_id',False))

		if str(active_model) == 'rentacar.avail.services':
			rec = self.env['rentacar.avail.services'].browse(self._context.get('active_id',False))

		if str(active_model) == 'hertz.avail.services':
			rec = self.env['hertz.avail.services'].browse(self._context.get('active_id',False))

		if str(active_model) == 'carreg.avail.services':
			rec = self.env['carreg.avail.services'].browse(self._context.get('active_id',False))
		if str(active_model) == 'transport.avail.services':
			rec = self.env['transport.avail.services'].browse(self._context.get('active_id',False))

		if rec.service_id.request_type == 'hertz':
		
			if not rec.membership_id.chassis_no:
				raise UserError(_('Warning!'), _('Incomplete vehicle details!!'))

		if rec.membership_id.status != 'activate':
			raise UserError(_('You cannot claim this service as the membership not in Active!!'))

		vals ={
			'customer': rec.membership_id.end_user.id,
			'service': rec.service_id.id,
			'membership_id': rec.membership_id.id,
			'insurance_number': rec.membership_id.insurance_number,
			'membership_type': rec.membership_id.membership_type.id,
			'membership_id': rec.membership_id.id,
			'contact': rec.membership_id.contact_number,
			'type': rec.membership_id.type,
			'date_time': datetime.now(),
			'service_name': rec.service_id.name + rec.service_id.request_type, 
			'vehicle_make':rec.membership_id.vehicle_make.id,
			'car_plate':rec.membership_id.car_plate}
		
		res_id = self.env['claimed.service'].create(vals)

		if rec.service_id.request_type == 'concierge':
			view_id = self.env['ir.ui.view'].search([('name','=','concierge.test.form.view')])[0]
		
		elif rec.service_id.request_type == 'era':
			view_id = self.env['ir.ui.view'].search([('name','=','era.test.form.view')])[0]

		elif rec.service_id.request_type == 'rent-a-car':
			view_id = self.env['ir.ui.view'].search([('name','=','rent.acar.test.form.view')])[0]

		elif rec.service_id.request_type == 'registration':
			view_id = self.env['ir.ui.view'].search([('name','=','car.regis.test.form.view')])[0]

		elif rec.service_id.request_type == 'hertz':
			view_id = self.env['ir.ui.view'].search([('name','=','hertz.test.form.view')])[0]

		elif rec.service_id.request_type == 'transportation':
			view_id = self.env['ir.ui.view'].search([('name','=','transportation.test.form.view')])[0]

		else:
			view_id = self.env['ir.ui.view'].search([('name','=','claimed.service.form.view')])[0]
		
		return {  
			'name' : 'Claimed Services',
			'view_mode' : 'form',
			'view_type' : 'form',
			'view_id': view_id.id,
			'res_model' : 'claimed.service',
			'res_id' : res_id.id,
			'type' : 'ir.actions.act_window',
			'target' : 'current',
			# opening form in edit mode
			'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
		}
from odoo import api, fields, models, _
import xlrd
import os
from xlrd import open_workbook
import base64
from openerp.exceptions import except_orm, Warning, RedirectWarning
import datetime
from datetime import datetime
import socket
import xmlrpc.client
import logging
logger = logging.getLogger(__name__)

def get_ip_address():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8", 80))
	return s.getsockname()[0]
	
username = 'admin' #the user
pwd = 'imcadmin'      #the password of the user
dbname = 'IMC11'    #the database
# dbname = 'imc'    #the database


class Upload_Service(models.TransientModel):
	_name = "service.usage"

	upload_document = fields.Binary('Upload Document')
	filename = fields.Char('Filename')


	@api.multi
	def upload_service_usage_data(self):
		ip = get_ip_address()
		URL = "http://" + ip + ":8069/xmlrpc/common"
		sock_common = xmlrpc.client.ServerProxy(URL, transport=None,
												encoding=None, verbose=False, allow_none=False,
												use_datetime=False, context=None)
		uid = sock_common.login(dbname, username, pwd)
		sock = xmlrpc.client.ServerProxy("http://" + ip + ":8069/xmlrpc/" + 'object')

		input_data = self.read()[0]

		filename = input_data['filename']
		if not ('.xls' in filename or '.xlsx' in filename):
			raise Warning(_('Please choose .xls/.xlsx files only!!'))

		possible_strings = ['JOB OP DATE','CREDIT CARD NO','CUSTOMER NAME','MOBILE','FROM CITY','FROM LOCATION','TO CITY','TO LOCATION','SERVICE NAME','VEH MAKE','VEH MODEL','VEH REG NO','STATUS']

		xl_workbook = open_workbook(file_contents=base64.decodestring(input_data['upload_document']))

		sheet_name = xl_workbook.sheet_names()
			
		for each in sheet_name:
			actual_row, job_op_col, credit_card_col, name_col, mobile_col, from_city_col, from_loc_col, to_city_col, to_loc_col, serv_name_col, veh_make_col, veh_model_col, veh_regno_col, status_col = False, False, False, False, False, False, False, False, False, False, False, False, False, False
			xl_sheet = xl_workbook.sheet_by_name(each)

			for row in range(0, xl_sheet.nrows):

				for col in range(0, xl_sheet.ncols):

					if xl_sheet.cell(row, col).value in possible_strings: actual_row = row + 1

					if xl_sheet.cell(row, col).value in ['JOB OP DATE']:
						job_op_col = col
					elif xl_sheet.cell(row, col).value in ['CREDIT CARD NO']:
						credit_card_col = col
					elif xl_sheet.cell(row, col).value in ['CUSTOMER NAME']:
						name_col = col
					elif xl_sheet.cell(row, col).value in ['MOBILE']:
						mobile_col = col
					elif xl_sheet.cell(row, col).value in ['FROM CITY']:
						from_city_col = col
					elif xl_sheet.cell(row, col).value in ['FROM LOCATION']:
						from_loc_col = col
					elif xl_sheet.cell(row, col).value in ['TO CITY']:
						to_city_col = col
					elif xl_sheet.cell(row, col).value in ['TO LOCATION']:
						to_loc_col = col
					elif xl_sheet.cell(row, col).value in ['SERVICE NAME']:
						serv_name_col = col
					elif xl_sheet.cell(row, col).value in ['VEH MAKE']:
						veh_make_col = col
					elif xl_sheet.cell(row, col).value in ['VEH MODEL']:
						veh_model_col = col
					elif xl_sheet.cell(row, col).value in ['VEH REG NO']:
						veh_regno_col = col
					elif xl_sheet.cell(row, col).value in ['STATUS']:
						status_col = col
					else:
						pass

			for row in range(actual_row, xl_sheet.nrows):
				vals, val = {}, {}
				era_servs, concierge_servs, rentacar_servs, transport_servs, carreg_servs = [], [], [], [], []

				company_id = self.env['imc.clients'].search([('name','ilike',filename.split('.')[0])],limit=1).id
				vals.update({'insurance_company': company_id, 'service_closed_mail': True})
				val.update({'status': 'activate'})
				try:
					status,credit_card,name,mobile,from_city,from_loc,to_city,to_loc,serv_name,veh_make,veh_model,veh_regno=False,False,False,False,False,False,False,False,False,False,False,False

					if job_op_col: job_op = xl_sheet.cell(row, int(job_op_col)).value
					if credit_card_col: credit_card = xl_sheet.cell(row, int(credit_card_col)).value
					if name_col: name = xl_sheet.cell(row, int(name_col)).value
					if mobile_col: mobile = xl_sheet.cell(row, int(mobile_col)).value
					if from_city_col: from_city = xl_sheet.cell(row, int(from_city_col)).value
					if from_loc_col: from_loc = xl_sheet.cell(row, int(from_loc_col)).value
					if to_city_col: to_city = xl_sheet.cell(row, int(to_city_col)).value
					if to_loc_col: to_loc = xl_sheet.cell(row, int(to_loc_col)).value
					if serv_name_col: serv_name = xl_sheet.cell(row, int(serv_name_col)).value
					if veh_make_col: veh_make = xl_sheet.cell(row, int(veh_make_col)).value
					if veh_model_col: veh_model = xl_sheet.cell(row, int(veh_model_col)).value
					if veh_regno_col: veh_regno = xl_sheet.cell(row, int(veh_regno_col)).value
					if status_col: status = xl_sheet.cell(row, int(status_col)).value


					if job_op:
						if type(job_op) == float: vals.update({'scheduled_date' : str((xlrd.xldate.xldate_as_datetime(job_op, xl_workbook.datemode)).date())})
						else:vals.update({'scheduled_date' : str(datetime.strptime(str(job_op).strip(), '%d/%m/%Y'))})

					if credit_card:
						credit_card = str(credit_card).replace(' ', '')
						val.update({'type':'bank','credit_card_number': credit_card})
						membership_types = self.env['membership.services'].search([('bin_numbers.number','=', credit_card[:6])])

						if membership_types:
							val.update({'membership_type': membership_types[0].category_id.id, 'company_id': membership_types[0].client_id.id})
							rex = membership_types[0]

							for i in rex.era_available_services:
								era_servs += [(0, 0,
											   {'request_type': i.request_type, 'service_id': i.service_id.id,
												'unlimited': i.unlimited,
												'number_of_services': i.number_of_services})]

							for i in rex.concierge_available_services:
								concierge_servs += [(0, 0, {'request_type': i.request_type,
															'service_id': i.service_id.id,
															'unlimited': i.unlimited,
															'number_of_services': i.number_of_services})]

							for i in rex.carreg_available_services:
								carreg_servs += [(0, 0, {'request_type': i.request_type,
														 'service_id': i.service_id.id,
														 'unlimited': i.unlimited,
														 'number_of_services': i.number_of_services})]

							for i in rex.rentacar_available_services:
								rentacar_servs += [(0, 0, {'request_type': i.request_type,
														   'service_id': i.service_id.id,
														   'unlimited': i.unlimited,
														   'number_of_services': i.number_of_services})]

							for i in rex.transport_available_services:
								transport_servs += [(0, 0, {'request_type': i.request_type,
															'service_id': i.service_id.id,
															'unlimited': i.unlimited,
															'number_of_services': i.number_of_services})]

						val.update({'era_available_services': era_servs,
									 'concierge_available_services': concierge_servs,
									 'carreg_available_services': carreg_servs,
									 'rentacar_available_services': rentacar_servs,
									 'transport_available_services': transport_servs})

					
					if mobile:
						vals.update({'contact':mobile})
						val.update({'contact_number':mobile})
					
					vals.update({'country_from_id':self.env['res.country'].search([('code','=','AE')],limit=1).id,'country_to_id':self.env['res.country'].search([('code','=','AE')],limit=1).id})
					
					if from_city:
						if from_city == 'Western Region': from_city = 'Abu Dhabi'
						if from_city == 'Al Ain': code = 'AIN'
						if from_city == 'Umm Al Quwain' :code = 'UAQ'
						if from_city == 'Ras Al Khaimah' :code ='RAK'
						if from_city == 'Khorfakkan': code = 'KRF'
						city = self.env['res.country.state'].search([('country_id.code','=','AE'),('name','ilike',from_city)])
						if city:vals.update({'state_from_id':city[0].id})
						else:vals.update({'state_from_id':sock.execute(dbname, uid, pwd, 'res.country.state', 'create',{'code':code,'name':from_city,'country_id':self.env['res.country'].search([('name','ilike','United Arab Emirates'),('code','=','AE')],limit=1).id})})
				
					if from_loc:
						location = self.env['res.state.locations'].search([('name','ilike',from_loc),('country_id.code','=','AE'),('state_id','=',vals['state_from_id'])],limit=1).id
						if location: vals.update({'location_from':location})
						else:vals.update({'location_from':sock.execute(dbname, uid, pwd, 'res.state.locations', 'create', {'country_id':vals['country_from_id'],'state_id':vals['state_from_id'],'name':from_loc})})
				
					if to_city:
						if to_city == 'Western Region': to_city = 'Abu Dhabi'
						if to_city == 'Al Ain': code = 'AIN'
						if to_city == 'Umm Al Quwain' :code = 'UAQ'
						if to_city == 'Ras Al Khaimah' :code ='RAK'
						if to_city == 'Khorfakkan': code = 'KRF'
						city_to = self.env['res.country.state'].search([('name','ilike',to_city),('country_id.code','=','AE')])
						if city_to: vals.update({'state_to_id':city_to[0].id})
						else:vals.update({'state_to_id':sock.execute(dbname, uid, pwd, 'res.country.state', 'create', {'code':code,'name':to_city,'country_id':self.env['res.country'].search([('name','ilike','United Arab Emirates'),('code','=','AE')]).id})})

					if to_loc:
						location = self.env['res.state.locations'].search([('name','ilike',to_loc),('country_id.code','=','AE'),('state_id','=',vals['state_to_id'])],limit=1).id
						if location: vals.update({'location_to':location})
						else:vals.update({'location_to':sock.execute(dbname, uid, pwd, 'res.state.locations', 'create', {'country_id':vals['country_to_id'],'state_id':vals['state_to_id'],'name':to_loc})})

					if serv_name:
						# service = self.env['service.menu'].search([('name','ilike',str(serv_name).replace(" ", "")),('request_type','=','era')])
						# print serv_name
						# if not service:
						if "limit" in str(serv_name).lower():
							serv_name = "Citylimit Towing"
						if "inter" in str(serv_name).lower():
							serv_name = "Intercity Towing"
						if "inter" in str(serv_name).lower():
							serv_name = "Intercity Towing"

						if "rent" in str(serv_name).lower():
							serv_name = "Car Replacement"

						if "jump" in str(serv_name).lower():
							serv_name = "Battery Boosting"

						if "renewal" in str(serv_name).lower():
							serv_name = "Registration Renewal"

						service = self.env['service.menu'].search([('name','ilike',str(serv_name).strip()),('request_type','=','era')])
						if not service:
							service = self.env['service.menu'].search([('name','ilike',str(serv_name).strip().title())])
						service = service[0]
						vals.update({'service_name':str(service.name)+service.request_type,'request_type':service.request_type,'service':service.id})

					if veh_make: 
						make = self.env['vehicles.master'].search([('name','ilike',veh_make)])
						if make: vals.update({'vehicle_make':make[0].id})
						else:  vals.update({'vehicle_make':sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name':veh_make})})
					
					if veh_model: 
						model = self.env['vehicle.model'].search([('make_id','=',vals['vehicle_make']),('model','ilike',veh_model)])
						if model: vals.update({'model':model[0].id})
						else: vals.update({'model': sock.execute(dbname, uid, pwd, 'vehicle.model', 'create', {'make_id':vals['vehicle_make'],'model':veh_model})})
					
					val.update({'vehicle_make':vals['vehicle_make'],'model':vals['model']})

					if veh_regno:
						vals.update({'car_plate':veh_regno})
						val.update({'car_plate':veh_regno})
					if status:
						if 'CANCELED' in status: vals.update({'state':'cancelled'})
						if 'CLOSED' in status: vals.update({'state':'feedback'})

					if name or mobile:
						if name and mobile:
							cust = self.env['res.partner'].search([('name','=',name),('mobile','=',mobile)])
						else:
							cust = self.env['res.partner'].search(['|',('name','=',name),('mobile','=',mobile)])
						if not cust:
							customer = sock.execute(dbname, uid, pwd, 'res.partner', 'create', {'name':name, 'mobile':mobile})
						else:
							customer=cust[0].id
					
						vals.update({'customer':customer})
						val.update({'end_user':customer})

					# if name and mobile:
					membership_id = self.env['membership.menu'].search([('end_user','=',customer),('contact_number','=',mobile),('credit_card_number','=', credit_card)])
					if not membership_id:
						membership_id = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', val)

					else:
						eras, concs, carregs, tranports, rntacar = [], [], [], [], []
						test = []
						membership_write = self.env['membership.menu'].browse(membership_id[0].id)
						membership_id = membership_write.id
						membership_era = membership_write.era_available_services.ids
						membership_con = membership_write.concierge_available_services.ids
						membership_carreg = membership_write.carreg_available_services.ids
						membership_rentcar = membership_write.rentacar_available_services.ids
						membership_transport = membership_write.transport_available_services.ids

						era_ones = val.get('era_available_services', [])
						con_ones = val.get('concierge_available_services', [])
						carreg_ones = val.get('carreg_available_services', [])
						rentcar_ones = val.get('rentacar_available_services', [])
						transport_ones = val.get('transport_available_services', [])

						for i, j in [(membership_era, era_ones), (membership_con, con_ones),
									 (membership_carreg, carreg_ones), (membership_rentcar, rentcar_ones),
									 (membership_transport, transport_ones)]:

							if i == membership_era and j == era_ones:
								model, test = 'era.avail.services', eras
							elif i == membership_con and j == con_ones:
								model, test = 'concierge.avail.services', concs
							elif i == membership_carreg and j == carreg_ones:
								model, test = 'carreg.avail.services', carregs
							elif i == membership_rentcar and j == rentcar_ones:
								model, test = 'rentacar.avail.services', rntacar
							elif i == membership_transport and j == transport_ones:
								model, test = 'transport.avail.services', tranports
							else:
								pass

							# if len(j) != 0:
							if len(i) < len(j):
								for mem_serv in i:
									for each in j[0:len(i)]:
										if j.index(each) == i.index(mem_serv):
											test += [(1, mem_serv, each[2])]
								for rest in j[len(i):]:
									test += [(0, 0, rest[2])]
							elif len(i) > len(j):
								for mem_serv in i[0:len(j)]:
									for each in j:
										if j.index(each) == i.index(mem_serv):
											test += [(1, mem_serv, each[2])]
								self.env[model].browse(i[len(j):]).unlink()
							else:
								for mem_serv in i:
									for each in j:
										if j.index(each) == i.index(mem_serv):
											test += [(1, mem_serv, each[2])]

						val.pop('era_available_services', None)
						val.pop('concierge_available_services', None)
						val.pop('carreg_available_services', None)
						val.pop('rentacar_available_services', None)
						val.pop('transport_available_services', None)
						val.update({'era_available_services': eras, 'concierge_available_services': concs,
									 'carreg_available_services': carregs,
									 'rentacar_available_services': rntacar,
									 'transport_available_services': tranports})
						sock.execute(dbname, uid, pwd, 'membership.menu', 'write', membership_write.id, val)

					vals.update({'membership_id': membership_id})

					sock.execute(dbname, uid, pwd, 'claimed.service', 'create', vals)
					logger.info("USAGE ROW: %s;" % row)
				except Exception as e:
					logger.exception("Error: %s;" % e)
					pass
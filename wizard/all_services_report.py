from odoo import api, fields, models,_
import datetime
from datetime import datetime,timedelta
import time
import collections

def date_range(start, end):
    r = (datetime.strptime(end, "%Y-%m-%d")-datetime.strptime(start, "%Y-%m-%d")+timedelta(days=1)).days
    return [datetime.strptime(start, "%Y-%m-%d")+timedelta(days=i) for i in range(r)]


class Allservices_report(models.TransientModel):

	_name = "all.services.report"
	
	from_date = fields.Date('From')
	to_date = fields.Date('To')
	request_type = fields.Selection([('concierge', 'Concierge'),
                                     ('era', 'ERA'),
                                     ('rent-a-car', 'Rent a Car'),
                                     ('registration', 'Car Registration'),
                                     ('transportation', 'Transportation'),
                                     ('hertz', 'Hertz'),
                                     ], 'Request Type')
	service = fields.Many2one('service.menu','Name',domain="[('request_type','=',request_type)]")

	@api.multi
	def print_allservices_report(self):
		vals = self.read()[0]
		context = self._context
		datas = {'form': vals}
		
		return {'type': 'ir.actions.report','report_name': 'imc.all_services_report','report_type':"qweb-pdf",'data': datas,}


	@api.multi
	def print_allservices_excel_report(self):
		vals = self.read()[0]
		service= False
		allservs = []
		dateList = date_range(vals['from_date'],vals['to_date'])
		request_type = vals['request_type']
		if vals['service']: service = vals['service'][1]
		
		if request_type and service:
			services = self.env['claimed.service'].search([('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date']),('request_type','=',request_type),('service','=',service)]).ids
		else:
			services = self.env['claimed.service'].search([('scheduled_date','>=',vals['from_date']),('scheduled_date','<=',vals['to_date']),('request_type','=',request_type)]).ids

		
		if request_type == 'rent-a-car':
			name = 'Rent A Car Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.rentacar')])[0]
		elif request_type == 'registration':
			name = 'Car Registration Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.registration')])[0]
		elif request_type == 'concierge':
			name = 'Concierge Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.concierge')])[0]
		elif request_type == 'era':
			name = 'ERA Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.era')])[0]
		elif request_type == 'transportation':
			name = 'Transportation Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.transportation')])[0]
		else:
			name = 'All Services'
			tree = self.env['ir.ui.view'].search([('name', '=', 'all.excel.report.view')])[0]

		return {
			'name': name,
			'res_model': 'claimed.service',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', services)],
			'views': [(tree.id, 'tree')],
		}

class all_services_report_wizard(models.AbstractModel):
	_name = 'report.imc.all_services_report'

	@api.multi
	def get_report_values(self, docids, data=None):
		data = dict(data or {})
		allservs = []
		dateList = date_range(data['form']['from_date'],data['form']['to_date'])
		request_type = data['form']['request_type']
		service = data['form']['service']

		if request_type and not service:
			if request_type == 'concierge':
				services = self.env['service.menu'].search([('request_type','=',request_type)])
			elif request_type == 'era':
				services = self.env['service.menu'].search([('request_type','=',request_type)])
			elif request_type == 'rent-a-car':
				services = self.env['service.menu'].search([('request_type','=',request_type)])
			elif request_type == 'registration':
				services = self.env['service.menu'].search([('request_type','=',request_type)])
			elif request_type == 'transportation':
				services = self.env['service.menu'].search([('request_type','=',request_type)])
			elif request_type == 'hertz':
				services = self.env['service.menu'].search([('request_type','=',request_type)])
		else:
			services = self.env['service.menu'].search([('name','=',service),('request_type','=',request_type)])

		total_services = []
		needed_servs ={}
		for serv in services:
			total_services += [serv.name]
			needed_servs[serv.name]=serv

		for each in dateList:
			vals= {}
			vals['total_services']=needed_servs
			vals['date']= each.date().strftime('%d-%m-%Y')
			nextt = False
			if dateList.index(each)+1 < len(dateList): nextt = str(dateList[dateList.index(each)+1])
			else: nextt = str(dateList[-1])
			date = str(each)

			for eh in total_services:
				vals[eh] = self.env['claimed.service'].search_count([('request_type','=',request_type),('service','=',eh),('scheduled_date','>=',date),('scheduled_date','<=',nextt)])
			vals['total'] = self.env['claimed.service'].search_count([('request_type','=',request_type),('scheduled_date','>=',date),('scheduled_date','<=',nextt)])
			vals['driver'] = self.env['claimed.service'].search_count([('request_type','=',request_type),('assigned_driver','!=',False),('scheduled_date','>=',date),('scheduled_date','<=',nextt)])
			vals['subcon'] = self.env['claimed.service'].search_count([('request_type','=',request_type),('assigned_party','!=',False),('scheduled_date','>=',date),('scheduled_date','<=',nextt)])
			allservs += [vals]

		data.update({'final':allservs})
		return data



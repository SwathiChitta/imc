from odoo import api, fields, models,_

import datetime

class documents_recieved_wizard(models.TransientModel):

	
	_name = "documents.received"

	documents_id=fields.One2many('doc.doc','rel_id','Documents')


	# def save_documents_toform(self,cr,uid,ids,context=None):
	# 	vals=[]
	# 	data = self.read(cr, uid, ids)[0]
	# 	service_name = self.pool.get('claimed.service').browse(cr,uid,context['active_ids']).service_name
	# 	if service_name in ["Airport Transfer - Arrivalconcierge","Airport Transfer - Departureconcierge", "Marhaba - Meet & Greet Gold - Departureconcierge","Marhaba - Meet & Greet Gold - Arrivalconcierge", "Own Damagerent-a-car"] :
	# 		# if service_name == "Own Damagerent-a-car":
	# 		# 	fld = 'rent_owndamage'
	# 		# else:
	# 		# 	fld = 'conceirge_airport'
	# 		for i in data['documents_id']:
	# 			rec = self.pool.get('doc.doc').browse(cr,uid,i)
	# 			vals += [(0,0,{'name':rec.name, 'document':rec.document})]
	# 		self.pool.get('claimed.service').write(cr,uid,context['active_ids'],{'documents':vals,'state':'documents-received'},context=context)
		
	# 	return {
 #                'type': 'ir.actions.client',
 #                'tag': 'reload',  
 #                }


class Documents(models.TransientModel):
	_name = "doc.doc"

	
	name=fields.Char('Name')
	document=fields.Binary('Document')
	rel_id=fields.Many2one('documents.received','Rel to Wizard')

	
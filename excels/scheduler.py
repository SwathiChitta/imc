# import xlrd
import xmlrpclib
 
import datetime
import os
import socket
from openerp.osv import fields, osv
import random
import xlrd

username = 'admin' #the user
pwd = 'imcadmin'      #the password of the user
dbname = 'IMC11'    #the database

# ip = socket.gethostbyname(('adib.hitec.ae'))

proxy = 'http://' + '35.186.157.240:8069' + '/xmlrpc/'
# Get the uid
sock_common = xmlrpclib.ServerProxy(proxy + 'common')
uid = sock_common.login(dbname, username, pwd)
 
# # # #replace localhost with the address of the server
sock = xmlrpclib.ServerProxy(proxy + 'object')

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class imc_schedulers(osv.osv):

	_inherit = 'membership.menu'

	def import_client_1_data(self, cr, uid, context=None):
		xl_workbook = xlrd.open_workbook(BASE_DIR +'/excels/Client 1.xlsx')
		sheet_names = xl_workbook.sheet_names()#retrieving all sheet names in that xsl file

		for name in sheet_names:
			xl_sheet0 = xl_workbook.sheet_by_name(name)#accessing the first sheet of workbook
			# August Sheet
			if 'Aug' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 2).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 3).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 4).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 5).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})
						car_plate = xl_sheet0.cell(row, 6).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': chassis_no})
						mem_type = xl_sheet0.cell(row, 8).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type_new).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type_new).rstrip()})
							vals.update({'membership_type': membership_type_id[0]})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-1')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						membership_id =  sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)])
						if not membership_id:
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership
						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number

					except Exception as e:
						# print e
						pass

			# September Sheet
			if 'Sep' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 3).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 4).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 2).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 5).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})
						year = xl_sheet0.cell(row, 6).value

						if year:
							vals.update({'year': str(year).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 7).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type_new).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type_new).rstrip()})
							vals.update({'membership_type': membership_type_id[0]})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-1')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						if not sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('insurance_number','=',insurance_number),('end_user','=',customer_id)]):
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership
						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass

			# October Sheet
			if 'Oct' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 3).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 4).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 2).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 5).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})
						chassis_no = xl_sheet0.cell(row, 6).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 7).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type_new).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type_new).rstrip()})
							vals.update({'membership_type': membership_type_id[0]})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-1')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals
						if not sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)]):
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership

						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass


			# November Sheet
			if 'Nov' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 3).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 4).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 2).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 5).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})

						car_plate = xl_sheet0.cell(row, 6).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 8).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type_new).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type_new).rstrip()})
							vals.update({'membership_type': membership_type_id[0]})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-1')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals
						if not sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)]):
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership

						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass

			# December Sheet
			if 'Dec' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 3).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 4).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 2).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 5).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})

						car_plate = xl_sheet0.cell(row, 6).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 8).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type_new).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type_new).rstrip()})
							vals.update({'membership_type': membership_type_id[0]})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-1')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals
						if not sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)]):
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership

						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass

			# January Sheet
			if 'Jan' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})


						issued_date = xl_sheet0.cell(row, 3).value
						if issued_date:
							issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
							issued_date = issued_date.strftime("%Y-%m-%d")
							vals.update({'issued_date': issued_date})

						start_date = xl_sheet0.cell(row, 4).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 5).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})

						veh_reg_no = xl_sheet0.cell(row, 6).value
						if veh_reg_no:
							vals.update({'veh_reg_no': str(veh_reg_no).rstrip('0').rstrip('.')})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						
						make = xl_sheet0.cell(row, 8).value
						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})

						body_type = xl_sheet0.cell(row, 9).value

						if body_type:
							
							vals.update({'body_type': body_type})


						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 10).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type_new).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type_new).rstrip()})
							vals.update({'membership_type': membership_type_id[0]})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-1')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals
						if not sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)]):
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership

						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass

			# February Sheet
			if 'Feb' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})


						issued_date = xl_sheet0.cell(row, 3).value
						if issued_date:
							issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
							issued_date = issued_date.strftime("%Y-%m-%d")
							vals.update({'issued_date': issued_date})
						start_date = xl_sheet0.cell(row, 4).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 5).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})

						veh_reg_no = xl_sheet0.cell(row, 6).value
						if veh_reg_no:
							vals.update({'veh_reg_no': str(veh_reg_no).rstrip('0').rstrip('.')})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						
						make = xl_sheet0.cell(row, 8).value
						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})

						body_type = xl_sheet0.cell(row, 9).value

						if body_type:
							
							vals.update({'body_type': body_type})


						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 10).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type_new).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type_new).rstrip()})
							vals.update({'membership_type': membership_type_id[0]})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-1')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals
						if not sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)]):
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership

						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass

			# March Sheet
			if 'Mar' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})


						issued_date = xl_sheet0.cell(row, 3).value
						if issued_date:
							issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
							issued_date = issued_date.strftime("%Y-%m-%d")
							vals.update({'issued_date': issued_date})
						start_date = xl_sheet0.cell(row, 4).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 5).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})

						veh_reg_no = xl_sheet0.cell(row, 6).value
						if veh_reg_no:
							vals.update({'veh_reg_no': str(veh_reg_no).rstrip('0').rstrip('.')})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						
						make = xl_sheet0.cell(row, 8).value
						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})

						body_type = xl_sheet0.cell(row, 9).value

						if body_type:
							
							vals.update({'body_type': body_type})


						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 10).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type_new).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type_new).rstrip()})
							vals.update({'membership_type': membership_type_id[0]})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-1')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals
						if not sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)]):
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership

						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass

			# April Sheet
			if 'Apr' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}
						insurance_number = xl_sheet0.cell(row, 1).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})


						issued_date = xl_sheet0.cell(row, 3).value
						if issued_date:
							issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
							issued_date = issued_date.strftime("%Y-%m-%d")
							vals.update({'issued_date': issued_date})

						start_date = xl_sheet0.cell(row, 4).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 5).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})

						veh_reg_no = xl_sheet0.cell(row, 6).value
						if veh_reg_no:
							vals.update({'veh_reg_no': str(veh_reg_no).rstrip('0').rstrip('.')})

						customer_name = xl_sheet0.cell(row, 2).value
						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						
						make = xl_sheet0.cell(row, 8).value
						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})

						body_type = xl_sheet0.cell(row, 9).value

						if body_type:
							
							vals.update({'body_type': body_type})


						chassis_no = xl_sheet0.cell(row, 7).value

						if chassis_no:
							vals.update({'chassis_no': str(chassis_no).rstrip('0').rstrip('.')})

						mem_type = xl_sheet0.cell(row, 10).value

						if mem_type:
							mem_type_new = mem_type.split(' ', 1)[0]
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type_new).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type_new).rstrip()})
							vals.update({'membership_type': membership_type_id[0]})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-1')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals
						if not sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)]):
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership
						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass

	def import_client_2_data(self, cr, uid, context=None):
		xl_workbook = xlrd.open_workbook(BASE_DIR +'/excels/Client 2.xlsx')
		sheet_names = xl_workbook.sheet_names()#retrieving all sheet names in that xsl file
		for name in sheet_names:
			xl_sheet0 = xl_workbook.sheet_by_name(name)#accessing the first sheet of workbook
			# August Sheet
			if 'Feb' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 4).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 2).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 3).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 1).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 6).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})
						car_plate = xl_sheet0.cell(row, 8).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 5).value

						if chassis_no:
							vals.update({'chassis_no': chassis_no})
						model = xl_sheet0.cell(row, 7).value

						if model:
							
							vals.update({'model': str(model)})


						membership_type_id = sock.execute(dbname, uid, pwd, 'service.type','search',[('name','=', 'Standard')])
						
						if not membership_type_id:
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type','create',{'name': 'Standard'})
						else:
							membership_type_id = membership_type_id[0]

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-2')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})
						vals.update({'membership_type': membership_type_id})

						# print vals

						membership_id = sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)])
						if not membership_id:
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								# print membership

						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
						
					except Exception as e:
						# print e
						pass

			# March Sheet
			if 'Mar' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 4).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 2).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 3).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 1).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 6).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})
						car_plate = xl_sheet0.cell(row, 8).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 5).value

						if chassis_no:
							vals.update({'chassis_no': chassis_no})
						model = xl_sheet0.cell(row, 7).value

						if model:
							
							vals.update({'model': str(model)})

						membership_type_id = sock.execute(dbname, uid, pwd, 'service.type','search',[('name','=', 'Standard')])
						
						if not membership_type_id:
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type','create',{'name': 'Standard'})
						else:
							membership_type_id = membership_type_id[0]

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-2')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})
						vals.update({'membership_type': membership_type_id})

						# print vals

						membership_id = sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)])
						if not membership_id:
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								
								# print membership

						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass


			# April Sheet
			if 'Apr' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 4).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})
						start_date = xl_sheet0.cell(row, 2).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 3).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 1).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 6).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})
						car_plate = xl_sheet0.cell(row, 8).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 5).value

						if chassis_no:
							vals.update({'chassis_no': chassis_no})
						model = xl_sheet0.cell(row, 7).value

						if model:
							
							vals.update({'model': str(model)})

						membership_type_id = sock.execute(dbname, uid, pwd, 'service.type','search',[('name','=', 'Standard')])
						
						if not membership_type_id:
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type','create',{'name': 'Standard'})
						else:
							membership_type_id = membership_type_id[0]

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-2')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})
						vals.update({'membership_type': membership_type_id})

						# print vals

						membership_id = sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)])
						if not membership_id:
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								
								# print membership
						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass
	
	def import_client_3_data(self, cr, uid, context=None):

		xl_workbook = xlrd.open_workbook(BASE_DIR +'/excels/Client 3.xlsx')
		sheet_names = xl_workbook.sheet_names()#retrieving all sheet names in that xsl file
		# mems = sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('company_id','=',3)])
		# print sock.execute(dbname, uid, pwd, 'membership.menu', 'unlink', mems)

		for name in sheet_names:
			xl_sheet0 = xl_workbook.sheet_by_name(name)#accessing the first sheet of workbook
			# Jan Sheet
			if 'Jan' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 4).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})

						year = xl_sheet0.cell(row, 11).value
						if year:
							vals.update({'year': str(year).rstrip('0').rstrip('.')})

						start_date = xl_sheet0.cell(row, 5).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 6).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 7).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 8).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})
						car_plate = xl_sheet0.cell(row, 10).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 12).value

						if chassis_no:
							vals.update({'chassis_no': chassis_no})

						repair_inside_agency = xl_sheet0.cell(row, 13).value
						if repair_inside_agency:
							vals.update({'repair_inside_agency': str(repair_inside_agency)})

						rac = xl_sheet0.cell(row, 14).value
						if rac:
							vals.update({'rac': str(rac)})
						
						model = xl_sheet0.cell(row, 9).value

						if model:
							
							vals.update({'model': str(model)})

						mem_type = xl_sheet0.cell(row, 15).value
						if mem_type:
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type).rstrip()})
								vals.update({'membership_type': membership_type_id})
							else:
								membership_type_id = membership_type_id[0]
								vals.update({'membership_type': membership_type_id})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-3')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals

						membership_id = sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)])
						if not membership_id:
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								
								# print membership
						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number

					except Exception as e:
						# print e
						pass

			# Feb Sheet
			if 'Feb' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 4).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})

						year = xl_sheet0.cell(row, 11).value
						if year:
							vals.update({'year': str(year).rstrip('0').rstrip('.')})

						start_date = xl_sheet0.cell(row, 5).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 6).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 7).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 8).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})
						car_plate = xl_sheet0.cell(row, 10).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 12).value

						if chassis_no:
							vals.update({'chassis_no': chassis_no})

						repair_inside_agency = xl_sheet0.cell(row, 13).value
						if repair_inside_agency:
							vals.update({'repair_inside_agency': str(repair_inside_agency)})

						rac = xl_sheet0.cell(row, 14).value
						if rac:
							vals.update({'rac': str(rac)})
						
						model = xl_sheet0.cell(row, 9).value

						if model:
							
							vals.update({'model': str(model)})

						mem_type = xl_sheet0.cell(row, 15).value
						if mem_type:
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type).rstrip()})
								vals.update({'membership_type': membership_type_id})
							else:
								membership_type_id = membership_type_id[0]
								vals.update({'membership_type': membership_type_id})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-3')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals

						membership_id = sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)])
						if not membership_id:
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								
								# print membership
						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass

			# March Sheet
			if 'Mar' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 4).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})

						year = xl_sheet0.cell(row, 12).value
						if year:
							vals.update({'year': str(year).rstrip('0').rstrip('.')})


						issued_date = xl_sheet0.cell(row, 5).value
						if issued_date:
							issued_date = xlrd.xldate.xldate_as_datetime(issued_date, xl_workbook.datemode)
							issued_date = issued_date.strftime("%Y-%m-%d")
							vals.update({'issued_date': issued_date})

						start_date = xl_sheet0.cell(row, 6).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 7).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						customer_name = xl_sheet0.cell(row, 8).value

						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 9).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})

						veh_type = xl_sheet0.cell(row, 10).value

						if veh_type:
							vals.update({'veh_type': str(veh_type)})

						car_plate = xl_sheet0.cell(row, 11).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 13).value

						if chassis_no:
							vals.update({'chassis_no': chassis_no})

						repair_inside_agency = xl_sheet0.cell(row, 15).value
						if repair_inside_agency:
							vals.update({'repair_inside_agency': str(repair_inside_agency)})

						rac = xl_sheet0.cell(row, 16).value
						if rac:
							vals.update({'rac': str(rac)})
						
						model = xl_sheet0.cell(row, 9).value

						if model:
							
							vals.update({'model': str(model)})

						mem_type = xl_sheet0.cell(row, 14).value
						if mem_type:
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type).rstrip()})
								vals.update({'membership_type': membership_type_id})
							else:
								membership_type_id = membership_type_id[0]
								vals.update({'membership_type': membership_type_id})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-3')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals

						membership_id = sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)])
						if not membership_id:
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								
								# print membership
						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass

			# April Sheet
			if 'Apr' in name:
				for row in range(1,xl_sheet0.nrows):
					try:
						vals = {}

						insurance_number = xl_sheet0.cell(row, 4).value
						if insurance_number:
							vals.update({'insurance_number': str(insurance_number).rstrip()})

						year = xl_sheet0.cell(row, 11).value
						if year:
							vals.update({'year': str(year).rstrip('0').rstrip('.')})


						start_date = xl_sheet0.cell(row, 5).value
						if start_date:
							start_date = xlrd.xldate.xldate_as_datetime(start_date, xl_workbook.datemode)
							start_date = start_date.strftime("%Y-%m-%d")
							vals.update({'start_date': start_date})

						expiry_date = xl_sheet0.cell(row, 6).value
						if expiry_date:
							expiry_date = xlrd.xldate.xldate_as_datetime(expiry_date, xl_workbook.datemode)
							expiry_date = expiry_date.strftime("%Y-%m-%d")
							vals.update({'expiry_date': expiry_date})
						
						customer_name = xl_sheet0.cell(row, 7).value
						if customer_name:
							customer_id = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('name','=', str(customer_name).rstrip())])
							if not customer_id:
								customer_id = sock.execute(dbname, uid, pwd, 'res.partner','create',{'name': str(customer_name).rstrip()})
							else:
								customer_id = customer_id[0]

							vals.update({'end_user': customer_id})
						make = xl_sheet0.cell(row, 8).value

						if make:

							make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'search', [('name','=', str(make))])
							if not make_id:
								make_id = sock.execute(dbname, uid, pwd, 'vehicles.master', 'create', {'name': str(make).rstrip()})
							else:
								make_id = make_id[0]
							
							vals.update({'vehicle_make': make_id})

						veh_type = xl_sheet0.cell(row, 9).value

						if veh_type:
							vals.update({'veh_type': str(veh_type)})

						car_plate = xl_sheet0.cell(row, 10).value

						if car_plate:
							vals.update({'car_plate': str(car_plate).rstrip('0').rstrip('.')})

						chassis_no = xl_sheet0.cell(row, 12).value

						if chassis_no:
							vals.update({'chassis_no': chassis_no})

						repair_inside_agency = xl_sheet0.cell(row, 15).value
						if repair_inside_agency:
							vals.update({'repair_inside_agency': str(repair_inside_agency)})

						rac = xl_sheet0.cell(row, 16).value
						if rac:
							vals.update({'rac': str(rac)})
						
						model = xl_sheet0.cell(row, 9).value

						if model:
							
							vals.update({'model': str(model)})

						mem_type = xl_sheet0.cell(row, 14).value
						if mem_type:
							membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'search', [('name','ilike', str(mem_type).rstrip())])
							if not membership_type_id:
								membership_type_id = sock.execute(dbname, uid, pwd, 'service.type', 'create', {'name': str(mem_type).rstrip()})
								vals.update({'membership_type': membership_type_id})
							else:
								membership_type_id = membership_type_id[0]
								vals.update({'membership_type': membership_type_id})

						company_id = sock.execute(dbname, uid, pwd, 'res.company', 'search', [('name','=','Client-3')])
						type_ids = sock.execute(dbname, uid, pwd, 'res.company', 'read', company_id, ['membership_types'])
						com_service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'search', [('rel_ids','in',company_id),('service_cat_id','=',membership_type_id)])
						service_ids = sock.execute(dbname, uid, pwd, 'membership.type', 'read',com_service_ids, ['service_ids'])
						for i in service_ids:
							service_data = sock.execute(dbname, uid, pwd, 'membership.services', 'read',i['service_ids'])
							avail_services = []
							for data in service_data:
								service_obj = sock.execute(dbname, uid, pwd, 'service.menu', 'read',data['service_id'][0])
								avail_services.append([0, False, 
								{'service_id': data['service_id'][0], 'number_of_services':data['number_services'],'request_type': service_obj['request_type'],'unlimited': data['unlimited']}
								])
							vals.update({'available_services': avail_services})
						vals.update({'company_id': company_id[0]})
						vals.update({'type': 'insurance'})

						# print vals

						membership_id = sock.execute(dbname, uid, pwd, 'membership.menu', 'search', [('chassis_no','=',chassis_no),('insurance_number','=',insurance_number),('end_user','=',customer_id)])
						if not membership_id:
							if insurance_number:
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								
								# print membership
						else:
							if insurance_number:
								rand_str = ''.join(random.choice('0123456789ABCDEF') for i in range(6))
								insurance_number = insurance_number+rand_str
								vals.update({'insurance_number': insurance_number})
								membership = sock.execute(dbname, uid, pwd, 'membership.menu', 'create', vals)
								#print 'insurance_number',insurance_number
								# print 'insurance_number',insurance_number
					except Exception as e:
						# print e
						pass
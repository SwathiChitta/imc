from odoo import api, models, _, SUPERUSER_ID
import xmlrpc.client
import logging
import socket
from datetime import datetime

logger = logging.getLogger(__name__)


def get_ip_address():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8", 80))
	return s.getsockname()[0]
	
username = 'admin' #the user
pwd = 'imcadmin'      #the password of the user
dbname = 'IMC11'    #the database

class update_expired(models.Model):

	_inherit = 'membership.menu'

	@api.multi
	def update_expired_memberships(self):
		ip = get_ip_address()
		URL = "http://" + ip + ":8069/xmlrpc/common"
		sock_common = xmlrpc.client.ServerProxy(URL, transport=None,
												encoding=None, verbose=False, allow_none=False,
												use_datetime=False, context=None)
		uid = sock_common.login(dbname, username, pwd)
		sock = xmlrpc.client.ServerProxy("http://" + ip + ":8069/xmlrpc/" + 'object')
		args = [('expiry_date', '<', str(datetime.now().date())),('status','not in',['expired','cancel'])]
		s_ids = sock.execute(dbname, uid, pwd, 'membership.menu', 'search', args)
		counter = 0
		for i in enumerate(s_ids):
			try:
				sock.execute(dbname, uid, pwd, 'membership.menu', 'write', i[1], {'status': 'expired'})
				counter += 1
				logger.info("Updated Membership: %s;" % counter)
			except Exception as e:
				pass
				logger.exception("Error: %s;" % e)
		return True

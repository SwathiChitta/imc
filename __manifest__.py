{
	'name': 'IMC Insurance',
	'version': '1.0',
	'category': 'Vehicle Insurance',

	'description': """
	Vehicle Insurance Module
	""",

	'author': 'ehAPI Team',
	'website': 'www.ehapi.com',
	'depends': ['base', 'mail','resource','hr'],

	'data': [
		'security/insurance_security_view.xml',
		'security/ir.model.access.csv',

		'reports/excel_reports.xml',
		
		'reports/car_reg_report_template.xml',
		'reports/era_report_template.xml',
		'reports/clientwise_service_report.xml',
		'reports/all_services_report.xml',
		'reports/drivers_report.xml',
		'reports/subcontractors_report.xml',
		'data/mail_templates.xml',
		
		'sequence/sequence.xml',

		'wizard/check_user_wizard_view.xml',
		'wizard/claim_service_complete_wizard.xml',
		'wizard/wizard_claimed_services_view.xml',
		'wizard/dispatched_claime_service_view.xml',
		'wizard/display_time_slots_view.xml',
		'wizard/documents_recieved_view.xml',
		'wizard/choose_vehicle_wizard_view.xml',
		'wizard/claim_service_dialog.xml',
		'wizard/extend_days_view.xml',
		'wizard/display_claimed_services.xml',
		'wizard/upload_memberships_view.xml',
		'wizard/service_usage_view.xml',

		'view/insurance_view.xml',
		'view/clients_view.xml',

		'view/end_user_view.xml',
	
		# 'view/service_agents_view.xml',
		'view/membership_view.xml',
		'view/claimed_services_view.xml',
		'view/temporary_users_view.xml',
		'view/driver_checkins_view.xml',
		'view/driver_break_time_view.xml',
		'view/type_change_records_view.xml',
		'view/repair_logs_view.xml',
		'view/fuel_record_view.xml',

		'view/service_providers_view.xml',
		'view/service_type_view.xml',
		'view/internal_users_view.xml',
		'view/operations_dashboard_view.xml',
		# 'view/ongoing_services_dashboard.xml',
		'view/dashboard_view.xml',

		"view/service_type_action_view.xml",
		"view/cities_master_view.xml",
		"view/locations_master_view.xml",
		"view/garages_view.xml",
		"view/drivers_view.xml",
		"view/vehicles_plate_no_view.xml",
		"view/rent_a_car_test_view.xml",
     	"view/era_test_view.xml",
		"view/car_registration_test_view.xml",
		"view/concierge_test_view.xml",
		"view/hertz_test_view.xml",
		"view/transportation_test_view.xml",
		"view/calling_team_view.xml",
	   
		'view/feedback_tobe_taken_view.xml',
		
		"view/costs_view.xml",
		"view/bin_master_view.xml",
		"view/template_view.xml",
		"view/various_items_view.xml",
		"view/time_slot_view.xml",
		"view/make_model_view.xml",
		"view/costs_master_view.xml",
		"view/staff_view.xml",
		"view/temporary_claimed_services.xml",
		"view/complaints_view.xml",
		"view/vehicle_maintenance_view.xml",
		
		'data/scheduler_view.xml',
		'wizard/driver_wizard_view.xml',
		
		# 'wizard/clientwise_service_report_view.xml',
		# 'wizard/all_services_report_view.xml',
		# 'wizard/drivers_report_view.xml',
		# 'wizard/subcontractors_report_view.xml',
		# 'wizard/temporary_service_report.xml',
		# 'wizard/membership_status_report.xml',
		# 'wizard/bank_insurance_wise_report.xml',
		# 'wizard/drivers_efficiency_report.xml',
		# 'wizard/truck_usage_report.xml',
		# 'wizard/fuel_usage_report.xml',
		'wizard/generic_report.xml',
				
		'view/reports.xml',
		'view/history_view.xml',
		'view/timeslot_values.xml',
		'view/test.xml',

		],

	'qweb': [
		'static/src/xml/base.xml',
		# 'static/src/xml/imc_dashboard.xml',
    ],
	
	'installable': True,
	'auto_install': False,
	'application': True,
}

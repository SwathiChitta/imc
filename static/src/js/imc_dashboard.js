odoo.define('dashboard.dashboard', function(require) {
    "use strict";
    var core = require('web.core');
    var session = require('web.session');
    var ajax = require('web.ajax');
    var ActionManager = require('web.ActionManager');
    var view_registry = require('web.view_registry');
    var Widget = require('web.Widget');
    var ControlPanelMixin = require('web.ControlPanelMixin');
    var QWeb = core.qweb;
    var _t = core._t;
    var _lt = core._lt;
    var HrDashboardView = Widget.extend(ControlPanelMixin, {
        template: 'dashboard.dashboard',
        // events: _.extend({}, Widget.prototype.events, {
        //     'click .pending_services_after_dispatch': 'action_pending_services_after_dispatch',
        //     'click .recieved_dispatched': 'action_recieved_dispatched',
        //     'click .all_ongoing_services': 'action_all_ongoing_services',
        //     'click .driver_subcontractor': 'action_driver_subcontractor',
        //     'click .clients_memberships': 'action_clients_memberships',
        //     'click .clients_temporary': 'action_clients_temporary',
        // }),
        init: function(parent, context) {
            this._super(parent, context);
            // var fetched_values = [];
            // var pending_dispatchd_count = 0;
            // var recieved_services = 0;
            // var dispatched_services = 0;
            // var driver_completed_services = 0;
            // var subcon_completed_services = 0;
            // var citylimit_towing = 0;
            // var flat_tyre = 0;
            // var fuel_supply = 0;
            // var jump_start = 0;
            // var lockout_service = 0;
            // var offroad_rec = 0;
            // var pull_out = 0;
            // var intercity_towing = 0;
            // var battery_boosting = 0;
            // var onsitebat_replace = 0;
            // var battery_boost = 0;
            // var airport_pickdrop = 0;
            // var car_service = 0;
            // var chauffeur_service = 0;
            // var emergency_home = 0;
            // var meet_greet = 0;
            // var doc_delivery = 0;
            // var gift_delivery = 0;
            // var own_damage = 0;
            // var third_party = 0;
            // var reg_renewal = 0;
            // var transport_citylimit = 0;
            // var transport_intercity = 0;
            // var transport_waiting = 0;
            // var transport_kycol = 0;
            // var trabnsport_keyd = 0;
            // var clientwise_active_memberships;
            // var clientwise_temp_users;
            // var dailywsise_services_count;
            var self = this;
            if (context.tag) {
                self._rpc({
                    model: 'dashboard.dashboard',
                    method: 'get_claimed_services_data',
                }, []).then(function(result) {
                    self.fetched_values = result[0];

                
                    // if (result[0]['clientwise_active_memberships'] != 'undefined') {
                    //     self.clientwise_active_memberships = result[0]['clientwise_active_memberships'];
                    // }
                    // if (result[0]['clientwise_temp_users'] != 'undefined') {
                    //     self.clientwise_temp_users = Object.values(result[0]['clientwise_temp_users'])
                    // }
                    // if (result[0]['dailywsise_services_count'] != 'undefined') {
                    //     self.dailywsise_services_count = Object.values(result[0]['dailywsise_services_count'])
                    // }
                    self.clientwise_active_memberships = result[0]['clientwise_active_memberships'];
                    self.clientwise_temp_users = result[0]['clientwise_temp_users'];
                    self.dailywsise_services_count = result[0]['dailywsise_services_count'];
                    self.pending_dispatchd_count = result[0]['pending_after_dispatch'];
                    self.recieved_services = result[0]['received_services'];
                    self.dispatched_services = result[0]['dispatched_services'];
                    self.driver_completed_services = result[0]['driver_completed'];
                    self.subcon_completed_services = result[0]['subcon_completed'];
                    self.citylimit_towing = result[0]['citylimit_towing'];
                    self.flat_tyre = result[0]['flat_tyre'];
                    self.fuel_supply = result[0]['fuel_supply'];
                    self.jump_start = result[0]['jump_start'];
                    self.lockout_service = result[0]['lockout_service'];
                    self.offroad_rec = result[0]['offroad_rec'];
                    self.pull_out = result[0]['pull_out'];
                    self.intercity_towing = result[0]['intercity_towing'];
                    self.battery_boosting = result[0]['battery_boosting'];
                    self.onsitebat_replace = result[0]['onsitebat_replace'];
                    self.airport_pickdrop = result[0]['airport_pickdrop'];
                    self.car_service = result[0]['car_service'];
                    self.chauffeur_service = result[0]['chauffeur_service'];
                    self.emergency_home = result[0]['emergency_home'];
                    self.meet_greet = result[0]['meet_greet'];
                    self.doc_delivery = result[0]['doc_delivery'];
                    self.gift_delivery = result[0]['gift_delivery'];
                    self.own_damage = result[0]['own_damage'];
                    self.third_party = result[0]['third_party'];
                    self.reg_renewal = result[0]['reg_renewal'];
                    self.transport_citylimit = result[0]['transport_citylimit'];
                    self.transport_intercity = result[0]['transport_intercity'];
                    self.transport_waiting = result[0]['transport_waiting'];
                    self.transport_kycol = result[0]['transport_kycol'];
                    self.trabnsport_keyd = result[0]['trabnsport_keyd'];
                }).done(function() {
                    self.render();
                    self.href = window.location.href;
                });
            }
        },
        willStart: function() {
            return $.when(ajax.loadLibs(this), this._super());
        },
        start: function() {
            var self = this;
            return this._super();
        },
        render: function() {
            var super_render = this._super;
            var self = this;
            var dahsboard = QWeb.render('dashboard.dashboard', {
                widget: self,
            });
            $(".o_control_panel").addClass("o_hidden");
            $(dahsboard).prependTo(self.$el);
            return dahsboard
        },
        reload: function() {
            window.location.href = this.href;
        },
        // action_pending_services_after_dispatch: function(event) {
        //     var self = this;
        //     event.stopPropagation();
        //     event.preventDefault();
        //     this.do_action({
        //         name: _t("Pending Services After Dispatch"),
        //         type: 'ir.actions.act_window',
        //         res_model: 'claimed.service',
        //         view_mode: 'tree,form',
        //         view_type: 'form',
        //         views: [
        //             [false, 'list']
        //         ],
        //         domain: [
        //             ['id', 'in', self.fetched_values['pending_after_dispatch']]
        //         ],
        //         target: 'current',
        //     }, {
        //         on_reverse_breadcrumb: function() {
        //             return self.reload();
        //         }
        //     })
        // },
        // action_recieved_dispatched: function(event) {
        //     var self = this;
        //     event.stopPropagation();
        //     event.preventDefault();
        //     this.do_action({
        //         name: _t("Received Dispatched Services"),
        //         type: 'ir.actions.act_window',
        //         res_model: 'claimed.service',
        //         view_mode: 'tree,form',
        //         view_type: 'form',
        //         views: [
        //             [false, 'list']
        //         ],
        //         domain: [
        //             ['id', 'in', self.fetched_values['received_services'].concat(self.fetched_values['dispatched_services'])]
        //         ],
        //         target: 'current',
        //     }, {
        //         on_reverse_breadcrumb: function() {
        //             return self.reload();
        //         }
        //     })
        // },
        // action_clients_memberships: function(event) {
        //     var self = this;
        //     event.stopPropagation();
        //     event.preventDefault();
        //     this.do_action({
        //         name: _t("Clients Memberships"),
        //         type: 'ir.actions.act_window',
        //         res_model: 'membership.menu',
        //         view_mode: 'tree,form',
        //         view_type: 'form',
        //         views: [
        //             [false, 'list']
        //         ],
        //         domain: [
        //             ['status', '=', 'activate']
        //         ],
        //         target: 'current',
        //     }, {
        //         on_reverse_breadcrumb: function() {
        //             return self.reload();
        //         }
        //     })
        // },
        // action_clients_temporary: function(event) {
        //     var self = this;
        //     event.stopPropagation();
        //     event.preventDefault();
        //     this.do_action({
        //         name: _t("Clients Temporary Users"),
        //         type: 'ir.actions.act_window',
        //         res_model: 'temporary.users',
        //         view_mode: 'tree,form',
        //         view_type: 'form',
        //         views: [
        //             [false, 'list']
        //         ],
        //         domain: [
        //             ['status', '=', 'verified']
        //         ],
        //         target: 'current',
        //     }, {
        //         on_reverse_breadcrumb: function() {
        //             return self.reload();
        //         }
        //     })
        // },
        // action_driver_subcontractor: function(event) {
        //     var self = this;
        //     event.stopPropagation();
        //     event.preventDefault();
        //     this.do_action({
        //         name: _t("Received Dispatched Services"),
        //         type: 'ir.actions.act_window',
        //         res_model: 'claimed.service',
        //         view_mode: 'tree,form',
        //         view_type: 'form',
        //         views: [
        //             [false, 'list']
        //         ],
        //         domain: [
        //             ['id', 'in', self.fetched_values['driver_completed'].concat(self.fetched_values['subcon_completed'])]
        //         ],
        //         target: 'current',
        //     }, {
        //         on_reverse_breadcrumb: function() {
        //             return self.reload();
        //         }
        //     })
        // },
        // action_all_ongoing_services: function(event) {
        //     var self = this;
        //     event.stopPropagation();
        //     event.preventDefault();
        //     this.do_action({
        //         name: _t("All Ongoing Services"),
        //         type: 'ir.actions.act_window',
        //         res_model: 'claimed.service',
        //         view_mode: 'tree,form',
        //         view_type: 'form',
        //         views: [
        //             [false, 'list']
        //         ],
        //         domain: [
        //             ['id', 'in', self.fetched_values['citylimit_towing'].concat(self.fetched_values['flat_tyre']).concat(self.fetched_values['fuel_supply']).concat(self.fetched_values['jump_start']).concat(self.fetched_values['lockout_service']).concat(self.fetched_values['offroad_rec']).concat(self.fetched_values['pull_out']).concat(self.fetched_values['intercity_towing']).concat(self.fetched_values['battery_boosting']).concat(self.fetched_values['onsitebat_replace']).concat(self.fetched_values['battery_boost']).concat(self.fetched_values['airport_pickdrop']).concat(self.fetched_values['car_service']).concat(self.fetched_values['chauffeur_service']).concat(self.fetched_values['emergency_home']).concat(self.fetched_values['meet_greet']).concat(self.fetched_values['doc_delivery']).concat(self.fetched_values['gift_delivery']).concat(self.fetched_values['own_damage']).concat(self.fetched_values['third_party']).concat(self.fetched_values['reg_renewal']).concat(self.fetched_values['transport_citylimit']).concat(self.fetched_values['transport_intercity']).concat(self.fetched_values['transport_waiting']).concat(self.fetched_values['transport_kycol']).concat(self.fetched_values['trabnsport_keyd'])]
        //         ],
        //         target: 'current',
        //     }, {
        //         on_reverse_breadcrumb: function() {
        //             return self.reload();
        //         }
        //     })
        // },
    });
    core.action_registry.add('dashboard.dashboard', HrDashboardView);
    return HrDashboardView
});
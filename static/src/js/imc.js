odoo.define('web.ListRenderers', function (require) {
"use strict";

// Tracking script
    // window.smartlook||(function(d) {
    // var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    // var c=d.createElement('script');o.api=new Array();
    // c.async=true;
    // c.type='text/javascript';
    // c.charset='utf-8';
    // c.src='https://rec.smartlook.com/recorder.js';
    // h.appendChild(c);
    // })(document);
    // smartlook('init', 'c5ff852316928c798a23a34056d95ce7647b5bb2');

var BasicRenderer = require('web.ListRenderer');

var ListRenderers = BasicRenderer.include({

	// no pop up form view on clicking row of any tree view

	_onRowClicked: function (event) {
		if (!$(event.target).prop('special_click')) {
			var id = $(event.currentTarget).data('id');
			
			// for claimed service type
			if (id) {
				console.log(id);

				if (id.match(/era.avail.services/g)){
					console.log(id.match(/era.avail.services/g));
				}
				else if (id.match(/concierge.avail.services/g)){
					console.log(id.match(/concierge.avail.services/g));
				}
				else if (id.match(/rentacar.avail.services/g)){
					console.log(id.match(/rentacar.avail.services/g));
				}
				else if (id.match(/carreg.avail.services/g)){
					console.log(id.match(/carreg.avail.services/g));
				}
				else if (id.match(/transport.avail.services/g)){
					console.log(id.match(/transport.avail.services/g));
				}

				else if (id.match(/change.password.user/g)){
					console.log(id.match(/change.password.user/g));
				}
			 
				else if (id.match(/display.timeslots.info/g)){
					console.log(id.match(/display.timeslots.info/g));
				}	
			  
				// rest of the tree views
				else if (id){
					this.trigger_up('open_record', {id:id, target: event.target});
				}

				// extra pass statement
				else{

					console.log("pass");
				}
			}
		}
	},
});

});